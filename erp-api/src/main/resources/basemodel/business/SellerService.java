/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.converters.SellerConverters;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.seller.AddSellerRequest;
import regiec.knast.erp.api.dto.seller.GetSellerRequest;
import regiec.knast.erp.api.dto.seller.RemoveSellerRequest;
import regiec.knast.erp.api.dto.seller.RestoreSellerRequest;
import regiec.knast.erp.api.dto.seller.SellerDTO;
import regiec.knast.erp.api.dto.seller.SellersListDTO;
import regiec.knast.erp.api.dto.seller.UpdateSellerRequest;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class SellerService {

    @Inject
    private SellerConverters sellerConverter;

    @NetAPI
    public SellerDTO addSeller(AddSellerRequest request) {
        SellerDTO ret = sellerConverter.add(request);
        return ret;
    }

    @NetAPI
    public SellerDTO getSeller(GetSellerRequest request) {
        SellerDTO ret = sellerConverter.get(request);
        return ret;
    }

    @NetAPI
    public SellerDTO updateSeller(UpdateSellerRequest request) {
        SellerDTO ret = sellerConverter.update(request);
        return ret;
    }

    @NetAPI
    public RemoveResponse removeSeller(RemoveSellerRequest request) {
        RemoveResponse ret = sellerConverter.remove(request);
        return ret;

    }

    @NetAPI
    public SellerDTO restoreSeller(RestoreSellerRequest request) {
        SellerDTO ret = sellerConverter.restore(request);
        return ret;
    }

    @NetAPI
    public SellersListDTO listSellers(PaginableFilterWithCriteriaRequest request) {
        SellersListDTO ret = sellerConverter.list(request);
        return ret;
    }
}
