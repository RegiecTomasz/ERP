/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.converters;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import org.jaxygen.typeconverter.ClassToClassTypeConverter;
import org.jaxygen.typeconverter.TypeConverter;
import org.jaxygen.typeconverter.converters.PartialToPaginableConverter;
import regiec.knast.erp.api.ConvertersRegistry;
import regiec.knast.erp.api.dao.interfaces.SellerDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.seller.AddSellerRequest;
import regiec.knast.erp.api.dto.seller.GetSellerRequest;
import regiec.knast.erp.api.dto.seller.RemoveSellerRequest;
import regiec.knast.erp.api.dto.seller.RestoreSellerRequest;
import regiec.knast.erp.api.dto.seller.SellerDTO;
import regiec.knast.erp.api.dto.seller.SellersListDTO;
import regiec.knast.erp.api.dto.seller.UpdateSellerRequest;
import regiec.knast.erp.api.entities.SellerEntity;
import regiec.knast.erp.api.entities.containers.SellerEntitiesList;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.SellerState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class SellerConverters implements Converters {

    public final static EntityType MY_ENTITY_TYPE = EntityType.SELLER;

    @Inject
    private SellerDAOInterface sellerDAO;

    @Inject
    private Validator validator;

    public SellerDTO add(AddSellerRequest from) {
        validator.validateNotNull(from).throww();
        validator.validateUniqueness("name", from.getName(), null, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", from.getNip(), null, MY_ENTITY_TYPE).throww();

        SellerEntity entity = BeanUtilSilent.translateBeanAndReturn(from, new SellerEntity());
        entity.setState(SellerState.NORMAL);
        SellerEntity entity1 = sellerDAO.add(entity);
        return ConvertersRegistry.convert(entity1, SellerDTO.class);
    }

    public SellerDTO get(GetSellerRequest from) {
        validator.validateNotNull(from).throww();
        String id = from.getId();
        validator.validateNotNull(id).throww();
        SellerEntity entity = sellerDAO.get(id);
        return ConvertersRegistry.convert(entity, SellerDTO.class);
    }

    public SellerDTO update(UpdateSellerRequest from) {
        validator.validateNotNull(from).throww();
        String id = from.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("name", from.getName(), id, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", from.getNip(), id, MY_ENTITY_TYPE).throww();
        SellerEntity entity = sellerDAO.get(id);
        validator.validateNotNull(entity).throww();
        SellerEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new SellerEntity());
        BeanUtilSilent.translateBean(from, updatedEntity);
        sellerDAO.update(updatedEntity);
        return ConvertersRegistry.convert(updatedEntity, SellerDTO.class);
    }

    public RemoveResponse remove(RemoveSellerRequest from) {
        validator.validateNotNull(from).throww();
        String id = from.getId();
        validator.validateNotNull(id).throww();
        SellerEntity sellerEntity = sellerDAO.get(id);
        if (from.getConfirmed()) {
            sellerEntity.setState(SellerState.REMOVED);
            sellerDAO.update(sellerEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(sellerEntity));
            return new RemoveResponse(false, null);
        }
    }

    public SellerDTO restore(RestoreSellerRequest from) {
        validator.validateNotNull(from).throww();
        String id = from.getId();
        validator.validateNotNull(id).throww();

        SellerEntity sellerEntity = sellerDAO.get(id);
        if (sellerEntity.getState() != SellerState.REMOVED) {
            throw new ConversionErrorWithCode("Restore is avaliable only in '" + SellerState.REMOVED.toString() + "' state", ExceptionCodes.CANNOT_RESTORE_SELLER_IN_THIS_STATE);
        }
        sellerEntity.setState(SellerState.NORMAL);
        sellerDAO.update(sellerEntity);
        return ConvertersRegistry.convert(sellerEntity, SellerDTO.class);
    }

    public SellersListDTO list(PaginableFilterWithCriteriaRequest from) {
        validator.validateNotNull(from).throww();
        SellerEntitiesList entities = sellerDAO.list(from);
        return ConvertersRegistry.convert(entities, SellersListDTO.class);
    }

    private final TypeConverter<SellerEntity, SellerDTO> entityToDTO = new ClassToClassTypeConverter<SellerEntity, SellerDTO>() {

        @Override
        public SellerDTO convert(SellerEntity entity) {
            validator.validateNotNull(entity).throww();
            return BeanUtilSilent.translateBeanAndReturn(entity, new SellerDTO());
        }
    };
    /*  private final TypeConverter<SellerDTO, SellerEntity> dtoToEntity = new BeanConverter<SellerDTO, SellerEntity>() {
    };*/
    private final TypeConverter<SellerEntitiesList, SellersListDTO> partialToPaginable = new PartialToPaginableConverter<SellerEntitiesList, SellersListDTO>() {
    };

    @Override
    public List<TypeConverter> getConverters() {
        List<TypeConverter> converters = new ArrayList();
        converters.add(entityToDTO);
        converters.add(partialToPaginable);
        return converters;
    }

}
