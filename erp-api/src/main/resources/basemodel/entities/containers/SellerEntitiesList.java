/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities.containers;

import org.jaxygen.collections.PartialArrayList;
import regiec.knast.erp.api.entities.SellerEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class SellerEntitiesList extends PartialArrayList<SellerEntity> {

}
