/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.DocumentEntity;
import regiec.knast.erp.api.entities.containers.DocumentEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface DocumentDAOInterface extends DAOBaseInterface<DocumentEntity, DocumentEntitiesList> {

    DocumentEntity get(String id);

    DocumentEntitiesList list(PaginableFilterWithCriteriaRequest paginableFilterWithCriteria);
}
