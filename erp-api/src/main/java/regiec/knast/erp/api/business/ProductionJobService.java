/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;

import regiec.knast.erp.api.dao.interfaces.ProductionJobDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsRequest;
import regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsResponse;
import regiec.knast.erp.api.dto.productionjob.ProductionJobDTO;
import regiec.knast.erp.api.dto.productionjob.ProductionJobsListDTO;
import regiec.knast.erp.api.dto.productionjob.UpdateProductionJobRequest;
import regiec.knast.erp.api.entities.ProductionJobEntity;
import regiec.knast.erp.api.manager.orders.GenerateProductionJobsTransformer; 
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ProductionJobService extends AbstractService<ProductionJobEntity, ProductionJobDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.PRODUCTION_JOB;

    @Inject
    private ProductionJobDAOInterface productionJobDAO;
    @Inject
    private Validator validator;
    @Inject
    private GenerateProductionJobsTransformer productionJobManager;

//    @NetAPI
//    public ProductionJobDTO addProductionJob(AddProductionJobRequest request) {      
//        validator.validateNotNull(request).throww();
//            //    validator.validateUniqueness("name", request.getName(), null, MY_ENTITY_TYPE).throww();
//
//            ProductionJobEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new ProductionJobEntity());
//            entity.setState(ProductionJobState.NEW);
//            ProductionJobEntity entity1 = productionJobDAO.add(entity);
//            return entity1.convertToDTO();
//    }

    @NetAPI
    public ProductionJobDTO getProductionJob(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public ProductionJobDTO updateProductionJob(UpdateProductionJobRequest request) {
            validator.validateNotNull(request).throww();
            String id = request.getId();
            validator.validateNotNull(id).throww();
            //   validator.validateUniqueness("name", request.getName(), id, MY_ENTITY_TYPE).throww();
            ProductionJobEntity entity = productionJobDAO.get(id);
            validator.validateNotNull(entity).throww();
            
            entity.setComments(request.getComments());
//            ProductionJobEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new ProductionJobEntity());
//            BeanUtilSilent.translateBean(request, updatedEntity);
            productionJobDAO.update(entity);
            return entity.convertToDTO();
    }

//    @NetAPI
//    public RemoveResponse removeProductionJob(RemoveProductionJobRequest request) {
//        RemoveResponse ret = ConvertersRegistry.convert(request, RemoveResponse.class);
//        return ret;
//    }

    @NetAPI
    public ProductionJobsListDTO listProductionJobs(PaginableFilterWithCriteriaRequest request) {
        return (ProductionJobsListDTO) super.list(request);
    }

    @NetAPI
    public GenerateProductionJobsResponse generateProductionJobs(GenerateProductionJobsRequest request) {
        return productionJobManager.generateProductionJobs(request);
    }
}
