/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author fbrzuzka
 */
public enum SellUnit {
    
    mb,
    laga,
    arkusz,
    kg,
    tona
}
