/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author fbrzuzka
 */
public enum TreeNodeType {
    ROOT,
    LEAF
}
