/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.materialstate;

import regiec.knast.erp.api.model.states.MaterialState;
import regiec.knast.erp.api.model.materialstate.StateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class MaterialStateDone extends StateBase {

    public MaterialStateDone() {
        super(MaterialState.DONE);
    }

    private String location;
}
