/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.otherstuff;

import com.google.common.base.Strings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import regiec.knast.erp.api.dto.EntityStatesDto;
import regiec.knast.erp.api.dto.StateVerticleAdjacentDto;
import regiec.knast.erp.api.dto.StateVerticleDto;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.MaterialUnit;
import regiec.knast.erp.api.model.SellUnit;
import regiec.knast.erp.api.model.SortOrder;
import regiec.knast.erp.api.model.WareDimension;
import regiec.knast.erp.api.model.WareSizeType;
import regiec.knast.erp.api.model.states.MaterialState;
import regiec.knast.erp.api.model.states.MaterialTemplateState;
import regiec.knast.erp.api.model.states.MaterialTypeState;
import regiec.knast.erp.api.model.states.StateDescriptor;
import regiec.knast.erp.api.model.states.StateInterface;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.EntityTypeToStates_Map;
import regiec.knast.erp.api.validation.state.StateMachine;
import regiec.knast.erp.api.validation.state.StateMachine.Adjacent;
import regiec.knast.erp.api.validation.state.StateMachineAgregat;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class ProgramConstsDTO extends ProgramConstsBase {

    public static final String SEND_EXCEPTION_EMAIL = "SEND_EXCEPTION_EMAIL";

    private boolean doSendExceptionEmail = false;
    private Map<String, EntityStatesDto> entityStatesMap = createEntitiesStateMap();

    private List<MaterialState> materialState = Arrays.asList(MaterialState.values());
    private List<MaterialTemplateState> materialTemplateStates = Arrays.asList(MaterialTemplateState.values());
    private List<MaterialTypeState> materialTypeState = Arrays.asList(MaterialTypeState.values());

    private Map<EntityType, Map> statesMap = createStatesMap();

    private List<MaterialUnit> materialUnit = Arrays.asList(MaterialUnit.values());
    private List<Dimension> dimension = Arrays.asList(Dimension.values());
    private List<WareDimension> wareDimension = Arrays.asList(WareDimension.values());
    private List<WareSizeType> sizeType = Arrays.asList(WareSizeType.values());
    private List<ConstraintType> constraintType = Arrays.asList(ConstraintType.values());
    private List<SortOrder> sortOrder = Arrays.asList(SortOrder.values());
    private List<SellUnit> sellUnit = Arrays.asList(SellUnit.values());
    private NormSetDTO norm = new NormSetDTO();
    private SteelGradeDTO steelGrade = new SteelGradeDTO();
    private ToleranceDTO tolerance = new ToleranceDTO();
    private MaterialKindDTO materialKind = new MaterialKindDTO();

    public ProgramConstsDTO() {
        String property = System.getProperty(SEND_EXCEPTION_EMAIL);
        if (property != null) {
            doSendExceptionEmail = Boolean.parseBoolean(property);
        }
    }

    public static Map<EntityType, Map> createStatesMap() {
        Map<EntityType, Map> statesMap = new HashMap();
        Map<EntityType, Class<? extends StateInterface>> class_map = EntityTypeToStates_Map.getCLASS_MAP();
        for (Map.Entry<EntityType, Class<? extends StateInterface>> entry : class_map.entrySet()) {
            EntityType key = entry.getKey();
            Class<? extends StateInterface> value = entry.getValue();
            statesMap.put(key, StateDescriptor.getTranslationsMap(value));
        }
        return statesMap;
    }

    public static ProgramConstsDTO fromEntity(ProgramConstsEntity entity) {
        ProgramConstsDTO programConstsDTO = new ProgramConstsDTO();
        BeanUtilSilent.translateBean(entity, programConstsDTO);
        programConstsDTO.setNorm(NormSetDTO.fromNormSet(entity.getNorm()));
        programConstsDTO.setSteelGrade(SteelGradeDTO.fromNormSet(entity.getSteelGrade()));
        programConstsDTO.setTolerance(ToleranceDTO.fromNormSet(entity.getTolerance()));
        programConstsDTO.setMaterialKind(MaterialKindDTO.fromNormSet(entity.getMaterialKind()));
        return programConstsDTO;
    }

    public Map<String, EntityStatesDto> createEntitiesStateMap() {
        Map<String, EntityStatesDto> ret = new HashMap();
        add(ret, OrderEntity.class, StateMachineAgregat.ORDER);
        add(ret, ProductionOrderEntity.class, StateMachineAgregat.PRODUCTION_ORDER);
        add(ret, MaterialEntity.class, StateMachineAgregat.MATERIAL);
        add(ret, MaterialTemplateEntity.class, StateMachineAgregat.MATERIAL_TEMPLATE);
//        add(ret, WareEntity.class, StateMachineAgregat.WARE);
        return ret;
    }

    private void add(Map<String, EntityStatesDto> ret, Class clazz, StateMachine stateMachine) {
//        StateMachine.PossibleChangesMap<MaterialState> materials = StateMachineAgregat.MATERIAL.listaNaztępnikow();
        StateMachine.PossibleChangesMap possibleChanges = stateMachine.listaNaztępnikow();
        EntityStatesDto materialEntityStateDto = possibleChangesToDto(clazz, possibleChanges);
        ret.put(clazz.getSimpleName(), materialEntityStateDto);
    }

    private EntityStatesDto possibleChangesToDto(Class entityClazz, Map<? extends Object, ? extends Object> possibleChangesMap) {
        EntityStatesDto materialEntityStateDto = new EntityStatesDto(entityClazz.getSimpleName());
        for (Map.Entry<? extends Object, ? extends Object> e : possibleChangesMap.entrySet()) {
            StateInterface key = (StateInterface) e.getKey();
            StateVerticleDto stateVerticle = new StateVerticleDto(key.name(), key.isRemovable());
            materialEntityStateDto.getVerticles().add(stateVerticle);
            Set<Adjacent<StateInterface>> adjacents = (Set<Adjacent<StateInterface>>) e.getValue();
            for (Adjacent<StateInterface> adjacent : adjacents) {
                if (key != adjacent.stateToGo) {
                    String state = adjacent.stateToGo.name();
                    String method = adjacent.method;
                    String visibleName = adjacent.methodVisibleName;
                    if (!Strings.isNullOrEmpty(method)) {
                        stateVerticle.getAdjacents().add(new StateVerticleAdjacentDto(state, method, visibleName, "info & comment"));
                    }
                }
            }
        }
        return materialEntityStateDto;
    }

}
