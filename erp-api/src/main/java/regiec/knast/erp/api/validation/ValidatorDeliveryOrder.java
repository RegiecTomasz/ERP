/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.validation;

import java.util.List;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ValidatorDeliveryOrder extends Validator {

    
    public void validatePositions(List<DeliveryOrderPosition> positions) {
        if(positions != null){
            for (DeliveryOrderPosition pos : positions) {
                validateNotNull(pos.getAssortmentId(), ExceptionCodes.NULL_VALUE, "assortmentId").throww();
                validateNotNull(pos.getMaterialTemplateId(), ExceptionCodes.NULL_VALUE, "materialTempalteId").throww();
                validateNotNull(pos.getCount(), ExceptionCodes.NULL_VALUE, "count").throww();
                validateCount(pos.getCount(), ExceptionCodes.MATERIAL__COUNT_NOT_VALID).throww();
            }
        }
    }
    
}
