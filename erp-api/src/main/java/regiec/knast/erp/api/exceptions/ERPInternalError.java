/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author fbrzuzka
 */
public class ERPInternalError extends ConversionErrorWithCode{
    
    
    public ERPInternalError(String message) {
        super(message, ExceptionCodes.INTERNAL_ERROR);
    }
    
    public ERPInternalError(String message, Exception ex) {
        super(message, ExceptionCodes.INTERNAL_ERROR, null, ex);
    }
    
}
