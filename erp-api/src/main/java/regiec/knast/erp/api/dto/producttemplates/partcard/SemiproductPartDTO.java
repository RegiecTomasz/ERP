/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import regiec.knast.erp.api.dto.producttemplates.ProductTemplateDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class SemiproductPartDTO extends SemiproductPartBase {

    private String positionId;
    private ProductTemplateDTO productTemplate;
    private String productTemplateId;
}
