/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.manager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;
import regiec.knast.erp.api.model.ConstraintType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialTemplateManager {

    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;    
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;    
    
    
    public List<String> findMaterialTemplateUsage(String materialTemplateId) {
        List<String> renew = new ArrayList();
        ProductTemplateEntitiesList usages = productTemplateDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("partCard.materialParts.materialTemplate", materialTemplateId, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        for (ProductTemplateEntity usage : usages) {
            renew.add("numer: " + usage.getProductNo() + ", nazwa: " + usage.getName());
        }
        return renew;
    }
    public void refreshUsage(String materialTemplateId){
        List<String> renew = findMaterialTemplateUsage(materialTemplateId); 
        materialTemplateDAO.updateOneField(materialTemplateId, "materialUsages", renew);
    }
    
}
