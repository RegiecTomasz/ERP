/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddPartCard {

    private List<AddMaterialPart> materialParts = new ArrayList();
    private List<AddWarePart> wareParts = new ArrayList();
    private List<AddSemiproductPart> semiproductParts = new ArrayList();
}
