/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.ResourceInfoEntity;
import regiec.knast.erp.api.entities.containers.ResourceInfoEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ResourceInfoDAOInterface extends DAOBaseInterface<ResourceInfoEntity, ResourceInfoEntitiesList> {

    ResourceInfoEntity get(String id);
    
}
