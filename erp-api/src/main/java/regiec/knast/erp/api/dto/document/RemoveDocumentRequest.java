/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.document;

import regiec.knast.erp.api.dto.RemoveWithConfirmationRequest;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class RemoveDocumentRequest extends RemoveWithConfirmationRequest {

}
