/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.waretypes;

import regiec.knast.erp.api.model.bases.WareTypeBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddWareTypeRequest extends WareTypeBase{

}
