/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class Validate2UniquenessRequest {

    //  private ValidationType validationType;
    private EntityType entityType;
    private String field1;
    private String value1;
    private String field2;
    private String value2;
    private String myId;
}
