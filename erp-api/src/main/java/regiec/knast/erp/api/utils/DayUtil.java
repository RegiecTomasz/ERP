/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class DayUtil {

    private static final SimpleDateFormat todayDateOnlyFormat = new SimpleDateFormat("yyyy-mm-dd");

    public static String todayDateOnly() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        String todayDateOnly = todayDateOnlyFormat.format(today);
        return todayDateOnly;
    }
}
