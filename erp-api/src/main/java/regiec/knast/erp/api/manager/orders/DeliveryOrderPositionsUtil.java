/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.orders;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.DeliveryOrderDAOInterface;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.model.MaterialAssortmentItem;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;
import regiec.knast.erp.api.model.bases.DeliveryOrderBase;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class DeliveryOrderPositionsUtil {

    @Inject
    private DeliveryOrderDAOInterface deliveryOrderDAO;
//    @Inject
//    private ProviderDAOInterface providerDAO;
    @Inject
    private Validator validator;


        public PositionAssortmentTemp findAssortmentInProvider(ProviderEntity provider, String assortmentId) {
            for (MaterialAssortmentItem mai : provider.getAssortment().getMaterialAssortment()) {
                for (MaterialAssortmentPosition maPos : mai.getMaterialAssortmentPositions()) {
                    if (maPos.getPositionId().equals(assortmentId)) {
                        return new PositionAssortmentTemp(mai.getMaterialCode(), mai.getMaterialTemplateId(), maPos);
                    }
                }
            }
            return null;
        }
        


        public String strPositions(List<DeliveryOrderPosition> positions) {
            StringBuilder ret = new StringBuilder();
            for (DeliveryOrderPosition p : positions) {
                ret.append("typ: ")
                        .append(p.getMaterialName())
                        .append("\tkod: ")
                        .append(p.getMaterialCode())
                        .append("\tilość: ")
                        .append(p.getCount())
                        .append("\nids: ")
                        .append(String.join(", ", p.getMaterialsIds())).append("\n");
            }
            return ret.toString();
        }

    public void ensurePositionsAreNotNull(DeliveryOrderBase deliveryOrder) {
        if(deliveryOrder.getPositions() == null){
            deliveryOrder.setPositions(new ArrayList());
        }
    }

}
