/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import org.bson.types.ObjectId;
import org.seenmodit.core.interfaces.IdGenerator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class IDUtil implements IdGenerator{

    public static String nextId_() {
        return new ObjectId().toHexString();
    }
    
    @Override
    public String nextId() {
        return new ObjectId().toHexString();
    }
}
