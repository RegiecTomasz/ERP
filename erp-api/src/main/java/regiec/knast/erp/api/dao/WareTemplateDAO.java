/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.WareTemplateDAOInterface;
import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.entities.containers.WareTemplateEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WareTemplateDAO extends DAOBase<WareTemplateEntity, WareTemplateEntitiesList> implements WareTemplateDAOInterface {

    private final Datastore datastore;
    private final Class entityClass = WareTemplateEntity.class;

    @Inject
    public WareTemplateDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
