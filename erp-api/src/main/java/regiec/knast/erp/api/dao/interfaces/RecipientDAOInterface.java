/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.entities.containers.RecipientEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface RecipientDAOInterface extends DAOBaseInterface<RecipientEntity, RecipientEntitiesList> {

}
