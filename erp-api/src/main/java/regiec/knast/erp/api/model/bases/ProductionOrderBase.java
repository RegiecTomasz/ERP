/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import regiec.knast.erp.api.dto.orderedproducts.OrderPosition;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductionOrderBase extends ObjectWithDate {

    private String no;
    private String orderNo;
    @ErpIdEntity(entityClass = OrderEntity.class)
    private String orderId;
    @ErpIdEntity(entityClass = OrderPosition.class)
    private String orderPositionId;
    private List<Integer> testCasesNumbers = new ArrayList();
    private String comments;
    private Date deliveryDate;
    private List<String> materialDemandsStr;
    private int count;

}
