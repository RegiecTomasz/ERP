/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.pguidegenerator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JsonDataSource;
import org.codehaus.groovy.control.CompilationFailedException;
import org.jaxygen.dto.Downloadable;
import org.jaxygen.network.DownloadableFile;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductionGuideGeneratorPDF {

    private final String jasperPGuideFileName = "11.jasper";
    private final String outPath = "C://Users/fbrzuzka/sample_Production_Guide.pdf";
    private static final Gson GSON = new GsonBuilder().create();
    private static final String FOO_DATA = "{\n"
            + "	\"orderNo\": \"123-12-3123\",\n"
            + "	\"date\": \"22 Maj 2017\",\n"
            + "	\"endDate\": \"15 Czerwiec 2017\",\n"
            + "	\"products\": [{\n"
            + "		\"no\": 1,\n"
            + "		\"operation\": \"Spawanie\",\n"
            + "		\"professionGroup\": \"spawacz\",\n"
            + "		\"operationBarcode\": \"1302837402\"\n"
            + "	},{\n"
            + "		\"no\": 2,\n"
            + "		\"operation\": \"Montowanie\",\n"   
            + "		\"professionGroup\": \"Monter\",\n"
            + "		\"operationBarcode\": \"1kocham2ewe\"\n"
            + "	}]\n"
            + "}";
    private final InputStream jsonInput;

    public ProductionGuideGeneratorPDF(ProductionGuideData data) {
        String toJson = GSON.toJson(data);
        jsonInput = new ByteArrayInputStream(toJson.getBytes(Charset.forName("utf-8"))); 
    }
    
    public static ProductionGuideData getFooData(){
        return GSON.fromJson(FOO_DATA, ProductionGuideData.class);
    }

    public void generateToStream(OutputStream pdfOutputStream) throws PdfGeneratorException {
        File jaspperWzFile = getJasperTemplate();

        JsonDataSource as;
        try {
            as = new JsonDataSource(jsonInput);
        } catch (JRException ex) {
            throw new PdfGeneratorException("Cannot create JsonDataSource. Problem with jsonInput? ", ex);
        }
        Map parameters = new HashMap();
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(jaspperWzFile.getAbsolutePath(), parameters, as);
            if (jasperPrint != null) {
                /**
                 * 1- export to PDF
                 */
                JasperExportManager.exportReportToPdfStream(jasperPrint, pdfOutputStream);
            }
        } catch (JRException e) {
            throw new CompilationFailedException(0, null, e);
        }
    }

    private File getJasperTemplate() {
        ClassLoader classLoader = getClass().getClassLoader();
        File jaspperWzFile = new File(classLoader.getResource(jasperPGuideFileName).getFile());
        return jaspperWzFile;
    }

    public File generate() throws PdfGeneratorException {
        File outFile = new File(outPath);
        if (!outFile.exists()) {
            try {
                outFile.createNewFile();
            } catch (IOException ex) {
                throw new PdfGeneratorException("Cannot create output file", ex);
            }
        }
        File jaspperWzFile = getJasperTemplate();
        String printFileName = null;
        InputStream jsonInput = new ByteArrayInputStream(FOO_DATA.getBytes(Charset.forName("utf-8")));
        JsonDataSource as;
        try {
            as = new JsonDataSource(jsonInput);
        } catch (JRException ex) {
            throw new PdfGeneratorException("Cannot create JsonDataSource. Problem with jsonInput? ", ex);
        }
        Map parameters = new HashMap();
        try {
            printFileName = JasperFillManager.fillReportToFile(jaspperWzFile.getAbsolutePath(), parameters, as);
            if (printFileName != null) {
                /**
                 * 1- export to PDF
                 */
                JasperExportManager.exportReportToPdfFile(printFileName, outFile.getAbsolutePath());
            }
        } catch (JRException e) {
            throw new CompilationFailedException(0, null, e);
        }
        return outFile;
    }

    public DownloadableFile generateToDownloadable() throws PdfGeneratorException {

        File wzFile = generate();
        return new DownloadableFile(wzFile, Downloadable.ContentDisposition.attachment, "");
    }

}
