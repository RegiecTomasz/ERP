/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.validation;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.DAOBaseInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.interfaces.RemovableEntity;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.utils.DaoHandler;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class Validator {

    @Inject
    protected DaoHandler daoHandler;

    public ValidationOut validateNotNull(Object o) {
        return validateNotNull(o, ExceptionCodes.NULL_VALUE, null);
    }

    public ValidationOut validateNotNull(Object o, ExceptionCodes exceptionCode) {
        return validateNotNull(o, exceptionCode, null);
    }

    public ValidationOut validateNotNull(Object o, String fieldName) {
        return validateNotNull(o, ExceptionCodes.NULL_VALUE, fieldName);
    }

    public ValidationOut validateNotNull(Object o, ExceptionCodes exceptionCode, String fieldName) {
        if (o == null) {
            return new ValidationOut(new ValidationException("Object is null",
                    exceptionCode,
                    ImmutableMap.of("field", Strings.nullToEmpty(fieldName)),
                    "null"));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateNotRemoved(RemovableEntity o) {
        if (o.getRemoved()) {
            return new ValidationOut(new ValidationException("entity is removed",
                    ExceptionCodes.ENTITY_IS_REMOVED,
                    ImmutableMap.of("class", o.getClass().getSimpleName()),
                    "entity is removed"));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public boolean listNullOrEmpty(List l) {
        return l == null || l.isEmpty();
    }

    public ValidationOut validateNotNullAndNotEmpty(String o) {
        return validateNotNullAndNotEmpty(o, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, null);
    }

    public ValidationOut validateNotNullAndNotEmpty(String o, ExceptionCodes exceptionCode) {
        return validateNotNullAndNotEmpty(o, exceptionCode, null);
    }

    public ValidationOut validateNotNullAndNotEmpty(String o, ExceptionCodes exceptionCode, String fieldName) {
        if (Strings.isNullOrEmpty(o) || o.trim().isEmpty()) {
            return new ValidationOut(new ValidationException("Object is null or empty",
                    exceptionCode,
                    ImmutableMap.of("field", Strings.nullToEmpty(fieldName)),
                    "null or empty"));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateCount(Integer count) {
        return validateCount(count, ExceptionCodes.MATERIAL__COUNT_NOT_VALID);
    }

    public ValidationOut validateCount(Integer count, ExceptionCodes code) {
        if (count == null || count <= 0) {
            return new ValidationOut(new ValidationException("count must be in grather than 0 but is " + String.valueOf(count),
                    code,
                    ImmutableMap.<String, String>builder()
                            .put("count", nullToEmpty(count))
                            .build(),
                    ""));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateUniqueness(String field, String valueToValidate, String myId, EntityType entityType) {
        return validateUniqueness(field, valueToValidate, myId, entityType, getUniquenessExceptionCode(entityType));
    }

    public ValidationOut validateUniqueness(String field, String valueToValidate, String myId, EntityType entityType, ExceptionCodes code) {
        DAOBaseInterface dao = daoHandler.getDaoByEntityType(entityType);
        validateNotNull(valueToValidate).throww();
        validateNotNull(entityType).throww();
        PaginableFilterWithCriteriaRequest filter = PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint(field, valueToValidate, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build();
        if (!Strings.isNullOrEmpty(myId)) {
            filter.getCriteriaFilters().getConstraints().add(new Constraint("id", myId, ConstraintType.NOT_EQUALS));
        }
        if (dao.getOneByFilter(filter).oneOrMoreExist()) {
            return new ValidationOut(new ValidationException("There is already defined " + entityType + " with that " + field + ": " + valueToValidate,
                    code,
                    ImmutableMap.<String, String>builder()
                            .put("field", field)
                            .put("value", valueToValidate)
                            .put("entityType", entityType.toString())
                            .build(),
                    "duplicate " + field));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateUniqueness2(
            String field1, String valueToValidate1, String field2, String valueToValidate2, String myId, EntityType entityType) {
        ExceptionCodes code = getUniquenessExceptionCode(entityType);
        return validateUniqueness2(field1, valueToValidate1, field2, valueToValidate2, myId, entityType, code);
    }

    public ValidationOut validateUniqueness2(
            String field1, String valueToValidate1, String field2, String valueToValidate2, String myId, EntityType entityType,
            ExceptionCodes code) {
        DAOBaseInterface dao = daoHandler.getDaoByEntityType(entityType);
        validateNotNull(valueToValidate1).throww();
        validateNotNull(valueToValidate2).throww();
        validateNotNull(entityType).throww();
        PaginableFilterWithCriteriaRequest filter = PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint(field1, valueToValidate1, ConstraintType.EQUALS)
                .addConstraint(field2, valueToValidate2, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build();
        if (!Strings.isNullOrEmpty(myId)) {
            filter.getCriteriaFilters().getConstraints().add(new Constraint("id", myId, ConstraintType.NOT_EQUALS));
        }
        if (dao.getOneByFilter(filter).oneOrMoreExist()) {
            return new ValidationOut(new ValidationException("There is already defined " + entityType + " with that " + field1 + ": " + valueToValidate1 + " and " + field2 + ": " + valueToValidate2,
                    code,
                    ImmutableMap.<String, String>builder()
                            .put("field1", field1)
                            .put("value1", valueToValidate1)
                            .put("field2", field2)
                            .put("value2", valueToValidate2)
                            .put("entityType", entityType.toString())
                            .build(),
                    "duplicate " + field1 + " with " + field2));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateCuttingSize(CuttingSize salesLength) {
        if (salesLength.getIsOneDimensial()) {
            if (salesLength.getLength() == null || salesLength.getLength() <= 0) {
                return new ValidationOut(new ValidationException("length must be grather than 0", ExceptionCodes.INTERNAL_ERROR, null));
            }
        } else {
            if (salesLength.getArea() == null || salesLength.getArea().getA() <= 0 || salesLength.getArea().getB() <= 0) {
                return new ValidationOut(new ValidationException("A and B in area must be grather than 0", ExceptionCodes.INTERNAL_ERROR, null));
            }
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateCuttingSize(Dimension dimension, String size) {
        if (!CuttingSizeManager.isValid(dimension, size)) {
            return new ValidationOut(new ValidationException("Size '" + size + "'is not a valid for dimension " + dimension, ExceptionCodes.INTERNAL_ERROR, null));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public String nullToEmpty(Object o) {
        if (o == null) {
            return "";
        } else {
            return o.toString();
        }
    }

    protected ExceptionCodes getUniquenessExceptionCode(EntityType entityType) {
        switch (entityType) {
            case MATERIAL:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case MATERIAL_TEMPLATE:
                return ExceptionCodes.MATERIAL_TEMPLATE_CODE_NOT_UNIQUE;
            case MATERIAL_TYPE:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case WARE:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case WARE_TEMPLATE:
                return ExceptionCodes.WARE_TEMPLATE_CODE_NOT_UNIQUE;
            case WARE_TYPE:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case SEMIPRODUCT_TEMPLATE:
                return ExceptionCodes.SEMIPRODUCT_TEMPLATE_NO_NOT_UNIQUE;
            case PRODUCT:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case PRODUCT_TEMPLATE:
                return ExceptionCodes.PRODUCT_TEMPLATE_NO_NOT_UNIQUE;
            case ORDER:
                return ExceptionCodes.ORDER_NO_NOT_UNIQUE_FOR_RECIPIENT;
            case PRODUCTION_ORDER:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case DELIVERY_ORDER:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case PROVIDER:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case RECIPIENT:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case SELLER:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
            case WORKER:
                return ExceptionCodes.WORKER__CODE_NOT_UNIQUE;
            case MACHINE:
                return ExceptionCodes.MACHINE_CODE_NOT_UNIQUE;
            default:
                return ExceptionCodes.FIELD_NOT_UNIQUE;
        }
    }
}
