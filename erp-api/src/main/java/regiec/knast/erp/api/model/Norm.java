/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.EqualsAndHashCode
@lombok.NoArgsConstructor
public final class Norm implements Comparable<Norm> {

    private String norm;

    public Norm(String norm) {
        this.norm = norm;
    }

    @Override
    public String toString() {
        return norm;
    }

    @Override
    public int compareTo(Norm o) {
        return this.getNorm().compareTo(o.getNorm());
    }

}
