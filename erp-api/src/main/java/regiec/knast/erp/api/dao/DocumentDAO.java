/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.DocumentDAOInterface;
import regiec.knast.erp.api.entities.DocumentEntity;
import regiec.knast.erp.api.entities.containers.DocumentEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class DocumentDAO extends DAOBase<DocumentEntity, DocumentEntitiesList> implements DocumentDAOInterface {

    private final Datastore datastore;


    @Inject
    public DocumentDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
