/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.pguidegenerator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class ProductionPlanItem {

    private Integer no = 0;
    private String operation = "operation";
    private String professionGroup = "professionGroup";
    private String operationBarcode = "12345678";
    private String comments = "komentarz do operacji produkcyjnej";
    
/*
    {
	"orderNo": "123-12-3123",
	"date": "22 Maj 2017",
	"endDate": "15 Czerwiec 2017",
	"products": [{
		"no": 1,
		"operation": "Spawanie",
		"professionGroup": "spawacz",
		"operationBarcode": "1234567"
	},{
		"no": 2,
		"operation": "Montowanie",
		"professionGroup": "Monter",
		"operationBarcode": "abcdefd"
	}]
    }
    */

}
