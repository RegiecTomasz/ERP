/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.provider;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class FindMaterialsFromProviderResponse {

    private List<MaterialAssortmentPosition> materialAssortment = new ArrayList();
}
