/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.RecipientDAOInterface;
import regiec.knast.erp.api.dto.*;
import regiec.knast.erp.api.dto.recipient.*;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.RecipientState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class RecipientService extends AbstractService<RecipientEntity, RecipientDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.RECIPIENT;

    @Inject
    private RecipientDAOInterface recipientDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public RecipientDTO addRecipient(AddRecipientRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("name", request.getName(), null, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", request.getNip(), null, MY_ENTITY_TYPE).throww();

        RecipientEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new RecipientEntity());
        entity.setState(RecipientState.NORMAL);
        RecipientEntity entity1 = recipientDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public RecipientDTO getRecipient(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public RecipientDTO updateRecipient(UpdateRecipientRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("name", request.getName(), id, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", request.getNip(), id, MY_ENTITY_TYPE).throww();
        RecipientEntity entity = recipientDAO.get(id);
        validator.validateNotNull(entity).throww();
        RecipientEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new RecipientEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
        recipientDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeRecipient(RemoveRecipientRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        RecipientEntity recipientEntity = recipientDAO.get(id);
        if (request.getConfirmed()) {
            recipientEntity.setRemoved(true);
            recipientDAO.update(recipientEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(recipientEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public RecipientDTO restoreRecipient(RestoreRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        RecipientEntity recipientEntity = recipientDAO.get(id);
        if (recipientEntity.getRemoved() == false) {
            throw new ConversionErrorWithCode("Restore is avaliable only if recipient is removed", ExceptionCodes.CANNOT_RESTORE_RECIPIENT_IN_THIS_STATE);
        }
        recipientEntity.setState(RecipientState.NORMAL);
        RecipientEntity updated = recipientDAO.update(recipientEntity);
        return updated.convertToDTO();
    }

    @NetAPI
    public RecipientsListDTO listRecipients(PaginableFilterWithCriteriaRequest request) {
        return (RecipientsListDTO) super.list(request);
    }
}
