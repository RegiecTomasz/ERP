/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api;

import com.google.inject.AbstractModule;
import java.util.HashMap;
import java.util.Map;
import regiec.knast.erp.api.dao.*;
import regiec.knast.erp.api.dao.driver.MongoDriver;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MyBinder extends AbstractModule {

    public final static Map<Class, Class> BIND_MAP = new HashMap();

    static {
        BIND_MAP.put(MongoInterface.class, MongoDriver.class);
        BIND_MAP.put(ProgramConstsDAOInterface.class, ProgramConstsDAO.class);
        BIND_MAP.put(ProgramIndexesDAOInterface.class, ProgramIndexesDAO.class);

        BIND_MAP.put(MaterialDAOInterface.class, MaterialDAO.class);
        BIND_MAP.put(OrderDAOInterface.class, OrderDAO.class);
        BIND_MAP.put(ResourceInfoDAOInterface.class, ResourceInfoDAO.class);
        BIND_MAP.put(ProductDAOInterface.class, ProductDAO.class);
        BIND_MAP.put(WareDAOInterface.class, WareDAO.class);
        BIND_MAP.put(ProviderDAOInterface.class, ProviderDAO.class);
        BIND_MAP.put(SellerDAOInterface.class, SellerDAO.class);

        BIND_MAP.put(MaterialTypeDAOInterface.class, MaterialTypeDAO.class);
        BIND_MAP.put(WareTypeDAOInterface.class, WareTypeDAO.class);

        BIND_MAP.put(MaterialTemplateDAOInterface.class, MaterialTemplateDAO.class);
        BIND_MAP.put(WareTemplateDAOInterface.class, WareTemplateDAO.class);
        BIND_MAP.put(ProductTemplateDAOInterface.class, ProductTemplateDAO.class);

        BIND_MAP.put(ResourceInfoDAOInterface.class, ResourceInfoDAO.class);
        BIND_MAP.put(RecipientDAOInterface.class, RecipientDAO.class);
        BIND_MAP.put(ProductionOrderDAOInterface.class, ProductionOrderDAO.class);
        BIND_MAP.put(DeliveryOrderDAOInterface.class, DeliveryOrderDAO.class);
        BIND_MAP.put(WorkerDAOInterface.class, WorkerDAO.class);
        BIND_MAP.put(MachineDAOInterface.class, MachineDAO.class);

        BIND_MAP.put(ProductionJobDAOInterface.class, ProductionJobDAO.class);
        BIND_MAP.put(MaterialDemandDAOInterface.class, MaterialDemandDAO.class);
        BIND_MAP.put(DevelopmentDAOInterface.class, DevelopmentDAO.class);
        BIND_MAP.put(ShoppingCartDAOInterface.class, ShoppingCartDAO.class);
BIND_MAP.put(TodoIssuesDAOInterface.class, TodoIssuesDAO.class);
BIND_MAP.put(OperationNameDAOInterface.class, OperationNameDAO.class);
BIND_MAP.put(ProfessionGroupDAOInterface.class, ProfessionGroupDAO.class);
BIND_MAP.put(DocumentDAOInterface.class, DocumentDAO.class);
//Here_will_be_insert_binding_dao
    }

    @Override
    protected void configure() {
        for (Map.Entry<Class, Class> e : BIND_MAP.entrySet()) {
            bind(e.getKey()).to(e.getValue());
        }
    }
}
