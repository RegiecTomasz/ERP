/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.pguidegenerator;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.mongodb.gridfs.GridFSDBFile;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.jaxygen.dto.Downloadable;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.business.apitests.pdf.ProductionGuideGeneratorRunner;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProgramIndexesDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ResourceInfoDAOInterface;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;
import regiec.knast.erp.api.manager.ResourceManager;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.wzgenerator.DownloadableStream;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductionGuideGenerator {

    @Inject
    private ProgramIndexesDAOInterface indexDAO;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private ResourceManager resourceManager;
    @Inject
    private ResourceInfoDAOInterface resourceInfoDAO;
    @Inject
    private Validator validator;
    private ProdGuideIdGen idGen;

    public static void main(String[] args) throws PdfGeneratorException, IOException {
        ProductionGuideGeneratorRunner.main(args);
    }

    private String nextBarcode() {
//        return "test";
        return String.valueOf(indexDAO.nextIndex(EntityType.PRODUCTION_GUIDE_BARCODE));
    }

    public Downloadable getProductionGuide(String productionOrderId, String productTemplateId, boolean forceApplicationDataContentType) {
        Long prodGuideNextIndex = indexDAO.nextIndexOrReserInNewYear(EntityType.PRODUCTION_GUIDE);
        idGen = new ProdGuideIdGen(prodGuideNextIndex);
        ByteArrayOutputStream arrayOutStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutStream = new BufferedOutputStream(arrayOutStream);
        String pdfName;

        if (!Strings.isNullOrEmpty(productionOrderId)) {
            ProductionOrderEntity productionOrder = productionOrderDAO.get(productionOrderId);
            validator.validateNotNull(productionOrder).throww();

            pdfName = "przedownik zamowienia " + Strings.nullToEmpty(productionOrder.getNo()) + ".pdf";
            GenerateProductionGuideRequest request = new GenerateProductionGuideRequest(productionOrder, true);
            generateFromProductionOrder(request, bufferedOutStream);

        } else {
            pdfName = "sks";
        }
        ByteArrayInputStream inputStream = new ByteArrayInputStream(arrayOutStream.toByteArray());
        DownloadableStream downloadableStream = new DownloadableStream(inputStream, pdfName);
        if (forceApplicationDataContentType) {
            downloadableStream.setContentType("application/data");
        }
        return downloadableStream;

    }

    public void generateFromProductTemplate(ProductTemplateEntity productTemplate, OrderedPages orderedPages, OutputStream outputStream) {
        //LinkedHashMap for save insertion order
        Map<String, String> ptToDrafts = new LinkedHashMap();
        List<String> orderedPagesIds = orderedPages.getOrderedPagesIdsList();
        List<String> orderedDraftsIds = orderedPages.getOrderedDraftsIdsList();

        Map<String, ProductionGuideData> guideDataMap = ProductionGuideData.prepareProdGuideData(productTemplate, null, new CommonProductionOrderData(), orderedPagesIds, this::nextBarcode, idGen);
        addDraftFromPT(productTemplate, null, ptToDrafts, orderedDraftsIds);

        mergePdfs(orderedPages, guideDataMap, ptToDrafts, outputStream);
    }

    public void generateFromProductionOrder(GenerateProductionGuideRequest request, OutputStream outputStream) {
        //LinkedHashMap for save insertion order
        Map<String, String> ptToDrafts = new LinkedHashMap();
        OrderedPages orderedPages = request.getOrderedPages();
        List<String> orderedPagesIds = orderedPages.getOrderedPagesIdsList();
        List<String> orderedDraftsIds = orderedPages.getOrderedDraftsIdsList();

        ProductionOrderEntity productionOrder = productionOrderDAO.get(request.getProductionOrderId());
        ProductTemplateEntity productTemplate = productionOrder.getProduct().getProductTemplate();

        Map<String, ProductionGuideData> guideDataMap = ProductionGuideData.prepareProdGuideData(productTemplate, null, new CommonProductionOrderData(productionOrder), orderedPagesIds, this::nextBarcode, idGen);
        addDraftFromPT(productTemplate, productionOrder, ptToDrafts, orderedDraftsIds);
        mergePdfs(orderedPages, guideDataMap, ptToDrafts, outputStream);
    }

    private void mergePdfs(OrderedPages orderedPages, Map<String, ProductionGuideData> ptToGuides, Map<String, String> ptToDrafts,
            OutputStream outputStream) {
        try {
            PDFMergerUtility ut = new PDFMergerUtility();

            int i = 1;
            int limit = 5;
            for (OrderedPage op : orderedPages) {
                if (op.getProductGuide()) {
                    ProductionGuideData guideData = ptToGuides.get(op.getProductTempalteId());
                    if (guideData == null) {
                        throw new ConversionError("There is no pdf file in Map, this should never happend");
                    }
//                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
//                    System.out.println(gson.toJson(guideData));
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    new ProductionGuideGeneratorPDF(guideData).generateToStream(out);
                    ut.addSource(new ByteArrayInputStream(out.toByteArray()));
                }
                if (op.getDraft()) {
                    System.out.println("teraz kolej: " + op.getProductTempalteId());
                    String draftFilePAth = ptToDrafts.get(op.getProductTempalteId());
                    if (draftFilePAth != null) {
                        File draftFile = new File(draftFilePAth);
                        InputStream resourceInputStream = new FileInputStream(draftFile);

//                    if (i <= limit) {
//                        i++;
                        System.out.println("dodałem: " + op.getProductTempalteId());
                        ut.addSource(resourceInputStream);
                    }
//                    }
                }
            }
            ut.setDestinationStream(outputStream);
            ut.mergeDocuments();
        } catch (IOException ex) {
            throw new ConversionError("Cannot merge production guide pdf files", ex);
        }
    }

    public static String filePath(String fileName) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd.hh.mm.ss");
        String now = dt.format(new Date());
        return "C://Users/fbrzuzka/prodGuide/" + fileName + "-" + now + ".pdf";
    }
// Add drafts

    private void addDraftFromPT(ProductTemplateEntity pt, ProductionOrderEntity productionOrder,
            Map<String, String> ptToDrafts, List<String> orderedDraftsIds) {
        if (orderedDraftsIds.contains(pt.getId())) {
            if (pt.getTechDrawingPDF() != null) {
                String resourceId = pt.getTechDrawingPDF().getResourceId();
                GridFSDBFile resource = resourceManager.getResourceStreamById(resourceId);
                if (resource.getLength() == 0) {
                    throw new ConversionError("resource length == 0 for file: " + resource.getFilename());
                }
                System.out.println("PDF resource length == " + resource.getLength() + " for file: " + resource.getFilename());

                if (isPdf(resource.getFilename())) {
                    InputStream input = resource.getInputStream();
                    try {
                        File tmp = File.createTempFile(resource.getFilename(), "");
                        IOUtils.copy(input, new FileOutputStream(tmp));
                        ptToDrafts.put(pt.getId(), tmp.getAbsolutePath());
                    } catch (IOException ex) {
                        throw new ConversionError("Cannot create tmp file for draft", ex);
                    }
                } else {
                    System.out.println("File: " + resource.getFilename() + " is not a pdf file.");
                }
            }
        }
        addInnerDrafts(pt, productionOrder, ptToDrafts, orderedDraftsIds);
    }

    private void addInnerDrafts(ProductTemplateEntity productTemplate, ProductionOrderEntity productionOrder,
            Map<String, String> ptToDrafts, List<String> orderedDraftsIds) {
        if (productTemplate != null && productTemplate.getPartCard() != null
                && productTemplate.getPartCard().getSemiproductParts() != null) {
            for (SemiproductPart spp : productTemplate.getPartCard().getSemiproductParts()) {
                ProductTemplateEntity pt = spp.getProductTemplate();
                addDraftFromPT(pt, productionOrder, ptToDrafts, orderedDraftsIds);
            }
        }
    }

    private boolean isPdf(String fileName) {
        return fileName.toUpperCase().contains(".PDF");
    }

    public static class MyInputStream extends InputStream {

        private ResourceInfoDAOInterface resourceInfoDAO;
        private String resourceId;

        public MyInputStream(ResourceInfoDAOInterface resourceInfoDAO, String resourceId) {
            this.resourceInfoDAO = resourceInfoDAO;
            this.resourceId = resourceId;
        }

        InputStream singleton = null;

        private InputStream getSingleton() {
            if (singleton == null) {
                System.out.println("initialize singleton for resource: " + resourceId);
                GridFSDBFile fileById = resourceInfoDAO.getFileById(resourceId);
                if (fileById == null) {
                    throw new ConversionError("file not exists");
                }
                singleton = fileById.getInputStream();
            }
            return singleton;
        }

        @Override
        public int read() throws IOException {
            return getSingleton().read();
        }
    }

}
