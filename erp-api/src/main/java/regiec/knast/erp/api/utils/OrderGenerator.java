/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import com.google.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.business.OrderService;
import regiec.knast.erp.api.business.apitests.GuiceInjector;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.order.AddOrderRequest;
import regiec.knast.erp.api.dto.orderedproducts.AddOrderPosition;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class OrderGenerator extends GuiceInjector {

    @Inject
    private ValidatorDetal validatorDetal;

    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private OrderService orderService;

    private Random rand = new Random(System.currentTimeMillis());

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(OrderGenerator.class);
        OrderGenerator test = (OrderGenerator) create;
        try {
            test.generateRandonOrders();
            //test.validateSemi("592ea2565c448d08cc60b095");
        } catch (Exception ex) {
            Logger.getLogger(OrderGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            test.cleanUp();
        }
    }

    public void generateRandonOrders() {
        List<ProductTemplateEntity> pteList = productTemplateDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder().addConstraint("validationStatus", "tak", ConstraintType.EQUALS).buildCriteriaFilter()
                .build());
        //                   minuty * godziny * dni * pracownicy?
        int workingMinutesLimit = 60 * 24 * 6 * 10;
        for (int i = 0; i < 30; i++) {
            AddOrderRequest a = new AddOrderRequest();
            a.setNr("testCase " + i);
//            a.setTestCasesNumbers("" + i);
            a.setRecipientId("5975c8134b13ec08e472ddc2");
//            a.setTestCasesNumbers("0;" + randCases(15));
            ArrayList<AddOrderPosition> orderPositions = new ArrayList();
            a.setOrderPositions(orderPositions);
            List<String> selectedPte = new ArrayList();
            int workingMinutes = 0;
            int positiononOrder = 1;
            while (workingMinutes < workingMinutesLimit) {
                ProductTemplateEntity pteToAdd = randPte(pteList, selectedPte);
                AddOrderPosition aop = new AddOrderPosition(pteToAdd.getId());
                aop.setPositionOnOrder("" + positiononOrder++);
                int count = generateOrderedProductCount(pteToAdd);
                aop.setCount(count);
                aop.setPiecePrice(MoneyERP.create(new BigDecimal(1.0), "PLN"));
                aop.setPiecePriceWithDiscount(MoneyERP.create(new BigDecimal(1.0), "PLN"));
                orderPositions.add(aop);
                workingMinutes += pteToAdd.getTotalBuildingTime() * count;
            }
            System.out.println("limit: " + workingMinutesLimit + " workingMinutes: " + workingMinutes
                    + " limit: " + workingMinutesLimit / 60 + " workingMinutes: " + workingMinutes / 60
                    + " limit: " + workingMinutesLimit / (60 * 24) + " workingMinutes: " + workingMinutes / (60 * 24));
            orderService.addOrder(a);
        }
    }

    private ProductTemplateEntity randPte(List<ProductTemplateEntity> pteList, List<String> selectedList) {
        ProductTemplateEntity selected = null;
        do {
            selected = pteList.get(rand.nextInt(pteList.size()));
        } while (selectedList.contains(selected.getId()));
        selectedList.add(selected.getId());
        return selected;
    }

    private String randCases(int i) {
        Random r = new Random(System.currentTimeMillis());
        Set<Integer> generated = new LinkedHashSet();
        while (generated.size() < 5) {
            Integer next = r.nextInt(i) + 1;
            generated.add(next);
        }
        List<String> str = generated.stream().map(j -> String.valueOf(j)).collect(Collectors.toList());
        return String.join(";", str);
    }
    Random r = new Random(System.currentTimeMillis());

    private Integer generateOrderedProductCount(ProductTemplateEntity pte) {
        int tt = (int) (pte.getTotalBuildingTime() * 60);
        if (tt < 20) {
            return 30 + rand.nextInt(70);
        } else if (tt < 50) {
            return 20 + rand.nextInt(30);
        } else if (tt < 100) {
            return 10 + rand.nextInt(20);
        } else if (tt < 400) {
            return 5 + rand.nextInt(10);
        } else if (tt < 800) {
            return 2 + rand.nextInt(5);
        }
        return 1 + rand.nextInt(1);
    }
}
