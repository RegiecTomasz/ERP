/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.mongodb.WriteResult;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.BooleanConverter;
import org.apache.commons.beanutils.converters.ByteConverter;
import org.apache.commons.beanutils.converters.CharacterConverter;
import org.apache.commons.beanutils.converters.DoubleConverter;
import org.apache.commons.beanutils.converters.FloatConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.LongConverter;
import org.apache.commons.beanutils.converters.ShortConverter;
import org.apache.commons.beanutils.converters.StringConverter;
import org.bson.types.ObjectId;
import org.jaxygen.collections.PartialArrayList;
import org.jaxygen.converters.properties.EnumConverter;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.jodah.typetools.TypeResolver;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.CriteriaContainer;
import org.mongodb.morphia.query.CriteriaContainerImpl;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.DAOBaseInterface;
import regiec.knast.erp.api.dao.util.OneByFilterResponse;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.Projection;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.model.AndOrEnum;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CriteriaFilter;
import regiec.knast.erp.api.model.Sort;
import regiec.knast.erp.api.model.SortOrder;
import regiec.knast.erp.api.utils.EntityFieldsCache;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.utils.entityfieldscache.InterfaceField;
import regiec.knast.erp.api.utils.entityfieldscache.ValueCreator;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.ValidationException;

/**
 *
 * @author jknast jakub.knast@gmail.com
 * @param <ENTITY>
 * @param <LIST>
 */
public class DAOBase<ENTITY extends ConvertableEntity, LIST extends PartialArrayList<ENTITY>> implements DAOBaseInterface<ENTITY, LIST> {

    private final Datastore datastore;
    private final GridFS gridFS;
    protected final Class<ENTITY> entityClazz;
    protected final Class<LIST> entityListClazz;

    @Inject
    private ValueCreator<ENTITY> valueCreator;

    @Inject
    public DAOBase(MongoInterface driver) {
        this.gridFS = driver.getGridFS();
        this.datastore = driver.getDatastore();

        Class<?>[] types = TypeResolver.resolveRawArguments(DAOBase.class, this.getClass());
        entityClazz = (Class<ENTITY>) types[0];
        entityListClazz = (Class<LIST>) types[1];
    }

    @Override
    public ENTITY add(ENTITY entity) {
        entity.setId(IDUtil.nextId_());
        datastore.save(entity);
        return entity;
    }

    @Override
    public ENTITY add(ENTITY entity, String id) {
        entity.setId(id);
        datastore.save(entity);
        return entity;
    }

    @Override
    public ENTITY get(String id) {
        ENTITY entity = datastore.createQuery(entityClazz).disableValidation().field("id").equal(id).get();
        return entity;
    }

    @Override
    public ENTITY update(ENTITY entity) {
        if (Strings.isNullOrEmpty(entity.getId())) {
            throw new ERPInternalError("Entity to update has empty field");
        }
        datastore.save(entity);
        return entity;
    }

    @Override
    public int updateOneField(String entityId, String fieldName, Object value) {
        Query<ENTITY> query = datastore.createQuery(entityClazz).disableValidation().field("id").equal(entityId);
        UpdateOperations<ENTITY> updateOperation = datastore.createUpdateOperations(entityClazz).disableValidation().set(fieldName, value);
        UpdateResults result = datastore.update(query, updateOperation);
        int n = result.getWriteResult().getN();
        return n;
    }

    @Override
    public int remove(String id) {
        WriteResult writeResult = datastore.delete(entityClazz, id);
        return writeResult.getN();
    }

    @Override
    public Boolean isEntityExist(Class<ENTITY> clazz, String id) {
//        datastore.exists(new Key(clazz, ))
        long a = datastore.createQuery(clazz).disableValidation().field("id").equal(id).count();
        return a > 0;
    }

    @Override
    public Boolean isEntityExist(Class<ENTITY> clazz, String idFieldName, String id) {
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(clazz);
        if (fieldsTypesFromClass.containsKey(idFieldName)) {
            long a = datastore.createQuery(clazz).disableValidation().field(idFieldName).equal(id).count();
            return a > 0;

        } else {
            throw new ConversionError("there is no class for field '" + idFieldName + "' in class '" + clazz.getName() + "'.");
        }
    }

    @Override
    public LIST list() throws ConversionError {
        try {
            List<ENTITY> entities = datastore.createQuery(entityClazz).disableValidation().asList();
            LIST entitiesList = entityListClazz.newInstance();
            entitiesList.addAll(entities);
            entitiesList.setTotalSize(entities.size());
            return entitiesList;
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ConversionError("Cannot initialize LIST class: ", ex);
        }
    }

    @Override
    public LIST list(PaginableFilterWithCriteriaRequest paginableFilterWithCriteria) {
        try {
            final long page = paginableFilterWithCriteria.getPage();
            final long pageSize = paginableFilterWithCriteria.getPageSize();
            LIST entitiesList = entityListClazz.newInstance();
            Query<ENTITY> query = datastore.createQuery(entityClazz).disableValidation();
            addCriteriaToQuery(entityClazz, query, paginableFilterWithCriteria.getCriteriaFilters());

            List<ENTITY> entities;
            long firstResult = page * pageSize;
            String stringQuery = query.toString();
//            System.out.println("query: " + stringQuery);

            addSortOrderToQuery(query, paginableFilterWithCriteria.getSort());
//            stringQuery = query.toString();
//            System.out.println("query: " + stringQuery);

            addProjectionToQuery(query, paginableFilterWithCriteria.getProjection());
            stringQuery = query.toString();
            System.out.println("query: " + stringQuery);

            if (pageSize == 0) {
                entities = query.offset((int) firstResult).asList();
            } else {
                entities = query.offset((int) firstResult).limit((int) pageSize).asList();
            }
            entitiesList.setTotalSize(query.countAll());
            entitiesList.addAll(entities);
            return entitiesList;
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ConversionError("Cannot initialize LIST class: ", ex);
        }
    }

    private Query<ENTITY> addSortOrderToQuery(Query<ENTITY> query, Sort sort) {
        Query<ENTITY> q = query;
        if (validateSortQuery(sort)) {
            if (sort.getSortOrder() == SortOrder.ASCENDING) {
                q = query.order(sort.getField());
                return query;
            } else if (sort.getSortOrder() == SortOrder.DESCENDING) {
                q = query.order("-" + sort.getField());
            }
        }
        String stringQuery = q.toString();
        System.out.println("query: " + stringQuery);
        return q;
    }

    private boolean validateSortQuery(Sort sort) {
        if (sort == null || sort.getSortOrder() == null || Strings.isNullOrEmpty(sort.getField())) {
            return false;
        }
        return true;
    }

    private CriteriaContainer addCriteriaToQuery(Class<ENTITY> clazz, Query<ENTITY> query, CriteriaFilter criteriaFilter) {
//        String stringQuery = query.toString();
//        System.out.println("query: " + stringQuery);
        List<Criteria> summed = new ArrayList();
        if (isValidCriteriaFilter(criteriaFilter)) {
            AndOrEnum andOrEnum = criteriaFilter.getValue();
            summed.addAll(giveMeConstraints(clazz, criteriaFilter.getConstraints()));
            summed.addAll(giveMeCriteria(clazz, query, criteriaFilter.getCriteriaFilters()));
//        stringQuery = query.toString();
//        System.out.println("query: " + stringQuery);
            Criteria[] summedCreiteria = summed.toArray(new Criteria[summed.size()]);
            if (summedCreiteria != null && summedCreiteria.length > 0) {
                if (andOrEnum != null && andOrEnum == AndOrEnum.AND) {
                    CriteriaContainer c = query.and(summedCreiteria);
//                stringQuery = query.toString();
//                System.out.println("query: " + stringQuery);
                    return c;

                } else if (andOrEnum != null && andOrEnum == AndOrEnum.OR) {
                    CriteriaContainer c = query.or(summedCreiteria);
//                stringQuery = query.toString();
//                System.out.println("query: " + stringQuery);
                    return c;
                }
            }
        }
        return null;
    }

    private Query<ENTITY> addProjectionToQuery(Query<ENTITY> query, Projection projection) {
        if (projection != null && projection.size() > 0) {
            projection.validateMe();
            for (Projection.ProjecttionOne p : projection) {
                query.project(p.getField(), p.getInclude());
            }
        }
        return query;
    }

    private List<Criteria> giveMeConstraints(Class<ENTITY> clazz, List<Constraint> constraints) {

        List<Criteria> summedCriteriaList = new ArrayList();
        if (constraints != null) {
            for (Constraint constraint : constraints) {
                Criteria c = null;
                ConstraintType constraintType = constraint.getConstraintType();
                String fieldName = constraint.getField();
                Object value = valueCreator.createValue(constraint.getField(), constraint.getValue(), clazz);
                if (constraintType != null && value != null) {
                    switch (constraintType) {
                        case EXISTS:
                            if ("true".equals(value)) {
                                c = datastore.createQuery(clazz).criteria(fieldName).exists();
                            } else {
                                c = datastore.createQuery(clazz).criteria(fieldName).doesNotExist();
                            }
                            summedCriteriaList.add(c);
                            break;
                        case EQUALS:
                            if (value instanceof InterfaceField) {
                                Query<ENTITY> q = datastore.createQuery(clazz).disableValidation();
                                InterfaceField interfaceField = (InterfaceField) value;
                                List<String> names = interfaceField.getOverrideNames();
                                Criteria[] criterias = new CriteriaContainerImpl[names.size()];
                                for (int i = 0; i < names.size(); i++) {
                                    criterias[i] = q.criteria(names.get(i)).equal(interfaceField.getValue());
                                }
                                c = q.or(criterias);
                            } else {
                                c = datastore.createQuery(clazz).criteria(fieldName).equal(value);
                            }
                            summedCriteriaList.add(c);
                            break;
                        case EQUALS_IGNORE_CASE:
                            c = datastore.createQuery(clazz).criteria(fieldName).equalIgnoreCase(value);
                            summedCriteriaList.add(c);
                            break;
                        case LIKE_IGNORE_CASE:
                            if (value instanceof InterfaceField) {
                                Query<ENTITY> q = datastore.createQuery(clazz).disableValidation();
                                InterfaceField interfaceField = (InterfaceField) value;
                                List<String> names = interfaceField.getOverrideNames();
                                Criteria[] criterias = new CriteriaContainerImpl[names.size()];
                                for (int i = 0; i < names.size(); i++) {
                                    if (interfaceField.getValue().getClass().isEnum() || interfaceField.getValue().getClass().equals(String.class)) {
                                        criterias[i] = q.criteria(names.get(i)).containsIgnoreCase(constraint.getValue());
                                    } else {
                                        criterias[i] = q.criteria(names.get(i)).equalIgnoreCase(interfaceField.getValue());
                                    }
                                }
                                c = q.or(criterias);
                            } else if (value.getClass().isEnum() || value.getClass().equals(String.class)) {
                                c = datastore.createQuery(clazz).criteria(fieldName).containsIgnoreCase(constraint.getValue());
                            } else {
                                c = datastore.createQuery(clazz).criteria(fieldName).equalIgnoreCase(value);
                            }
                            summedCriteriaList.add(c);
                            break;
                        case LIKE:
                            if (value.getClass().isEnum() || value.getClass().equals(String.class)) {
                                c = datastore.createQuery(clazz).criteria(fieldName).contains(constraint.getValue());
                            } else {
                                c = datastore.createQuery(clazz).criteria(fieldName).equal(value);
                            }
                            summedCriteriaList.add(c);
                            break;
                        case NOT_EQUALS:
                            c = datastore.createQuery(clazz).criteria(fieldName).notEqual(value);
                            summedCriteriaList.add(c);
                            break;
                        case GRATHER_THAN:
                            c = datastore.createQuery(clazz).criteria(fieldName).greaterThan(value);
                            summedCriteriaList.add(c);
                            break;
                        case GRATHER_THAN_OR_EQUAL:
                            c = datastore.createQuery(clazz).criteria(fieldName).greaterThanOrEq(value);
                            summedCriteriaList.add(c);
                            break;
                        case LESS_THAN:
                            c = datastore.createQuery(clazz).criteria(fieldName).lessThan(value);
                            summedCriteriaList.add(c);
                            break;
                        case LESS_THAN_OR_EQUAL:
                            c = datastore.createQuery(clazz).criteria(fieldName).lessThanOrEq(value);
                            summedCriteriaList.add(c);
                            break;
                        default:
                            throw new ConversionError("Unrecognized operator: " + constraintType);
                    }
                }
            }
        }
        return summedCriteriaList;
    }

    private List<Criteria> giveMeCriteria(Class<ENTITY> clazz, Query<ENTITY> query, List<CriteriaFilter> criteriaFilters) {
        List<Criteria> summedCriteriaList = new ArrayList();

        if (criteriaFilters != null) {
            for (CriteriaFilter criteriaFilter : criteriaFilters) {
                CriteriaContainer criteriaContainer = addCriteriaToQuery(clazz, query, criteriaFilter);
                if (criteriaContainer != null) {
                    summedCriteriaList.add(criteriaContainer);
                }
            }
        }
        return summedCriteriaList;
    }

    private boolean isValidCriteriaFilter(CriteriaFilter criteriaFilter) {

        if (criteriaFilter == null || criteriaFilter.getValue() == null) {
            return false;
        }
        return true;
    }

    @Override
    public OneByFilterResponse getOneByFilter(PaginableFilterWithCriteriaRequest paginableFilterWithCriteria) {
        LIST list = list(paginableFilterWithCriteria);
        if (list == null || list.isEmpty()) {
            //nothing there
            return new OneByFilterResponse(new ValidationException("entity not erxist",
                    ExceptionCodes.ENTITY_NOT_EXIST,
                    ImmutableMap.<String, String>builder().put("clazz", entityClazz.getCanonicalName()).build()));

        } else if (list.size() > 1) {
            //there is multiple results
            return new OneByFilterResponse(new ValidationException("multiple instance of entity",
                    ExceptionCodes.MULTIPLE_INSTANCE_OF_ENTITY,
                    ImmutableMap.<String, String>builder().put("clazz", entityClazz.getCanonicalName()).build()));
        } else {
            // is ok
            return new OneByFilterResponse(list.get(0));
        }

    }

    static final Map<Class<?>, Converter> converters = new HashMap<Class<?>, Converter>();

    static {
        converters.put(Boolean.class, new BooleanConverter());
        converters.put(Boolean.TYPE, new BooleanConverter());
        converters.put(Byte.class, new ByteConverter());
        converters.put(Byte.TYPE, new ByteConverter());
        converters.put(Character.class, new CharacterConverter());
        converters.put(Character.TYPE, new CharacterConverter());
        converters.put(Float.class, new FloatConverter());
        converters.put(Float.TYPE, new FloatConverter());
        converters.put(Double.class, new DoubleConverter());
        converters.put(Double.TYPE, new DoubleConverter());
        converters.put(double.class, new DoubleConverter());
        converters.put(Integer.class, new IntegerConverter());
        converters.put(Integer.TYPE, new IntegerConverter());
        converters.put(Long.class, new LongConverter());
        converters.put(Long.TYPE, new LongConverter());
        converters.put(Short.class, new ShortConverter());
        converters.put(Short.TYPE, new ShortConverter());
        converters.put(Enum.class, new EnumConverter());
        converters.put(String.class, new StringConverter());
        for (Class<?> c : converters.keySet()) {
            ConvertUtils.register(converters.get(c), c);
        }
    }

    @Override
    public String addFile(InputStream inputStream, String fileName) {
        final GridFSInputFile createFile = gridFS.createFile(inputStream, fileName);
        createFile.save();
        return createFile.getId().toString();
    }

    @Override
    public GridFSDBFile getFileById(String fileId) {
        return gridFS.find(new ObjectId(fileId));
    }

    @Override
    public void removeFile(String fileId) {
        gridFS.remove(new ObjectId(fileId));
    }

}
