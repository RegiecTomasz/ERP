/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JsonDataSource;
import org.codehaus.groovy.control.CompilationFailedException;
import org.jaxygen.dto.Downloadable;
import org.jaxygen.network.DownloadableFile;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.RecipientDAOInterface;
import regiec.knast.erp.api.dao.interfaces.SellerDAOInterface;
import regiec.knast.erp.api.dto.product.GenerateWzPdfRequest;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.entities.SellerEntity;
import regiec.knast.erp.api.entities.containers.ProductEntitiesList;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;
import regiec.knast.erp.api.model.wzgenerator.RecipientInfo;
import regiec.knast.erp.api.model.wzgenerator.SellerInfo;
import regiec.knast.erp.api.model.wzgenerator.WzData;
import regiec.knast.erp.api.model.wzgenerator.WzItem;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WzGenerator extends PdfGenerator<WzData> {

    private SellerDAOInterface sellerDao;
    private RecipientDAOInterface recipientDAO;
    private ProductDAOInterface productDAO;

    private final String jasperWZFileName = "wz-template-11.jasper";
    private final String outPath = "C://Users/fbrzuzka/sample_report.pdf";
    private final String jsonData;
    private final WzData wzData;

    public WzGenerator() {
        System.out.println("created wz generator with foo data");
        this.jsonData = getFooData();
        this.wzData = new WzData();
    }

    public WzGenerator(String jsonData) {
        System.out.println("created wz generator with json data");
        this.jsonData = jsonData;
        this.wzData = new WzData();
    }

    public WzGenerator(WzData data) {
        System.out.println("created wz generator with object data");
        this.wzData = data;
        this.jsonData = super.g.toJson(wzData);
    }

    @Override
    public WzData getData() {
        return wzData;
    }

    public final void initInjected() {
        this.sellerDao = super.buildObj(SellerDAOInterface.class);
        this.recipientDAO = super.buildObj(RecipientDAOInterface.class);
        this.productDAO = super.buildObj(ProductDAOInterface.class);
    }

    public WzGenerator(GenerateWzPdfRequest request) {
        System.out.println("created wz generator from request");
        initInjected();
        this.wzData = new WzData();

        SellerEntity seller = sellerDao.get(request.getSellerId());
        SellerInfo sellerInfo = new SellerInfo(seller.getName(), seller.getStreet(), seller.getPostalCode(), seller.getCity());
        wzData.setProviderInfo(sellerInfo);

        RecipientEntity recipient = recipientDAO.get(request.getRecipientId());
        RecipientInfo recipientinfo = new RecipientInfo(recipient.getName(), recipient.getStreet(), recipient.getPostalCode(), recipient.getCity());
        wzData.setRecipientInfo(recipientinfo);
        
        ProductEntitiesList list = new ProductEntitiesList();
        List<String> productsIdsSplited = request.splitIds();
        for (String productId : productsIdsSplited) {
            ProductEntity product = productDAO.get(productId);
            list.add(product);
        }

        ProductEntitiesList mergedProducts = list.mergeProducts();
        int i = 1;
        for (ProductEntity product : mergedProducts) {
            WzItem wzItem = new WzItem();
            wzItem.setCount(product.getCount());
            wzItem.setName(product.getProductTemplate().getName());
            wzItem.setProductNo(product.getProductTemplate().getProductNo());
            wzItem.setNo(i++);
            wzItem.setOrderNo(product.getOrderNo());
            wzItem.setPosition(product.getPositionOnOrder());
            wzItem.setUnit("szt");
            wzData.getProducts().add(wzItem);
        }

        this.jsonData = super.g.toJson(wzData);
    }

    @Override
    public void generateToStream(OutputStream pdfOutputStream) throws PdfGeneratorException {
        File jaspperWzFile = getJasperTemplate();
        InputStream jsonInput = new ByteArrayInputStream(this.jsonData.getBytes(Charset.forName("utf-8")));
        JsonDataSource as;
        try {
            as = new JsonDataSource(jsonInput);
        } catch (JRException ex) {
            throw new PdfGeneratorException("Cannot create JsonDataSource. Problem with jsonInput? ", ex);
        }
        Map parameters = new HashMap();
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(jaspperWzFile.getAbsolutePath(), parameters, as);
            if (jasperPrint != null) {
                /**
                 * 1- export to PDF
                 */
                JasperExportManager.exportReportToPdfStream(jasperPrint, pdfOutputStream);
            }
        } catch (JRException e) {
            throw new CompilationFailedException(0, null, e);
        }
    }

    private File getJasperTemplate() {
        ClassLoader classLoader = getClass().getClassLoader();
        File jaspperWzFile = new File(classLoader.getResource(jasperWZFileName).getFile());
        return jaspperWzFile;
    }

    /**
     * Generates to File. Use generateToStream(outputStream)
     *
     * @return
     * @throws PdfGeneratorException
     * @deprecated
     */
    @Deprecated
    public File generate() throws PdfGeneratorException {
        File outFile = new File(outPath);
        if (!outFile.exists()) {
            try {
                outFile.createNewFile();
            } catch (IOException ex) {
                throw new PdfGeneratorException("Cannot create output file", ex);
            }
        }
        File jaspperWzFile = getJasperTemplate();
        String printFileName = null;
        InputStream jsonInput = new ByteArrayInputStream(this.jsonData.getBytes(Charset.forName("utf-8")));
        JsonDataSource as;
        try {
            as = new JsonDataSource(jsonInput);
        } catch (JRException ex) {
            throw new PdfGeneratorException("Cannot create JsonDataSource. Problem with jsonInput? ", ex);
        }
        Map parameters = new HashMap();
        try {
            printFileName = JasperFillManager.fillReportToFile(jaspperWzFile.getAbsolutePath(), parameters, as);
            if (printFileName != null) {
                /**
                 * 1- export to PDF
                 */
                JasperExportManager.exportReportToPdfFile(printFileName, outFile.getAbsolutePath());
            }
        } catch (JRException e) {
            throw new CompilationFailedException(0, null, e);
        }
        return outFile;
    }

    public DownloadableFile generateToDownloadable() throws PdfGeneratorException {

        File wzFile = generate();
        return new DownloadableFile(wzFile, Downloadable.ContentDisposition.attachment, "");
    }

    private String getFooData() {
        return "{\n"
                + "	\"documentNo\": \"123123123\",\n"
                + "	\"providerInfo\": {\n"
                + "		\"name\": \"P.P.H.U. :\\\"JANGRA-BIS\\\" Export-Import Machyński Łukasz\",\n"
                + "		\"street\": \"Czerniejewska 4\",\n"
                + "		\"postal\": \"62-230\",\n"
                + "		\"city\": \"Września\"\n"
                + "	},\n"
                + "	\"recipientInfo\": {\n"
                + "		\"name\": \"HOMAG Machinery Sp. z o.o.\",\n"
                + "		\"street\": \"Prądzyńskiego 24\",\n"
                + "		\"postal\": \"63-000\",\n"
                + "		\"city\": \"Środa Wlkp.\"\n"
                + "	},\n"
                + "	\"date\": \"22 marca 2017\",\n"
                + "	\"products\": [{\n"
                + "		\"no\": 1,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 2,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 3,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 4,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	},\n"
                + "	{\n"
                + "		\"no\": 5,\n"
                + "		\"productNo\": \"2009682390\",\n"
                + "		\"name\": \"szczęki y-null, x1 greifer\",\n"
                + "		\"count\": 1,\n"
                + "		\"unit\": \"szt.\",\n"
                + "		\"orderNo\": \"2.105320-22\",\n"
                + "		\"position\": \"10\"\n"
                + "	}]\n"
                + "}";
    }
}
