/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.otherstuff;

import java.util.Set;
import java.util.TreeSet;
import regiec.knast.erp.api.model.Tolerance;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class ToleranceDTO extends TreeSet<String> {

    public static ToleranceDTO fromNormSet(Set<Tolerance> tolerances) {
        ToleranceDTO ret = new ToleranceDTO();
        for (Tolerance tolerance : tolerances) {
            ret.add(tolerance.toString());
        }
        return ret;
    }

}
