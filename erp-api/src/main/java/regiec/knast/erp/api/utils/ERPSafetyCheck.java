/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dao.interfaces.DAOBaseInterface;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ERPSafetyCheck {

    public boolean checkIfEntityIsReferenced(Class removedEntityClass, String id) {
        boolean checkIfEntityIsReferencedById = checkIfEntityIsReferencedById(removedEntityClass, id);
        boolean checkIfEntityIsReferencedByObject = checkIfEntityIsReferencedByObject(removedEntityClass, id);
        return checkIfEntityIsReferencedById || checkIfEntityIsReferencedByObject;
    }

    //----------------- private String materialId;
    public boolean checkIfEntityIsReferencedById(Class removedEntityClass, String id) {
        List<ReflectionUtil.FieldInfo> fieldInfos = getFieldsAnotatedByErpIdEntityOfType(removedEntityClass);
        for (ReflectionUtil.FieldInfo fi : fieldInfos) {
            try {
                Class<? extends EntityBase> clazz = fi.clazz;
                DaoHandler daoHandler = ObjectBuilderFactory.instance().create(DaoHandler.class);
                DAOBaseInterface dao = daoHandler.getDaoByEntityClass(clazz);
                System.out.println("Checking reference for " + removedEntityClass.getSimpleName() + " with id " + id + " in: "
                        + clazz.getSimpleName() + " on field " + fi.fieldPath());
                Boolean ret = dao.isEntityExist(clazz, fi.fieldPath(), id);
                return ret;
            } catch (ObjectCreateError ex) {
                throw new ERPInternalError("Cannot create class DAoHandler", ex);
            }
        }
        return false;
    }

    public Map<Class, List<ReflectionUtil.FieldInfo>> getFieldsAnotatedByErpIdEntity() {
        Map<Class, List<ReflectionUtil.FieldInfo>> ret = new HashMap();
        Set<Class<? extends EntityBase>> allEntitiesClasses = ReflectionUtil.getAllEntitiesClasses();

        Predicate<? super Field> namePredicate = (f -> f.getName().contains("Id"));
        Predicate<? super Field> stopInnerPredicate = (f -> EntityBase.class.isAssignableFrom(f.getType()));

        for (Class<? extends EntityBase> clazz : allEntitiesClasses) {
            List<ReflectionUtil.FieldInfo> result = ReflectionUtil.getRecursiveFieldsByPredicate(clazz, namePredicate, stopInnerPredicate);
            if (!result.isEmpty()) {
                ret.put(clazz, result);
            }
        }
        return ret;
    }

    public List<ReflectionUtil.FieldInfo> getFieldsAnotatedByErpIdEntityInClass(Class entityClass) {
        Map<Class, List<ReflectionUtil.FieldInfo>> map = getFieldsAnotatedByErpIdEntity();
        List<ReflectionUtil.FieldInfo> ret = map.containsKey(entityClass) ? map.get(entityClass) : new ArrayList();
        return ret;
    }

    public List<ReflectionUtil.FieldInfo> getFieldsAnotatedByErpIdEntityOfType(Class entityType) {
        Map<Class, List<ReflectionUtil.FieldInfo>> map = getFieldsAnotatedByErpIdEntity();
        List<ReflectionUtil.FieldInfo> ret = new ArrayList();
        for (List<ReflectionUtil.FieldInfo> list : map.values()) {
            for (ReflectionUtil.FieldInfo fieldInfo : list) {
                ErpIdEntity annotation = fieldInfo.field.getAnnotation(ErpIdEntity.class);
                if (annotation.entityClass().equals(entityType)) {
                    ret.add(fieldInfo);
                }
            }
        }
        return ret;
    }

    //----------------- private MaterialEntity material;
    public boolean checkIfEntityIsReferencedByObject(Class removedEntityClass, String id) {
        List<ReflectionUtil.FieldInfo> fieldInfos = getFieldsEntityOfType(removedEntityClass);
        for (ReflectionUtil.FieldInfo fi : fieldInfos) {
            try {
                Class<? extends EntityBase> clazz = fi.clazz;
                DaoHandler daoHandler = ObjectBuilderFactory.instance().create(DaoHandler.class);
                DAOBaseInterface dao = daoHandler.getDaoByEntityClass(clazz);
                Boolean ret;
                if (fi.field.isAnnotationPresent(Reference.class)) {
                    // its mongo reference
                    System.out.println("Checking @Reference entity object for " + removedEntityClass.getSimpleName() + " with id " + id + " in: "
                            + clazz.getSimpleName() + " on field " + fi.fieldPath());
                    ret = dao.isEntityExist(clazz, fi.fieldPath(), id);
                } else {
                    String fieldPath = fi.fieldPath() + ".id";
                    System.out.println("Checking entity object for " + removedEntityClass.getSimpleName() + " with id " + id + " in: "
                            + clazz.getSimpleName() + " on field " + fieldPath);
                    ret = dao.isEntityExist(clazz, fieldPath, id);
                }
                return ret;
            } catch (ObjectCreateError ex) {
                throw new ERPInternalError("Cannot create class DAoHandler", ex);
            }
        }
        return false;
    }

    public Map<Class, List<ReflectionUtil.FieldInfo>> getFieldsEntity() {
        Map<Class, List<ReflectionUtil.FieldInfo>> ret = new HashMap();
        Set<Class<? extends EntityBase>> allEntitiesClasses = ReflectionUtil.getAllEntitiesClasses();

        Predicate<? super Field> entityFieldPredicate = (f -> EntityBase.class.isAssignableFrom(f.getType()));
        Predicate<? super Field> stopInnerPredicate = (f -> EntityBase.class.isAssignableFrom(f.getType()));

        for (Class<? extends EntityBase> clazz : allEntitiesClasses) {
            List<ReflectionUtil.FieldInfo> result = ReflectionUtil.getRecursiveFieldsByPredicate(clazz, entityFieldPredicate, stopInnerPredicate);
            if (!result.isEmpty()) {
                ret.put(clazz, result);
            }
        }
        return ret;
    }

    public List<ReflectionUtil.FieldInfo> getFieldsEntityInClass(Class entityClass) {
        Map<Class, List<ReflectionUtil.FieldInfo>> map = getFieldsEntity();
        List<ReflectionUtil.FieldInfo> ret = map.containsKey(entityClass) ? map.get(entityClass) : new ArrayList();
        return ret;
    }

    public List<ReflectionUtil.FieldInfo> getFieldsEntityOfType(Class entityType) {
        Map<Class, List<ReflectionUtil.FieldInfo>> map = getFieldsEntity();
        List<ReflectionUtil.FieldInfo> ret = new ArrayList();
        for (List<ReflectionUtil.FieldInfo> list : map.values()) {
            for (ReflectionUtil.FieldInfo fieldInfo : list) {
                Class<?> type = fieldInfo.field.getType();
                if (type.equals(entityType)) {
                    ret.add(fieldInfo);
                }
            }
        }
        return ret;
    }
}
