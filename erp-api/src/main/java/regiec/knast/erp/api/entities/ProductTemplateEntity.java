/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.ArrayList;
import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.producttemplates.ProductTemplateDTO;
import regiec.knast.erp.api.dto.producttemplates.partcard.PartCard;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlan;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.ProductTemplateBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@Entity("productTemplates")
public class ProductTemplateEntity extends ProductTemplateBase implements EntityBase, ConvertableEntity<ProductTemplateDTO> {

    @Id
    private String id;
    private PartCard partCard;
    private ArrayList<ProductionPlan> productionPlan = new ArrayList();

    @Reference
    private ResourceInfoEntity techDrawingPDF;
    @Reference
    private ResourceInfoEntity techDrawingDXF;
    @Reference
    private ResourceInfoEntity reportTemplate;

    public ProductTemplateEntity(String id) {
        super();
        this.id = id;
    }

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public ProductTemplateDTO convertToDTO() {
        ProductTemplateDTO dTO = new ProductTemplateDTO();
        BeanUtilSilent.translateBean(this, dTO);
        if (this.getTechDrawingPDF() != null) {
            dTO.setTechDrawingPDFdto(this.getTechDrawingPDF().convertToDTO());
        }
        if (this.getTechDrawingDXF() != null) {
            dTO.setTechDrawingDXFdto(this.getTechDrawingDXF().convertToDTO());
        }
        if (this.getReportTemplate() != null) {
            dTO.setReportTemplatedto(this.getReportTemplate().convertToDTO());
        }
        if (this.getPartCard() != null) {
            dTO.setPartCard(this.getPartCard().convertToDto());
        }
        if (this.getProductionPlan() != null) {
            dTO.setProductionPlan(ProductionPlan.productionPlanToDTO(this.getProductionPlan()));
        }
        return dTO;
    }
    
}
