/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class UIReference {

    public UIReference(String id, String value) {
        this.id = id;
        this.name = value;
        this.nr = value;
    }

    private String id;
    private String name;
    private String nr;
}
