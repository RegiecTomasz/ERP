/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductTemplateDAO extends DAOBase<ProductTemplateEntity, ProductTemplateEntitiesList> implements ProductTemplateDAOInterface {

    private final Datastore datastore;

    @Inject
    public ProductTemplateDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

    @Override
    public ProductTemplateEntity getByProductNo(String productNo) {
        ProductTemplateEntity entity = datastore.createQuery(ProductTemplateEntity.class).disableValidation().field("productNo").equal(productNo).get();
        return entity;
    }
}
