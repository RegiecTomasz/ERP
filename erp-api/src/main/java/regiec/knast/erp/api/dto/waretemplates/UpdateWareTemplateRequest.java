/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.waretemplates;

import regiec.knast.erp.api.model.bases.WareTemplateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class UpdateWareTemplateRequest extends WareTemplateBase {

    private String id;
}
