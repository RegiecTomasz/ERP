/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.validation;

import java.util.Map;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.typeconverter.exceptions.ConversionError;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class ConversionErrorWithCode extends ConversionError {

    private ExceptionCodes code;
    private String message;
    private Map params;

    public ConversionErrorWithCode(String message, ExceptionCodes code) {
        super(message);
        this.code = code;
        this.message = message;
        this.params = null;
    }
    public ConversionErrorWithCode(String message, ExceptionCodes code, Map params) {
        super(message);
        this.code = code;
        this.message = message;
        this.params = params;
    }

    public ConversionErrorWithCode(String message, ExceptionCodes code, Map params, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
        this.params = params;
    }

    @NetAPI
    public Object getCode() {
        return code.getValue();
    }

    @NetAPI
    public Object getCodeName() {
        return code;
    }

    @NetAPI
    public Object getMessageInfo() {
        return message;
    }

    @NetAPI
    public Object getParams() {
        return params;
    }


}
