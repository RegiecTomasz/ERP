/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.OperationNameDAOInterface;
import regiec.knast.erp.api.entities.OperationNameEntity;
import regiec.knast.erp.api.entities.containers.OperationNameEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class OperationNameDAO extends DAOBase<OperationNameEntity, OperationNameEntitiesList> implements OperationNameDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = OperationNameEntity.class;

    @Inject
    public OperationNameDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }


}
