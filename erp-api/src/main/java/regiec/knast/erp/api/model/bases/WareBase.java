/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.states.WareState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WareBase extends ObjectWithDate {

    private int count;
    private int reserved;
    private int free;
    private String deliveryDate;
    private String stateDescription;
    private String comments;
    private WareState state;
    private boolean removed;
}
