/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.util;

import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.ValidationException;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class OneByFilterResponse<ENTITY extends EntityBase> {

    private ENTITY entityBase;
    private ValidationException validationException;

    public OneByFilterResponse(ENTITY e) {
        this.entityBase = e;
    }

    public OneByFilterResponse(ValidationException validationException) {
        this.validationException = validationException;
    }

    public boolean oneEntityExist() {
        return validationException == null && entityBase != null;
    }

    public boolean multipleEntityExist() {
        return validationException != null && validationException.getCodeName() == ExceptionCodes.MULTIPLE_INSTANCE_OF_ENTITY;
    }
    
    public boolean oneOrMoreExist() {
        return oneEntityExist() || multipleEntityExist();
    }

    public boolean onlyOneEntityExist() {
        return oneEntityExist() && !multipleEntityExist();
    }

    public ENTITY getEntity() {
        return entityBase;

    }

    public ValidationException getValidationException() {
        return validationException;
    }

    public void throww() {
        if (validationException != null) {
            throw validationException;
        }
    }

}
