/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.seenmodit.core.PositionManager;
import org.seenmodit.core.exceptions.model.PositionsChangesRet;
import regiec.knast.erp.api.dao.interfaces.ShoppingCartDAOInterface;
import regiec.knast.erp.api.dto.shoppingCart.AddPositionToShoppingCartRequest;
import regiec.knast.erp.api.dto.shoppingCart.ShoppingCartDTO;
import regiec.knast.erp.api.dto.shoppingCart.ShoppingCartPosition;
import regiec.knast.erp.api.dto.shoppingCart.UpdatePositionInShoppingCartRequest;
import regiec.knast.erp.api.dto.shoppingCart.UpdateShoppingCartRequest;
import regiec.knast.erp.api.entities.ShoppingCartEntity;
import regiec.knast.erp.api.manager.ShoppingCartManager;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ShoppingCartService {

    public final static EntityType MY_ENTITY_TYPE = EntityType.SHOPPING_CART;

    @Inject
    private ShoppingCartDAOInterface shoppingCartDAO;
    @Inject
    private Validator validator;
    @Inject
    private ShoppingCartManager shoppingCartManager;
    private PositionManager positionManager = PositionManager.create(new IDUtil());

    @NetAPI
    public ShoppingCartDTO getShoppingCart() {
        ShoppingCartEntity entity = shoppingCartDAO.getExistingOne();
        return entity.convertToDTO();
    }

    @NetAPI
    public ShoppingCartDTO updateShoppingCart(UpdateShoppingCartRequest request) {
        try {
            validator.validateNotNull(request).throww();
            ShoppingCartEntity entity = shoppingCartDAO.getExistingOne();
            validator.validateNotNull(entity).throww();
            ShoppingCartEntity updatedEntity = new ShoppingCartEntity();
            updatedEntity.setId(entity.getId());

            PositionsChangesRet<ShoppingCartPosition, ShoppingCartPosition> findAddedRemovedUpdated
                    = positionManager.findAddedRemovedUpdated(entity.getPositions(), request.getPositions());

            shoppingCartManager.handleAddedPositioms(findAddedRemovedUpdated.getAdded(), updatedEntity);
            shoppingCartManager.handleRemovedPositioms(findAddedRemovedUpdated.getRemoved(), updatedEntity);
            shoppingCartManager.handleUpdatedPositions(findAddedRemovedUpdated.getOther(), updatedEntity);

            shoppingCartDAO.update(updatedEntity);
            return updatedEntity.convertToDTO();
        } catch (Exception ex) {
            throw new ConversionError("", ex);
        }
    }

    @NetAPI
    public ShoppingCartDTO addPositionToShoppingCart(AddPositionToShoppingCartRequest request) {
        ShoppingCartDTO ret = shoppingCartManager.addPositionToShoppingCart(request);
        return ret;
    }

    @NetAPI
    public ShoppingCartDTO updatePositionInShoppingCart(UpdatePositionInShoppingCartRequest request) {
        ShoppingCartDTO ret = shoppingCartManager.updatePositionInShoppingCart(request);
        return ret;
    }

    @NetAPI
    public ShoppingCartDTO finishShoppingCart() throws ConversionError {
        return shoppingCartManager.finishShoppingCart();
    }
}
