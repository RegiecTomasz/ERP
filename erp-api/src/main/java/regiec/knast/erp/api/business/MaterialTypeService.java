/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTypeDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.RestoreRequest;
import regiec.knast.erp.api.dto.materialtemplates.RemoveMaterialTemplateRequest;
import regiec.knast.erp.api.dto.materialtemplates.UpdateMaterialTemplateRequest;
import regiec.knast.erp.api.dto.materialtypes.AddMaterialTypeRequest;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypesListDTO;
import regiec.knast.erp.api.dto.materialtypes.RemoveMaterialTypeRequest;
import regiec.knast.erp.api.dto.materialtypes.UpdateMaterialTypeRequest;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.entities.containers.MaterialTemplateEntitiesList;
import regiec.knast.erp.api.model.states.MaterialTypeState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.manager.MaterialTypeUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialTypeService extends AbstractService<MaterialTypeEntity, MaterialTypeDTO> {

    @Inject
    private MaterialTemplateService materialTemplateService;
    @Inject
    private MaterialTypeDAOInterface materialTypeDAO;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private MaterialTypeUtil materialTypeUtil;
    @Inject
    private Validator validator;

    @NetAPI
    public MaterialTypeDTO addMaterialType(AddMaterialTypeRequest request) {
        MaterialTypeEntity entity = new MaterialTypeEntity();
        BeanUtilSilent.translateBean(request, entity);
        entity.setState(MaterialTypeState.NORMAL);
        MaterialTypeEntity entity1 = materialTypeDAO.add(entity);
        return entity1.convertToDTO(); 
    }

    @NetAPI
    public MaterialTypeDTO getMaterialType(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public MaterialTypeDTO updateMaterialType(UpdateMaterialTypeRequest request) {
        String id = request.getId();
        MaterialTypeEntity entity = materialTypeDAO.get(id);
        MaterialTypeEntity updatedEntity = new MaterialTypeEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        materialTypeDAO.update(updatedEntity);
        updateMaterialTemplates(updatedEntity);
        return updatedEntity.convertToDTO(); 
    }

    private void updateMaterialTemplates(MaterialTypeEntity updatedEntity) {
        MaterialTemplateEntitiesList materialTemplateEntitiesList = materialTypeUtil.getMaterialTemplatesWithMaterialType(updatedEntity.getId());
        for (MaterialTemplateEntity materialTemplateEntity : materialTemplateEntitiesList) {
            materialTemplateEntity.setMaterialType(updatedEntity);
            materialTemplateDAO.update(materialTemplateEntity); //to update template entity
            UpdateMaterialTemplateRequest updateMaterialTemplateRequest = new UpdateMaterialTemplateRequest();
            BeanUtilSilent.translateBean(materialTemplateEntity, updateMaterialTemplateRequest);
            // to update materials with this template
            materialTemplateService.updateMaterialTemplate(updateMaterialTemplateRequest);
        }
    }

    @NetAPI
    public RemoveResponse removeMaterialType(RemoveMaterialTypeRequest request) {
        String id = request.getId();
        MaterialTypeEntity materialTypeEntity = materialTypeDAO.get(id);
        if (request.getConfirmed()) {
            materialTypeEntity.setRemoved(true);
            materialTypeDAO.update(materialTypeEntity);
            removeMaterialTemplates(materialTypeEntity);
            return new RemoveResponse(true, null);
        } else {
            //build consequencesMap
            Map<String, Object> consequencesMap = materialTypeUtil.buildRemoveConsequencesMap(materialTypeEntity);
            return new RemoveResponse(false, consequencesMap);
        }
    }

    private void removeMaterialTemplates(MaterialTypeEntity updatedEntity) {
        MaterialTemplateEntitiesList materialTemplateEntitiesList = materialTypeUtil.getMaterialTemplatesWithMaterialType(updatedEntity.getId());
        RemoveMaterialTemplateRequest removeMaterialTemplateRequest = new RemoveMaterialTemplateRequest("", true, true);
        for (MaterialTemplateEntity materialTemplateEntity : materialTemplateEntitiesList) {
            removeMaterialTemplateRequest.setId(materialTemplateEntity.getId());
            materialTemplateService.removeMaterialTemplate(removeMaterialTemplateRequest);
        }
    }

    @NetAPI
    public MaterialTypeDTO restoreMaterialTemplate(RestoreRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        MaterialTypeEntity entity = materialTypeDAO.get(id);
        if (entity.getRemoved() == false) {
            throw new ConversionErrorWithCode("Restore is avaliable only in when sth is removed", ExceptionCodes.CANNOT_RESTORE_SELLER_IN_THIS_STATE);
        }
        materialTypeDAO.update(entity);
        return entity.convertToDTO();
    }

    @NetAPI
    public MaterialTypesListDTO listMaterialTypes(PaginableFilterWithCriteriaRequest request) {
        return (MaterialTypesListDTO) super.list(request);
    }
}
