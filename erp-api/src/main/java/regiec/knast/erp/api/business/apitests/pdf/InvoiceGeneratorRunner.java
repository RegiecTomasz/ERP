/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import regiec.knast.erp.api.business.ProductService;
import static regiec.knast.erp.api.business.apitests.pdf.PdfGeneratorBase.filePath;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.product.GenerateInvoicePdfRequest;
import regiec.knast.erp.api.dto.product.ProductListDTO;
import regiec.knast.erp.api.exceptions.PdfGeneratorException;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.utils.InvoiceGenerator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class InvoiceGeneratorRunner extends PdfGeneratorBase {

    private ProductService productService;

    public static void main(String[] args) throws PdfGeneratorException {
        InvoiceGeneratorRunner runner = new InvoiceGeneratorRunner();
        try {
            runner.testGenereteTostream();
        } catch (IOException ex) {
            Logger.getLogger(InvoiceGeneratorRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public InvoiceGeneratorRunner() {
        this.productService = super.buildObj(ProductService.class);
    }

    public void testGenereteTostream() throws PdfGeneratorException, IOException {
        ProductListDTO listProducts = productService.listProducts(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder().addConstraint("state", ProductState.FINISHED.name(), ConstraintType.EQUALS).buildCriteriaFilter()
                .projectioinInclude("id")
                .build());
        List<String> productIds = listProducts.getElements().stream().map(p -> p.getId()).collect(Collectors.toList());
        GenerateInvoicePdfRequest request = new GenerateInvoicePdfRequest("5975d0e44b13ec08e472ddc6", "5975c8134b13ec08e472ddc2", productIds);

        InvoiceGenerator invoiceGenerator = new InvoiceGenerator(request);
        File outFile = createFile(new File(filePath("invoice-tempalte")));
        System.out.println("zapisuję do pliku " + outFile.getAbsolutePath());
        try (FileOutputStream fileOutputStream = new FileOutputStream(outFile)) {
            invoiceGenerator.generateToStream(fileOutputStream);
            fileOutputStream.flush();
        }
        openPdfInViever(outFile);

    }

    public void testGenereteTostream_FooData() throws PdfGeneratorException, IOException {
        InvoiceGenerator invoiceGenerator = new InvoiceGenerator();
        File outFile = createFile(new File(filePath("invoice-tempalte")));
        System.out.println("zapisuję do pliku " + outFile.getAbsolutePath());
        try (FileOutputStream fileOutputStream = new FileOutputStream(outFile)) {
            invoiceGenerator.generateToStream(fileOutputStream);
            fileOutputStream.flush();
        }
        openPdfInViever(outFile);

    }

}
