/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public enum Dimension {

    fi,
    dł,
    Gr,
    A,
    AxA,
    AxB,
    fixGr,
    AxBxGr,
    fixC,
    AxBxC
}
