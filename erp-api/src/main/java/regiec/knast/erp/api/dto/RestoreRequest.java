/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter 
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class RestoreRequest {

    private String id;
}
