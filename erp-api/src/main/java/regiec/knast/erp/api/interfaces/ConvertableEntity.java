/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.interfaces;

import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.EntityHeadquarters;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ConvertableEntity<DTO> extends EntityBase {

    public default Class<DTO> getDtoClass() {
        return EntityHeadquarters.getDto(this.getClass());
    }

    public default DTO convertToDTO() {
        try {
            return BeanUtilSilent.translateBeanAndReturn(this, getDtoClass().newInstance());
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ERPInternalError("cannot convert " + this.getClass().getCanonicalName() + "to DTO object");
        }
    }

}
