/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.model;

import java.util.ArrayList;
import java.util.List;
import org.assertj.core.util.Lists;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class StateList<T> {

    private List<T> list = new ArrayList();

    public StateList(T... states){
        this.list.addAll(Lists.newArrayList(states));
    }
    
    public boolean contains(T elem){
        return list.contains(elem);
    }
}
