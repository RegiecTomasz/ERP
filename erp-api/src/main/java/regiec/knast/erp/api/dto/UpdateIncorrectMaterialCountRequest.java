/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class UpdateIncorrectMaterialCountRequest {
    
    private String detalNo;
    private String detalType;
    private String detalId;
    private int materialPartIndex;
    private int count;
}
