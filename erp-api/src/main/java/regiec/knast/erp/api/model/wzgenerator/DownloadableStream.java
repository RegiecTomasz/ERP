/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.wzgenerator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.jaxygen.dto.Downloadable;
import org.jaxygen.mime.MimeTypeAnalyser;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class DownloadableStream implements Downloadable {

    private String contentType = "application/data";
    private ContentDisposition disposition = ContentDisposition.inline;
    private Charset charset = Charset.forName("UTF-8");
    private InputStream stream;
    private String fileName;

    public DownloadableStream(InputStream inputStream, String fileName) {
        this.stream = inputStream;
        this.fileName = fileName;
        this.contentType = MimeTypeAnalyser.getMimeForExtension(fileName);
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public ContentDisposition getDispositon() {
        return disposition;
    }

    public void setDisposition(ContentDisposition disposition) {
        this.disposition = disposition;
    }

    @Override
    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    @Override
    public InputStream getStream() throws IOException {
        return this.stream;
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }

    @Override
    public void dispose() throws IOException {
        if (this.stream != null) {
            stream.close();
        }
    }

    @Override
    public String getFileName() {
        return this.fileName;
    }

    @Override
    @Deprecated
    public Long contentSize() {
        return 1L;
    }

    @Override
    @Deprecated
    public String getETag() {
        return fileName;
    }

}
