/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.materialtemplates;

import regiec.knast.erp.api.model.bases.MaterialTemplateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddMaterialTemplateRequest extends MaterialTemplateBase {

    private String materialTypeId;
}
