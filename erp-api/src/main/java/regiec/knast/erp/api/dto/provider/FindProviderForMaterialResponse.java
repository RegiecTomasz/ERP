/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.provider;

import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class FindProviderForMaterialResponse {

    private List<AssortmentForMaterialResponse> assortmentForMaterialResponse;
    private String materialTemplateId;
}
