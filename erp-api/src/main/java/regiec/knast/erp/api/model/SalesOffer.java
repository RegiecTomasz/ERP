/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.model;

import regiec.knast.erp.api.model.bases.RecipientBaseBrief;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class SalesOffer {
    
    private RecipientBaseBrief recipient;
    private MoneyERP price;
    private Double discount = 0.0;
}
