/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.productionplan;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductionPlanBase {

    private Double transportTime;
    private Double preparationTime;
    private Double operationTime;
    private Integer operationNumber;
    private String comments;
    private String comments2;
}
