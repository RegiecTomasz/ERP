/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities.containers;

import java.util.ArrayList;
import java.util.List;
import org.jaxygen.collections.PartialArrayList;
import org.jaxygen.dto.collections.PaginableListResponseBaseDTO;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.utils.EntityHeadquarters;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialDemandEntitiesList extends PartialArrayList<MaterialDemandEntity> {

    public <LIST_DTO extends PaginableListResponseBaseDTO> LIST_DTO listToDto() {

        Class<? extends PaginableListResponseBaseDTO> listDTOClass = EntityHeadquarters.getListDto(MaterialDemandEntity.class);
        LIST_DTO rc;
        try {
            rc = (LIST_DTO) listDTOClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ConversionError("Could not create an instace of retun class while converting paginable collections", ex);
        }

        List arrayList = new ArrayList(this.size());
        for (ConvertableEntity entity : this) {
            arrayList.add(entity.convertToDTO());
        }
        rc.setElements(arrayList);
        rc.setSize(this.getTotalSize());
        return rc;
    }
}
