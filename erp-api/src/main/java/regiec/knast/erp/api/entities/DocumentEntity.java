/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.document.DocumentDTO;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.DocumentInterface;
import regiec.knast.erp.api.model.bases.DocumentBase;
import regiec.knast.erp.api.model.states.DocumentState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@Entity("documents")
public class DocumentEntity extends DocumentBase implements EntityBase, ConvertableEntity<DocumentDTO> {

    @Id
    private String id;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    public DocumentEntity(String name, DocumentInterface content, DocumentState state) {
        super(name, content, state, false);
    }

    public DocumentEntity(String id, String name, DocumentInterface content, DocumentState state) {
        super(name, content, state, false);
        this.id = id;
    }

    public void validateMe() {
        if (this.getContent() == null) {
            throw new ERPInternalError("Data in wz document in DB are not valid. in entity: " + this.id);
        }
    }
}
