/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.Date;
import regiec.knast.erp.api.entities.DocumentEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductBase extends ObjectWithDate {

    private Integer count;
    @ErpIdEntity(entityClass = RecipientEntity.class)
    private String recipientId;
    private Date deadline;
    @ErpIdEntity(entityClass = ProductionOrderEntity.class)
    private String productionOrderId;
    private String orderNo;
    private String productionOrderNo;
    private String positionOnOrder;
    private String localization;
    @ErpIdEntity(entityClass = DocumentEntity.class)
    private String wzId;
    @ErpIdEntity(entityClass = DocumentEntity.class)
    private String invoiceId;
    private String comments;
    private ProductState state = ProductState.NORMAL;
    private boolean removed;
}
