/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTypeDAOInterface;
import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.entities.containers.MaterialTypeEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialTypeDAO extends DAOBase<MaterialTypeEntity, MaterialTypeEntitiesList> implements MaterialTypeDAOInterface {

    private final Datastore datastore;
    private final Class entityClass = MaterialTypeEntity.class;
    
    @Inject
    public MaterialTypeDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

    @Override
    public MaterialTypeEntity getMaterialTypeByName(String name) {
        MaterialTypeEntity entity = datastore.createQuery(MaterialTypeEntity.class).disableValidation().field("name").equal(name).get();
        return entity;
    }
    
}
