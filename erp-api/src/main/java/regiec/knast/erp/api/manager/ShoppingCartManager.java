/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.seenmodit.core.exceptions.model.Pair;
import regiec.knast.erp.api.business.MaterialService;
import regiec.knast.erp.api.dao.interfaces.DeliveryOrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProviderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ShoppingCartDAOInterface;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;
import regiec.knast.erp.api.dto.material.AddMaterialRequest;
import regiec.knast.erp.api.dto.material.MaterialDTO;
import regiec.knast.erp.api.dto.shoppingCart.AddPositionToShoppingCartRequest;
import regiec.knast.erp.api.dto.shoppingCart.ShoppingCartDTO;
import regiec.knast.erp.api.dto.shoppingCart.ShoppingCartPosition;
import regiec.knast.erp.api.dto.shoppingCart.UpdatePositionInShoppingCartRequest;
import regiec.knast.erp.api.entities.DeliveryOrderEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.entities.ShoppingCartEntity;
import regiec.knast.erp.api.manager.orders.DeliveryOrderManager;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;
import regiec.knast.erp.api.model.states.DeliveryOrderState;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class ShoppingCartManager {

    @Inject
    private ShoppingCartDAOInterface shoppingCartDAO;
    @Inject
    private DeliveryOrderDAOInterface deliveryOrderDAO;
    @Inject
    private DeliveryOrderManager deliveryOrderManager;
    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private Validator validator;
    @Inject
    private ProviderDAOInterface providerDAO;
    @Inject
    private ProviderAssortmentUtil providerAssortmentUtil;
    @Inject
    private MaterialService materialService;

    public void handleUpdatedPositions(List<Pair<ShoppingCartPosition, ShoppingCartPosition>> other, ShoppingCartEntity shoppingCart) {
        for (Pair<ShoppingCartPosition, ShoppingCartPosition> pair : other) {
            ShoppingCartPosition actual = pair.getFirst();
            ShoppingCartPosition updated = pair.getSecond();
            for (int i = 0; i < shoppingCart.getPositions().size(); i++) {
                if (actual.getPositionId().equals(shoppingCart.getPositions().get(i))) {
                    shoppingCart.getPositions().set(i, updated);
                }
            }
        }
    }

    public void handleAddedPositioms(List<ShoppingCartPosition> added, ShoppingCartEntity shoppingCart) {
        for (ShoppingCartPosition addedPosition : added) {
            shoppingCart.getPositions().add(addedPosition);
        }
    }

    public void handleRemovedPositioms(List<ShoppingCartPosition> removed, ShoppingCartEntity shoppingCart) {
        for (ShoppingCartPosition removedPosition : removed) {
            String positionId = removedPosition.getPositionId();
            shoppingCart.getPositions()
                    .removeIf(ma -> ma.getPositionId().equals(positionId));
        }
    }

    public ShoppingCartDTO finishShoppingCart() {
        ShoppingCartEntity existingOne = shoppingCartDAO.getExistingOne();
        List<ShoppingCartPosition> positions = existingOne.getPositions();

        Map<String, List<ShoppingCartPosition>> map = new HashMap();
        for (ShoppingCartPosition pos : positions) {
            List<ShoppingCartPosition> entry = map.get(pos.getProviderId());
            if (entry == null) {
                map.put(pos.getProviderId(), new ArrayList());
            }
            map.get(pos.getProviderId()).add(pos);
        }

        for (Map.Entry<String, List<ShoppingCartPosition>> e : map.entrySet()) {
            String providerId = e.getKey();
            DeliveryOrderEntity deliveryOrder = createDeliveryOrderfromShoppingCartPositions(providerId, e.getValue());
            deliveryOrderDAO.add(deliveryOrder);
        }
        ShoppingCartEntity resetShoppingCart = shoppingCartDAO.resetShoppingCart();
        return resetShoppingCart.convertToDTO();
    }

    public DeliveryOrderEntity createDeliveryOrderfromShoppingCartPositions(String providerId, List<ShoppingCartPosition> shoppingCartPositionsForOneProvider) {
        DeliveryOrderEntity deliveryOrder = new DeliveryOrderEntity();
        deliveryOrder.setState(DeliveryOrderState.WAITIN_FOR_APROVEMENT);
        deliveryOrder.setNo(deliveryOrderManager.getNextDeliveryOrderNo());
        deliveryOrder.setProviderId(providerId);
        validator.validateNotNull(providerId, "providerId").throww();
        ProviderEntity providerEntity = providerDAO.get(providerId);
        validator.validateNotNull(providerEntity, "providerEntity").throww();

        deliveryOrder.setProviderName(providerEntity.getName());
        deliveryOrder.setPositions(new ArrayList());
        for (ShoppingCartPosition shoppingCartPosition : shoppingCartPositionsForOneProvider) {

            validator.validateNotNull(shoppingCartPosition.getMaterialTemplateId(), "shoppingCartPosition.getMaterialTemplateId").throww();
            MaterialTemplateEntity materialTemplate = materialTemplateDAO.get(shoppingCartPosition.getMaterialTemplateId());
            validator.validateNotNull(materialTemplate, "materialTemplate").throww();

            DeliveryOrderPosition deliveryOrderPosition = DeliveryOrderPosition.fromShoppingCartPosition(shoppingCartPosition, materialTemplate);

            setMaterialOnStockAsInDeliveryState(deliveryOrder.getId(), shoppingCartPosition);
            deliveryOrder.getPositions().add(deliveryOrderPosition);
        }
        // this "setOrderMail" must be at the end of creating deliveryOrder, it uses added positions and other fields.
        deliveryOrder.setOrderMail(deliveryOrderManager.generateDeliveryOrderMessage(deliveryOrder));
        return deliveryOrder;
    }

    private void setMaterialOnStockAsInDeliveryState(String deliveryOrderId, ShoppingCartPosition shoppingCartPosition) {
        MaterialEntity material = materialDAO.get(shoppingCartPosition.getMaterialOnStockId());
        validator.validateNotNull(material, "material").throww();
        material.setDeliveryOrderId(deliveryOrderId);
        material.goToOrderedState();
        materialDAO.update(material);
    }

    public ShoppingCartDTO addPositionToShoppingCart(AddPositionToShoppingCartRequest request) {
        ShoppingCartEntity existingOne = shoppingCartDAO.getExistingOne();
        validator.validateNotNull(request, "AddPositionToShoppingCartRequest").throww();
        validator.validateNotNull(request.getShoppingCartPosition(), "ShoppingCartPosition").throww();
        validator.validateNotNull(request.getShoppingCartPosition().getMaterialTemplateId(), "materialTemplateId").throww();
        validator.validateNotNull(request.getShoppingCartPosition().getAssortmentId(), "assortmentId").throww();
        validator.validateNotNull(request.getShoppingCartPosition().getProviderId(), "providerId").throww();
        int count = request.getShoppingCartPosition().getCount();
        validator.validateCount(count);

        ProviderEntity provider = providerDAO.get(request.getShoppingCartPosition().getProviderId());
        validator.validateNotNull(provider, "provider").throww();

        MaterialAssortmentPosition assortmentPosition = providerAssortmentUtil.findAssortmentPositionInProvider(provider,
                request.getShoppingCartPosition().getAssortmentId());
        validator.validateNotNull(assortmentPosition, "assortmentPosition").throww();

        MaterialTemplateEntity materialTemplateEntity = materialTemplateDAO.get(request.getShoppingCartPosition().getMaterialTemplateId());
        validator.validateNotNull(materialTemplateEntity, "materialTemplateEntity").throww();

        String newMaterialId = createMaterialInShoppingCart(request.getShoppingCartPosition(), assortmentPosition);

        ShoppingCartPosition shoppingCartPosition = new ShoppingCartPosition(provider, materialTemplateEntity, newMaterialId, assortmentPosition, count);
        shoppingCartPosition.setPositionId(IDUtil.nextId_());

        existingOne.getPositions().add(shoppingCartPosition);
        ShoppingCartEntity update = shoppingCartDAO.update(existingOne);
        return update.convertToDTO();
    }

    private String createMaterialInShoppingCart(ShoppingCartPosition shoppingCartPosition, MaterialAssortmentPosition assortmentPosition) {
        AddMaterialRequest addMaterialRequest = new AddMaterialRequest();
        addMaterialRequest.setCount(shoppingCartPosition.getCount());
        addMaterialRequest.setCreationContext(AddMaterialRequest.CreationContext.SHOPPING_CART);
        addMaterialRequest.setMaterialTemplateId(shoppingCartPosition.getMaterialTemplateId());
//                    addMaterialRequest.setDeliveryOrderId(shoppingCartPosition.getdeli); 
        addMaterialRequest.setLength(assortmentPosition.getSalesLength());
        MaterialDTO materialDTO = materialService.addMaterial(addMaterialRequest);
        return materialDTO.getId();
    }

    public ShoppingCartDTO updatePositionInShoppingCart(UpdatePositionInShoppingCartRequest request) {
        ShoppingCartEntity existingOne = shoppingCartDAO.getExistingOne();
        for (int i = 0; i < existingOne.getPositions().size(); i++) {
            if (request.getPositionId().equals(existingOne.getPositions().get(i).getPositionId())) {
                existingOne.getPositions().set(i, request.getShoppingCartPosition());
            }
        }
        ShoppingCartEntity update = shoppingCartDAO.update(existingOne);
        return update.convertToDTO();

    }

}
