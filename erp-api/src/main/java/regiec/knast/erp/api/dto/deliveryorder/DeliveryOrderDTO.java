/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.deliveryorder;

import regiec.knast.erp.api.model.bases.DeliveryOrderBase;
import regiec.knast.erp.api.model.states.DeliveryOrderState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class DeliveryOrderDTO extends DeliveryOrderBase {

    private String id;
    private String no;
    private DeliveryOrderState state;
    private boolean removed;

}
