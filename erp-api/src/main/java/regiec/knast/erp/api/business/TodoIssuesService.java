/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.TodoIssuesDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.todoIssues.AddTodoIssuesRequest;
import regiec.knast.erp.api.dto.todoIssues.RemoveTodoIssuesRequest;
import regiec.knast.erp.api.dto.todoIssues.TodoIssuesDTO;
import regiec.knast.erp.api.dto.todoIssues.TodoIssuessListDTO;
import regiec.knast.erp.api.dto.todoIssues.UpdateTodoIssuesRequest;
import regiec.knast.erp.api.entities.TodoIssuesEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class TodoIssuesService extends AbstractService<TodoIssuesEntity, TodoIssuesDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.TODO_ISSUES;

    @Inject
    private TodoIssuesDAOInterface todoIssuesDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public TodoIssuesDTO addTodoIssues(AddTodoIssuesRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNull(request.getText()).throww();

        TodoIssuesEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new TodoIssuesEntity());
        TodoIssuesEntity entity1 = todoIssuesDAO.add(entity);
        return entity1.convertToDTO(); 
    }

    @NetAPI
    public TodoIssuesDTO getTodoIssues(BasicGetRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        TodoIssuesEntity entity = todoIssuesDAO.get(id);
        return entity.convertToDTO(); 
    }

    @NetAPI
    public TodoIssuesDTO updateTodoIssues(UpdateTodoIssuesRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNull(request.getText()).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        TodoIssuesEntity entity = todoIssuesDAO.get(id);
        validator.validateNotNull(entity).throww();
        TodoIssuesEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new TodoIssuesEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
        todoIssuesDAO.update(updatedEntity);
        return updatedEntity.convertToDTO(); 
    }

    @NetAPI
    public RemoveResponse removeTodoIssues(RemoveTodoIssuesRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        todoIssuesDAO.remove(id);
        return new RemoveResponse(Boolean.TRUE, null);
    }

    @NetAPI
    public TodoIssuessListDTO listTodoIssuess(PaginableFilterWithCriteriaRequest request) {
        return (TodoIssuessListDTO) super.list(request);
    }
}
