/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.DeliveryOrderEntity;
import regiec.knast.erp.api.entities.containers.DeliveryOrderEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface DeliveryOrderDAOInterface extends DAOBaseInterface<DeliveryOrderEntity, DeliveryOrderEntitiesList> {

    Boolean removeFromDB(String deliveryOrderId);
    
}
