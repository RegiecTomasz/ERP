/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.manager.crud;

import com.google.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import regiec.knast.erp.api.dao.interfaces.OperationNameDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProfessionGroupDAOInterface;
import regiec.knast.erp.api.dto.producttemplates.productionplan.AddProductionPlan;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlan;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlanDTO;
import regiec.knast.erp.api.dto.producttemplates.productionplan.UpdateProductionPlan;
import regiec.knast.erp.api.entities.OperationNameEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.exceptions.NotImplementedException;
import regiec.knast.erp.api.manager.DetalTemplateManager;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.manager.BuildingTimeCalculator;
import regiec.knast.erp.api.utils.DaoHandler;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductionPlanCRUD {

    @Inject
    private Validator validator;
    @Inject
    private OperationNameDAOInterface operationNameDAO;
    @Inject
    private ProfessionGroupDAOInterface professionGroupDAO;
    @Inject
    private DaoHandler daoHandler;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private DetalTemplateManager detalTemplateManager;

    public List<ProductionPlanDTO> addProdPlanItemToDetalTempalte(ProductTemplateEntity dte, AddProductionPlan prodPlanItem) {
        validator.validateNotNull(prodPlanItem).throww();
        validator.validateNotNull(prodPlanItem.getTransportTime(), ExceptionCodes.PRODUCTION_PLAN__TRANSPORT_TIME_NOT_SPECIFIED).throww();
        validator.validateNotNull(prodPlanItem.getOperationTime(), ExceptionCodes.PRODUCTION_PLAN__OPERATION_TIME_NOT_SPECIFIED).throww();
        validator.validateNotNull(prodPlanItem.getPreparationTime(), ExceptionCodes.PRODUCTION_PLAN__PREPARATION_TIME_NOT_SPECIFIED).throww();
        validator.validateNotNull(prodPlanItem.getOperationNameId(), ExceptionCodes.PRODUCTION_PLAN__OPERATION_NAME_NOT_SPECIFIED).throww();
        validator.validateNotNull(prodPlanItem.getProfessionGroupId(), ExceptionCodes.PRODUCTION_PLAN__PROFESSION_GROUP_NOT_SPECIFIED).throww();

        OperationNameEntity operationName = operationNameDAO.get(prodPlanItem.getOperationNameId());
        ProfessionGroupEntity professionGroup = professionGroupDAO.get(prodPlanItem.getProfessionGroupId());
        validator.validateNotNull(operationName, ExceptionCodes.PRODUCTION_PLAN__OPERATION_NAME_NOT_SPECIFIED).throww();
        validator.validateNotNull(professionGroup, ExceptionCodes.PRODUCTION_PLAN__PROFESSION_GROUP_NOT_SPECIFIED).throww();

        ProductionPlan productionPlan = BeanUtilSilent.translateBeanAndReturn(prodPlanItem, new ProductionPlan());
        productionPlan.setPositionId(IDUtil.nextId_());
        productionPlan.setOperationName(operationName);
        productionPlan.setProfessionGroup(professionGroup);

        dte.getProductionPlan().add(productionPlan);
        updateTimesIfNeeded(dte);
        Collections.sort(dte.getProductionPlan());
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = productTemplateDAO.update(dte);
        return ProductionPlan.productionPlanToDTO(updated.getProductionPlan());
    }

    public List<ProductionPlanDTO> updateProdPlanItemToDetalTempalte(ProductTemplateEntity dte, UpdateProductionPlan updatedPPItem) {
        validator.validateNotNull(updatedPPItem).throww();
        validator.validateNotNull(updatedPPItem.getTransportTime(), ExceptionCodes.PRODUCTION_PLAN__TRANSPORT_TIME_NOT_SPECIFIED).throww();
        validator.validateNotNull(updatedPPItem.getOperationTime(), ExceptionCodes.PRODUCTION_PLAN__OPERATION_TIME_NOT_SPECIFIED).throww();
        validator.validateNotNull(updatedPPItem.getPreparationTime(), ExceptionCodes.PRODUCTION_PLAN__PREPARATION_TIME_NOT_SPECIFIED).throww();
        validator.validateNotNull(updatedPPItem.getOperationNameId(), ExceptionCodes.PRODUCTION_PLAN__OPERATION_NAME_NOT_SPECIFIED).throww();
        validator.validateNotNull(updatedPPItem.getProfessionGroupId(), ExceptionCodes.PRODUCTION_PLAN__PROFESSION_GROUP_NOT_SPECIFIED).throww();

        List<ProductionPlan> ppList = dte.getProductionPlan();
        String posId = updatedPPItem.getPositionId();
        OptionalInt indexOpt = IntStream.range(0, ppList.size())
                .filter(i -> posId.equals(ppList.get(i).getPositionId()))
                .findFirst();
        if (!indexOpt.isPresent()) {
            throw new NotImplementedException("There is no production plan item like you give an id");
        }
        ProductionPlan ePP = ppList.get(indexOpt.getAsInt());
        OperationNameEntity operationName;
        ProfessionGroupEntity professionGroup;

        if (ePP.getOperationName().getId().equals(updatedPPItem.getOperationNameId())) {
            operationName = ePP.getOperationName();
        } else {
            operationName = operationNameDAO.get(updatedPPItem.getOperationNameId());
            validator.validateNotNull(operationName, ExceptionCodes.PRODUCTION_PLAN__OPERATION_NAME_NOT_SPECIFIED).throww();
        }
        if (ePP.getProfessionGroup().getId().equals(updatedPPItem.getOperationNameId())) {
            professionGroup = ePP.getProfessionGroup();
        } else {
            professionGroup = professionGroupDAO.get(updatedPPItem.getProfessionGroupId());
            validator.validateNotNull(professionGroup, ExceptionCodes.PRODUCTION_PLAN__PROFESSION_GROUP_NOT_SPECIFIED).throww();
        }

        ProductionPlan uProductionPlan = BeanUtilSilent.translateBeanAndReturn(updatedPPItem, new ProductionPlan());
        uProductionPlan.setOperationName(operationName);
        uProductionPlan.setProfessionGroup(professionGroup);
        ppList.set(indexOpt.getAsInt(), uProductionPlan);

        dte.getProductionPlan().set(indexOpt.getAsInt(), uProductionPlan);
        updateTimesIfNeeded(dte);
        Collections.sort(dte.getProductionPlan());
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = productTemplateDAO.update(dte);
        return ProductionPlan.productionPlanToDTO(updated.getProductionPlan());
    }

    public List<ProductionPlanDTO> removeProdPlanItemInDetalTempalte(ProductTemplateEntity dte, String prodPlanItemId) {
        List<ProductionPlan> ppList = dte.getProductionPlan();
        boolean removeStatus = ppList.removeIf(pp -> pp.getPositionId().equals(prodPlanItemId));
        if (!removeStatus) {
            throw new NotImplementedException("There is no production plan like you give an id");
        }
        updateTimesIfNeeded(dte);
        Collections.sort(dte.getProductionPlan());
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = productTemplateDAO.update(dte);
        return ProductionPlan.productionPlanToDTO(updated.getProductionPlan());
    }

    private void updateTimesIfNeeded(ProductTemplateEntity dte) {
        BuildingTimeCalculator.TimeLaps timeLaps = BuildingTimeCalculator.buildingTimesTimelaps(dte);
        BuildingTimeCalculator.updateBuildingTimes(dte);
        if (BuildingTimeCalculator.isRefreshTimesInParentDetailsNeed(timeLaps, dte)) {
            detalTemplateManager.updateTimesInParentDetals(dte);
        }
    }
}
