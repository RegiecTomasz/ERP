/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.base.Strings;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.exceptions.NotValidCuttingSizeException;
import regiec.knast.erp.api.model.Area;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.Dimension;

/**
 *
 * @author fbrzuzka
 */
public class CuttingSizeManager {

    private static final String ONE_DIM_REGEX_BASE = "([0-9]+|[0-9]+\\.[0-9]+|[0-9]+,[0-9]+)";
    private static final String ONE_DIM_REGEX = "^" + ONE_DIM_REGEX_BASE + "$";
    private static final Pattern ONE_DIM_PATTERN = Pattern.compile(ONE_DIM_REGEX);
    private static final String TWO_DIM_REGEX = "^" + ONE_DIM_REGEX_BASE + "x" + ONE_DIM_REGEX_BASE + "$";
    private static final Pattern TWO_DIM_PATTERN = Pattern.compile(TWO_DIM_REGEX);
    private static final String THREE_DIM_REGEX = "^" + ONE_DIM_REGEX_BASE + "x" + ONE_DIM_REGEX_BASE + "x" + ONE_DIM_REGEX_BASE + "$";
    private static final Pattern THREE_DIM_PATTERN = Pattern.compile(THREE_DIM_REGEX);

    private static final String LENGTH_REGEX = "^([0-9][0-9]*|[0-9][0-9]*\\.[0-9][0-9]*)$";
    private static final Pattern LENGTH_PATTERN = Pattern.compile(LENGTH_REGEX);

    private static final String AREA_REGEX = "^([0-9][0-9]*|[0-9][0-9]*\\.[0-9][0-9]*)x([0-9][0-9]*|[0-9][0-9]*\\.[0-9][0-9]*)$";
    private static final Pattern AREA_PATTERN = Pattern.compile(AREA_REGEX);

    public static CuttingSize createEmptyCuttingSizeOneDim() {
        return createEmptyCuttingSize(Dimension.dł);
    }

    public static CuttingSize createEmptyCuttingSizeSheet() {
        return createEmptyCuttingSize(Dimension.AxB);
    }

    public static CuttingSize createEmptyCuttingSize(Dimension cuttingDimension) {
        CuttingSize cuttingSize = new CuttingSize();
        cuttingSize.setCuttingDimension(cuttingDimension);

        String origSize;
        switch (cuttingDimension) {
            case dł: {
                origSize = "0.0";
                cuttingSize.setOrigSize(origSize);
                cuttingSize.setLength(parseLength(origSize));
                cuttingSize.setIsOneDimensial(Boolean.TRUE);
                break;
            }
            case AxB: {
                origSize = "0.0x0.0";
                cuttingSize.setOrigSize(origSize);
                cuttingSize.setArea(parseArea(origSize));
                cuttingSize.setIsOneDimensial(Boolean.FALSE);
                break;
            }
            default: {
                throw new ConversionError("Cannot create cuttingSize from dimension '" + cuttingDimension.name() + "'.");
            }
        }
        return cuttingSize;
    }

    public static CuttingSize createCuttingSize(Dimension cuttingDimension, String origSize) {
        return createCuttingSize(cuttingDimension, origSize, true);
    }

    private static CuttingSize createCuttingSize(Dimension cuttingDimension, String origSize, boolean doValidate) {
        if (doValidate) {
            if (!isValid(cuttingDimension, origSize)) {
                throw new NotValidCuttingSizeException("Cannot create cuttingSize, size: '" + Strings.nullToEmpty(origSize)
                        + "' is not valid for dimension: '" + cuttingDimension + "'");
            }
        }
        CuttingSize cuttingSize = new CuttingSize();
        cuttingSize.setOrigSize(origSize);
        cuttingSize.setCuttingDimension(cuttingDimension);

        switch (cuttingDimension) {
            case dł: {
                cuttingSize.setLength(parseLength(origSize));
                cuttingSize.setIsOneDimensial(Boolean.TRUE);
                break;
            }
            case AxB: {
                cuttingSize.setArea(parseArea(origSize));
                cuttingSize.setIsOneDimensial(Boolean.FALSE);
                break;
            }
            default: {
                throw new ConversionError("Cannot create cuttingSize from dimension '" + cuttingDimension.name() + "'.");
            }
        }
        return cuttingSize;
    }

    public static CuttingSize createOneDimCuttingSize(Double length) {
        CuttingSize cuttingSize = new CuttingSize();
        cuttingSize.setOrigSize(String.valueOf(length));
        cuttingSize.setCuttingDimension(Dimension.dł);
        cuttingSize.setLength(length);
        cuttingSize.setIsOneDimensial(Boolean.TRUE);
        return cuttingSize;
    }

    public static CuttingSize createTwoDimCuttingSize(Double a, Double b) {
        CuttingSize cuttingSize = new CuttingSize();
        cuttingSize.setOrigSize(String.valueOf(a) + "x" + String.valueOf(b));
        cuttingSize.setCuttingDimension(Dimension.AxB);
        cuttingSize.setArea(new Area(a, b));
        cuttingSize.setIsOneDimensial(false);
        return cuttingSize;
    }

    public static boolean isValid(Dimension cuttingDimension, String origSize) {
        if (cuttingDimension == null) {
            return false;
        }
        if (origSize == null) {
            return false;
        }
        String size = StringUtils.replace(origSize, ",", ".");
        size = StringUtils.replace(size, "X", "x");
        switch (cuttingDimension) {
            case dł:
                return validateOneDim(size);

            case A:
                return validateOneDim(size);

            case Gr:
                return validateOneDim(size);

            case fi:
                return validateOneDim(size);

            case AxA:
                return validateTwoIdenticalDim(size);

            case AxB:
                return validateTwoDifferentDim(size);

            case fixC:
                return validateTwoDifferentDim(size);

            case fixGr:
                return validateTwoDifferentDim(size);

            case AxBxC:
                return validateThreeDifferentDim(size);

            case AxBxGr:
                return validateThreeDifferentDim(size);

            default:
                return false;
        }
    }

    private static boolean validateOneDim(String size) {
        boolean match = LENGTH_PATTERN.matcher(size).matches();
        if (!match) {
            return false;
        }
        double length = Double.valueOf(size);
        return length > 0.0;
    }

    private static boolean validateTwoDifferentDim(String size) {
        Matcher matcher = AREA_PATTERN.matcher(size);
        boolean match = matcher.matches();
        if (!match) {
            return false;
        }
        double a = Double.valueOf(matcher.group(1));
        double b = Double.valueOf(matcher.group(2));
        return !(a <= 0.0 || b <= 0.0);
    }

    private static boolean validateTwoIdenticalDim(String size) {
        Matcher matcher = AREA_PATTERN.matcher(size);
        boolean match = matcher.matches();
        if (!match) {
            return false;
        }
        double a = Double.valueOf(matcher.group(1));
        double b = Double.valueOf(matcher.group(2));
        return !(a <= 0.0 || b <= 0.0 || a != b);
    }

    private static boolean validateThreeDifferentDim(String size) {
        Matcher matcher = THREE_DIM_PATTERN.matcher(size);
        boolean match = matcher.matches();
        if (!match) {
            return false;
        }
        double a = Double.valueOf(matcher.group(1));
        double b = Double.valueOf(matcher.group(2));
        double c = Double.valueOf(matcher.group(3));
        return !(a <= 0.0 || b <= 0.0 || c <= 0.0);
    }

    public static Area parseArea(String origSize) {
        String size = StringUtils.replace(origSize, ",", ".");
        size = StringUtils.replace(size, "X", "x");
        boolean match = AREA_PATTERN.matcher(size).matches();
        if (match) {
            try {
                String[] splited = size.split("x");
                Double a = Double.valueOf(splited[0]);
                Double b = Double.valueOf(splited[1]);
                return new Area(a, b);
            } catch (Exception e) {
                throw new ConversionError("Could not convert area '" + origSize + "'", e);
            }
        } else {
            throw new ConversionError("Area '" + origSize + "' not match pattern");
        }

    }

    public static Double parseLength(String origSize) {
        String size = StringUtils.replace(origSize, ",", ".");
        boolean match = LENGTH_PATTERN.matcher(size).matches();
        if (match) {
            return Double.valueOf(size);
        } else {
            throw new ConversionError("Length '" + origSize + "' not match pattern");
        }
    }
}
