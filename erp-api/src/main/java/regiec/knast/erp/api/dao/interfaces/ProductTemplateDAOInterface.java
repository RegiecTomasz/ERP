/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProductTemplateDAOInterface extends DAOBaseInterface<ProductTemplateEntity, ProductTemplateEntitiesList> {

    ProductTemplateEntity getByProductNo(String productNo);

}
