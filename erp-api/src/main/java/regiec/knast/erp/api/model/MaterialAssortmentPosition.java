/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

import org.seenmodit.core.interfaces.Position;
import org.seenmodit.core.interfaces.UpdatePosition;
import regiec.knast.erp.api.validation.ValidationException;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialAssortmentPosition implements Position, UpdatePosition {

    private String positionId;
    private CuttingSize salesLength;
    private MoneyERP salesPrice;
    private SellUnit sellUnit;

    @Override
    public String getPositionId() {
        return positionId;
    }

    @Override
    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    void validateMe() {
        if (salesLength == null || !salesLength.valid()) {
            throw new ValidationException("salesLength is null or not valid");
        }
        if (salesPrice == null || !salesPrice.valid()) {
            throw new ValidationException("salesPrice is null or not valid");
        }
        if (sellUnit == null) {
            throw new ValidationException("sellUnit is null");
        }
    }
}
