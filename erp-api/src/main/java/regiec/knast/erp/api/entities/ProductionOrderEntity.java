/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.productionorder.ProductionOrderDTO;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.ProductionOrderBase;
import regiec.knast.erp.api.model.states.ProductionOrderState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.state.StateMachineAgregat;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@Entity("productionOrders")
public class ProductionOrderEntity extends ProductionOrderBase implements EntityBase, ConvertableEntity {

    @Id
    private String id;
    @Reference
    private ProductEntity product;
    @Reference
    private List<ProductionJobEntity> productionJobs = new ArrayList();
    @Reference
    private List<MaterialDemandEntity> materialDemands = new ArrayList();
    private ProductionOrderState state = ProductionOrderState.INITIAL;
    private boolean removed = false;

    public ProductionOrderEntity(ProductionOrderEntity e) {
        this.state = e.state;
    }

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    public void validateStateToFinish() {
        StateMachineAgregat.PRODUCTION_ORDER.validateChangeState(this.state, ProductionOrderState.FINISHED, new ERPInternalError("ProductionOrder has wrong state for finishing: " + this.state));
    }

    @Override
    public ProductionOrderDTO convertToDTO() {
        this.getProduct().getProductTemplate().setPartCard(null);
        this.getProduct().getProductTemplate().setTechDrawingDXF(null);
        this.getProduct().getProductTemplate().setTechDrawingPDF(null);
        this.getProduct().getProductTemplate().setProductionPlan(null);
        for (ProductionJobEntity pt : this.getProductionJobs()) {
            pt.setParent(null);
            pt.getProductEntity().getProductTemplate().setPartCard(null);
            if (pt.getChildren() != null) {
                for (ProductionJobEntity child : pt.getChildren()) {
                    child.getProductEntity().getProductTemplate().setPartCard(null);
                }
            }
        }
        ProductionOrderDTO dTO = BeanUtilSilent.translateBeanAndReturn(this, new ProductionOrderDTO());
        dTO.setProduct(this.getProduct().convertToDTO());
        dTO.setProductionJobs(new ArrayList());
        
        for (ProductionJobEntity prodJob : this.getProductionJobs()) {
            dTO.getProductionJobs().add(prodJob.convertToDTO());
        }
        return dTO;
    }

}
