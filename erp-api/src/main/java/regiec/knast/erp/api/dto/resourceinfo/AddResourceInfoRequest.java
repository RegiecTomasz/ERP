/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.resourceinfo;

import java.io.InputStream;
import regiec.knast.erp.api.model.bases.ResourceInfoBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */

@lombok.Getter
@lombok.Setter
public class AddResourceInfoRequest extends ResourceInfoBase {

    private InputStream inputStream;
}
