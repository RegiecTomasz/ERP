/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.shoppingCart;

import org.jaxygen.annotations.HiddenField;
import org.seenmodit.core.interfaces.Position;
import org.seenmodit.core.interfaces.UpdatePosition;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.MaterialAssortmentPosition;
import regiec.knast.erp.api.model.MoneyERP;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode
public class ShoppingCartPosition implements Position, UpdatePosition {

    //provider part
    private String providerId;
    @HiddenField
    private String providerName;

    //materialtempalte part
    private String materialTemplateId;
    @HiddenField
    private String positionId;
    @HiddenField
    private String materialName;
    @HiddenField
    private String materialCode;
    @HiddenField
    private String materialOnStockId;
    @HiddenField
    private String materialNorm;
    @HiddenField
    private String materialTolerance;
    @HiddenField
    private String materialSteelGrade;

    //assortment part
    private String assortmentId;
    private CuttingSize salesLength;
    private MoneyERP salesPrice;

    // specific
    private Integer count;

    public ShoppingCartPosition(ProviderEntity provider,
            MaterialTemplateEntity mte,
            String newMaterialId,
            MaterialAssortmentPosition assortmentPosition,
            int count) {

        this.addValuesFromProvider(provider);
        this.setMaterialOnStockId(newMaterialId);
        this.addValuesFromMaterial(mte);
        this.addValuesFromAssortment(assortmentPosition);
        this.count = count;
    }

    private void addValuesFromProvider(ProviderEntity provider) {
        this.providerId = provider.getId();
        this.providerName = provider.getName();

    }

    private void addValuesFromAssortment(MaterialAssortmentPosition assortmentPosition) {
        this.salesLength = assortmentPosition.getSalesLength();
        this.salesPrice = assortmentPosition.getSalesPrice();
        this.assortmentId = assortmentPosition.getPositionId();
    }

    private void addValuesFromMaterial(MaterialTemplateEntity mte) {
        this.setMaterialTemplateId(mte.getId());
        this.setMaterialCode(mte.getMaterialCode());
        this.setMaterialName(mte.getMaterialType().getName());
        this.setMaterialNorm(mte.getNorm() != null ? mte.getNorm().getNorm() : null);
        this.setMaterialTolerance(mte.getTolerance() != null ? mte.getTolerance().getTolerance() : null);
        this.setMaterialSteelGrade(mte.getSteelGrade() != null ? mte.getSteelGrade().getSteelGrade() : null);
    }

    public ShoppingCartPosition(String positionId, String assortmentId, Integer count) {
        this.positionId = positionId;
        this.count = count;
        this.assortmentId = assortmentId;
    }

    @Override
    public String getPositionId() {
        return positionId;
    }

    @Override
    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }
}
