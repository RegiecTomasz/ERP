/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities.containers;

import java.util.Collection;
import org.jaxygen.collections.PartialArrayList;
import regiec.knast.erp.api.entities.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class ProductEntitiesList extends PartialArrayList<ProductEntity> {

    public ProductEntitiesList(Collection<? extends ProductEntity> c) {
        super(c);
    }

    public ProductEntitiesList(Collection<? extends ProductEntity> c, int totalSize) {
        super(c, totalSize);
    }
    
    public ProductEntitiesList mergeProducts(){
        ProductEntitiesList ret = new ProductEntitiesList();
        for (ProductEntity next : this) {
            ProductEntity similar = ret.getSimilarOne(next);
            if(similar != null){
                similar.setCount(similar.getCount() + next.getCount());
                similar.setId(similar.getId() + "-" + next.getId());
            }else{
                ret.add(next);
            }
        }
        ret.setTotalSize(ret.size());
        return ret;
    }
    private ProductEntity getSimilarOne(ProductEntity entity){
        for (ProductEntity thi : this) {
            if(thi.getOrderNo().equals(entity.getOrderNo()) && 
                    thi.getProductionOrderNo().equals(entity.getProductionOrderNo()) && 
                    thi.getPositionOnOrder().equals(entity.getPositionOnOrder())){
                return thi;
            }
        }
        return null;
    }
}

