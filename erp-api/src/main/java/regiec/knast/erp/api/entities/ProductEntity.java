/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import com.google.common.collect.ImmutableMap;
import java.util.Date;
import java.util.Map;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.materialDemand.TopProductInfo;
import regiec.knast.erp.api.dto.product.DetalDTO;
import regiec.knast.erp.api.dto.product.ProductDTO;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.ProductBase;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("products")
public class ProductEntity extends ProductBase implements EntityBase, ConvertableEntity {

    @Id
    private String id;
    private ProductTemplateEntity productTemplate;
    @Reference
    private RecipientEntity recipient;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    public void updateProductionOrderData(String prodOrderId, String prodOrderNo) {
        this.setProductionOrderId(prodOrderId);
        this.setProductionOrderNo(prodOrderNo);
    }

    public void disposeMe(String prodOrderId, String prodOrderNo, Date dedline, int count) {
        this.setProductionOrderId(prodOrderId);
        this.setProductionOrderNo(prodOrderNo);
        this.setState(ProductState.PLANNED);
        this.setDeadline(dedline);
        this.setCount(count);
    }

    public void validateNewFromOrder(boolean isframework) {
        boolean valid = this.getState() == ProductState.ORDERED
                && this.getCount() > 0
                && this.recipient != null
                && this.productTemplate != null;
        if (!isframework) {
            valid = valid && getDeadline() != null;
        }
        if (!valid) {
            throw new ERPInternalError("created product is not valid ;/ ");
        }
    }

    @Override
    public ProductDTO convertToDTO() {
        ProductDTO dTO = new ProductDTO();
        BeanUtilSilent.translateBean(this, dTO);
        if (this.getProductTemplate() != null) {
            dTO.setProductTemplate(this.getProductTemplate().convertToDTO());
            dTO.setProductTemplateId(this.getProductTemplate().getId());
            if (this.getRecipient() != null) {
                dTO.setRecipient(this.getRecipient().convertToDTO());
            }
        }
        return dTO;
    }

    public DetalDTO convertToDetalDTO() {
        return new DetalDTO(
                id,
                this.getProductTemplate().getProductNo(),
                this.getProductTemplate().getName(),
                this.getCount()
        );
    }
    
    public TopProductInfo toTopProductInfo(){
        return new TopProductInfo(this.id, this.productTemplate.getProductNo(), this.productTemplate.getName());
    }

    public Map<String, Object> buildRemoveConsequencesMap() {
        return ImmutableMap.<String, Object>builder()
                .put("entityType", ProductEntity.class.getSimpleName())
                .put("id", this.getId())
                .put("productNo", this.getProductTemplate().getProductNo())
                .put("count", this.getCount())
                .build();
    }
}
