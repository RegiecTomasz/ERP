/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.resourceinfo;

import regiec.knast.erp.api.model.bases.ResourceInfoBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
//@lombok.NoArgsConstructor
//@lombok.AllArgsConstructor
public class ResourceInfoDTO extends ResourceInfoBase {

    private String id;
}
