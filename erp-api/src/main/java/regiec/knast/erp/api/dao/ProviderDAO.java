/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ProviderDAOInterface;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.entities.containers.ProviderEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProviderDAO extends DAOBase<ProviderEntity, ProviderEntitiesList> implements ProviderDAOInterface {

    private final Datastore datastore;
    private final Class entityClass = ProviderEntity.class;

    @Inject
    public ProviderDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
