/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.containers.MaterialEntitiesList;
import regiec.knast.erp.api.model.AndOrEnum;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CriteriaFilter;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ListTest extends GuiceInjector {

    @Inject
    private MaterialDAOInterface materialDAO;

    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(ListTest.class);
        ListTest mongoTest = (ListTest) create;
        try {
            mongoTest.testGenereteToFile();
        } catch (Exception ex) {
            Logger.getLogger(ListTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void testGenereteToFile() throws ConversionError {
        PaginableFilterWithCriteriaRequest request = new PaginableFilterWithCriteriaRequest();
        CriteriaFilter criteriaFilters = new CriteriaFilter();
        criteriaFilters.setValue(AndOrEnum.AND);
        criteriaFilters.setConstraints(Lists.newArrayList(new Constraint("materialTemplate.materialKind", "stal", ConstraintType.EQUALS)));
        request.setCriteriaFilters(criteriaFilters);
        request.setPage(0);
        request.setPageSize(0);
        MaterialEntitiesList ret = materialDAO.list(request);

        System.out.println("ret: " + ret);
    }
}
