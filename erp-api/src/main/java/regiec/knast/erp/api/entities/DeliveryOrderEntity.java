/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.DeliveryOrderBase;
import regiec.knast.erp.api.model.states.DeliveryOrderState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("deliveryOrders")
public class DeliveryOrderEntity extends DeliveryOrderBase implements EntityBase, ConvertableEntity<DeliveryOrderDTO> {

    @Id
    private String id;
    private String no;
    private DeliveryOrderState state; 
    private boolean removed = false;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
