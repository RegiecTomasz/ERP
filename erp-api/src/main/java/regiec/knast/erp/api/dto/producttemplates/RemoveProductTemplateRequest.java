/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import regiec.knast.erp.api.dto.RemoveWithConfirmationRequest;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class RemoveProductTemplateRequest extends RemoveWithConfirmationRequest {

    public RemoveProductTemplateRequest(String id, Boolean confirmed, Boolean removeRequestFromUp) {
        super(id, confirmed, removeRequestFromUp);
    }

}
