/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.order;

import java.util.List;
import regiec.knast.erp.api.model.SalesOffer;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductDiscountAutoFilledRequest {
    
    private String orderPositionId;
    private String recipientId;
    private List<SalesOffer> salesOffers;
}