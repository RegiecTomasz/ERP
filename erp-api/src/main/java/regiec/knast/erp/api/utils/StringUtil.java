/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import com.google.common.base.Strings;
import org.apache.commons.io.FilenameUtils;
import regiec.knast.erp.api.entities.ProductTemplateEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class StringUtil {

    public static String extToLowerCase(String filename) {
        final String ext = FilenameUtils.getExtension(filename);
        if (ext == null) {
            return filename;
        }
        final String lowerExt = ext.toLowerCase();
        final String newFileNme = FilenameUtils.getBaseName(filename) + "." + lowerExt;
        return newFileNme;
    }

    /**
     * This method changes null to empty and add ' sings on begin and end of
     * given text
     *
     * @param text
     * @return
     */
    public static String describe(String text) {
        return "'" + Strings.nullToEmpty(text) + "'";
    }
    
    public static String describeDetalTempl(ProductTemplateEntity dte) {
        String no = dte!=null ? dte.getProductNo(): "";
        String name = dte!=null ? dte.getName() : "";
        return " detal template with name: '" + name + "' and with number: '" + no + "'";
    }

}
