/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.adaptersgson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import lombok.extern.java.Log;
import regiec.knast.erp.api.ContextInitializer;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Log
public class DateDeserializer implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        String date = element.getAsString();
        try {
            return ContextInitializer.DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            log.log(Level.SEVERE, "Failed to parse Date due to:", e);
            return null;
        }
    }
}
