/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.dto.orderedproducts;

import com.google.common.collect.ImmutableMap;
import java.util.Date;
import java.util.List;
import java.util.Map;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface DispositionOrderPosition extends OrderPositionInterface {

    int getDisposedCount();

    void setDisposedCount(int disposedCount);

    int getToDisposeCount();

    void setToDisposeCount(int toDisposeCount);

    List<String> getDispositionsId();

    void setDispositionsId(List<String> dispositionsId);

    public default ProductionOrderEntity createDispositionProductionOrder(OrderEntity orderEntity, ProductionOrderNoGenerator generator, Date deliveryDate, int countOnDisposition) {
        ProductionOrderEntity productionOrderEntity = createBaseOfProductionOrder(orderEntity, generator);
        productionOrderEntity.setDeliveryDate(deliveryDate);
        productionOrderEntity.setCount(countOnDisposition);

        return productionOrderEntity;
    }

    public default void substractDisposition(int countOnDisposition) {
        validateDispositionCounts();
        validateGratherThan0("countOnDisposition", countOnDisposition);
        validateLessOrEqualThan("countOnDisposition", "count", countOnDisposition, getCount(), ExceptionCodes.ORDER_POSITION__TO_DISPOSE_IS_TO_BIG, ImmutableMap.of("toDispose", countOnDisposition));
        validateLessOrEqualThan("countOnDisposition", "toDispose", countOnDisposition, getToDisposeCount(), ExceptionCodes.ORDER_POSITION__TO_DISPOSE_IS_TO_BIG, ImmutableMap.of("toDispose", countOnDisposition));
        this.setToDisposeCount(this.getToDisposeCount() - countOnDisposition);
        this.setDisposedCount(this.getDisposedCount() + countOnDisposition);
        validateDispositionCounts();
    }

    public default void validateDispositionCounts() {
        validateGratherThan0("count", getCount());
        validateGratherOrEqual0("disposed", getDisposedCount());
        validateGratherOrEqual0("toDispose", getToDisposeCount());
        validateLessOrEqualThan("disposed", "count", getDisposedCount(), getCount(), ExceptionCodes.INCORRECT_VALUE);
        validateLessOrEqualThan("toDispose", "count", getToDisposeCount(), getCount(), ExceptionCodes.ORDER_POSITION__TO_DISPOSE_IS_TO_BIG, ImmutableMap.of("toDispose", getToDisposeCount()));
        if (getCount() != getDisposedCount() + getToDisposeCount()) {
            thr("toDispose + disposed is not equal to whole count: " + getToDisposeCount() + " + " + getDisposedCount() + " != " + getCount(), ExceptionCodes.INTERNAL_ERROR);
        }
    }

    public default void validateLessOrEqualThan(String smaler, String grather, int v1, int vNotGrather, ExceptionCodes code, Map map) {
        if (!(v1 <= vNotGrather)) {
            thr(smaler + " with value " + v1 + " is more than you can have in " + grather + " (" + vNotGrather + ")", code, map);
        }
    }

    public default void validateLessOrEqualThan(String smaler, String grather, int v1, int vNotGrather, ExceptionCodes code) {
        if (!(v1 <= vNotGrather)) {
            thr(smaler + " with value " + v1 + " is more than you can have in " + grather + " (" + vNotGrather + ")", code);
        }
    }

    public default void validateGratherThan0(String fieldName, int value) {
        if (!(value > 0)) {
            thr(fieldName + " is less or equal 0, is: " + value, ExceptionCodes.INCORRECT_VALUE);
        }
    }

    public default void validateGratherOrEqual0(String fieldName, int value) {
        if (!(value >= 0)) {
            thr(fieldName + " is less or equal 0, is: " + value, ExceptionCodes.INCORRECT_VALUE);
        }
    }

    public default void thr(String message, ExceptionCodes code) {
        throw new ConversionErrorWithCode(message, code);
    }//ExceptionCodes.ORDER_POSITION__TO_DISPOSE_IS_TO_BIG
    
    public default void thr(String message, ExceptionCodes code, Map map) {
        throw new ConversionErrorWithCode(message, code, map);
    }//ExceptionCodes.ORDER_POSITION__TO_DISPOSE_IS_TO_BIG

    public default void initToDispose() {
        this.setToDisposeCount(this.getCount());
    }

    public default void refreshToDispose() {
        this.setToDisposeCount(this.getCount() - this.getDisposedCount());
    }

    public static interface ProductionOrderNoGenerator {

        String generateNo(String orderEntityNo);
    }
}
