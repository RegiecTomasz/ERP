/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.entities.containers.WareTemplateEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface WareTemplateDAOInterface extends DAOBaseInterface<WareTemplateEntity, WareTemplateEntitiesList> {

}
