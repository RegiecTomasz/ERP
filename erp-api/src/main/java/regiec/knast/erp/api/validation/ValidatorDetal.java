/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.validation;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dao.interfaces.MaterialTypeDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.producttemplates.partcard.MaterialPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.WarePart;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlan;
import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.MaterialTypeEntitiesList;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.ProductTemplateType;
import regiec.knast.erp.api.model.SalesOffer;
import regiec.knast.erp.api.model.bases.ProductTemplateCreateBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ValidatorDetal extends Validator {

    @Inject
    protected ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private MaterialTypeDAOInterface materialTypeDAO;

    private List<String> typesForWypalanie = null;
    private List<String> typesForCiecie = null;

    public ValidationOut validateLoopInSemiProductPartCartRecursively(SemiproductPart semiproductPart, ProductTemplateEntity productTemplateEntity) {

        validateLoopInSemiProductPartCart(semiproductPart, productTemplateEntity).throww();
        ProductTemplateEntity firstTemplateToCheck = semiproductPart.getProductTemplate();
        // sprawdz w glab juz posiadane templaty.
        if (firstTemplateToCheck.getPartCard() != null && firstTemplateToCheck.getPartCard().getSemiproductParts() != null) {
            List<SemiproductPart> partsInWhichToCheck = firstTemplateToCheck.getPartCard().getSemiproductParts();
            for (SemiproductPart checkThisPart : partsInWhichToCheck) {
                validateLoopInSemiProductPartCartRecursively(checkThisPart, productTemplateEntity).throww();
            }
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateLoopInSemiProductPartCart(SemiproductPart semiproductPart, ProductTemplateEntity productTemplateEntity) {
        final String myTemplateId = productTemplateEntity.getId();
        if (semiproductPart.getProductTemplate().getId().equals(myTemplateId)) {
            return new ValidationOut(new ValidationException("Loop in partcard of " + myTemplateId + ".",
                    ExceptionCodes.LOOP_IN_PARTCARD,
                    ImmutableMap.<String, String>builder().put("semiproductNo",
                            semiproductPart.getProductTemplate().getProductNo()).build(),
                    "semiproduct partcard loop"));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateProductTemplatePrice(List<SalesOffer> salesOffers) {
        if (salesOffers != null) {
            for (SalesOffer so : salesOffers) {
                return validateProductTemplatePrice(so.getPrice());
            }
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateProductTemplatePriceValue(Double price) {
        if (price == null || price <= 0) {
            return new ValidationOut(new ValidationException("price must be in grather than 0 but is " + String.valueOf(price),
                    ExceptionCodes.PRODUCT_TEMPLATE__PRICE_NOT_VALID,
                    ImmutableMap.<String, String>builder()
                    .put("price", nullToEmpty(price))
                    .build(),
                    ""));
        }
        return new ValidationOut(Boolean.TRUE);
    }


    public ValidationOut validateProductTemplatePrice(MoneyERP price) {
        if (price == null || price.isNegativeOrZero()) {
            String strPrice = price != null ? String.valueOf(price.getNumber().doubleValue()) : "0";
            return new ValidationOut(new ValidationException("price must be in grather than 0 but is " + String.valueOf(price),
                    ExceptionCodes.PRODUCT_TEMPLATE__PRICE_NOT_VALID,
                    ImmutableMap.<String, String>builder()
                    .put("price", strPrice)
                    .build(),
                    ""));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateProductCount(Integer count) {
        return validateCount(count, ExceptionCodes.PRODUCT__COUNT_NOT_VALID);
    }

    public ValidationOut validateCuttedSectionSize(Dimension dimension, String size, ExceptionCodes code) {
        if (!CuttingSizeManager.isValid(dimension, size)) {
            String dimensionstr = dimension == null ? "dimension is null" : dimension.toString();
            return new ValidationOut(new ValidationException(
                    "Cutted section size is not valid. Dimension: " + dimensionstr + ", size:" + Strings.nullToEmpty(size),
                    ExceptionCodes.PART_CARD__CUTTED_DIMENSION_SIZE_NOT_VALID,
                    ImmutableMap.<String, String>builder()
                    .put("dimension", dimensionstr)
                    .put("size", Strings.nullToEmpty(size))
                    .build(),
                    ""));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    public ValidationOut validateProductDiscount(Double discount) {
        if (discount != null && (discount < 0 || discount > 100)) {
            return new ValidationOut(new ValidationException("discount must be in (0, 100) but is " + String.valueOf(discount),
                    ExceptionCodes.ORDER_POSITION__DISCOUNT_NOT_VALID,
                    ImmutableMap.<String, String>builder()
                    .put("discount", discount.toString())
                    .build(),
                    ""));
        }
        return new ValidationOut(Boolean.TRUE);
    }

    /*
    //------------------------------------------------------------------------------------------------------------------------
     */
    public List<String> validateProductTemplate(String productTemplateId) {
        return validateProductTemplate(productTemplateDAO.get(productTemplateId));
    }

    public List<String> validateProductTemplate(ProductTemplateEntity dte) {
        return validateProductTemplate(dte, new ArrayList());
    }

    private List<String> validateProductTemplate(ProductTemplateEntity dte, List<String> errors) {
        if (dte == null) {
            errors.add("detal nie istnieje");
        } else if (Strings.isNullOrEmpty(dte.getId())) {
            errors.add("detal nie ma Id");
        } else if (Strings.isNullOrEmpty(dte.getProductNo())) {
            errors.add("detal " + dte.getId() + " nie ma numeru lub jest on pusty");
        } else if (Strings.isNullOrEmpty(dte.getName())) {
            errors.add("detal " + dte.getProductNo() + " nie ma nazwy lub jest ona pusta");
        } else {
            validateProductTemplatePrice(dte, errors);
            validateProductionSalesOffers(dte, errors);
            validatePartCard(dte, errors);
            validateProductionPlan(dte, errors);
            validateCountsInPartCard(dte, errors);
//            validateIfThereIsOperationForMaterials(dte, errors);
            validateChildren(dte, errors);
        }
        return errors;
    }

    private void validateProductionSalesOffers(ProductTemplateEntity dte, List<String> errors) {
        ValidationOut out = validateProductionSalesOffers(dte);
        if (!out.status()) {
            errors.add(productInfo(dte) + out.getExceptionReason());
        }
    }

    public ValidationOut validateProductionSalesOffers(ProductTemplateCreateBase dte) {
        final boolean isSales = dte.getType() != null && dte.getType() == ProductTemplateType.SALE;
        if (isSales && (dte.getSalesOffers() == null || dte.getSalesOffers().isEmpty())) {
            String errorMessage = "Detal jest do sprzedaży a nie ma żadnej ceny";
            return new ValidationOut(new ValidationException(errorMessage,
                    ExceptionCodes.PRODUCT_TEMPLATE__SALE_OFFER_IS_EMPTY,
                    ImmutableMap.<String, String>builder()
                    .build(),errorMessage));
        } else {
            return new ValidationOut(Boolean.TRUE);
        }
    }

    private void validatePartCard(ProductTemplateEntity dte, List<String> errors) {
        if (dte.getPartCard() == null
                || (listNullOrEmpty(dte.getPartCard().getMaterialParts())
                && listNullOrEmpty(dte.getPartCard().getWareParts())
                && listNullOrEmpty(dte.getPartCard().getSemiproductParts()))) {
            errors.add(productInfo(dte) + " ma pustą listę materiałową");
        } else if (!listNullOrEmpty(dte.getPartCard().getMaterialParts())) {
            for (MaterialPart mp : dte.getPartCard().getMaterialParts()) {
                boolean ok = true;
                if (!CuttingSizeManager.isValid(mp.getMaterialTemplate().getMaterialType().getCuttingDimensionTemplate(), mp.getCuttedSectionSize())) {
                    errors.add(productInfo(dte) + " ma niepoprawną długość ciętki w liscie materiałowej dla materiału " + mp.getMaterialTemplate().getMaterialCode());
                    ok = false;
                }
                if (!CuttingSizeManager.isValid(mp.getMaterialTemplate().getMaterialType().getCuttingDimensionTemplate(), mp.getCuttedSectionSizeWithBorders())) {
                    errors.add(productInfo(dte) + " ma niepoprawny limit ciętki w liscie materiałowej dla materiału " + mp.getMaterialTemplate().getMaterialCode());
                    ok = false;
                }
                if (ok) {
                    ValidationOut cuttingOut = validateIsCuttingSizeSmallerOrEqualThanCuttingSizeWithBorders(mp);
                    if (!cuttingOut.status()) {
                        errors.add(productInfo(dte) + cuttingOut.getExceptionReason());
                    }
                }
            }
        }
    }

    public ValidationOut validateIsCuttingSizeSmallerOrEqualThanCuttingSizeWithBorders(MaterialPart mp) {
        final String cutted = mp.getCuttedSectionSize();
        final String cuttedBorders = mp.getCuttedSectionSizeWithBorders();
        final Dimension dimension = mp.getMaterialTemplate().getMaterialType().getCuttingDimensionTemplate();
        boolean status = CuttingSizeManager.createCuttingSize(dimension, cutted).smallerOrEqualThan(CuttingSizeManager.createCuttingSize(dimension, cuttedBorders));
        if (!status) {
            return new ValidationOut(new ValidationException("Limit ciętki mniejszy niż ciętka",
                    ExceptionCodes.PART_CARD__CUTTED_SIZE_WITH_BORDERS_SMALL_THAN_WITHOUT,
                    ImmutableMap.<String, String>builder()
                    .put("cuttedSectionSizeWithBorders", mp.getCuttedSectionSizeWithBorders())
                    .put("cuttedSectionSize", mp.getCuttedSectionSize())
                    .put("materialCode", mp.getMaterialTemplate().getMaterialCode())
                    .build(),
                    " limit ciętki: " + mp.getCuttedSectionSizeWithBorders() + " jest mniejszy niż ciętka " + mp.getCuttedSectionSize()
                    + ", dla materiału " + mp.getMaterialTemplate().getMaterialCode()));
        } else {
            return new ValidationOut(Boolean.TRUE);
        }
    }

    private void validateProductionPlan(ProductTemplateEntity dte, List<String> errors) {
        if (listNullOrEmpty(dte.getProductionPlan())) {
            errors.add(productInfo(dte) + " ma pustą marszrutę");
        } else {
            for (ProductionPlan productionPlan : dte.getProductionPlan()) {
                if (productionPlan.getOperationName() == null
                        || productionPlan.getOperationNumber() == null
                        || productionPlan.getOperationNumber() <= 0
                        || productionPlan.getTransportTime() == null
                        || productionPlan.getTransportTime() <= 0
                        || productionPlan.getOperationTime() == null
                        || productionPlan.getOperationTime() <= 0
                        || productionPlan.getPreparationTime() == null
                        || productionPlan.getPreparationTime() <= 0
                        || productionPlan.getProfessionGroup() == null) {
                    errors.add(productInfo(dte) + " ma niepoprawne dane w marszrucie");
                }
            }
        }
    }

    private void validateChildren(ProductTemplateEntity dte, List<String> errors) {
        if (dte != null
                && dte.getPartCard() != null
                && dte.getPartCard().getSemiproductParts() != null) {
            for (SemiproductPart semiproductPart : dte.getPartCard().getSemiproductParts()) {
                if (semiproductPart.getCount() == null || semiproductPart.getCount() <= 0) {
                    errors.add(productInfo(dte) + " ma niepoprawną ilość półproduktów w liście materiałowej");
                }
                if (semiproductPart.getProductTemplate() == null) {
                    errors.add(productInfo(dte) + " półprodukt na liście materiałowej nie ma wybranego szablonu");
                } else {
                    validateProductTemplate(semiproductPart.getProductTemplate(), errors);
                }
            }
        }
    }

    private void validateCountsInPartCard(ProductTemplateEntity dte, List<String> errors) {
        if (dte != null && dte.getPartCard() != null) {
            if (dte.getPartCard().getMaterialParts() != null) {
                for (MaterialPart p : dte.getPartCard().getMaterialParts()) {
                    if (p.getCount() == null || p.getCount() <= 0) {
                        errors.add(productInfo(dte) + " ma niepoprawną ilość materiałów w liście materiałowej: " + String.valueOf(p.getCount()));
                    }
                    if (p.getMaterialTemplate() == null) {
                        errors.add(productInfo(dte) + " ma materiał, który na liście materiałowej nie ma wybranego szablonu");
                    }
                }
            }
            if (dte.getPartCard().getWareParts() != null) {
                for (WarePart p : dte.getPartCard().getWareParts()) {
                    if (p.getCount() == null || p.getCount() <= 0) {
                        errors.add(productInfo(dte) + " ma niepoprawną ilość towarów w liście materiałowej: " + String.valueOf(p.getCount()));
                    }
                    if (p.getWareTemplate() == null) {
                        errors.add(productInfo(dte) + " ma materiał towar, który na liście materiałowej nie ma wybranego szablonu");
                    }
                }
            }
            if (dte.getPartCard().getSemiproductParts() != null) {
                for (SemiproductPart semiproductPart : dte.getPartCard().getSemiproductParts()) {
                    if (semiproductPart.getCount() == null || semiproductPart.getCount() <= 0) {
                        errors.add(productInfo(dte) + " ma niepoprawną ilość półproduktów w liście materiałowej");
                    }
                    if (semiproductPart.getProductTemplate() == null) {
                        errors.add(productInfo(dte) + " półprodukt na liście materiałowej nie ma wybranego szablonu");
                    }
                }
            }
        }
    }

    public List<String> validateAllDetalsIfThereIsOperationForMaterials() {
        List<String> statuses = new ArrayList();
        for (ProductTemplateEntity pte : productTemplateDAO.list()) {
            validateIfThereIsOperationForMaterials(pte, statuses);
        }
//        int i = 1;
//        for (String s : statuses) {
//            System.out.println(String.valueOf(i++) + " " + s);
//        }
        return statuses;
    }

    private void validateProductTemplatePrice(ProductTemplateEntity dte, List<String> errors) {
        if (dte.getType() == ProductTemplateType.SALE && dte.getSalesOffers() != null) {
            for (SalesOffer so : dte.getSalesOffers()) {
                MoneyERP pr = so.getPrice();
                if (pr == null) {
                    errors.add(productInfo(dte) + " nie ma uzupełnionej ceny");
                } else if (!validateProductTemplatePrice(pr).status()) {
                    errors.add(productInfo(dte) + " ma uzupełnoną niepoprawną cenę: " + pr);
                }
            }
        }
    }

    public void validateIfThereIsOperationForMaterials(ProductTemplateEntity detal, List<String> statuses) {

        //TODO
//        throw new NotImplementedException("validateIfThereIsOperationForMaterials");
//        if (detal.getPartCard() == null) {
//            statuses.add(detalInfo(detal) + " nie ma założonej listy materiałowej!!!!!!!!!!!!!!!!!");
//        } else {
//            final List<MaterialPart> materials = detal.getPartCard().getMaterialParts();
//            final List<WarePart> wares = detal.getPartCard().getWareParts();
//            for (ProductionPlan pp : detal.getProductionPlan()) {
//                if (pp.getOperationName() == OperationName.Cięcie) {
//                    final boolean isMaterial = isMaterialTypeMatch(materials, getTypesForCiecie());
//                    final boolean isWare = isWareTypeMatch(wares, typesForCiecie);
//                    if (!(isMaterial || isWare)) {
//                        statuses.add(detalInfo(detal) + " nie ma materiału ani towaru do Cięcia, a w marszrucie jest operacja \"Cięcie\"");
//                    }
//                } else if (pp.getOperationName() == OperationName.Wypalanie) {
//                    final boolean isMaterial = isMaterialTypeMatch(materials, getTypesForWypalanie());
//                    if (!isMaterial) {
//                        statuses.add(detalInfo(detal) + " nie ma materiału do Wypalania, a w marszrucie jest operacja \"Wypalanie\"");
//                    }
//                }
//            }
//        }
    }

    private boolean isWareTypeMatch(List<WarePart> wares, List<String> types) {
        for (WarePart w : wares) {
            if (types.contains(w.getWareTemplate().getWareType().getName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isMaterialTypeMatch(List<MaterialPart> materials, List<String> types) {
        for (MaterialPart m : materials) {
            if (types.contains(m.getMaterialTemplate().getMaterialType().getName())) {
                return true;
            }
        }
        return false;
    }

    private String productInfo(ProductTemplateEntity dte) {
        return "detal o numerze " + dte.getProductNo() + " i nazwie " + dte.getName();
    }

    private List<String> getTypesForWypalanie() {
        if (typesForWypalanie == null) {
            typesForWypalanie = Lists.newArrayList("Blachy");
        }
        return typesForWypalanie;
    }

    private List<String> getTypesForCiecie() {
        if (typesForCiecie == null) {
            typesForCiecie = Lists.newArrayList("Pręt gwintowany");
            typesForCiecie.add("Lina");
            typesForCiecie.add("nit");
            MaterialTypeEntitiesList materialtypes = materialTypeDAO.list();
            for (MaterialTypeEntity mt : materialtypes) {
                if (!mt.getName().equals("Blachy")) {
                    typesForCiecie.add(mt.getName());
                }
            }

        }
        return typesForCiecie;
    }
}
