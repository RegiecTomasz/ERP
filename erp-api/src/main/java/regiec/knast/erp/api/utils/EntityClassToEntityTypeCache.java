/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.util.HashMap;
import java.util.Map;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.entities.*;
import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class EntityClassToEntityTypeCache {

    private static final Map<EntityType, Class> CLASS_MAP = new HashMap();

    static {
        CLASS_MAP.put(EntityType.MATERIAL, MaterialEntity.class);
        CLASS_MAP.put(EntityType.MATERIAL_TEMPLATE, MaterialTemplateEntity.class);
        CLASS_MAP.put(EntityType.MATERIAL_TYPE, MaterialTypeEntity.class);
        CLASS_MAP.put(EntityType.WARE, WareEntity.class);
        CLASS_MAP.put(EntityType.WARE_TEMPLATE, WareTemplateEntity.class);
        CLASS_MAP.put(EntityType.WARE_TYPE, WareTypeEntity.class);
        CLASS_MAP.put(EntityType.PRODUCT, ProductEntity.class);
        CLASS_MAP.put(EntityType.PRODUCT_TEMPLATE, ProductTemplateEntity.class);
        CLASS_MAP.put(EntityType.PROVIDER, ProviderEntity.class);
        CLASS_MAP.put(EntityType.SELLER, SellerEntity.class);
        CLASS_MAP.put(EntityType.RECIPIENT, RecipientEntity.class);
        CLASS_MAP.put(EntityType.PRODUCTION_ORDER, ProductionOrderEntity.class);
        CLASS_MAP.put(EntityType.ORDER, OrderEntity.class);
        CLASS_MAP.put(EntityType.DELIVERY_ORDER, DeliveryOrderEntity.class);
        CLASS_MAP.put(EntityType.WORKER, WorkerEntity.class);
        CLASS_MAP.put(EntityType.MACHINE, MachineEntity.class);
        CLASS_MAP.put(EntityType.PRODUCTION_JOB, ProductionJobEntity.class);
        CLASS_MAP.put(EntityType.MATERIAL_DEMAND, MaterialDemandEntity.class);
        CLASS_MAP.put(EntityType.DEVELOPMENT, DevelopmentEntity.class);
        CLASS_MAP.put(EntityType.SHOPPING_CART, ShoppingCartEntity.class);
        CLASS_MAP.put(EntityType.TODO_ISSUES, TodoIssuesEntity.class);
        CLASS_MAP.put(EntityType.OPERATION_NAME, OperationNameEntity.class);
        CLASS_MAP.put(EntityType.PROFESSION_GROUP, ProfessionGroupEntity.class);
CLASS_MAP.put(EntityType.DOCUMENT, DocumentEntity.class);
//Here_will_be_insert_entityType_to_entityClass_mapping
    }

    public static Class get(EntityType type) {
        Class ret = CLASS_MAP.get(type);
        if (ret == null) {
            throw new ConversionError("There is no mapped entity type: " + type);
        }
        return ret;
    }

}
