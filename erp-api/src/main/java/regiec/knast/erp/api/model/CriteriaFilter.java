/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Setter
@lombok.Getter
@lombok.EqualsAndHashCode
public class CriteriaFilter {

    private AndOrEnum value = AndOrEnum.AND;
    private List<Constraint> constraints = new ArrayList();
    private List<CriteriaFilter> criteriaFilters = new ArrayList();
}
