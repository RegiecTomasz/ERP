/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.workers.WorkerDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.WorkerBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("workers")
public class WorkerEntity extends WorkerBase implements EntityBase, ConvertableEntity {

    @Id
    private String id;
    private String workerCode; // code is this.professionGroup.code + '-' + this.workerMinorNo
    @Reference
    private ProfessionGroupEntity professionGroup;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public WorkerDTO convertToDTO() {
        WorkerDTO ret = BeanUtilSilent.translateBeanAndReturn(this, new WorkerDTO());
        ret.setProfessionGroup(this.getProfessionGroup().convertToDTO());
        return ret;
    }

}
