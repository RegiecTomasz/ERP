/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.material;

import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.bases.MaterialBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class UpdateMaterialRequest extends MaterialBase {

    private String id;
    private String materialTemplateId;
    private CuttingSize length;
}
