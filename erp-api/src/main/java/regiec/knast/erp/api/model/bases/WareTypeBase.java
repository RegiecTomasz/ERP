/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.WareDimension;
import regiec.knast.erp.api.model.WareSizeType;
import regiec.knast.erp.api.model.states.WareTypeState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WareTypeBase extends ObjectWithDate {

    private String name;
    private WareDimension dimension;
    private WareSizeType sizeType;
    private WareTypeState state = WareTypeState.NORMAL;
    private boolean removed;
}
