/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.EqualsAndHashCode
@lombok.NoArgsConstructor
public final class SteelGrade implements Comparable<SteelGrade> {

    private String steelGrade;

    public SteelGrade(String steelGrade) {
        this.steelGrade = steelGrade;
    }

    @Override
    public String toString() {
        return steelGrade;
    }

    @Override
    public int compareTo(SteelGrade o) {
        return this.getSteelGrade().compareTo(o.getSteelGrade());
    }
}
