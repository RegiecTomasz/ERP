/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.MachineDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.machines.AddMachineRequest;
import regiec.knast.erp.api.dto.machines.MachineDTO;
import regiec.knast.erp.api.dto.machines.MachinesListDTO;
import regiec.knast.erp.api.dto.machines.RemoveMachineRequest;
import regiec.knast.erp.api.dto.machines.UpdateMachineRequest;
import regiec.knast.erp.api.entities.MachineEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.MachineState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MachineService extends AbstractService<MachineEntity, MachineDTO> {

    private static final EntityType MY_ENTITY_TYPE = EntityType.MACHINE;

    @Inject
    private MachineDAOInterface machinesDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public MachineDTO addMachine(AddMachineRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("machineCode", request.getMachineCode(), null, MY_ENTITY_TYPE).throww();

        MachineEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new MachineEntity());
        entity.setState(MachineState.WORKING);

//            entity.setMachineWorkers(ConvertersRegistry.convert(request.getMachineWorkers(), MachineWorkerList.class));
        MachineEntity entity1 = machinesDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public MachineDTO getMachine(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public MachineDTO updateMachine(UpdateMachineRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("machineCode", request.getMachineCode(), id, MY_ENTITY_TYPE).throww();
        MachineEntity entity = machinesDAO.get(id);
        validator.validateNotNull(entity).throww();
        MachineEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new MachineEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
//            updatedEntity.setMachineWorkers(ConvertersRegistry.convert(request.getMachineWorkers(), MachineWorkerList.class));
        machinesDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeMachine(RemoveMachineRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        MachineEntity machinesEntity = machinesDAO.get(id);
        if (request.getConfirmed()) {
            machinesEntity.setRemoved(true);
            machinesDAO.update(machinesEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(machinesEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public MachinesListDTO listMachines(PaginableFilterWithCriteriaRequest request) {
        return (MachinesListDTO) super.list(request);
    }
}
