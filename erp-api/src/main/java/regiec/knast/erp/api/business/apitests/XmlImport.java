/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import regiec.knast.erp.api.business.apitests.xmlimport.Kopia_x0020_Materiały_w_ZP;
import regiec.knast.erp.api.business.apitests.xmlimport.XMLMaterialRoot;
import regiec.knast.erp.api.dto.materialtemplates.AddMaterialTemplateRequest;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.utils.FlatMaterialImporter;
import regiec.knast.erp.api.utils.ProfileImporter;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class XmlImport extends GuiceInjector {

//    @Inject
//    private ImportService importService;

//    public static void main(String args[]) throws Exception {
//        GuiceInjector.init();
//        Object create = ObjectBuilderFactory.instance().create(XmlImport.class);
//        XmlImport thisClass = (XmlImport) create;
//        try {
//            thisClass.importProductTemplatesFromService();
//        } catch (Exception ex) {
//            Logger.getLogger(ListTest.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            thisClass.cleanUp();
//        }
//    }
//
//    private void importProductTemplatesFromService() throws Exception {
//        String path = "C:\\Users\\fbrzuzka\\Desktop\\erp jangra\\B_RYS";
//        String excelFilePath = "C:\\Users\\fbrzuzka\\Desktop\\erp jangra\\B_RYS\\productTemplate1.xlsx";
//        File excelFile = new File(excelFilePath);
//        if (excelFile.exists()) {
//            Uploadable techDrawing = ProductTemplateImporter.createUploadable(excelFile, "application/vnd.ms-excel");
//            ImportProductTemplatesRequest request = new ImportProductTemplatesRequest(techDrawing, "Homag", path);
//            importService.importProductTemplates(request);
//        } else {
//            System.out.println("Excel fle not exist");
//        }
//    }

    /*  private static void importProductTemplates() throws Exception {
        ProductTemplatesListDTO ret = new ProductTemplatesListDTO();
        String productTypeId = "58caf73f3d1a9721603f4f38";
        ProductTemplateImporter importer = new ProductTemplateImporter(productTypeId);

        String excelFileName = "productTemplate1.xlsx";
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(excelFileName);

        List<AddProductTemplateRequest> giveMeAddRequestsFromExcelFile = importer.giveMeAddRequestsFromExcelFile(inputStream, excelFileName, "application/vnd.ms-excel");
        for (AddProductTemplateRequest addd : giveMeAddRequestsFromExcelFile) {
            System.out.println("blblblb " + addd.getName());
            System.out.println("is there uploadable " + addd.getTechDrawing() != null);
            System.out.println(" ");
        }
    }*/

    private static void importProfiles() throws Exception {
        ProfileImporter importer = new ProfileImporter();
        List<AddMaterialTemplateRequest> giveMeProfiles = importer.giveMeProfiles();
        for (AddMaterialTemplateRequest giveMeProfile : giveMeProfiles) {
            System.out.println(giveMeProfile);
        }
    }

    private static void importFlat() throws Exception {
        FlatMaterialImporter i = new FlatMaterialImporter();
        XMLMaterialRoot materials = i.importFlat();
        List<MaterialTemplateEntity> o = i.parseFlat(materials);

        List<Kopia_x0020_Materiały_w_ZP> notNullNameAndCode = i.removeNullnameAndCode(materials.getKopia_x0020_Materiały_w_ZP());
        List<Kopia_x0020_Materiały_w_ZP> materialsUnique = i.removeDuplicates(notNullNameAndCode);
        XMLMaterialRoot root = new XMLMaterialRoot();
        root.setKopia_x0020_Materiały_w_ZP(materialsUnique);
        System.out.println("materialsUnique.size: " + materialsUnique.size());
        File file = new File("/opt/materialsUnique.xml");

        i.objectToXML(file, root);
    }

    private static void importMaterials() throws Exception {
        JAXBContext jc = JAXBContext.newInstance(XMLMaterialRoot.class);
        Unmarshaller u = jc.createUnmarshaller();
        //  XMLMaterialRoot manager = (XMLMaterialRoot) u.unmarshal(ClassLoader.getSystemResourceAsStream("m.xml"));
        XMLMaterialRoot materials = (XMLMaterialRoot) u.unmarshal(ClassLoader.getSystemResourceAsStream("materiały.xml"));
    }

    /**
     * Marshall information into an xml file.
     */
    private static void xmlToObject() throws Exception {
        JAXBContext jc = JAXBContext.newInstance(XMLMaterialRoot.class);
        Unmarshaller u = jc.createUnmarshaller();
        XMLMaterialRoot manager = (XMLMaterialRoot) u.unmarshal(ClassLoader.getSystemResourceAsStream("m.xml"));
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(g.toJson(manager));
    }

}
