/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.model.pguidegenerator;

import java.util.Calendar;
import java.util.function.Supplier;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProdGuideIdGen implements Supplier<String> {

    private int minorNo = 1;
    private long prodGuideNo;
    private String date;

    public ProdGuideIdGen(long prodGuideNo) {
        this.prodGuideNo = prodGuideNo;
        Calendar calendar = Calendar.getInstance();
        this.date = calendar.get(Calendar.YEAR) + "";
//        this.date = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
    }
    
    @Override
    public String get() {
        return prodGuideNo + "/" + date + "-"+(minorNo++);
    }


}
