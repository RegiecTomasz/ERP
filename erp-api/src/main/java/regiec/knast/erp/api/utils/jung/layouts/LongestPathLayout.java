/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils.jung.layouts;

import edu.uci.ics.jung.algorithms.layout.AbstractLayout;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.graph.Graph;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class LongestPathLayout<V, E> extends AbstractLayout<V, E> {

//    protected GraphElementAccessor<V, E> elementAccessor = new RadiusGraphElementAccessor<V, E>();
    private V root;

    /**
     * Creates an <code>ISOMLayout</code> instance for the specified graph
     * <code>g</code>.
     *
     * @param g
     */
    public LongestPathLayout(Graph<V, E> g, V root) {
        super(g);
        this.root = root;
    }

    @Override
    public double getY(V v) {
        return super.getY(v); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getX(V v) {
        return super.getX(v); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Point2D transform(V v) {
        return super.transform(v); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initialize() {

        int as = 0;
//        Collection<V> vertices = graph.getVertices();
        Collection<V> longestPathLength = getLongestPathLength(root); //        this.graph.
        double startX = size.width * 0.5;
        double startY = size.height * 0.2;
        double dy = 50;

        for (V v : longestPathLength) {
            startY += dy;
            System.out.println("v: " + v);
            super.setLocation(v, startX, startY);
        }

    }

    public Deque<V> getLongestPathLength(V node) {
        return getLongestPathLength(node, new ArrayList());
    }

    private Deque<V> getLongestPathLength(V node, List<V> visited) {
        Deque<V> ret = new LinkedList();
        if (node == null) {
            return ret;
        }
        ISOMLayout s;
        Collection<V> consequents = this.graph.getSuccessors(node);
        System.out.println("node: " + node + "  with children: " + consequents);
        for (V child : consequents) {
            System.out.println("child: " + child + " visited: " + visited);
            if (!visited.contains(child)) {
                visited.add(node);
                System.out.println("check child: " + child + " with visited " + visited + "\n");
                ret = longer(getLongestPathLength(child, visited), ret);
            } else {
                System.out.println("skipped visited " + child);
            }
        }

        ret.addFirst(node);
        System.out.println("added " + node + " to ret, so it is: " + ret);
        return ret;
    }

    private <T> Deque<T> longer(Deque<T> a, Deque<T> b) {
        return a.size() > b.size() ? a : b;
    }

    int getLongestPathLength2(V node) {
        if (node == null) {
            return 0;
        }
        int max = 0;
        for (V child : this.graph.getSuccessors(node)) {
            max = Math.max(getLongestPathLength2(child), max);
        }
        return 1 + max;
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
