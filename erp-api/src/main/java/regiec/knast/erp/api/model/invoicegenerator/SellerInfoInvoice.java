/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.invoicegenerator;

import com.google.common.base.Strings;
import regiec.knast.erp.api.model.wzgenerator.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
//@lombok.AllArgsConstructor
public class SellerInfoInvoice extends CompanyInfoBase {

    private String nip = "a";
    private String bankName = "a";
    private String bankAccountNo = "a";

    public SellerInfoInvoice(String nip, String bankName, String bankAccountNo, String name, String street, String postal, String city) {
        super(Strings.nullToEmpty(name), Strings.nullToEmpty(street), Strings.nullToEmpty(postal), Strings.nullToEmpty(city));
        this.nip = Strings.nullToEmpty(nip);
        this.bankName = Strings.nullToEmpty(bankName);
        this.bankAccountNo = Strings.nullToEmpty(bankAccountNo);
    }

}
