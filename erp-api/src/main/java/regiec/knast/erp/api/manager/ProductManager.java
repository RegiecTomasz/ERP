/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.util.Lists;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.RecipientDAOInterface;
import regiec.knast.erp.api.dto.product.FromCreatingOrderContext;
import regiec.knast.erp.api.dto.product.ProductListDTO;
import regiec.knast.erp.api.dto.semiproduct.FromCreatingProductionJobContext;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.exceptions.ErpStateValidateionException;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author fbrzuzka
 */
public class ProductManager {

    @Inject
    private Validator validator;
    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private ProductDAOInterface productDAO;
    @Inject
    private RecipientDAOInterface recipientDAO;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;

    public ProductListDTO finishProduct(String productId, int countToFinish) {
        ProductListDTO ret = new ProductListDTO(new ArrayList());
        ProductEntity product = productDAO.get(productId);
        validator.validateNotNull(product, "product").throww();
        List<ProductState> statesToFinish = Lists.newArrayList(ProductState.IN_PRODUCTION);
        if (!statesToFinish.contains(product.getState())) {
            throw new ErpStateValidateionException(ExceptionCodes.PRODUCT__CANNOT_FINISH_PRODUCT_IN_THIS_STATE, "this is not a state for finish product: " + product.getState());
        }
        if (product.getCount() > countToFinish) {
            ProductEntity productCopy = productDAO.get(productId);
            productCopy.setCount(productCopy.getCount() - countToFinish);
            ProductEntity copyDto = productDAO.add(productCopy, IDUtil.nextId_());
            
            ret.getElements().add(copyDto.convertToDTO());
            updateCountOnProduct(product, countToFinish, ret);
        } else if (product.getCount() == countToFinish) {
            updateCountOnProduct(product, countToFinish, ret);
        } else {
            throw new ERPInternalError("Wow, how can you go there??");
        }
        return ret;
    }

    private ProductEntity updateCountOnProduct(ProductEntity product, int countToFinish, ProductListDTO ret) {
        product.setCount(countToFinish);
        product.setState(ProductState.FINISHED);
        product = productDAO.update(product);
        ret.getElements().add(product.convertToDTO());
        return product;
    }

    //-----------------------------------------------------------------
    public ProductEntity createToOrder(FromCreatingOrderContext from) {

        validator.validateNotNull(from).throww();
        validator.validateNotNull(from.getOrderNo()).throww();
        validator.validateNotNull(from.getRecipientId()).throww();
        validator.validateNotNull(from.getProductTemplateId()).throww();
        validatorDetal.validateCount(from.getCount(), ExceptionCodes.PRODUCT__COUNT_NOT_VALID).throww();

        ProductEntity productEntity = BeanUtilSilent.translateBeanAndReturn(from, new ProductEntity());

        ProductTemplateEntity productTemplate = productTemplateDAO.get(from.getProductTemplateId());
        validator.validateNotNull(productTemplate).throww();

        RecipientEntity recipientEntity = recipientDAO.get(from.getRecipientId());
        validator.validateNotNull(recipientEntity).throww();

        productEntity.setProductTemplate(productTemplate);
        productEntity.setRecipient(recipientEntity);
        productEntity.setState(ProductState.ORDERED);
        productEntity.setDeadline(from.getDeliveryDate());
        productEntity.setPositionOnOrder(from.getPositionOnOrder());
        productEntity.validateNewFromOrder(from.getFromFrameworkOrder());

        return productDAO.add(productEntity);

    }

    public ProductEntity createToProductionJob(FromCreatingProductionJobContext from) {

        validator.validateNotNull(from).throww();
        validator.validateNotNull(from.getProductTemplateId()).throww();
        validator.validateNotNull(from.getOrderNo()).throww();
        validator.validateNotNull(from.getOrderId()).throww();
        validator.validateNotNull(from.getProductionOrderId()).throww();
        validator.validateNotNull(from.getProductionOrderNo()).throww();
        validatorDetal.validateCount(from.getCount(), ExceptionCodes.PRODUCT__COUNT_NOT_VALID).throww();

        ProductEntity entity = new ProductEntity();

        BeanUtilSilent.translateBean(from, entity);

        validator.validateNotNull(from.getProductTemplateId()).throww();
        ProductTemplateEntity productTemplate = productTemplateDAO.get(from.getProductTemplateId());
        validator.validateNotNull(productTemplate).throww();
        entity.setProductTemplate(productTemplate);
        ProductEntity entity1 = productDAO.add(entity);

        return entity1;
    }

}
