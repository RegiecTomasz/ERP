/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.deliveryorder;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import org.jaxygen.annotations.APIBrowserIgnoreSetter;
import org.seenmodit.core.interfaces.Position;
import org.seenmodit.core.interfaces.UpdatePosition;
import regiec.knast.erp.api.dto.shoppingCart.ShoppingCartPosition;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.utils.IDUtil;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@lombok.Builder
@lombok.EqualsAndHashCode
public class DeliveryOrderPosition implements Position, UpdatePosition {

    private String positionId;
    private String materialTemplateId;
    private String materialName;
    private String materialCode;
    private String materialNorm;
    private String materialTolerance;
    private String materialSteelGrade;
    private Dimension materialBaseDimension;
    private String materialBaseDimensionUnit;
    private String materialParticularDimention;
    private Integer count;
    private List<String> materialsIds = new ArrayList();
    private String assortmentId;

    private CuttingSize salesLength;
//    @org.mongodb.morphia.annotations.ConstructorArgs(value = {"amount" , "currency" })
    private MoneyERP salesPrice;

    private boolean confirmed = false;

    public static DeliveryOrderPosition fromShoppingCartPosition(ShoppingCartPosition shoppingCartPosition, MaterialTemplateEntity materialTemplate) {
        DeliveryOrderPosition ret = new DeliveryOrderPosition();
        ret.setPositionId(IDUtil.nextId_());
        ret.setMaterialTemplateId(shoppingCartPosition.getMaterialTemplateId());
        ret.setMaterialName(shoppingCartPosition.getMaterialName());
        ret.setMaterialCode(shoppingCartPosition.getMaterialCode());
        ret.setCount(shoppingCartPosition.getCount());
        ret.setMaterialsIds(Lists.newArrayList(shoppingCartPosition.getMaterialOnStockId()));
        ret.setAssortmentId(shoppingCartPosition.getAssortmentId());
        ret.setSalesLength(shoppingCartPosition.getSalesLength());
        ret.setSalesPrice(shoppingCartPosition.getSalesPrice());
        
        ret.setMaterialNorm(shoppingCartPosition.getMaterialNorm());
        ret.setMaterialTolerance(shoppingCartPosition.getMaterialTolerance());
        ret.setMaterialSteelGrade(shoppingCartPosition.getMaterialSteelGrade());

        ret.setMaterialBaseDimension(materialTemplate.getMaterialType().getBaseDimensionTemplate());
        ret.setMaterialBaseDimensionUnit(materialTemplate.getMaterialType().getBaseDimensionUnit());
        ret.setMaterialParticularDimention(materialTemplate.getParticularDimension());
        return ret;
    }

    public DeliveryOrderPosition(String positionId, String assortmentId, Integer count) {
        this.positionId = positionId;
        this.count = count;
        this.assortmentId = assortmentId;
    }

    @APIBrowserIgnoreSetter
    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    @APIBrowserIgnoreSetter
    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    @APIBrowserIgnoreSetter
    public void setMaterialsIds(List<String> materialsIds) {
        this.materialsIds = materialsIds;
    }

    @Override
    public String getPositionId() {
        return positionId;
    }

    @APIBrowserIgnoreSetter
    @Override
    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }
}
