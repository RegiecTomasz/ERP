/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.states.WorkerState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WorkerBase extends ObjectWithDate {

    private String firstName;
    private String secondName;
    private String workerMinorNo;
    private WorkerState state;
    private String comment;
    private boolean removed;

}
