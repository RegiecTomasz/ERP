/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.productionplan;

import regiec.knast.erp.api.dto.operationName.OperationNameDTO;
import regiec.knast.erp.api.dto.professionGroup.ProfessionGroupDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProductionPlanDTO extends ProductionPlanBase {

    private OperationNameDTO operationName;
    private ProfessionGroupDTO professionGroup;
    private String positionId;
}
