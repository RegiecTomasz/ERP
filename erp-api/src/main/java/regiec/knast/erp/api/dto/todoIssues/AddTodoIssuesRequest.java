/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.todoIssues;

import regiec.knast.erp.api.model.bases.TodoIssuesBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class AddTodoIssuesRequest extends TodoIssuesBase {

}
