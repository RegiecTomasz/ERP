/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import regiec.knast.erp.api.dto.producttemplates.partcard.AddWarePart;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter 
@lombok.NoArgsConstructor 
@lombok.AllArgsConstructor
public class AddProductTemplateWarePartRequest {

    private String productTemplateId;    
    private AddWarePart warePart;            
}
