/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.containers.MaterialEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MaterialDAO extends DAOBase<MaterialEntity, MaterialEntitiesList> implements MaterialDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = MaterialEntity.class;
    @Inject
    public MaterialDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
