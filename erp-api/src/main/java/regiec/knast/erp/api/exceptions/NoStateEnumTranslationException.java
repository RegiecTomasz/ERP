/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import org.jaxygen.typeconverter.exceptions.ConversionError;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class NoStateEnumTranslationException extends ConversionError {

    public NoStateEnumTranslationException(Class clazz) {
        super("there is some undefined translations for " + clazz.getCanonicalName());
    }

}
