/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.mongodb.gridfs.GridFSDBFile;
import java.io.IOException;
import org.jaxygen.dto.Downloadable;
import org.jaxygen.dto.Uploadable;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.interfaces.ResourceInfoDAOInterface;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.producttemplates.AddProductTemplateRequest;
import regiec.knast.erp.api.dto.resourceinfo.AddResourceInfoRequest;
import regiec.knast.erp.api.dto.resourceinfo.ResourceInfoDTO;
import regiec.knast.erp.api.dto.resourceinfo.UpdateResourceInfoRequest;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.ResourceInfoEntity;
import regiec.knast.erp.api.model.wzgenerator.DownloadableStream;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.StringUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
public class ResourceManager {

    @Inject
    private Validator validator;
    @Inject
    private ResourceInfoDAOInterface resourceInfoDAO;

    public ResourceInfoEntity addResource(Uploadable uploadable, String entityId) {
        if (uploadable != null) {
            try {
                AddResourceInfoRequest addResourceInfoRequest = new AddResourceInfoRequest();
                addResourceInfoRequest.setInputStream(uploadable.getInputStream());
                addResourceInfoRequest.setResourceName(StringUtil.extToLowerCase(uploadable.getOriginalName()));
                addResourceInfoRequest.setLoobackId(entityId);

                ResourceInfoEntity resourceInfoEntity = addResourceInfo(addResourceInfoRequest);
                return resourceInfoEntity;
            } catch (IOException ex) {
                throw new ConversionError("error", ex);
            }
        }
        return null;
    }

    public ResourceInfoEntity addResourceInfo(AddResourceInfoRequest request) {
        String resourceId = resourceInfoDAO.addFile(request.getInputStream(), request.getResourceName());

        ResourceInfoEntity resourceInfoEntity = new ResourceInfoEntity();
        resourceInfoEntity.setResourceId(resourceId);
        resourceInfoEntity.setResourceName(request.getResourceName());
        resourceInfoEntity.setLoobackId(request.getLoobackId());
        resourceInfoDAO.add(resourceInfoEntity);
        return resourceInfoEntity;
    }

    public ResourceInfoDTO updateResourceInfo(UpdateResourceInfoRequest request) {
        String id = request.getId();
        ResourceInfoEntity entity = resourceInfoDAO.get(id);
        ResourceInfoEntity updatedEntity = new ResourceInfoEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        ResourceInfoEntity updated = resourceInfoDAO.update(updatedEntity);
        return updated.convertToDTO();
    }

    public RemoveResponse removeResourceInfoIfIdNotNull(ResourceInfoEntity resourceinfo, boolean removeFile) {
        if (resourceinfo != null) {
            return removeResourceInfo(resourceinfo.getId(), removeFile);
        }
        return new RemoveResponse(false, null);
    }

    public RemoveResponse removeResourceInfo(String id, boolean removeFile) {
        validator.validateNotNullAndNotEmpty(id, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "id");
        if (removeFile) {
            ResourceInfoEntity resourceinfoEntity = resourceInfoDAO.get(id);
            validator.validateNotNull(resourceinfoEntity, ExceptionCodes.NULL_VALUE, "resourceinfoEntity");
            resourceInfoDAO.removeFile(resourceinfoEntity.getResourceId());
        }
        int removed = resourceInfoDAO.remove(id);
        Boolean success = removed == 1;
        return new RemoveResponse(success, null);
    }

    public Downloadable getResourceById(String id, boolean forceApplicationDataContentType) {
        validator.validateNotNullAndNotEmpty(id, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "id");

        GridFSDBFile gridFile = resourceInfoDAO.getFileById(id);
        DownloadableStream downloadableStream = new DownloadableStream(gridFile.getInputStream(), gridFile.getFilename());
        if (forceApplicationDataContentType) {
            downloadableStream.setContentType("application/data");
        }
        return downloadableStream;
    }

    public GridFSDBFile getResourceStreamById(String id) {
        validator.validateNotNullAndNotEmpty(id, ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "id");
        GridFSDBFile gridFile = resourceInfoDAO.getFileById(id);
        return gridFile;
    }

    public <REQUEST extends AddProductTemplateRequest> void updateResources(ProductTemplateEntity updatedEntity, REQUEST request) {
        if (Strings.isNullOrEmpty(updatedEntity.getId())) {
            throw new ConversionErrorWithCode("There is no Id in updatedEntity", ExceptionCodes.INTERNAL_ERROR);
        }
        ResourceInfoEntity techDrawingPDF = addResource(request.getTechDrawingPDF(), updatedEntity.getId());
        ResourceInfoEntity techDrawingDXF = addResource(request.getTechDrawingDXF(), updatedEntity.getId());
        ResourceInfoEntity reportTemplate = addResource(request.getReportTemplate(), updatedEntity.getId());
        if (techDrawingPDF != null) {
            removeResourceInfoIfIdNotNull(updatedEntity.getTechDrawingPDF(), true);
            updatedEntity.setTechDrawingPDF(techDrawingPDF);
        }
        if (techDrawingDXF != null) {
            removeResourceInfoIfIdNotNull(updatedEntity.getTechDrawingDXF(), true);
            updatedEntity.setTechDrawingDXF(techDrawingDXF);
        }
        if (reportTemplate != null) {
            removeResourceInfoIfIdNotNull(updatedEntity.getReportTemplate(), true);
            updatedEntity.setReportTemplate(reportTemplate);
        }
    }
}
