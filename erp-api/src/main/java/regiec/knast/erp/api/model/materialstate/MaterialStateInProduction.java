/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.materialstate;

import regiec.knast.erp.api.model.states.MaterialState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class MaterialStateInProduction extends StateBase {

    public MaterialStateInProduction() {
        super(MaterialState.IN_PRODUCTION);
    }

    private String location;
}
