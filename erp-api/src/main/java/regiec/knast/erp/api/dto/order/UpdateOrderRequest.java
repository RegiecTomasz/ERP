/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.order;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dto.orderedproducts.UpdateOrderPosition;
import regiec.knast.erp.api.model.bases.OrderBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class UpdateOrderRequest extends OrderBase {

    private String id;
    private List<UpdateOrderPosition> orderPositions = new ArrayList();
    private String testCasesNumbers;
    private String recipientId;
    
}
