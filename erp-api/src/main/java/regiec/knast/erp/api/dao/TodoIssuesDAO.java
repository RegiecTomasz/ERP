/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.TodoIssuesDAOInterface;
import regiec.knast.erp.api.entities.TodoIssuesEntity;
import regiec.knast.erp.api.entities.containers.TodoIssuesEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class TodoIssuesDAO extends DAOBase<TodoIssuesEntity, TodoIssuesEntitiesList> implements TodoIssuesDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = TodoIssuesEntity.class;

    @Inject
    public TodoIssuesDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
