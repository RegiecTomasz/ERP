/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.recipient;

import regiec.knast.erp.api.model.bases.RecipientBase;
import regiec.knast.erp.api.model.states.RecipientState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.NoArgsConstructor
public class AddRecipientRequest extends RecipientBase {

    public AddRecipientRequest(String name, String nip, String street, String postalCode, String city, String email, String phone, int daysForPayment, RecipientState state, String comments) {
        super(name, nip, street, postalCode, city, email, phone, daysForPayment, state, comments, false);
    }

    
}
