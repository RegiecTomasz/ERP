/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.workers;

import regiec.knast.erp.api.dto.professionGroup.ProfessionGroupDTO;
import regiec.knast.erp.api.model.bases.WorkerBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WorkerDTO extends WorkerBase {

    private String id;
    private String workerCode; // code is this.professionGroup.code + '-' + this.workerMinorNo
    private ProfessionGroupDTO professionGroup;
}
