/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dto.otherstuff.ProgramConstsEntity;
import regiec.knast.erp.api.model.ConstType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProgramConstsDAOInterface {

    ProgramConstsEntity addConst(ConstType type, String value) throws ConversionError;

    ProgramConstsEntity getConsts();

}
