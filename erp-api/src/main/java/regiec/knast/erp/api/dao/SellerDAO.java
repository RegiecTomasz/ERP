/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.SellerDAOInterface;
import regiec.knast.erp.api.entities.SellerEntity;
import regiec.knast.erp.api.entities.containers.SellerEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class SellerDAO extends DAOBase<SellerEntity, SellerEntitiesList> implements SellerDAOInterface {

    private final Datastore datastore;

    @Inject
    public SellerDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
