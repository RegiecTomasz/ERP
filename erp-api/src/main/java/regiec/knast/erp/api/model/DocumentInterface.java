
package regiec.knast.erp.api.model;

import regiec.knast.erp.api.model.invoicegenerator.InvoiceData;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@org.jaxygen.annotations.HasImplementation(implementations = InvoiceData.class)
public interface DocumentInterface {
    DocumentType getType();
    
    public static enum DocumentType{
        INVOICE, 
        WZ
    }
}
