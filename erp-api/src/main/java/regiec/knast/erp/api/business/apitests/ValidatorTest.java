/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.inject.Inject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ValidatorTest extends GuiceInjector {

    @Inject
    private ValidatorDetal validatorDetal;

    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(ValidatorTest.class);
        ValidatorTest test = (ValidatorTest) create;
        try {
            test.validateAllProductTempaltes();
            //test.validateSemi("592ea2565c448d08cc60b095");
        } catch (Exception ex) {
            Logger.getLogger(ValidatorTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            test.cleanUp();
        }
    }

    public void validateAllProductTempaltes() {
        List<ProductTemplateEntity> l = productTemplateDAO.list();
        for (ProductTemplateEntity pte : l) {
            validate(pte);
        }
    }
    

    public void validateSemi(String productTemplateId) {
        List<String> o = validatorDetal.validateProductTemplate(productTemplateId);
        if (!o.isEmpty()) {
            System.out.println("\n\n " + productTemplateId);
        }
        for (String string : o) {
            System.out.println(string);
        }
    }

    private void validate(ProductTemplateEntity pte) {
        List<String> o = validatorDetal.validateProductTemplate(pte);
        if (!o.isEmpty()) {
            System.out.println("\n\n " + pte.getName());
        }
        for (String string : o) {
            System.out.println(string);
        }
    }
}
