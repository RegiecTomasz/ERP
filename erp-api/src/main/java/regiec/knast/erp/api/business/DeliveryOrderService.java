/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.seenmodit.core.PositionManager;
import org.seenmodit.core.exceptions.model.PositionsChangesRet;
import regiec.knast.erp.api.dao.interfaces.DeliveryOrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProviderDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.deliveryorder.ConfirmDeliveryOrderPositionRequest;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderDTO;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrdersListDTO;
import regiec.knast.erp.api.dto.deliveryorder.MoveDeliveryOrderPositionToOtherProviderRequest;
import regiec.knast.erp.api.dto.deliveryorder.UpdateDeliveryOrderRequest;
import regiec.knast.erp.api.entities.DeliveryOrderEntity;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.manager.orders.DeliveryOrderPositionsManager;
import regiec.knast.erp.api.manager.orders.DeliveryOrderPositionsUtil;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDeliveryOrder;

/** 
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class DeliveryOrderService extends AbstractService<DeliveryOrderEntity, DeliveryOrderDTO> {

    @Inject
    private DeliveryOrderDAOInterface deliveryOrderDAO;
    @Inject
    private ProviderDAOInterface providerDAO;
    @Inject
    private MaterialDAOInterface materialDAO;
    @Inject
    private DeliveryOrderPositionsManager deliveryOrderPositionsManager;
    @Inject
    private DeliveryOrderPositionsUtil deliveryOrderPositionsUtil;
    @Inject
    private Validator validator;
    @Inject
    private ValidatorDeliveryOrder validatorDeliveryOrder;

    private PositionManager positionManager = PositionManager.create(new IDUtil());

    @NetAPI
    public DeliveryOrderDTO getDeliveryOrder(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public DeliveryOrderDTO updateDeliveryOrder(UpdateDeliveryOrderRequest request) {
        try {
            validator.validateNotNull(request).throww();

            String id = request.getId();
            validator.validateNotNull(id).throww();
            DeliveryOrderEntity entity = deliveryOrderDAO.get(id);
            validator.validateNotNull(entity).throww();

            validator.validateNotNull(request.getProviderId()).throww();
            ProviderEntity provider = providerDAO.get(request.getProviderId());
            validator.validateNotNull(provider).throww();

            validatorDeliveryOrder.validatePositions(request.getPositions());

            deliveryOrderPositionsUtil.ensurePositionsAreNotNull(request);
            deliveryOrderPositionsUtil.ensurePositionsAreNotNull(entity);

            DeliveryOrderEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new DeliveryOrderEntity());
            BeanUtilSilent.translateBean(request, updatedEntity);

            // we revert positions due to special update
            updatedEntity.setPositions(entity.getPositions());

            PositionsChangesRet<DeliveryOrderPosition, DeliveryOrderPosition> findAddedRemovedUpdated
                    = positionManager.findAddedRemovedUpdated(entity.getPositions(), request.getPositions());

            deliveryOrderPositionsManager.handleAdded(findAddedRemovedUpdated.getAdded(), updatedEntity);
            deliveryOrderPositionsManager.handleRemoved(findAddedRemovedUpdated.getRemoved(), updatedEntity);
            deliveryOrderPositionsManager.handleOther(findAddedRemovedUpdated.getOther(), updatedEntity);

            deliveryOrderDAO.update(updatedEntity);

            return (DeliveryOrderDTO) updatedEntity.convertToDTO();
        } catch (Exception ex) {
            throw new ConversionError("", ex);
        }
    }

    @NetAPI
    public DeliveryOrdersListDTO listDeliveryOrders(PaginableFilterWithCriteriaRequest request) {
        return (DeliveryOrdersListDTO) super.list(request);
    }

    @NetAPI
    public DeliveryOrderDTO confirmDeliveryOrderPosition(ConfirmDeliveryOrderPositionRequest request) {
        DeliveryOrderDTO ret = deliveryOrderPositionsManager.confirmDeliveryOrderPosition(request.getDeliveryOrderId(), request.getPositionId());
        return ret;
    }

    @NetAPI
    public DeliveryOrderDTO moveDeliveryOrderPositionToOtherProvider(MoveDeliveryOrderPositionToOtherProviderRequest request) {
        DeliveryOrderDTO ret = deliveryOrderPositionsManager.moveDeliveryOrderPositionToOtherProvider(request);
        return ret;
    }

}
