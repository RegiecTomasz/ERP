/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.gson.Gson;
import org.jaxygen.converters.json.JSONBuilderRegistry;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.ContextInitializer;
import regiec.knast.erp.api.GuiceBuilder;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class GuiceInjector {

    public Gson g;


    public void cleanUp() {
        ContextInitializer.getInstance().contextDestroyed();
    }

    protected GuiceInjector() {
        ObjectBuilderFactory.configure(GuiceBuilder.getInstance());
        ContextInitializer.getInstance().contextInitialized();
        g = JSONBuilderRegistry.getBuilder().createBuilder().setPrettyPrinting().create();
    }

    protected final <T extends Object> T buildObj(Class<T> clazz) {
        T obj;
        try {
            obj = ObjectBuilderFactory.instance().create(clazz);
        } catch (ObjectCreateError ex) {
            throw new ConversionError("blabllblab");
        }
        return obj;
    }
    
    public void printResult(Object someObject){
        printObject("RESULT", someObject);
    }
    
    public void printRequest(Object someObject){
        printObject("REQUEST", someObject);
    }
    
    public void printObject(String label, Object someObject){
        String toJson = g.toJson(someObject);
        System.out.println("---=== "+label+" ===--- \n" + toJson);
    }

}
