/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.containers.OrderEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class OrderDAO extends DAOBase<OrderEntity, OrderEntitiesList> implements OrderDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = OrderEntity.class;

    @Inject
    public OrderDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
