/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.states.SellerState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class SellerBase extends ObjectWithDate {

    private String name;
    private String nip;
    private String postalCode;
    private String city;
    private String street;
    private String phone;
    private String email;
    private String regon;
    private String swift;
    private String bankNamePLN;
    private String bankAccountPLN;
    private String bankNameEUR;
    private String bankAccountEUR;
    private SellerState state;
    private String comments;
    private boolean removed;
}
