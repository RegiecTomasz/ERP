/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import com.google.common.collect.ImmutableMap;
import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTO;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTOBasisc;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.interfaces.RemovableEntity;
import regiec.knast.erp.api.model.bases.MaterialTemplateBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("materialTemplates")
public class MaterialTemplateEntity extends MaterialTemplateBase implements EntityBase, ConvertableEntity, RemovableEntity  {

    @Id
    private String id;

    private MaterialTypeEntity materialType;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public MaterialTemplateDTO convertToDTO() {
             MaterialTemplateDTO dTO =   BeanUtilSilent.translateBeanAndReturn(this, new MaterialTemplateDTO());
            if (this.getMaterialType() != null) {
                dTO.setMaterialType((MaterialTypeDTO)this.getMaterialType().convertToDTO());
                dTO.setMaterialTypeId(dTO.getMaterialType().getId());
            }
            return dTO;        
    }

    public MaterialTemplateDTOBasisc toMaterialTemplateDTObasisc() {
        MaterialTemplateDTOBasisc mtDTObasisc = BeanUtilSilent.translateBeanAndReturn(this, new MaterialTemplateDTOBasisc());
        mtDTObasisc.setMaterialType(ImmutableMap.of(
                "name", this.getMaterialType().getName(),
                "baseDimensionUnit", this.getMaterialType().getBaseDimensionUnit(),
                "cuttingDimensionUnit", this.getMaterialType().getCuttingDimensionUnit()
        ));
        mtDTObasisc.setNorm(this.getNorm() != null ? this.getNorm().getNorm() : null);
        mtDTObasisc.setTolerance(this.getTolerance() != null ? this.getTolerance().getTolerance() : null);
        mtDTObasisc.setSteelGrade(this.getSteelGrade()!= null ? this.getSteelGrade().getSteelGrade() : null);
        return mtDTObasisc;
    }
}
