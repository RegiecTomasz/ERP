/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

import java.util.ArrayList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class Projection extends ArrayList<Projection.ProjecttionOne> {

    public static Projection include(String... fields) {
        Projection ret = new Projection();
        for (String field : fields) {
            ret.add(new ProjecttionOne(field, true));
        }
        return ret;
    }

    public static Projection exclude(String... fields) {
        Projection ret = new Projection();
        for (String field : fields) {
            ret.add(new ProjecttionOne(field, false));
        }
        return ret;
    }

    @lombok.Getter
    @lombok.Setter
    @lombok.NoArgsConstructor
    @lombok.AllArgsConstructor
    public static class ProjecttionOne {

        private String field;
        private boolean include;
    }

    public void validateMe() {

        boolean firstIncludeOrDeclude;
        if (!this.isEmpty()) {
            firstIncludeOrDeclude = this.get(0).getInclude();
            for (ProjecttionOne p : this) {
                if(p.getInclude() != firstIncludeOrDeclude){
                    throw new InternalError("You cannot mix include and declude in projection.");
                }
            }
        }
    }
}
