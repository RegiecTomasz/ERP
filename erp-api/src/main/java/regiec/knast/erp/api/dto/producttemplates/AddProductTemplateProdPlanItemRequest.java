/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import regiec.knast.erp.api.dto.producttemplates.productionplan.AddProductionPlan;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
public class AddProductTemplateProdPlanItemRequest {

    private String productTemplateId;    
    private AddProductionPlan prodPlanItem;            

    public AddProductTemplateProdPlanItemRequest(String productTemplateId, AddProductionPlan prodPlanItem) {
        this.productTemplateId = productTemplateId;
        this.prodPlanItem = prodPlanItem;
    }
    
}
