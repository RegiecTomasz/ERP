/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.EqualsAndHashCode
@lombok.NoArgsConstructor
public final class Tolerance implements Comparable<Tolerance> {

    private String tolerance;

    public Tolerance(String tolerance) {
        this.tolerance = tolerance;
    }

    @Override
    public String toString() {
        return tolerance;
    }

    @Override
    public int compareTo(Tolerance o) {
        return this.getTolerance().compareTo(o.getTolerance());
    }
}
