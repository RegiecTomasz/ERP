/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.assertj.core.util.Lists;
import org.assertj.core.util.Strings;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.seenmodit.core.PositionManager;
import org.seenmodit.core.exceptions.model.Pair;
import org.seenmodit.core.exceptions.model.PositionsChangesRet;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.RecipientDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.order.*;
import regiec.knast.erp.api.dto.orderedproducts.*;
import regiec.knast.erp.api.dto.product.FromCreatingOrderContext;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.entities.containers.OrderEntitiesList;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.exceptions.ErpStateValidateionException;
import regiec.knast.erp.api.manager.OrderManager;
import regiec.knast.erp.api.manager.ProductManager;
import regiec.knast.erp.api.manager.orders.GenerateProductionOrderTransformer;
import regiec.knast.erp.api.model.*;
import regiec.knast.erp.api.model.states.OrderState;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class OrderService extends AbstractService<OrderEntity, OrderDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.ORDER;

    @Inject
    private OrderDAOInterface orderDAO;
    @Inject
    private RecipientDAOInterface recipientDAO;
    @Inject
    private ProductDAOInterface productDAO;
    @Inject
    private ProductManager productManager;
    @Inject
    private Validator validator;
    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private GenerateProductionOrderTransformer generateProductionOrderTransformer;
    @Inject
    private OrderManager orderManager;

    private PositionManager positionManager = PositionManager.create(new IDUtil());

    @NetAPI
    public OrderDTO addOrder(AddOrderRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNull(request.getRecipientId()).throww();
        validator.validateUniqueness("nr", request.getNr(), null, MY_ENTITY_TYPE).throww();
        validateOrderPositions(request.getOrderPositions());
        OrderEntity order = BeanUtilSilent.translateBeanAndReturn(request, new OrderEntity());
        RecipientEntity recipientEntity = recipientDAO.get(request.getRecipientId());
        validator.validateNotNull(recipientEntity).throww();
        order.setRecipient(recipientEntity);
        order.setOrderPositions(addOrderPositions(request.getOrderPositions(), request));
        if (order.getFrameworkContractOrder()) {
            order.setState(OrderState.NEW_F);
            for (OrderPosition orderPosition : order.getOrderPositions()) {
                ((DispositionOrderPosition) orderPosition).initToDispose();
            }
        } else {
            order.setState(OrderState.NEW);
        }
        OrderEntity entity1 = orderDAO.add(order);
        return entity1.convertToDTO();
    }

    private List<OrderPosition> addOrderPositions(final ArrayList<AddOrderPosition> addOrderedPositions, AddOrderRequest addOrderRequest) {
        final List<OrderPosition> orderPositions = new ArrayList();
        if (addOrderedPositions != null) {
            for (final AddOrderPosition addOrderedPosition : addOrderedPositions) {
                final OrderPosition orderPosition = createOrderPositionAndProductFromRequest(addOrderedPosition,
                        addOrderRequest.getNr(), addOrderRequest.getRecipientId(), addOrderRequest.getFrameworkContractOrder());
                orderPositions.add(orderPosition);
            }
        }
        return orderPositions;
    }

    @NetAPI
    public OrderDTO getOrder(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public OrderDTO updateOrder(UpdateOrderRequest request) {
        try {
            // validate and prepare
            validator.validateNotNull(request).throww();
            String id = request.getId();
            validator.validateNotNull(id).throww();
            validator.validateUniqueness("nr", request.getNr(), id, MY_ENTITY_TYPE).throww();

            validateOrderPositions(request.getOrderPositions());
            OrderEntity entity = orderDAO.get(id);
            validator.validateNotNull(entity).throww();
            validateUpdatedOrderState(entity.getState());
            OrderEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new OrderEntity(entity));

            // change recipient only if there is no positions and order is NEW
            if (!Strings.isNullOrEmpty(request.getRecipientId()) && !entity.getRecipient().getId().equals(request.getRecipientId())) {
                boolean hasNewState = entity.getState() == OrderState.NEW || entity.getState() == OrderState.NEW_F;
                boolean hasNoPositions = entity.getOrderPositions().isEmpty() && request.getOrderPositions().isEmpty();
                if (hasNewState && hasNoPositions) {
                    RecipientEntity recipientEntity = recipientDAO.get(request.getRecipientId());
                    validator.validateNotNull(recipientEntity).throww();
                    updatedEntity.setRecipient(recipientEntity);
                } else {
                    throw new ConversionErrorWithCode("You cannot change recipient if order is not NEW of NEW_F and it has some order positions", ExceptionCodes.ORDER__CAN_CHANGE_RECIPIENT_IF_NEW_AND_EMPTY_POSITIONS);
                }
            }

            //update part
            PositionsChangesRet<OrderPosition, UpdateOrderPosition> positionsChanges
                    = positionManager.findAddedRemovedUpdated(entity.getOrderPositions(), request.getOrderPositions());
            boolean isUpdatePossible = checkIfStatesAreGoodForUpdate(positionsChanges);
            if (!isUpdatePossible) {
                throw new ConversionErrorWithCode("Cannot update positions is some are in production", ExceptionCodes.ORDER_POSITION__CANNOT_UPDATE_IF_NOT_IN_ORDERED_STATE);
            }
            List<OrderPosition> newOrderPositions = updateOrderPositions(positionsChanges, entity);
            BeanUtilSilent.translateBean(request, updatedEntity);
            updatedEntity.setOrderPositions(newOrderPositions);
            orderDAO.update(updatedEntity);
            return updatedEntity.convertToDTO();
        } catch (Exception ex) {
            throw new ConversionError("", ex);
        }
    }

    private void validateUpdatedOrderState(OrderState state) {
        switch (state) {
            case NEW:
                break;
            case PLANNED:
                break;
            case NEW_F:
                break;
            case PARTLY_DISPOSED_F:
                break;
            default: {
                throw new ErpStateValidateionException(ExceptionCodes.UPDATE_ORDER_ONLY_IN__NEW_OR_PLANNED_OR_PARTLY_DISPOSED__STATE, "Cannot update order in state: " + state, null);
            }
        }
    }

    private boolean checkIfStatesAreGoodForUpdate(final PositionsChangesRet<OrderPosition, UpdateOrderPosition> positionsChanges) {
        List<UpdateOrderPosition> a = positionsChanges.getAdded();
        List<OrderPosition> r = positionsChanges.getRemoved();
        List<Pair<OrderPosition, UpdateOrderPosition>> o = positionsChanges.getOther();
        List<ProductState> statesForUpdateOrderPosition = Lists.newArrayList(ProductState.ORDERED);
        for (Pair<OrderPosition, UpdateOrderPosition> pair : o) {
            OrderPosition old = pair.getFirst();
            UpdateOrderPosition upd = pair.getSecond();
            ProductState productState = old.getProduct().getState();
            if (old.getProduct().getRemoved() == true || !statesForUpdateOrderPosition.contains(productState)) {
                return false;
            }
        }
        for (OrderPosition orderPosition : r) {
            ProductState productState = orderPosition.getProduct().getState();
            if (orderPosition.getProduct().getRemoved() == true || !statesForUpdateOrderPosition.contains(productState)) {
                return false;
            }
        }
        return true;
    }

    private List<OrderPosition> updateOrderPositions(final PositionsChangesRet<OrderPosition, UpdateOrderPosition> changed, final OrderEntity orderEntity) {

        String orderNumber = orderEntity.getNr();
        String recipientId = orderEntity.getRecipient().getId();

        final List<OrderPosition> newEntityOrderPositions = new ArrayList<>();

        List<UpdateOrderPosition> a = changed.getAdded();
        List<OrderPosition> r = changed.getRemoved();
        List<Pair<OrderPosition, UpdateOrderPosition>> o = changed.getOther();

        for (UpdateOrderPosition added : a) {
            final OrderPosition orderPosition = createOrderPositionAndProductFromRequest(added, orderNumber, recipientId, orderEntity.getFrameworkContractOrder());
            if (orderEntity.getState() == OrderState.PLANNED) {
                GenerateProductionOrderResponse response = generateProductionOrderTransformer.generateOneProductionOrder(orderPosition, orderEntity);
            }
            if (orderEntity.getFrameworkContractOrder()) {
                ((DispositionOrderPosition) orderPosition).initToDispose();
            }
            newEntityOrderPositions.add(orderPosition);
        }

        for (OrderPosition rem : r) {
            makeProduct_REMOVED_FROM_ORDER(rem.getProduct());
        }

        for (Pair<OrderPosition, UpdateOrderPosition> otherPair : o) {
            OrderPosition old = otherPair.getFirst();
            UpdateOrderPosition upd = otherPair.getSecond();

            if (old == null) {
                //dodano nową pozycję do zamówienia, coś jest nie tak, nie powinno tu wejść
                throw new ConversionError("coś jest nie tak, nie powinno tu wejśc");
            }
            if (!Objects.equals(upd.getProductTemplateId(), old.getProduct().getProductTemplate().getId())) {
                //edytowano productTemplateId - nigdy nie powinno się zdażyć
                throw new ConversionError("coś jest nie tak, nie powinno tu wejśc");
            }
            //Zmieniam te małoważne pola
            old.setPositionOnOrder(upd.getPositionOnOrder());
            old.setDiscount(upd.getDiscount() != null ? upd.getDiscount() : 0.0);
            old.setPiecePrice(upd.getPiecePrice());
            old.setPiecePriceWithDiscount(upd.getPiecePriceWithDiscount());
            old.setComments(upd.getComments());
            old.setDeliveryDate(upd.getDeliveryDate());

            if (!Objects.equals(upd.getCount(), old.getCount())) {
                //edytowano ilośc produktów na zamówieniu
                ProductEntity productEntity = productDAO.getProduct(old.getProduct().getId());
                if (productEntity.getState() == ProductState.ORDERED) {
                    productEntity.setCount(upd.getCount());
                    productDAO.update(productEntity);
                    old.setCount(upd.getCount());
                    newEntityOrderPositions.add(old);
                } else {
                    throw new ConversionError("you cannot edit position on order with product in prograss: " + productEntity.getState());
                }
            } else {
                // nie wykryto zmian zednych waznych pól. zwracam tę pozycję na zamowieniu.
                newEntityOrderPositions.add(old);
            }
            if (orderEntity.getFrameworkContractOrder()) {
                ((DispositionOrderPosition) old).refreshToDispose();
                ((DispositionOrderPosition) old).validateDispositionCounts();
            }
        }

        return newEntityOrderPositions;
    }

    private void makeProduct_REMOVED_FROM_ORDER(ProductEntity product) {
        ProductEntity product1 = productDAO.getProduct(product.getId());
        product1.setState(ProductState.REMOVED_FROM_ORDER);
        productDAO.update(product1);
    }

    private <T extends AddOrderPosition> OrderPosition createOrderPositionAndProductFromRequest(T orderPositionToAdd, String orderNumber, String recipientId, boolean isFramework) {
        final OrderPosition orderPosition = BeanUtilSilent.translateBeanAndReturn(orderPositionToAdd, new OrderPosition());

        String productionOrderId = IDUtil.nextId_();
        orderPosition.setDiscount(orderPositionToAdd.getDiscount() != null ? orderPositionToAdd.getDiscount() : 0.0);
        orderPosition.setOrderPositionId(productionOrderId);
        final FromCreatingOrderContext productCreatingContext = FromCreatingOrderContext.builder()
                .fromFrameworkOrder(isFramework)
                .count(orderPosition.getCount())
                .orderNo(orderNumber)
                .recipientId(recipientId)
                .productTemplateId(orderPositionToAdd.getProductTemplateId())
                .positionOnOrder(orderPositionToAdd.getPositionOnOrder())
                .deliveryDate(orderPositionToAdd.getDeliveryDate())
                .build();

        ProductEntity productEntity = productManager.createToOrder(productCreatingContext);
        orderPosition.setProduct(productEntity);
        return orderPosition;
    }

    private void validateOrderPositions(final List<? extends AddOrderPosition> addOrderedPositions) {
        if (addOrderedPositions != null) {
            for (AddOrderPosition addOrderedPosition : addOrderedPositions) {
                validator.validateNotNull(addOrderedPosition.getProductTemplateId(), ExceptionCodes.ORDER_POSITION__PRODUCT_TEMPLATE_ID_NOT_SPECIFIED).throww();
                validator.validateNotNullAndNotEmpty(addOrderedPosition.getPositionOnOrder(), ExceptionCodes.ORDER_POSITION__POSITION_ON_ORDER_NOT_SPECIFIED).throww();
                validator.validateNotNull(addOrderedPosition.getCount(), ExceptionCodes.ORDER_POSITION__COUNT_NOT_SPECIFIED).throww();
                validator.validateNotNull(addOrderedPosition.getDiscount(), ExceptionCodes.ORDER_POSITION__DISCOUNT_NOT_SPECIFIED).throww();
                validator.validateNotNull(addOrderedPosition.getPiecePrice(), ExceptionCodes.ORDER_POSITION__PRICE_PER_ITEM_NOT_SPECIFIED).throww();
                validator.validateNotNull(addOrderedPosition.getPiecePriceWithDiscount(), ExceptionCodes.ORDER_POSITION__PRICE_PER_ITEM_NOT_SPECIFIED).throww();
                validatorDetal.validateCount(addOrderedPosition.getCount(), ExceptionCodes.PRODUCT__COUNT_NOT_VALID).throww();
                validatorDetal.validateProductDiscount(addOrderedPosition.getDiscount()).throww();
                validatorDetal.validateProductTemplatePrice(addOrderedPosition.getPiecePrice()).throww();
                validatorDetal.validateProductTemplatePrice(addOrderedPosition.getPiecePriceWithDiscount()).throww();
            }
        }
    }

    private List<Integer> casesStringToIntegers(String testCasesNumbers) {
        List<Integer> ret = new ArrayList();
        String[] split = testCasesNumbers.split(";");
        for (String string : split) {
            ret.add(Integer.valueOf(string));
        }
        return ret;
    }

    @NetAPI
    public RemoveResponse removeOrder(RemoveOrderRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        OrderEntity orderEntity = orderDAO.get(id);
        if (request.getConfirmed()) {
            orderEntity.setRemoved(true);
            orderDAO.update(orderEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(OrderEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public OrdersListDTO listOrders(PaginableFilterWithCriteriaRequest request) {
        return (OrdersListDTO) super.list(request);
    }

    @NetAPI
    public Double productDiscountAutoFilled(ProductDiscountAutoFilledRequest request) {
        validator.validateNotNullAndNotEmpty(request.getRecipientId()).throww();
        validator.validateNotNull(request.getSalesOffers()).throww();
        if (request.getOrderPositionId() != null) {
            // assume that this is edit of order position, we return actual discount
            OrderPosition orderPosition = findOrderPositionByPositionId(request.getOrderPositionId());
            return orderPosition.getDiscount();
        } else {
            SalesOffer thatSo = findSalesOffer(request.getRecipientId(), request.getSalesOffers());
            final Double discount = thatSo.getDiscount();
            validatorDetal.validateProductDiscount(discount).throww();
            return discount;
        }
    }

    @NetAPI
    public MoneyERP productPriceAutoFilled(ProductPriceAutoFilledRequest request) {
        validator.validateNotNullAndNotEmpty(request.getRecipientId()).throww();
        validator.validateNotNull(request.getSalesOffers()).throww();
        if (request.getOrderPositionId() != null) {
            // assume that this is edit of order position, we return actual price
            OrderPosition orderPosition = findOrderPositionByPositionId(request.getOrderPositionId());
            return orderPosition.getPiecePrice();
        } else {
            SalesOffer thatSo = findSalesOffer(request.getRecipientId(), request.getSalesOffers());
            MoneyERP price = thatSo.getPrice();
            validatorDetal.validateProductTemplatePrice(price).throww();
            return price;
        }
    }

    @NetAPI
    public MoneyERP productPriceWithDiscountAutoFilled(OrderPositionPiecePriceWithDiscountRequest request) {
        validator.validateNotNull(request.getDiscount()).throww();
        validator.validateNotNull(request.getPiecePrice()).throww();
        double discountPercent = 100 - request.getDiscount();
        MoneyERP multiply = request.getPiecePrice().multiply(discountPercent / 100);
        return multiply;
//        return Math.round(p * 100.0) / 100.0;
    }

    @NetAPI
    public String calculateOrderPositionPrice(CalculateOrderPositionPriceRequest request) {
        validator.validateNotNull(request.getCount()).throww();
        validator.validateNotNull(request.getDiscount()).throww();
        validator.validateNotNull(request.getPiecePrice()).throww();
        double discountPercent = 100 - request.getDiscount();
        MoneyERP result = request.getPiecePrice().multiply(discountPercent / 100).multiply(request.getCount());
//        double p = request.getPiecePrice() * request.getCount() * discountPercent / 100;
//        double result = Math.round(p * 100.0) / 100.0;
        return request.getPiecePrice().getNumber() + " x " + request.getCount() + " x " + discountPercent + "% = " + result.getNumber();
    }

    @NetAPI
    public Integer calculateToDispose(CalculateToDisposeRequest request) {
        validator.validateNotNull(request.getCount()).throww();
        if (request.getOrderPositionId() != null) {
            // assume that this is edit of order position, we return actual value
            OrderPosition orderPosition = findOrderPositionByPositionId(request.getOrderPositionId());
            int disposed = orderPosition.getDisposedCount();
            return request.getCount() - disposed;
        } else {
            return request.getCount();
        }
    }

    private SalesOffer findSalesOffer(String recipientId, List<SalesOffer> salesOffers) {
        RecipientEntity recipient = recipientDAO.get(recipientId);
        validator.validateNotNull(recipient, "recipient").throww();
        if (salesOffers == null || salesOffers.isEmpty()) {
            throw new ConversionErrorWithCode("Product template has no sales offers.",
                    ExceptionCodes.PRODUCT_TEMPLATE__SALE_OFFER_IS_EMPTY);
        }
        return salesOffers.stream()
                .filter(Objects::nonNull)
                .filter(so -> so.getRecipient().getId().equals(recipientId))
                .findAny()
                .orElseThrow(() -> new ConversionErrorWithCode("There is no sales for this productTemplate for this recipient.",
                ExceptionCodes.PRODUCT_TEMPLATE__NO_SALES_OFFER_FOR_RECIPIENT, ImmutableMap.of("recipientName", recipient.getName())));
    }

    private OrderPosition findOrderPositionByPositionId(String orderPositionId) {
        OrderEntitiesList orderEntitiesList = orderDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("orderPositions.orderPositionId", orderPositionId, ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        return orderEntitiesList.stream()
                .flatMap(o -> o.getOrderPositions().stream())
                .filter(pos -> pos.getPositionId().equals(orderPositionId))
                .findAny()
                .orElseThrow(() -> new ERPInternalError("There is no Order with positionId like you give: " + orderPositionId));
    }

//    @NetAPI
//    public GenerateSTHResponse cancelOrder(CancelOrderRequest request) {
//        return orderManager.cancelOrder(request);
//    }
//
//    @NetAPI
//    public GenerateSTHResponse cancelAllOrders() {
//        return orderManager.cancelAllOrders();
//    }
//
//    @NetAPI
//    public GenerateSTHResponse generateAllNewOrders() {
//        return orderManager.generateAllNewOrders();
//    }
//
//    @NetAPI
//    public GenerateSTHResponse generateAllNewOrdersProdOrdAndProdJobs() {
//        return orderManager.generateAllNewOrdersProdOrdAndProdJobs();
//    }
//
//    @NetAPI
//    public GenerateSTHResponse regenerateAll() {
//        List li = new ArrayList();
//        GenerateSTHResponse c = cancelAllOrders();
//        GenerateSTHResponse g = generateAllNewOrdersProdOrdAndProdJobs();
//        li.add(c.getMessage());
//        li.add(g.getMessage());
//        return new GenerateSTHResponse(Boolean.TRUE, li);
//    }
//
//    @NetAPI
//    public GenerateSTHResponse generateRandomOrders() throws ObjectCreateError {
//        OrderGenerator.main(null);
//        return new GenerateSTHResponse(Boolean.TRUE, "ok");
//    }
}
