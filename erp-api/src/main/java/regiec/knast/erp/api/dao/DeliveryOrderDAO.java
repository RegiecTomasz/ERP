/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.DeliveryOrderDAOInterface;
import regiec.knast.erp.api.entities.DeliveryOrderEntity;
import regiec.knast.erp.api.entities.containers.DeliveryOrderEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class DeliveryOrderDAO extends DAOBase<DeliveryOrderEntity, DeliveryOrderEntitiesList> implements DeliveryOrderDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = DeliveryOrderEntity.class;

    @Inject
    public DeliveryOrderDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

    @Override
    public Boolean removeFromDB(String deliveryOrderId) {
        WriteResult a = datastore.delete(DeliveryOrderEntity.class, deliveryOrderId);        
        return a.getN() == 1;
    }
}
