/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Map;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.WareTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WareTypeDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.waretemplates.RemoveWareTemplateRequest;
import regiec.knast.erp.api.dto.waretypes.*;
import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.entities.WareTypeEntity;
import regiec.knast.erp.api.entities.containers.WareTemplateEntitiesList;
import regiec.knast.erp.api.manager.WareTemplateManager;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CriteriaFilter;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class WareTypeService extends AbstractService<WareTypeEntity, WareTypeDTO> {

    @Inject
    private WareTypeDAOInterface wareTypeDAO;
    @Inject
    private WareTemplateDAOInterface wareTemplateDAO;
    @Inject
    private WareTemplateManager wareTemplateManager;
    @Inject
    private WareTemplateService wareTemplateService;
    @Inject
    private Validator validator;

    @NetAPI
    public WareTypeDTO addWareType(AddWareTypeRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getName()).throww();
//                        validator.validateNotNullAndNotEmpty(request.getSizeType()).throww();
        WareTypeEntity entity = new WareTypeEntity();
        BeanUtilSilent.translateBean(request, entity);
        WareTypeEntity entity1 = wareTypeDAO.add(entity);
        return entity1.convertToDTO(); 
    }

    @NetAPI
    public WareTypeDTO getWareType(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public WareTypeDTO updateWareType(UpdateWareTypeRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getName()).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        WareTypeEntity entity = wareTypeDAO.get(id);
        WareTypeEntity updatedEntity = new WareTypeEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        wareTypeDAO.update(updatedEntity);
        return updatedEntity.convertToDTO(); 
    }

    @NetAPI
    public RemoveResponse removeWareType(RemoveWareTypeRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        WareTypeEntity wareTypeEntity = wareTypeDAO.get(id);
        if (request.getConfirmed()) {
            wareTypeEntity.setRemoved(true);
            wareTypeDAO.update(wareTypeEntity);
            removeWareTemplates(wareTypeEntity);
            return new RemoveResponse(true, null);
        } else {
            //build consequencesMap
            Map<String, Object> consequencesMap = buildRemoveConsequencesMap(wareTypeEntity);
            return new RemoveResponse(false, consequencesMap);
        }
    }

    private void removeWareTemplates(WareTypeEntity updatedEntity) {
        WareTemplateEntitiesList wareTemplateEntitiesList = getWareTemplatesWithWareType(updatedEntity.getId());
        RemoveWareTemplateRequest removeWareTemplateRequest = new RemoveWareTemplateRequest("", true, true);
        for (WareTemplateEntity wareTemplateEntity : wareTemplateEntitiesList) {
            removeWareTemplateRequest.setId(wareTemplateEntity.getId());
            wareTemplateService.removeWareTemplate(removeWareTemplateRequest);
        }
    }

    public WareTemplateEntitiesList getWareTemplatesWithWareType(String wareTypeEntityId) {
        PaginableFilterWithCriteriaRequest req = new PaginableFilterWithCriteriaRequest();
        CriteriaFilter criteriaFilters = new CriteriaFilter();
        req.setCriteriaFilters(criteriaFilters);
        criteriaFilters.setConstraints(Lists.newArrayList(new Constraint("wareType.id", wareTypeEntityId, ConstraintType.EQUALS)));
        return wareTemplateDAO.list(req);
    }

    public Map<String, Object> buildRemoveConsequencesMap(WareTypeEntity entity) {
        Map<String, Object> consequencesMap = ImmutableMap.<String, Object>builder()
                .put("entityType", entity.getClass().getSimpleName())
                .put("id", entity.getId())
                .put("name", entity.getName())
                .put("dimension", entity.getDimension())
                .put("leafs", new ArrayList<>())
                .build();
        final WareTemplateEntitiesList wareTemplateEntities = getWareTemplatesWithWareType(entity.getId());
        for (WareTemplateEntity wareTemplateEntity : wareTemplateEntities) {
            Map<String, Object> wareTemplateConsequencesMap = wareTemplateManager.buildRemoveConsequencesMap(wareTemplateEntity);
            ((ArrayList) consequencesMap.get("leafs")).add(wareTemplateConsequencesMap);
        }
        return consequencesMap;
    }

    @NetAPI
    public WareTypesListDTO listWareTypes(PaginableFilterWithCriteriaRequest request) {
        return (WareTypesListDTO) super.list(request);
    }
}
