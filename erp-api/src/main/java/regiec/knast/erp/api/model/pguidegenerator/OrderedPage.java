/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.model.pguidegenerator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class OrderedPage {
    
    private String productTempalteId;
    private boolean productGuide;
    private boolean draft;
    
    public boolean drawThisTemplate(String producTemplateId){
        return this.productTempalteId.equals(producTemplateId) && productGuide;
    }
}
