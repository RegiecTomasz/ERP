package regiec.knast.erp.api.utils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.Arrays;
import regiec.knast.erp.api.ContextInitializer;
import regiec.knast.erp.api.exceptions.ERPInternalError;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ErpPrinter {

    public static void printResult(Object someObject) {
        printObject(someObject, "RESULT");
    }

    public static void printRequest(Object someObject) {
        printObject(someObject, "REQUEST");
    }

    public static void printObject(Object someObject, String label) {
        someObject = someObject != null ? someObject : "null";
        String toJson = ContextInitializer.getInstance().GSON().toJson(someObject);
        System.out.println("---=== " + label + " ===--- \n" + toJson);
    }

    @lombok.NoArgsConstructor
    public static class Stopwatch {

        private String title;
        private long start;
        private long stop;

        public Stopwatch(String title) {
            this.title = title;
            start = System.currentTimeMillis();
        }
        public Stopwatch stop(){
            stop = System.currentTimeMillis();
            return this;
        }
        
        public void stopAndPrint(){
            stop = System.currentTimeMillis();
            printInSeconds();
        }
        public void printInSeconds(){
            System.out.println("Timer '" + title+ "' time: "  + (new Double(stop - start))/1000);   
        }        

    }

    @lombok.NoArgsConstructor
    public static class TablePrinter {

        private List<List<String>> rows = new ArrayList();
        private Integer columnsCount = null;
        private String title;

        public TablePrinter(String title) {
            this.title = title;
        }

        public void addRow(String... cells) {
            if (columnsCount == null) {
                columnsCount = cells.length;
            }
            if (!columnsCount.equals(cells.length)) {
                throw new ERPInternalError("Wrong number of cells in table printer. You put " + cells.length + " but you should put " + columnsCount);
            }
            this.rows.add(Lists.newArrayList(cells));
        }

        public void print() {
            if (!Strings.isNullOrEmpty(title)) {
                System.out.println("\n-----------===== " + title + " =====-----------");
            }
            int[] maxLengths = obtainColumnsMaxLengths();
            for (int i = 0; i < rows.size(); i++) {
                List<String> row = rows.get(i);
                String line = "";
                for (int col = 0; col < columnsCount; col++) {
                    String cell = row.get(col);
                    line += StringUtils.rightPad(cell, maxLengths[col]) + "   ";
                }
                System.out.println(line);
            }
            System.out.println("");

        }

        private int[] obtainColumnsMaxLengths() {
            int[] maxLengths = new int[columnsCount];
            Arrays.fill(maxLengths, 0);
            for (int col = 0; col < columnsCount; col++) {
                for (int i = 0; i < rows.size(); i++) {
                    List<String> row = rows.get(i);
                    String cell = row.get(col);
                    if (cell.length() > maxLengths[col]) {
                        maxLengths[col] = cell.length();
                    }
                }
            }
            return maxLengths;
        }

    }
}
