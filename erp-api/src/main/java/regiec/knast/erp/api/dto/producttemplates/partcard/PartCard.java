/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class PartCard {

    private List<MaterialPart> materialParts = new ArrayList();
    private List<WarePart> wareParts = new ArrayList();
    private List<SemiproductPart> semiproductParts = new ArrayList();

    public PartCard(PartCard partCard) {
        if (partCard.getMaterialParts() != null) {
            this.materialParts = new ArrayList(partCard.getMaterialParts());
        }
        if (partCard.getWareParts() != null) {
            this.wareParts = new ArrayList(partCard.getWareParts());
        }
        if (partCard.getSemiproductParts() != null) {
            this.semiproductParts = new ArrayList(partCard.getSemiproductParts());
        }
    }

    public PartCardDTO convertToDto() {
        PartCardDTO dTO = new PartCardDTO();
        dTO.setMaterialParts(MaterialPart.materialPartsListToDto(this.getMaterialParts()));
        dTO.setWareParts(WarePart.warePartsListToDto(this.getWareParts()));
        dTO.setSemiproductParts(SemiproductPart.semiproductPartsListToDto(this.getSemiproductParts()));
        return dTO;
    }

}
