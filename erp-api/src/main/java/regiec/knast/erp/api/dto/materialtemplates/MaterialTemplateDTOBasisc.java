/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.dto.materialtemplates;

import java.util.Map;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialTemplateDTOBasisc {
 
    private Map<String, Object> materialType;
    private String id;
    private String materialCode;
    private String particularDimension;
    private String norm;
    private String tolerance;
    private String steelGrade;
    private String comments;
}
