/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.WareTypeDAOInterface;
import regiec.knast.erp.api.entities.WareTypeEntity;
import regiec.knast.erp.api.entities.containers.WareTypeEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WareTypeDAO extends DAOBase<WareTypeEntity, WareTypeEntitiesList> implements WareTypeDAOInterface {

    private final Datastore datastore;
    private final Class entityClass = WareTypeEntity.class;

    @Inject
    public WareTypeDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }
}
