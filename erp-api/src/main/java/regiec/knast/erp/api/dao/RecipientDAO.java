/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.RecipientDAOInterface;
import regiec.knast.erp.api.entities.RecipientEntity;
import regiec.knast.erp.api.entities.containers.RecipientEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class RecipientDAO extends DAOBase<RecipientEntity, RecipientEntitiesList> implements RecipientDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = RecipientEntity.class;

    @Inject
    public RecipientDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
