/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import regiec.knast.erp.api.dao.interfaces.OrderDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.order.CancelOrderRequest;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderRequest;
import regiec.knast.erp.api.dto.order.GenerateProductionOrderResponse;
import regiec.knast.erp.api.dto.order.GenerateSTHResponse;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.entities.containers.OrderEntitiesList;
import regiec.knast.erp.api.entities.containers.ProductionOrderEntitiesList;
import regiec.knast.erp.api.manager.orders.GenerateProductionOrderTransformer;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.states.OrderState;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
public class OrderManager {

    private static int i = 0;
    @Inject
    private Validator validator;
    @Inject
    private OrderDAOInterface orderDAO;
    @Inject
    private ProductionOrderDAOInterface productionOrderDAO;
    @Inject
    private ProductionOrderManager productionOrderManager;
    @Inject
    private GenerateProductionOrderTransformer generateProductionOrderTransformer;

    public GenerateSTHResponse cancelOrder(CancelOrderRequest from) {
        validator.validateNotNull(from).throww();
        validator.validateNotNull(from.getOrderId()).throww();
        OrderEntity order = orderDAO.get(from.getOrderId());
        validator.validateNotNull(order).throww();

        if (null == order.getState()) {
            return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "Not implemented yet!").build());
        } else {
            switch (order.getState()) {
                case NEW:
                    return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "nothing to do").build());
                case PLANNED:
                    ProductionOrderEntitiesList productionOrders = productionOrderDAO.list(PaginableFilterWithCriteriaRequest.builder().
                            addCriteriaFilterBuilder().addConstraint("orderId", order.getId(), ConstraintType.EQUALS).buildCriteriaFilter()
                            .build());

                    List l = new ArrayList();
                    l.add("canceled order " + order.getNr());
                    for (ProductionOrderEntity productionOrder : productionOrders) {
                        GenerateSTHResponse resp = productionOrderManager.removeForSureProductionOrder(productionOrder);
                        if (resp.getMessage() instanceof Map) {
                            l.add(((Map) resp.getMessage()).get("message"));
                        } else {
                            l.add(resp.getMessage());
                        }
                    }
                    order.setState(OrderState.NEW);
                    orderDAO.update(order);
                    ImmutableMap.builder().put("message", l).build();
                    return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", l).build());
                default:
                    return new GenerateSTHResponse(Boolean.TRUE, ImmutableMap.builder().put("message", "Not implemented yet!").build());
            }
        }
    }

    public GenerateSTHResponse generateAllNewOrdersProdOrdAndProdJobs() {
        generateAllNewOrders();
        productionOrderManager.generateAllNewProductionOrders();
        return new GenerateSTHResponse(Boolean.TRUE, "Probably many prodOrders and jobs created...");
    }

    public GenerateSTHResponse generateAllNewOrders() {
        GenerateSTHResponse ret = new GenerateSTHResponse();
        ret.setStatus(Boolean.TRUE);
        List mesList = new ArrayList();
        ret.setMessage(mesList);

        OrderEntitiesList li = orderDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("state", "NORMAL", ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        for (OrderEntity oe : li) {
            GenerateProductionOrderResponse m = generateProductionOrderTransformer.generateProductionOrder(new GenerateProductionOrderRequest(oe.getId()));
            mesList.add(m.getMessage());
        }
        return ret;
    }

    public GenerateSTHResponse cancelAllOrders() {
        GenerateSTHResponse ret = new GenerateSTHResponse();
        ret.setStatus(Boolean.TRUE);
        List mesList = new ArrayList();
        ret.setMessage(mesList);

        OrderEntitiesList li = orderDAO.list(PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("state", "PLANNED", ConstraintType.EQUALS)
                .buildCriteriaFilter()
                .build());
        for (OrderEntity oe : li) {
            GenerateSTHResponse m = cancelOrder(new CancelOrderRequest(oe.getId()));
            mesList.add(m.getMessage());
        }
        return ret;
    }

}
