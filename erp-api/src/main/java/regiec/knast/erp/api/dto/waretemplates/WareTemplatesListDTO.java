/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.waretemplates;

import org.jaxygen.dto.collections.PaginableListResponseBaseDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class WareTemplatesListDTO extends PaginableListResponseBaseDTO<WareTemplateDTO> {

}
