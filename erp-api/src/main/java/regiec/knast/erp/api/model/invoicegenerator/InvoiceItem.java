/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.invoicegenerator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class InvoiceItem {

    private Integer no = 0;
    private String productNo = "a";
    private String name = "a";
    private Integer count = 10;
    private String unit = "a";
    private String unitPrice = "10";
    private String valueNetto = "100";
    private String vat = "23";
    private String valueBrutto = "123";
    private String orderNo = "a";
    private String position = "a";

}
