/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.semiproduct;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.Builder
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class FromCreatingProductionJobContext {

    private String productTemplateId;
    private Integer count;
    private String productionOrderNo;
    private String productionOrderId;
    private String orderNo;
    private String orderId;    
}
