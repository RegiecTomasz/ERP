/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.professionGroup;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.model.bases.ProfessionGroupBase;
import regiec.knast.erp.api.model.states.ProfessionGroupState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddProfessionGroupRequest extends ProfessionGroupBase {

    public AddProfessionGroupRequest(List<String> allowOperationsIds, String code, String name, String label, ProfessionGroupState state) {
        super(code, name, label, state, false);
        this.allowOperationsIds = allowOperationsIds;
    }

    private List<String> allowOperationsIds = new ArrayList();
    
}
