/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.manager.crud;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import java.util.stream.IntStream;
import regiec.knast.erp.api.dao.interfaces.DAOBaseInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WareTemplateDAOInterface;
import regiec.knast.erp.api.dto.producttemplates.partcard.AddMaterialPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.AddSemiproductPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.AddWarePart;
import regiec.knast.erp.api.dto.producttemplates.partcard.MaterialPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.MaterialPartDTO;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPartDTO;
import regiec.knast.erp.api.dto.producttemplates.partcard.UpdateMaterialPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.UpdateSemiproductPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.UpdateWarePart;
import regiec.knast.erp.api.dto.producttemplates.partcard.WarePart;
import regiec.knast.erp.api.dto.producttemplates.partcard.WarePartDTO;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.exceptions.NotImplementedException;
import regiec.knast.erp.api.manager.DetalTemplateManager;
import regiec.knast.erp.api.manager.MaterialTemplateManager;
import regiec.knast.erp.api.manager.WareTemplateManager;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.DaoHandler;
import regiec.knast.erp.api.utils.IDUtil;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class PartCardCRUD {

    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private DetalTemplateManager detalTemplateManager;
    @Inject
    private MaterialTemplateManager materialTemplateManager;
    @Inject
    private WareTemplateManager wareTemplateManager;
    @Inject
    private WareTemplateDAOInterface wareTemplateDAO;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private Validator validator;
    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private DaoHandler daoHandler;

    public List<MaterialPartDTO> addMaterialPartToDetalTempalte(ProductTemplateEntity dte, EntityType entityType, AddMaterialPart amp) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);

        validator.validateNotNull(amp).throww();
        validator.validateNotNull(amp.getMaterialTemplateId()).throww();
        MaterialPart materialPart = BeanUtilSilent.translateBeanAndReturn(amp, new MaterialPart());
        materialPart.setPositionId(IDUtil.nextId_());
        MaterialTemplateEntity materialTemplateEntity = materialTemplateDAO.get(amp.getMaterialTemplateId());
        validator.validateNotNull(materialTemplateEntity).throww();
        materialPart.setMaterialTemplate(materialTemplateEntity);

        Dimension dim = materialTemplateEntity.getMaterialType().getCuttingDimensionTemplate();
        validatorDetal.validateCount(materialPart.getCount(), ExceptionCodes.MATERIAL__COUNT_NOT_VALID).throww();
        validatorDetal.validateCuttedSectionSize(dim, materialPart.getCuttedSectionSize(), ExceptionCodes.MATERIAL__COUNT_NOT_VALID).throww();
        validatorDetal.validateCuttedSectionSize(dim, materialPart.getCuttedSectionSizeWithBorders(), ExceptionCodes.MATERIAL__COUNT_NOT_VALID).throww();
        validatorDetal.validateIsCuttingSizeSmallerOrEqualThanCuttingSizeWithBorders(materialPart).throww();

        detalTemplateManager.ensurePartCartListExist(dte);
        dte.getPartCard().getMaterialParts().add(materialPart);
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        materialTemplateManager.refreshUsage(amp.getMaterialTemplateId());
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return MaterialPart.materialPartsListToDto(updated2.getPartCard().getMaterialParts());
    }

    public List<WarePartDTO> addWarePartToDetalTempalte(ProductTemplateEntity dte, EntityType entityType, AddWarePart awp) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        validator.validateNotNull(awp).throww();

        WarePart warePart = BeanUtilSilent.translateBeanAndReturn(awp, new WarePart());
        warePart.setPositionId(IDUtil.nextId_());

        validator.validateNotNull(awp.getWareTemplateId()).throww();
        WareTemplateEntity wareTemplateEntity = wareTemplateDAO.get(awp.getWareTemplateId());
        validator.validateNotNull(wareTemplateEntity).throww();
        warePart.setWareTemplate(wareTemplateEntity);
        validatorDetal.validateCount(warePart.getCount(), ExceptionCodes.WARE__COUNT_NOT_VALID).throww();

        detalTemplateManager.ensurePartCartListExist(dte);
        dte.getPartCard().getWareParts().add(warePart);
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        wareTemplateManager.refreshUsage(awp.getWareTemplateId());
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return WarePart.warePartsListToDto(updated2.getPartCard().getWareParts());
    }

    public List<SemiproductPartDTO> addSemiproductPartToDetalTempalte(ProductTemplateEntity dte, EntityType entityType, AddSemiproductPart awp) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        validator.validateNotNull(awp).throww();

        SemiproductPart semiproductPart = BeanUtilSilent.translateBeanAndReturn(awp, new SemiproductPart());
        semiproductPart.setPositionId(IDUtil.nextId_());

        validator.validateNotNull(awp.getProductTemplateId()).throww();
        ProductTemplateEntity productTemplateEntity = productTemplateDAO.get(awp.getProductTemplateId());
        validator.validateNotNull(productTemplateEntity).throww();
        semiproductPart.setProductTemplate(productTemplateEntity);

        validatorDetal.validateCount(semiproductPart.getCount(), ExceptionCodes.SEMIPRODUCT__COUNT_NOT_VALID).throww();

        detalTemplateManager.ensurePartCartListExist(dte);
        dte.getPartCard().getSemiproductParts().add(semiproductPart);
        validatorDetal.validateLoopInSemiProductPartCartRecursively(semiproductPart, dte).throww();

        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        detalTemplateManager.refreshUsage(awp.getProductTemplateId());
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return SemiproductPart.semiproductPartsListToDto(updated2.getPartCard().getSemiproductParts());
    }

    public List<MaterialPartDTO> updateMaterialPartInDetalTempalte(ProductTemplateEntity dte, EntityType entityType, UpdateMaterialPart ump) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        detalTemplateManager.ensurePartCartListExist(dte);

        List<MaterialPart> mParts = dte.getPartCard().getMaterialParts();
        String posId = ump.getPositionId();
        int oldItemIndex = IntStream.range(0, mParts.size())
                .filter(i -> posId.equals(mParts.get(i).getPositionId()))
                .findFirst()
                .orElseThrow(() -> new NotImplementedException("There is no materialpart like you give an id"));
        MaterialPart oldItem = mParts.get(oldItemIndex);
        String oldMaterialId = oldItem.getMaterialTemplate().getId();
        MaterialPart updatedMaterialPart = BeanUtilSilent.translateBeanAndReturn(ump, new MaterialPart());
        boolean theSame = false;
        if (ump.getMaterialTemplateId().equals(oldItem.getMaterialTemplate().getId())) {
            updatedMaterialPart.setMaterialTemplate(oldItem.getMaterialTemplate());
            theSame = true;
        } else {
            MaterialTemplateEntity materialTemplateEntity = materialTemplateDAO.get(ump.getMaterialTemplateId());
            validator.validateNotNull(materialTemplateEntity).throww();
            updatedMaterialPart.setMaterialTemplate(materialTemplateEntity);
        }

        Dimension dim = updatedMaterialPart.getMaterialTemplate().getMaterialType().getCuttingDimensionTemplate();
        validatorDetal.validateCount(updatedMaterialPart.getCount(), ExceptionCodes.MATERIAL__COUNT_NOT_VALID).throww();
        validatorDetal.validateCuttedSectionSize(dim, updatedMaterialPart.getCuttedSectionSize(), ExceptionCodes.MATERIAL__COUNT_NOT_VALID).throww();
        validatorDetal.validateCuttedSectionSize(dim, updatedMaterialPart.getCuttedSectionSizeWithBorders(), ExceptionCodes.MATERIAL__COUNT_NOT_VALID).throww();
        validatorDetal.validateIsCuttingSizeSmallerOrEqualThanCuttingSizeWithBorders(updatedMaterialPart).throww();

        mParts.set(oldItemIndex, updatedMaterialPart);
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        if (theSame) {
            detalTemplateManager.refreshUsage(ump.getMaterialTemplateId());
        } else {
            detalTemplateManager.refreshUsage(ump.getMaterialTemplateId());
            detalTemplateManager.refreshUsage(oldMaterialId);
        }
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return MaterialPart.materialPartsListToDto(updated2.getPartCard().getMaterialParts());
    }

    public List<WarePartDTO> updateWarePartInDetalTempalte(ProductTemplateEntity dte, EntityType entityType, UpdateWarePart uwp) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        detalTemplateManager.ensurePartCartListExist(dte);

        List<WarePart> wParts = dte.getPartCard().getWareParts();
        String posId = uwp.getPositionId();
        int oldItemIndex = IntStream.range(0, wParts.size())
                .filter(i -> posId.equals(wParts.get(i).getPositionId()))
                .findFirst()
                .orElseThrow(() -> new NotImplementedException("There is no warepart like you give an id"));
        WarePart oldItem = wParts.get(oldItemIndex);
        String oldWareId = oldItem.getWareTemplate().getId();
        WarePart warePart = BeanUtilSilent.translateBeanAndReturn(uwp, new WarePart());
        boolean theSame = false;
        if (uwp.getWareTemplateId().equals(oldItem.getWareTemplate().getId())) {
            warePart.setWareTemplate(oldItem.getWareTemplate());
            theSame = true;
        } else {
            WareTemplateEntity wareTemplateEntity = wareTemplateDAO.get(uwp.getWareTemplateId());
            validator.validateNotNull(wareTemplateEntity).throww();
            warePart.setWareTemplate(wareTemplateEntity);
        }
        validatorDetal.validateCount(warePart.getCount(), ExceptionCodes.WARE__COUNT_NOT_VALID).throww();
        wParts.set(oldItemIndex, warePart);
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        if (theSame) {
            detalTemplateManager.refreshUsage(uwp.getWareTemplateId());
        } else {
            detalTemplateManager.refreshUsage(uwp.getWareTemplateId());
            detalTemplateManager.refreshUsage(oldWareId);
        }
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return WarePart.warePartsListToDto(updated2.getPartCard().getWareParts());
    }

    public List<SemiproductPartDTO> updateSemiproductPartInDetalTempalte(ProductTemplateEntity dte, EntityType entityType, UpdateSemiproductPart ump) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        detalTemplateManager.ensurePartCartListExist(dte);

        List<SemiproductPart> pt = dte.getPartCard().getSemiproductParts();
        String posId = ump.getPositionId();
        int oldItemIndex = IntStream.range(0, pt.size())
                .filter(i -> posId.equals(pt.get(i).getPositionId()))
                .findFirst()
                .orElseThrow(() -> new NotImplementedException("There is no productpart like you give an id"));
        SemiproductPart oldItem = pt.get(oldItemIndex);
        String oldSemiId = oldItem.getProductTemplate().getId();
        SemiproductPart updatedSPart = BeanUtilSilent.translateBeanAndReturn(ump, new SemiproductPart());
        boolean theSame = false;
        if (ump.getProductTemplateId().equals(oldSemiId)) {
            updatedSPart.setProductTemplate(oldItem.getProductTemplate());
            theSame = true;
        } else {
            ProductTemplateEntity productTemplateEntity = productTemplateDAO.get(ump.getProductTemplateId());
            validator.validateNotNull(productTemplateEntity).throww();
            updatedSPart.setProductTemplate(productTemplateEntity);
        }
        pt.set(oldItemIndex, updatedSPart);

        validatorDetal.validateCount(updatedSPart.getCount(), ExceptionCodes.SEMIPRODUCT__COUNT_NOT_VALID).throww();
        validatorDetal.validateLoopInSemiProductPartCartRecursively(updatedSPart, dte).throww();

        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        if (theSame) {
            detalTemplateManager.refreshUsage(ump.getProductTemplateId());
        } else {
            detalTemplateManager.refreshUsage(ump.getProductTemplateId());
            detalTemplateManager.refreshUsage(oldSemiId);
        }
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return SemiproductPart.semiproductPartsListToDto(updated2.getPartCard().getSemiproductParts());
    }

    public List<MaterialPartDTO> removeMaterialPartInDetalTempalte(ProductTemplateEntity dte, EntityType entityType, String materialPartId) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        detalTemplateManager.ensurePartCartListExist(dte);

        List<MaterialPart> mParts = dte.getPartCard().getMaterialParts();
        MaterialPart mPart = mParts.stream()
                .filter(m -> m.getPositionId().equals(materialPartId))
                .findAny()
                .orElseThrow(() -> new NotImplementedException("There is no materialPart like you give an id"));
        String materialTemplateId = mPart.getMaterialTemplate().getId();
        mParts.remove(mPart);
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        materialTemplateManager.refreshUsage(materialTemplateId);
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return MaterialPart.materialPartsListToDto(updated2.getPartCard().getMaterialParts());
    }

    public List<WarePartDTO> removeWarePartInDetalTempalte(ProductTemplateEntity dte, EntityType entityType, String warePartId) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        detalTemplateManager.ensurePartCartListExist(dte);

        List<WarePart> wParts = dte.getPartCard().getWareParts();
        WarePart wPart = wParts.stream()
                .filter(m -> m.getPositionId().equals(warePartId))
                .findAny()
                .orElseThrow(() -> new NotImplementedException("There is no warePart like you give an id"));
        String wareTemplateId = wPart.getWareTemplate().getId();
        wParts.remove(wPart);
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        wareTemplateManager.refreshUsage(wareTemplateId);
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return WarePart.warePartsListToDto(updated2.getPartCard().getWareParts());
    }

    public List<SemiproductPartDTO> removeSemiproductPartInDetalTempalte(ProductTemplateEntity dte, EntityType entityType, String productPartId) {
        DAOBaseInterface daoByEntityType = daoHandler.getDaoByEntityType(entityType);
        detalTemplateManager.ensurePartCartListExist(dte);
        List<SemiproductPart> pParts = dte.getPartCard().getSemiproductParts();
        SemiproductPart pPart = pParts.stream()
                .filter(m -> m.getPositionId().equals(productPartId))
                .findAny()
                .orElseThrow(() -> new NotImplementedException("There is no productPart like you give an id"));
        String productTemplateId = pPart.getProductTemplate().getId();
        pParts.remove(pPart);
        detalTemplateManager.handleValidationStatus(dte);
        ProductTemplateEntity updated = (ProductTemplateEntity) daoByEntityType.update(dte);
        detalTemplateManager.refreshUsage(productTemplateId);
        ProductTemplateEntity updated2 = (ProductTemplateEntity) daoByEntityType.get(updated.getId());
        return SemiproductPart.semiproductPartsListToDto(updated2.getPartCard().getSemiproductParts());
    }
}
