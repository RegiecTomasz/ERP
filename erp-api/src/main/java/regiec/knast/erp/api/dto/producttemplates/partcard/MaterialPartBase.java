/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialPartBase {

    private String cuttedSectionSize;
    private String cuttedSectionSizeWithBorders;
    private Integer count;
    private String comments;
    private String positionOnTechDraw;
}
