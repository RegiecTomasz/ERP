/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.entities.containers.ProductionOrderEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProductionOrderDAOInterface extends DAOBaseInterface<ProductionOrderEntity, ProductionOrderEntitiesList> {

}
