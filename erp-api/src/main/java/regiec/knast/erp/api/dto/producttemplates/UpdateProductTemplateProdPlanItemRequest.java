/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import regiec.knast.erp.api.dto.producttemplates.productionplan.UpdateProductionPlan;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */

@lombok.Getter
@lombok.Setter
public class UpdateProductTemplateProdPlanItemRequest {
    
    private String productTemplateId;    
    private UpdateProductionPlan prodPlanItem;   
}
