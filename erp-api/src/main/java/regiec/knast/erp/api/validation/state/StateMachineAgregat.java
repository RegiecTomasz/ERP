/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.validation.state;

import regiec.knast.erp.api.model.states.MaterialState;
import regiec.knast.erp.api.model.states.MaterialTemplateState;
import regiec.knast.erp.api.model.states.OrderState;
import regiec.knast.erp.api.model.states.ProductionOrderState;
import regiec.knast.erp.api.model.states.WareState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class StateMachineAgregat {

    private static <T> StateMachine.Adjacent<T> a(T stateToGo, String method, String methodVisibleName) {
        return new StateMachine.Adjacent(stateToGo, method, methodVisibleName);
    }

    public final static StateMachine<OrderState> ORDER = new StateMachine(
            new StateMachine.PossibleChangesMap<OrderState>()                    
                    .put(OrderState.NEW_F,
                            a(OrderState.NEW_F, null, null),
                            a(OrderState.PARTLY_DISPOSED_F, "generateProductionOrderDisposition", "Wykonaj dyspozycję")
                    )
                    .put(OrderState.PARTLY_DISPOSED_F,
                            a(OrderState.PARTLY_DISPOSED_F, "generateProductionOrderDisposition", "Wykonaj dyspozycję"),
                            a(OrderState.DISPOSED_F, "generateProductionOrderDisposition", "Wykonaj dyspozycję")
                    )
                    .put(OrderState.DISPOSED_F,
                            a(OrderState.DISPOSED_F, null, null)
                    )
                    .put(OrderState.ERROR,
                            a(OrderState.ERROR, null, null)
                    )
                    .put(OrderState.INITIAL,
                            a(OrderState.NEW, null, null),
                            a(OrderState.NEW_F, null, null)
                    )
                    .put(OrderState.NEW,
                            a(OrderState.NEW, null, null),
                            a(OrderState.PLANNED, "generateProductionOrder", "Generuj zadania produkcyjne")
                    )
                    .put(OrderState.PLANNED,
                            a(OrderState.PLANNED, null, null)
                    )
    ) {
    };

    public final static StateMachine<ProductionOrderState> PRODUCTION_ORDER = new StateMachine(
            new StateMachine.PossibleChangesMap<ProductionOrderState>()
                    .put(ProductionOrderState.CANCELED,
                            a(ProductionOrderState.CANCELED, null, null)
                    )
                    .put(ProductionOrderState.ERROR,
                            a(ProductionOrderState.ERROR, null, null)
                    )
                    .put(ProductionOrderState.FINISHED,
                            a(ProductionOrderState.FINISHED, null, null)
                    )
                    .put(ProductionOrderState.INITIAL,
                            a(ProductionOrderState.NEW, null, null)
                    )
                    .put(ProductionOrderState.MATERIAL_DEMANDS_UNCOMPLETE,
                            a(ProductionOrderState.MATERIAL_DEMANDS_UNCOMPLETE, null, null),
                            a(ProductionOrderState.WITH_MATERIAL_DEMANDS, null, null)
                    )
                    .put(ProductionOrderState.NEW,
                            a(ProductionOrderState.NEW, null, null),
                            a(ProductionOrderState.MATERIAL_DEMANDS_UNCOMPLETE, "generateMaterialDemands", "Generuj zapotrzebowania materiałowe")
                    )
                    .put(ProductionOrderState.PLANNED,
                            a(ProductionOrderState.PLANNED, null, null),
                            a(ProductionOrderState.FINISHED, null, null)
                    )
                    .put(ProductionOrderState.ON_PRODUCTION,
                            a(ProductionOrderState.ON_PRODUCTION, null, null)
                    )
                    .put(ProductionOrderState.WITH_MATERIAL_DEMANDS,
                            a(ProductionOrderState.WITH_MATERIAL_DEMANDS, null, null),
                            a(ProductionOrderState.PLANNED, "generateProductionJobs", "Generuj zadania produkcyjne")
                    )
    ) {
    };

    public final static StateMachine<MaterialState> MATERIAL = new StateMachine(
            new StateMachine.PossibleChangesMap<MaterialState>()
                    .put(MaterialState.ADDED_MANUALY,
                            a(MaterialState.ADDED_MANUALY, null, null),
                            a(MaterialState.ADDED_MANUALY_RESERVED, null, null)
                    )
                    .put(MaterialState.ADDED_MANUALY_RESERVED,
                            a(MaterialState.ADDED_MANUALY_RESERVED, null, null)
                    )
                    .put(MaterialState.BROKEN_ON_PRODUCTION,
                            a(MaterialState.BROKEN_ON_PRODUCTION, null, null)
                    )
                    .put(MaterialState.BROKEN_ON_STOCK,
                            a(MaterialState.BROKEN_ON_STOCK, null, null)
                    )
                    .put(MaterialState.CANCELED_FROM_ORDER,
                            a(MaterialState.CANCELED_FROM_ORDER, null, null)
                    )
                    .put(MaterialState.CANCELED_FROM_PRODUCTION,
                            a(MaterialState.CANCELED_FROM_PRODUCTION, null, null)
                    )
                    .put(MaterialState.DONE,
                            a(MaterialState.DONE, null, null)
                    )
                    .put(MaterialState.ERROR,
                            a(MaterialState.ERROR, null, null)
                    )
                    .put(MaterialState.INITIAL,
                            a(MaterialState.ADDED_MANUALY, null, null),
                            a(MaterialState.ON_STOCK_AVALIABLE, null, null),
                            a(MaterialState.IN_SHOPPING_CART, null, null)
                    )
                    .put(MaterialState.IN_PRODUCTION,
                            a(MaterialState.IN_PRODUCTION, null, null),
                            a(MaterialState.DONE, null, null),
                            a(MaterialState.CANCELED_FROM_PRODUCTION, null, null),
                            a(MaterialState.BROKEN_ON_PRODUCTION, null, null)
                    )
                    .put(MaterialState.IN_SHOPPING_CART,
                            a(MaterialState.IN_SHOPPING_CART, null, null),
                            a(MaterialState.IN_SHOPPING_CART_RESERVED, null, null),
                            a(MaterialState.ORDERED, null, null)
                    )
                    .put(MaterialState.IN_SHOPPING_CART_RESERVED,
                            a(MaterialState.IN_SHOPPING_CART_RESERVED, null, null),
                            a(MaterialState.ORDERED_RESERVED, null, null)
                    )
                    .put(MaterialState.ON_STOCK_AVALIABLE,
                            a(MaterialState.ON_STOCK_AVALIABLE, null, null),
                            a(MaterialState.ON_STOCK_RESERVED, null, null)
                    )
                    .put(MaterialState.ON_STOCK_RESERVED,
                            a(MaterialState.ON_STOCK_RESERVED, null, null),
                            a(MaterialState.IN_PRODUCTION, null, null)
                    )
                    .put(MaterialState.ORDERED,
                            a(MaterialState.ORDERED, null, null),
                            a(MaterialState.ORDERED_RESERVED, null, null),
                            a(MaterialState.CANCELED_FROM_ORDER, null, null)
                    )
                    .put(MaterialState.ORDERED_RESERVED,
                            a(MaterialState.ORDERED_RESERVED, null, null),
                            a(MaterialState.IN_PRODUCTION, null, null)
                    )
    ) {
    };
    
    public final static StateMachine<MaterialTemplateState> MATERIAL_TEMPLATE = new StateMachine(
            new StateMachine.PossibleChangesMap<MaterialTemplateState>()
                    .put(MaterialTemplateState.NORMAL,
                            a(MaterialTemplateState.NORMAL, null, null)
                    )
    ) {
    };
    
    public final static StateMachine<WareState> WARE = new StateMachine(
            new StateMachine.PossibleChangesMap<WareState>()
                    .put(WareState.ADDED_MANUALY,
                            a(WareState.ADDED_MANUALY, null, null)
                    )
    ) {
    };

}
