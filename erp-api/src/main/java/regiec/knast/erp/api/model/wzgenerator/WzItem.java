/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.wzgenerator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WzItem {

    private Integer no = 0;
    private String productNo = "a";
    private String name = "a";
    private Integer count = 1;
    private String unit = "a";
    private String orderNo = "a";
    private String position = "a";

}
