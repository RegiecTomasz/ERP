/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.order;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dto.orderedproducts.OrderPositionDTO;
import regiec.knast.erp.api.dto.recipient.RecipientDTO;
import regiec.knast.erp.api.model.bases.OrderBase;
import regiec.knast.erp.api.model.states.OrderState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class OrderDTO extends OrderBase {

    private String id;
    // private UIReference recipient;
    private RecipientDTO recipient;
    private String recipientId;
    private List<OrderPositionDTO> orderPositions = new ArrayList();
    private List<String> orderPositionsBriefly = new ArrayList();
    private boolean frameworkContractOrder;
    
    private OrderState state;
}
