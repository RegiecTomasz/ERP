/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.states.MaterialTypeState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class MaterialTypeBase extends ObjectWithDate {

    private String name;
    private Dimension cuttingDimensionTemplate;
    private Dimension baseDimensionTemplate;
    private String cuttingDimensionUnit;
    private String baseDimensionUnit;
    private MaterialTypeState state = MaterialTypeState.NORMAL;
}
