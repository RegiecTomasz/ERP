/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import lombok.extern.java.Log;
import org.jaxygen.converters.json.GSONBuilderFactory;
import org.jaxygen.converters.json.JSONBuilderRegistry;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.adaptersgson.DateDeserializer;
import regiec.knast.erp.api.adaptersgson.DateSerializer;
import regiec.knast.erp.api.adaptersgson.MoneyTypeAdapterFactory;
import regiec.knast.erp.api.dao.driver.MongoInterface;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
@Log
public class ContextInitializer {

    public static final String DATE_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);
    private Gson gson;

    static {
        TimeZone tz = TimeZone.getTimeZone("GMT");
        DATE_FORMAT.setTimeZone(tz);
    }
    @Inject
    private MongoInterface mongoDriver;

    private boolean isinitialized = false;
    private static ContextInitializer instance;

    public static ContextInitializer getInstance() {
        if (instance == null) {
            try {
                ObjectBuilderFactory.configure(GuiceBuilder.getInstance());
                instance = (ContextInitializer) ObjectBuilderFactory.instance().create(ContextInitializer.class);
            } catch (ObjectCreateError ex) {
                log.log(Level.SEVERE, null, ex);
            }
        }
        return instance;
    }

    @Inject
    private ContextInitializer() {
    }

    public void contextInitialized() {
        if (!isinitialized) {
            isinitialized = true;
            System.out.println("Welcome in ERP!");
            setupGsonBuilder();
            this.gson = JSONBuilderRegistry.getBuilder().createBuilder().setPrettyPrinting().create();
        }
    }

    public void contextDestroyed() {
        mongoDriver.closeConnections();
        System.out.println("The End");
    }

    public Gson GSON() {
        contextInitialized();
        return gson;
    }

    private void setupGsonBuilder() {
        JSONBuilderRegistry.setGSONBuilder(new GSONBuilderFactory() {
            @Override
            public GsonBuilder createBuilder() {
                return new GsonBuilder()
                        .registerTypeAdapter(Date.class, new DateDeserializer())
                        .registerTypeAdapter(Date.class, new DateSerializer())
                        .registerTypeAdapterFactory(new MoneyTypeAdapterFactory());
            }

            @Override
            public Gson build() {
                return createBuilder().create();
            }
        });
    }

}
