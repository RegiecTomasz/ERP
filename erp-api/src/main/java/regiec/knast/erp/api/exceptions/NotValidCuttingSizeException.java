/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import java.util.Map;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author fbrzuzka
 */
public class NotValidCuttingSizeException extends ConversionErrorWithCode{
    


    public NotValidCuttingSizeException(String message) {
        super(message, ExceptionCodes.NOT_VALID_CUTTING_SIZE);
    }
    
    public NotValidCuttingSizeException(String message,  Throwable cause) {
        super(message, ExceptionCodes.NOT_VALID_CUTTING_SIZE, null, cause);
    }
    
    public NotValidCuttingSizeException(String message, Map params, Throwable cause) {
        super(message, ExceptionCodes.NOT_VALID_CUTTING_SIZE, params, cause);
    }
    
    
}
