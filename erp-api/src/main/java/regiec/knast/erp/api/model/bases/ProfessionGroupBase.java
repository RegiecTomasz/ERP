/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import org.jaxygen.annotations.HiddenField;
import regiec.knast.erp.api.model.states.ProfessionGroupState;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProfessionGroupBase extends ObjectWithDate {

    private String code;
    private String name;
    @HiddenField
    private String label;
    private ProfessionGroupState state;
    private boolean removed;
}
