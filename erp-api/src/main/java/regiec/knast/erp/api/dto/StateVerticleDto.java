/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class StateVerticleDto {

    private String name;
    private boolean canBeRemoved = false;
    private List<StateVerticleAdjacentDto> adjacents = new ArrayList();

    public StateVerticleDto(String name) {
        this.name = name;
    }

    public StateVerticleDto(String name, boolean canBeRemoved) {
        this.name = name;
        this.canBeRemoved = canBeRemoved;
    }
    
}
