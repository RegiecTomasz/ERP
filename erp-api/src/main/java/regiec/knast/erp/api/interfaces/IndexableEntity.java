/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.interfaces;

/**
 *This is interface that is used to hide some 'id' fields like loopbackId. Used by ErpIdEntity and EPRSafetyCheck
 * 
 * @author jknast jakub.knast@gmail.com
 */
public interface IndexableEntity {


}
