/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.system;

import regiec.knast.erp.api.model.ConstType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class AddConstRequestDTO {

    private ConstType type;
    private String value;
}
