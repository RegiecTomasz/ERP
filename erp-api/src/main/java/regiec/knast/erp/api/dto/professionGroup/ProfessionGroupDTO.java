/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.professionGroup;

import java.util.ArrayList;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.operationName.OperationNameDTO;
import regiec.knast.erp.api.model.bases.ProfessionGroupBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class ProfessionGroupDTO extends ProfessionGroupBase {

    private String id;
    @Reference
    private ArrayList<OperationNameDTO> allowOperations = new ArrayList();


}
