/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.dto.Downloadable;
import regiec.knast.erp.api.dao.interfaces.DocumentDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.product.AddProductRequest;
import regiec.knast.erp.api.dto.product.FinishProductRequest;
import regiec.knast.erp.api.dto.product.GenerateInvoicePdfRequest;
import regiec.knast.erp.api.dto.product.GenerateWzPdfRequest;
import regiec.knast.erp.api.dto.product.GetDownloadableDocumentRequest;
import regiec.knast.erp.api.dto.product.ListProductsForInvoicePdf;
import regiec.knast.erp.api.dto.product.ListProductsForWzPdf;
import regiec.knast.erp.api.dto.product.ProductDTO;
import regiec.knast.erp.api.dto.product.ProductListDTO;
import regiec.knast.erp.api.dto.product.RemoveProductRequest;
import regiec.knast.erp.api.dto.product.UpdateProductRequest;
import regiec.knast.erp.api.entities.DocumentEntity;
import regiec.knast.erp.api.entities.ProductEntity;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductEntitiesList;
import regiec.knast.erp.api.exceptions.NotImplementedException;
import regiec.knast.erp.api.manager.ProductManager;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.invoicegenerator.InvoiceData;
import regiec.knast.erp.api.model.states.DocumentState;
import regiec.knast.erp.api.model.states.ProductState;
import regiec.knast.erp.api.model.wzgenerator.WzData;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.InvoiceGenerator;
import regiec.knast.erp.api.utils.PdfGenerator;
import regiec.knast.erp.api.utils.WzGenerator;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;
import regiec.knast.erp.api.validation.ValidatorDetal;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ProductService  extends AbstractService<ProductEntity, ProductDTO>{

    @Inject
    private ProductDAOInterface productDAO;
    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;
    @Inject
    private Validator validator;
    @Inject
    private ValidatorDetal validatorDetal;
    @Inject
    private ProductManager productManager;
    @Inject
    private DocumentDAOInterface documentDAO;

    @NetAPI
    public ProductDTO addProduct(AddProductRequest request) {
        ProductEntity entity = new ProductEntity();

        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getProductTemplateId()).throww();
        validatorDetal.validateCount(request.getCount(), ExceptionCodes.PRODUCT__COUNT_NOT_VALID).throww();

        ProductTemplateEntity productTemplate = productTemplateDAO.get(request.getProductTemplateId());
        validator.validateNotNull(productTemplate).throww();

        entity.setProductTemplate(productTemplate);
        entity.setCount(request.getCount());
        entity.setState(ProductState.ADDED_MANUALY);
        ProductEntity add = productDAO.add(entity);  
        return add.convertToDTO();
    }

    @NetAPI
    public ProductDTO getProduct(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public ProductDTO updateProduct(UpdateProductRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateNotNull(request.getOrderNo()).throww();
        validator.validateNotNull(request.getRecipientId()).throww();
        validatorDetal.validateCount(request.getCount(), ExceptionCodes.PRODUCT__COUNT_NOT_VALID).throww();
        ProductEntity entity = productDAO.getProduct(id);
        validator.validateNotNull(entity).throww();
        ProductEntity updatedEntity = new ProductEntity();
        BeanUtilSilent.translateBean(entity, updatedEntity);
        BeanUtilSilent.translateBean(request, updatedEntity);
        productDAO.updateProduct(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeProduct(RemoveProductRequest request) {
        String id = request.getId();
        ProductEntity productEntity = productDAO.get(id);
        if (request.getConfirmed()) {
            if (request.getRemoveRequestFromUp()) {
                productEntity.setProductTemplate(productTemplateDAO.get(productEntity.getProductTemplate().getId()));
            }
            //TODO: check state for remove
            productEntity.setRemoved(true);
            productDAO.update(productEntity);
            return new RemoveResponse(true, null);
        } else {
            return new RemoveResponse(false, productEntity.buildRemoveConsequencesMap());
        }
    }
    
    @NetAPI
    public ProductListDTO listProducts(PaginableFilterWithCriteriaRequest request) {
        return (ProductListDTO) super.list(request);
    }
    
    @NetAPI
    public ProductListDTO listProductsForWz(ListProductsForWzPdf request) {
        PaginableFilterWithCriteriaRequest filter = PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("removed", "false", ConstraintType.EQUALS)
                .addConstraint("recipient", request.getRecipientId(), ConstraintType.EQUALS)
                .addConstraint("state", "FINISHED", ConstraintType.EQUALS)
                .addConstraint("wzId", "false", ConstraintType.EXISTS)
                .buildCriteriaFilter()
                .projectioinExclude("productTemplate.partCard", "productTemplate.productionPlan")
                .build();
        ProductEntitiesList list = productDAO.list(filter);
        ProductEntitiesList mergedEntities = list.mergeProducts();   
        return (ProductListDTO) super.listToDto(mergedEntities);
    }
    
    @NetAPI
    public ProductListDTO listProductsForInvoice(ListProductsForInvoicePdf request) {
        PaginableFilterWithCriteriaRequest filter = PaginableFilterWithCriteriaRequest.builder()
                .addCriteriaFilterBuilder()
                .addConstraint("removed", "false", ConstraintType.EQUALS)
                .addConstraint("recipient", request.getRecipientId(), ConstraintType.EQUALS)
                .addConstraint("state", "FINISHED", ConstraintType.EQUALS)
                .addConstraint("wzId", "true", ConstraintType.EXISTS)
                .addConstraint("invoiceId", "false", ConstraintType.EXISTS)
                .buildCriteriaFilter()
                .projectioinExclude("productTemplate.partCard", "productTemplate.productionPlan")
                .build();
        ProductEntitiesList list = productDAO.list(filter);
        ProductEntitiesList mergedEntities = list.mergeProducts();   
        return (ProductListDTO) super.listToDto(mergedEntities);
    }

    @NetAPI
    public Downloadable generateWZPdf(GenerateWzPdfRequest request) {
        validator.validateNotNull(request, "GenerateWzPdfRequest").throww();
        validator.validateNotNullAndNotEmpty(request.getRecipientId()).throww();
        validator.validateNotNullAndNotEmpty(request.getSellerId()).throww();
        validator.validateNotNull(request.getProductsIds(), "productsIds").throww();

        String fileName = "pdfWz.pdf";
        WzGenerator wzGenerator = new WzGenerator(request);

        Downloadable downloadable = wzGenerator.toDownloadable(fileName);
        WzData wzData = wzGenerator.getData();
        DocumentEntity document = this.documentDAO.add(new DocumentEntity(fileName, wzData, DocumentState.NORMAL));
        List<String> splitIds = request.splitIds();
        updateDocumentInProducts(document, "wzId", splitIds);
        return downloadable;
    }

    @NetAPI
    public Downloadable generateInvoicePdf(GenerateInvoicePdfRequest request) {
        validator.validateNotNull(request, "GenerateInvoicePdfRequest").throww();
        validator.validateNotNullAndNotEmpty(request.getRecipientId()).throww();
        validator.validateNotNullAndNotEmpty(request.getSellerId()).throww();
        validator.validateNotNull(request.getProductsIds(), "productsIds").throww();

        String fileName = "faktura.pdf";
        InvoiceGenerator invoiceGenerator = new InvoiceGenerator(request);

        Downloadable downloadable = invoiceGenerator.toDownloadable(fileName);
        InvoiceData invoiceData = invoiceGenerator.getData();
        DocumentEntity document = this.documentDAO.add(new DocumentEntity(fileName, invoiceData, DocumentState.NORMAL));
        List<String> splitIds = request.splitIds(); 
        updateDocumentInProducts(document, "invoiceId", splitIds);
        return downloadable;
    }

    private void updateDocumentInProducts(DocumentEntity doc, String fieldName, List<String> productsIds) {
        for (String pid : productsIds) {
            productDAO.updateOneField(pid, fieldName, doc.getId());
        }
    }

    @NetAPI
    public Downloadable getMyPdfDocument(GetDownloadableDocumentRequest request) {
        validator.validateNotNull(request, "GetDownloadableDocumentRequest").throww();
        validator.validateNotNullAndNotEmpty(request.getDocumentId()).throww();

        DocumentEntity document = documentDAO.get(request.getDocumentId());
        validator.validateNotNull(document, "document").throww();
        document.validateMe();
        String fileName = document.getName();
        PdfGenerator pdfGenerator;
        if (document.getContent() instanceof WzData) {
            WzData wzData = (WzData) document.getContent();
            pdfGenerator = new WzGenerator(wzData);
        } else  if (document.getContent() instanceof InvoiceData) {
            InvoiceData invoiceData = (InvoiceData) document.getContent();
            pdfGenerator = new InvoiceGenerator(invoiceData);
        }else {
            throw new NotImplementedException("other documents");
        }
        return pdfGenerator.toDownloadable(fileName);
    }

    @NetAPI
    public ProductListDTO finishProduct(FinishProductRequest request) {
        validator.validateNotNull(request, "FinishProductRequest").throww();
        validator.validateNotNullAndNotEmpty(request.getProductId()).throww();
        validator.validateCount(request.getCount());
        return productManager.finishProduct(request.getProductId(), request.getCount());
    }
}
