/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business.apitests;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.producttemplates.ProductTemplatesListDTO;
import regiec.knast.erp.api.dto.provider.AddPositionToProviderAssortmentRequest;
import regiec.knast.erp.api.dto.provider.ProviderDTO;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.SellUnit;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class TestMoney extends AllServicesInjector {

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(TestMoney.class);
        TestMoney mongoTest = (TestMoney) create;
        try {
            mongoTest.initServices();
            mongoTest.test_listProductsTemplate();
        } catch (Exception ex) {
            Logger.getLogger(ListTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    private void test_updateProvider() {
        ProviderDTO addPositionToAssortment = providerService.addPositionToAssortment(
                new AddPositionToProviderAssortmentRequest("5975d40d4b13ec08e472ddc7", "13", 
                        MoneyERP.create(new BigDecimal(340), "PLN"), "10004753", SellUnit.mb));
        printResult(addPositionToAssortment);
        
    }

    private void test_getprovider() {
        ProviderDTO provider = providerService.getProvider(new BasicGetRequest("5975d40d4b13ec08e472ddc7"));
        
    }
    
    private void test_listProductsTemplate() {
        ProductTemplatesListDTO listProductTemplates = productTemplateService.listProductTemplates(new PaginableFilterWithCriteriaRequest());
        super.printResult(listProductTemplates.getElements().get(0));
    }
    
}
