/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests.xmlimport;

import javax.xml.bind.annotation.XmlRegistry;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public Kopia_x0020_Materiały_w_ZP createCustomer() {
        return new Kopia_x0020_Materiały_w_ZP();
    }
}
