/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.exceptions;

import java.util.Map;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;

/**
 *
 * @author fbrzuzka
 */
public class ErpStateValidateionException extends ConversionErrorWithCode {

    public ErpStateValidateionException(ExceptionCodes code, String message) {
        super(message, code, null);
    }

    public ErpStateValidateionException(ExceptionCodes code, String message, Map properties) {
        super(message, code, properties);
    }

}
