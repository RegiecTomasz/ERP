/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.MonetaryAmountFactory;
import javax.money.MonetaryContext;
import javax.money.NumberValue;
import org.javamoney.moneta.Money;
import regiec.knast.erp.api.dao.driver.MongoDriver;

/**
 * Its adapter for final Money class
 *
 * @author jknast jakub.knast@gmail.com
 */
@org.mongodb.morphia.annotations.Converters({MongoDriver.MoneyConverter.class})
public class MoneyERP {

    public static final DecimalFormat TWO_DECIMAL_PLACES = new DecimalFormat("0.00");

    @org.mongodb.morphia.annotations.Transient
    private org.javamoney.moneta.Money money;

    public String roundTwoDecimalPlaces() {
        double doubleValue = this.getNumber().doubleValue();
        return TWO_DECIMAL_PLACES.format(doubleValue);
    }

    private MoneyERP(Money moneta) {
        this.money = moneta;
    }

    public MoneyERP(BigDecimal amount, String currency) {
        this.money = Money.of(amount, currency);
    }

    public static MoneyERP create(BigDecimal amount, String currency) {
        return new MoneyERP(Money.of(amount, currency));
    }

    public MonetaryContext getContext() {
        return money.getContext();
    }

    public boolean isGreaterThan(MoneyERP amount) {
        return money.isGreaterThan(amount.money);
    }

    public boolean isGreaterThanOrEqualTo(MoneyERP amount) {
        return money.isGreaterThanOrEqualTo(amount.money);
    }

    public boolean isLessThan(MoneyERP amount) {
        return money.isLessThan(amount.money);
    }

    public boolean isLessThanOrEqualTo(MoneyERP amt) {
        return money.isLessThanOrEqualTo(amt.money);
    }

    public boolean isEqualTo(MoneyERP amount) {
        return money.isEqualTo(amount.money);
    }

    public boolean isNegative() {
        return money.isNegative();
    }

    public boolean isNegativeOrZero() {
        return money.isNegativeOrZero();
    }

    public boolean isPositive() {
        return money.isPositive();
    }

    public boolean isPositiveOrZero() {
        return money.isPositiveOrZero();
    }

    public boolean isZero() {
        return money.isZero();
    }

    public int signum() {
        return money.signum();
    }

    public MoneyERP add(MoneyERP amount) {
        return new MoneyERP(money.add(amount.money));
    }

    public MoneyERP subtract(MoneyERP amount) {
        return new MoneyERP(money.subtract(amount.money));
    }

    public MoneyERP multiply(long multiplicand) {
        return new MoneyERP(money.multiply(multiplicand));
    }

    public MoneyERP multiply(double multiplicand) {
        return new MoneyERP(money.multiply(multiplicand));
    }

    public MoneyERP multiply(Number multiplicand) {
        return new MoneyERP(money.multiply(multiplicand));
    }

    public MoneyERP divide(long divisor) {
        return new MoneyERP(money.divide(divisor));
    }

    public MoneyERP divide(double divisor) {
        return new MoneyERP(money.divide(divisor));
    }

    public MoneyERP divide(Number divisor) {
        return new MoneyERP(money.divide(divisor));
    }

    public MoneyERP remainder(long divisor) {
        return new MoneyERP(money.remainder(divisor));
    }

    public MoneyERP remainder(double divisor) {
        return new MoneyERP(money.remainder(divisor));
    }

    public MoneyERP remainder(Number divisor) {
        return new MoneyERP(money.remainder(divisor));
    }
//
//    public MoneyERP[] divideAndRemainder(long divisor) {
//        return money.divideAndRemainder(divisor);
//    }
//
//    public MoneyERP[] divideAndRemainder(double divisor) {
//        return money.divideAndRemainder(divisor);
//    }
//
//    public MoneyERP[] divideAndRemainder(Number divisor) {
//        return money.divideAndRemainder(divisor);
//    }

    public MoneyERP divideToIntegralValue(long divisor) {
        return new MoneyERP(money.divideToIntegralValue(divisor));
    }

    public MoneyERP divideToIntegralValue(double divisor) {
        return new MoneyERP(money.divideToIntegralValue(divisor));
    }

    public MoneyERP divideToIntegralValue(Number divisor) {
        return new MoneyERP(money.divideToIntegralValue(divisor));
    }

    public MoneyERP scaleByPowerOfTen(int power) {
        return new MoneyERP(money.scaleByPowerOfTen(power));
    }

    public MoneyERP abs() {
        return new MoneyERP(money.abs());
    }

    public MoneyERP negate() {
        return new MoneyERP(money.negate());
    }

    public MoneyERP plus() {
        return new MoneyERP(money.plus());
    }

    public MoneyERP stripTrailingZeros() {
        return new MoneyERP(money.stripTrailingZeros());
    }

    public CurrencyUnit getCurrency() {
        return money.getCurrency();
    }

    public NumberValue getNumber() {
        return money.getNumber();
    }

    public int compareTo(MoneyERP o) {
        return money.compareTo(o.money);
    }

    public MonetaryAmountFactory<? extends MonetaryAmount> getFactory() {
        return money.getFactory();
    }

    public boolean valid() {        
        if (this.isNegativeOrZero()) {
            return false;
        }
        return true;
    }

}
