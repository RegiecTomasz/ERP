/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.entities.containers.ProfessionGroupEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface ProfessionGroupDAOInterface extends DAOBaseInterface<ProfessionGroupEntity, ProfessionGroupEntitiesList> {
    
    ProfessionGroupEntitiesList listProfessionGroupsWithOperationId(String  operationNameId);
}
