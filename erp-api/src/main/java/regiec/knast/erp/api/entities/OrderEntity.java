/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.assertj.core.util.Lists;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.order.OrderDTO;
import regiec.knast.erp.api.dto.orderedproducts.OrderPosition;
import regiec.knast.erp.api.dto.orderedproducts.OrderPositionDTO;
import regiec.knast.erp.api.exceptions.ErpStateValidateionException;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.OrderBase;
import regiec.knast.erp.api.model.states.OrderState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.state.StateMachineAgregat;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
@Entity("orders")
public class OrderEntity extends OrderBase implements EntityBase, ConvertableEntity {

    @Id
    private String id;
    @Reference
    private RecipientEntity recipient;
    private List<OrderPosition> orderPositions = new ArrayList();

    private boolean frameworkContractOrder;
    private OrderState state = OrderState.INITIAL;

    public OrderEntity(OrderEntity order) {
        this.state = order.state;
    }

    public void validateStateForGenetatingProductionOrder(OrderState stateToGo) {
        List<OrderState> orderStatesToGenerateDisposition = Lists.newArrayList(OrderState.NEW_F, OrderState.PARTLY_DISPOSED_F);
        StateMachineAgregat.ORDER.validateChangeState(this.state, stateToGo,
                new ErpStateValidateionException(ExceptionCodes.GENERATE_PRODUCTION_ORDERS_ONLY_IN__NEW__STATE, "cannot do generateProductionOrder on order in state: " + state.name(), null));

        if (this.getFrameworkContractOrder() && !orderStatesToGenerateDisposition.contains(stateToGo)) {
            throw new ErpStateValidateionException(ExceptionCodes.GENERATE_PRODUCTION_ORDERS_DISPOSITION__WRONG_STATE, "cannot disposition on order in state: " + state.name(), null);
        } else if (!this.getFrameworkContractOrder() && (stateToGo != OrderState.PLANNED || this.state != OrderState.NEW)) {
            throw new ErpStateValidateionException(ExceptionCodes.GENERATE_PRODUCTION_ORDERS_ONLY_IN__NEW__STATE, "cannot do generateProductionOrder on order in state: " + state.name(), null);
        }
    }

    public OrderPosition findOrderPosition(String orderPositionId) {
        return this.getOrderPositions().stream()
                .filter(pos -> pos.getPositionId().equals(orderPositionId))
                .findAny()
                .get();
    }

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    public void setState(OrderState toGo) {
        StateMachineAgregat.ORDER.validateChangeState(this.getState(), toGo);
        this.state = toGo;
    }

    @Override
    public OrderDTO convertToDTO() {
        OrderDTO orderDTO = BeanUtilSilent.translateBeanAndReturn(this, new OrderDTO());
        orderDTO.setRecipient(this.getRecipient().convertToDTO());
        orderDTO.setRecipientId(this.getRecipient().getId());
        orderDTO.setOrderPositions(orderPositionToDTO(this.getOrderPositions()));
        orderDTO.setOrderPositionsBriefly(orderPositionToBrief(this.getOrderPositions()));
        return orderDTO;
    }

    private List<OrderPositionDTO> orderPositionToDTO(final List<OrderPosition> orderPositions) {
        final List<OrderPositionDTO> orderPositionDTOs = new ArrayList();
        if (orderPositions != null) {
            for (final OrderPosition orderPosition : orderPositions) {
                final OrderPositionDTO orderPositionDTO = BeanUtilSilent.translateBeanAndReturn(orderPosition, new OrderPositionDTO());
                orderPositionDTO.setProductId_itShouldBeProductReference(orderPosition.getProduct().getId());
                orderPosition.getProduct().getProductTemplate().setPartCard(null);
                orderPosition.getProduct().getProductTemplate().setProductionPlan(null);
                orderPosition.getProduct().getProductTemplate().setTechDrawingPDF(null);
                orderPosition.getProduct().getProductTemplate().setTechDrawingDXF(null);
                orderPosition.getProduct().getProductTemplate().setReportTemplate(null);
                orderPositionDTO.setProductTemplateId(orderPosition.getProduct().getProductTemplate().getId());
                orderPositionDTO.setProductTemplate(orderPosition.getProduct().getProductTemplate().convertToDTO());
                orderPositionDTOs.add(orderPositionDTO);
            }
        }
        return orderPositionDTOs;
    }

    private List<String> orderPositionToBrief(List<OrderPosition> orderPositions) {
        final List<String> ret = new ArrayList();
        if (orderPositions != null) {
            for (final OrderPosition op : orderPositions) {
                ret.add(op.getProduct().getProductTemplate().getProductNo() + " ||| " + op.getProduct().getProductTemplate().getName() + " ||| " + op.getProduct().getCount());
            }
        }
        return ret;
    }

}
