/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.bases;

import java.util.ArrayList;
import java.util.List;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;
import regiec.knast.erp.api.entities.ProviderEntity;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class DeliveryOrderBase extends ObjectWithDate {

    @ErpIdEntity(entityClass = ProviderEntity.class)
    private String providerId;
    private String providerName;
    private String positionsStr;
    private String orderMail;
    private List<DeliveryOrderPosition> positions = new ArrayList();
    
}
