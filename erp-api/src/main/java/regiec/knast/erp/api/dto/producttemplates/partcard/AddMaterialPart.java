/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddMaterialPart extends MaterialPartBase {

    private String materialTemplateId;

    public AddMaterialPart(String materialTemplateId, String cuttedSectionSize, String cuttedSectionSizeWithBorders, Integer count, String comments, String positionOnTechDraw) {
        super(cuttedSectionSize, cuttedSectionSizeWithBorders, count, comments, positionOnTechDraw);
        this.materialTemplateId = materialTemplateId;
    }
    
}
