/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.util.HashMap;
import java.util.Map;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.*;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class EntityTypeToStates_Map {

    private static final Map<EntityType, Class<? extends StateInterface>> CLASS_MAP = new HashMap();

    static {
        CLASS_MAP.put(EntityType.MATERIAL, MaterialState.class);
        CLASS_MAP.put(EntityType.MATERIAL_TEMPLATE, MaterialTemplateState.class);
        CLASS_MAP.put(EntityType.MATERIAL_TYPE, MaterialTypeState.class);
        CLASS_MAP.put(EntityType.WARE, WareState.class);
        CLASS_MAP.put(EntityType.WARE_TEMPLATE, WareTemplateState.class);
        CLASS_MAP.put(EntityType.WARE_TYPE, WareTypeState.class);
        CLASS_MAP.put(EntityType.PRODUCT, ProductState.class);
        CLASS_MAP.put(EntityType.PRODUCT_TEMPLATE, ProductTemplateState.class);
        CLASS_MAP.put(EntityType.SELLER, SellerState.class);
        CLASS_MAP.put(EntityType.RECIPIENT, RecipientState.class);
        CLASS_MAP.put(EntityType.PRODUCTION_ORDER, ProductionOrderState.class);
        CLASS_MAP.put(EntityType.ORDER, OrderState.class);
        CLASS_MAP.put(EntityType.DELIVERY_ORDER, DeliveryOrderState.class);
        CLASS_MAP.put(EntityType.WORKER, WorkerState.class);
        CLASS_MAP.put(EntityType.MACHINE, MachineState.class);
        CLASS_MAP.put(EntityType.PROVIDER, ProviderState.class);
        CLASS_MAP.put(EntityType.PRODUCTION_JOB, ProductionJobState.class);
        CLASS_MAP.put(EntityType.MATERIAL_DEMAND, MaterialDemandState.class);
        CLASS_MAP.put(EntityType.DEVELOPMENT, DevelopmentState.class);
        CLASS_MAP.put(EntityType.OPERATION_NAME, OperationNameState.class);
        CLASS_MAP.put(EntityType.PROFESSION_GROUP, ProfessionGroupState.class);
        CLASS_MAP.put(EntityType.DOCUMENT, DocumentState.class);
//Here_will_be_insert_entityType_to_EntityStateClass_mapping
    }

    public static Class<? extends StateInterface> get(EntityType type) {
        Class ret = CLASS_MAP.get(type);
        if (ret == null) {
            throw new ConversionError("There is no mapped entity type: " + type);
        }
        return ret;
    }

    public static Map<EntityType, Class<? extends StateInterface>> getCLASS_MAP() {
        return CLASS_MAP;
    }

}
