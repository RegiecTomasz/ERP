/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import org.jaxygen.dto.Downloadable;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.resourceinfo.AddResourceInfoRequest;
import regiec.knast.erp.api.dto.resourceinfo.GetResourceByIdRequest;
import regiec.knast.erp.api.dto.resourceinfo.RemoveResourceInfoRequest;
import regiec.knast.erp.api.dto.resourceinfo.ResourceInfoDTO;
import regiec.knast.erp.api.dto.resourceinfo.ResourceInfoListDTO;
import regiec.knast.erp.api.dto.resourceinfo.UpdateResourceInfoRequest;
import regiec.knast.erp.api.entities.ResourceInfoEntity;
import regiec.knast.erp.api.manager.ResourceManager;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class ResourceInfoService extends AbstractService<ResourceInfoEntity, ResourceInfoDTO> {

    @Inject
    private ResourceManager resourceManager;

    @NetAPI
    public ResourceInfoDTO addResourceInfo(AddResourceInfoRequest request) {
        ResourceInfoEntity entity = resourceManager.addResourceInfo(request);
        return (ResourceInfoDTO)entity.convertToDTO();
    }

    @NetAPI
    public ResourceInfoDTO getResourceInfo(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public Downloadable getResourceById(GetResourceByIdRequest request) {
        return resourceManager.getResourceById(request.getId(), request.getForceApplicationDataContentType());
    }

    @NetAPI
    public ResourceInfoDTO updateResourceInfo(UpdateResourceInfoRequest request) {
        return resourceManager.updateResourceInfo(request);
    }

    @NetAPI
    public RemoveResponse removeResourceInfo(RemoveResourceInfoRequest request) {
        return resourceManager.removeResourceInfo(request.getId(), request.getRemoveFile());
    }

    @NetAPI
    public ResourceInfoListDTO listResourceInfos(PaginableFilterWithCriteriaRequest request) {
        return (ResourceInfoListDTO) super.list(request);
    }
}
