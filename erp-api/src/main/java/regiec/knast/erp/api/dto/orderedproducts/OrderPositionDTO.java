/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.orderedproducts;

import regiec.knast.erp.api.dto.producttemplates.ProductTemplateDTO;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class OrderPositionDTO extends OrderPositionBase {

    private String productId_itShouldBeProductReference;
    private ProductTemplateDTO productTemplate;
    private String productTemplateId;
}
