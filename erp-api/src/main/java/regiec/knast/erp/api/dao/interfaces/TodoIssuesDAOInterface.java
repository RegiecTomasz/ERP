/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.TodoIssuesEntity;
import regiec.knast.erp.api.entities.containers.TodoIssuesEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface TodoIssuesDAOInterface extends DAOBaseInterface<TodoIssuesEntity, TodoIssuesEntitiesList> {

}