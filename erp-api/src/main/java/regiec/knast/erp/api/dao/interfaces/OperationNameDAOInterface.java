/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.OperationNameEntity;
import regiec.knast.erp.api.entities.containers.OperationNameEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface OperationNameDAOInterface extends DAOBaseInterface<OperationNameEntity, OperationNameEntitiesList> {

}
