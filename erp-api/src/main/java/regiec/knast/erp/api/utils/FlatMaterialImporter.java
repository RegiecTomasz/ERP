/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import regiec.knast.erp.api.business.apitests.xmlimport.Kopia_x0020_Materiały_w_ZP;
import regiec.knast.erp.api.business.apitests.xmlimport.XMLMaterialRoot;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.model.Norm;
import regiec.knast.erp.api.model.SteelGrade;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class FlatMaterialImporter {

    private static Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public XMLMaterialRoot importFlat() throws JAXBException {

        JAXBContext jc = JAXBContext.newInstance(XMLMaterialRoot.class);
        Unmarshaller u = jc.createUnmarshaller();
        //  XMLMaterialRoot manager = (XMLMaterialRoot) u.unmarshal(ClassLoader.getSystemResourceAsStream("m.xml"));
        InputStream is = FlatMaterialImporter.class.getResourceAsStream("/materiały.xml");
        XMLMaterialRoot materials = (XMLMaterialRoot) u.unmarshal(is);
        return materials;
    }

    public void objectToXML(File file, Object objectToMarshall) throws Exception {
        file.createNewFile();

        JAXBContext jc = JAXBContext.newInstance(XMLMaterialRoot.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(objectToMarshall, file);

    }

    public List<MaterialTemplateEntity> parseFlat(XMLMaterialRoot materials) {

        List<MaterialTemplateEntity> templates = new ArrayList();

        int materialCount = materials.getKopia_x0020_Materiały_w_ZP().size();
        System.out.println("all: " + materials.getKopia_x0020_Materiały_w_ZP().size());

        List<Kopia_x0020_Materiały_w_ZP> notNullNameAndCode = removeNullnameAndCode(materials.getKopia_x0020_Materiały_w_ZP());
        System.out.println("notNullNameAndCode: " + notNullNameAndCode.size());

        List<Kopia_x0020_Materiały_w_ZP> materialsNotDuplicatedByCode = removeDuplicates(notNullNameAndCode);
        System.out.println("materialsNotDuplicatedByCode: " + materialsNotDuplicatedByCode.size());

        List<Kopia_x0020_Materiały_w_ZP> plaskowniki = colectByName(materialsNotDuplicatedByCode, "płaskownik");
        System.out.println("plaskowniki: " + plaskowniki.size());
        plaskowniki.sort(new CustomComparator());

        List<Kopia_x0020_Materiały_w_ZP> plaskownikiDIN = selectByPattern(plaskowniki, "PŁASKOWNIK DIN 59200.*");
        System.out.println("plaskownikiDIN: " + plaskownikiDIN.size());
        System.out.println(gson.toJson(plaskownikiDIN));
        List<MaterialTemplateEntity> dinEntities = cerateDinTempaltes(plaskownikiDIN);
        templates.addAll(dinEntities);

        List<Kopia_x0020_Materiały_w_ZP> plaskownikiDIN_EN = selectByPattern(plaskowniki, "PŁASKOWNIK DIN EN.*");
        System.out.println(gson.toJson(plaskownikiDIN_EN));

        /* System.out.println("plaskowniki count: " + plaskowniki.size());
        System.out.println("material count: " + materialCount);
        System.out.println("plaskownikiNotDuplicated count: " + plaskowniki.size());
         */
        return templates;
    }

    public List<MaterialTemplateEntity> cerateDinTempaltes(List<Kopia_x0020_Materiały_w_ZP> plaskownikiDIN) {
        List<MaterialTemplateEntity> templates = new ArrayList();
        for (Kopia_x0020_Materiały_w_ZP p : plaskownikiDIN) {
            if (!Strings.isNullOrEmpty(p.getNazwa())) {
                final String name = p.getNazwa();
                String[] pp = name.split(" ");
                System.out.println("PP; " + pp.length);
                if (pp.length == 6) {
                    pp[4] = pp[4] + pp[5];
                }
                MaterialTemplateEntity m = new MaterialTemplateEntity();
                m.setMaterialType(null);
                m.setNorm(new Norm(pp[1] + " " + pp[2]));
                m.setSteelGrade(new SteelGrade(pp[3]));
                m.setParticularDimension(pp[4]);
                m.setMaterialCode(p.getNr_x0020_detalu());
                templates.add(m);
            }

        }
        System.out.println(gson.toJson(templates));

        return templates;
    }

    public List<Kopia_x0020_Materiały_w_ZP> selectByPattern(Collection<Kopia_x0020_Materiały_w_ZP> plaskownikiNotDuplicated, String pattern) {
        List<Kopia_x0020_Materiały_w_ZP> ret = new ArrayList();

        for (Kopia_x0020_Materiały_w_ZP p : plaskownikiNotDuplicated) {
            if (p.getNazwa().matches(pattern)) {
                ret.add(p);
            }
        }
        return ret;
    }

    public List<Kopia_x0020_Materiały_w_ZP> colectByName(List<Kopia_x0020_Materiały_w_ZP> materials, String name) {
        List<Kopia_x0020_Materiały_w_ZP> plaskowniki = new ArrayList();
        for (Kopia_x0020_Materiały_w_ZP plaskownik : materials) {
            if (plaskownik.getNazwa().toLowerCase().contains(name)) {
                plaskowniki.add(plaskownik);
            }
        }
        return plaskowniki;
    }

    public List<Kopia_x0020_Materiały_w_ZP> removeNullnameAndCode(List<Kopia_x0020_Materiały_w_ZP> materials) {
        List<Kopia_x0020_Materiały_w_ZP> ret = Lists.newArrayList();
        for (Kopia_x0020_Materiały_w_ZP m : materials) {
            if (m.getNazwa() == null) {
                System.out.println("null nazwa");
            } else if (m.getMateriał() == null) {
                System.out.println("null kod");
            } else {
                ret.add(m);
            }
        }
        return ret;
    }

    public List<Kopia_x0020_Materiały_w_ZP> removeDuplicates(List<Kopia_x0020_Materiały_w_ZP> plaskowniki) {
        List<Kopia_x0020_Materiały_w_ZP> ret = new ArrayList<>();
        for (Kopia_x0020_Materiały_w_ZP p : plaskowniki) {
            if (!ret.contains(p)) {
                ret.add(p);
            }
        }
        return ret;
    }

    public class CustomComparator implements Comparator<Kopia_x0020_Materiały_w_ZP> {

        @Override
        public int compare(Kopia_x0020_Materiały_w_ZP o1, Kopia_x0020_Materiały_w_ZP o2) {
            return o1.getNazwa().compareTo(o2.getNazwa());
        }
    }
}
