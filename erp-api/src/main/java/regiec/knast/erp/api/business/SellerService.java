/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.SellerDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.RestoreRequest;
import regiec.knast.erp.api.dto.seller.AddSellerRequest;
import regiec.knast.erp.api.dto.seller.RemoveSellerRequest;
import regiec.knast.erp.api.dto.seller.SellerDTO;
import regiec.knast.erp.api.dto.seller.SellersListDTO;
import regiec.knast.erp.api.dto.seller.UpdateSellerRequest;
import regiec.knast.erp.api.entities.SellerEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.SellerState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class SellerService extends AbstractService<SellerEntity, SellerDTO> {

    private static final EntityType MY_ENTITY_TYPE = EntityType.SELLER;

    @Inject
    private SellerDAOInterface sellerDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public SellerDTO addSeller(AddSellerRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("name", request.getName(), null, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", request.getNip(), null, MY_ENTITY_TYPE).throww();

        SellerEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new SellerEntity());
        entity.setState(SellerState.NORMAL);
        SellerEntity entity1 = sellerDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public SellerDTO getSeller(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public SellerDTO updateSeller(UpdateSellerRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("name", request.getName(), id, MY_ENTITY_TYPE).throww();
        validator.validateUniqueness("nip", request.getNip(), id, MY_ENTITY_TYPE).throww();
        SellerEntity entity = sellerDAO.get(id);
        validator.validateNotNull(entity).throww();
        SellerEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new SellerEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
        sellerDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeSeller(RemoveSellerRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        SellerEntity sellerEntity = sellerDAO.get(id);
        if (request.getConfirmed()) {
            sellerEntity.setRemoved(true);
            sellerDAO.update(sellerEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(sellerEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public SellerDTO restoreSeller(RestoreRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        SellerEntity sellerEntity = sellerDAO.get(id);
        if (sellerEntity.getRemoved() == false) {
            throw new ConversionErrorWithCode("Restore is avaliable only when seller is removed", ExceptionCodes.CANNOT_RESTORE_SELLER_IN_THIS_STATE);
        }
        sellerEntity.setState(SellerState.NORMAL);
        SellerEntity updated = sellerDAO.update(sellerEntity);
        return updated.convertToDTO();
    }

    @NetAPI
    public SellersListDTO listSellers(PaginableFilterWithCriteriaRequest request) {
        return (SellersListDTO) super.list(request);
    }

}
