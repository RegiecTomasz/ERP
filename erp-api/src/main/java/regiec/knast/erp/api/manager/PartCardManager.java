/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.interfaces.ProductTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.producttemplates.partcard.MaterialPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.dto.producttemplates.partcard.WarePart;
import regiec.knast.erp.api.entities.ProductTemplateEntity;
import regiec.knast.erp.api.entities.containers.ProductTemplateEntitiesList;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class PartCardManager  {

    @Inject
    private ProductTemplateDAOInterface productTemplateDAO;

    public ProductTemplateEntitiesList getPartCardsCompatibileEntitiesWithElementFromCollection(
            EntityType partElementType, String elementId) {
        Constraint constraint;
        if (null == partElementType) {
            throw new ConversionError("There is no part card in ");
        } else {
            switch (partElementType) {
                case MATERIAL_TEMPLATE:
                    constraint = new Constraint("partCard.materialParts.materialTemplate", elementId, ConstraintType.EQUALS);
                    break;
                case WARE_TEMPLATE:
                    constraint = new Constraint("partCard.wareParts.wareTemplate", elementId, ConstraintType.EQUALS);
                    break;
                case SEMIPRODUCT_TEMPLATE:
                    constraint = new Constraint("partCard.semiproductParts.productTemplate", elementId, ConstraintType.EQUALS);
                    break;
                default:
                    throw new ConversionError("There is no part card in: ");
            }
        }
        PaginableFilterWithCriteriaRequest req = PaginableFilterWithCriteriaRequest
                .builder()
                .addCriteriaFilterBuilder()
                .addConstraint(constraint)
                .buildCriteriaFilter()
                .build();
        return productTemplateDAO.list(req);
    }

    public void removeEntityTemplateFromPartCardCompatibileEntity(
            EntityType templateEntityToRemoveType, String templateEntityToRemoveId) {
        final ProductTemplateEntitiesList semiproductTempaltesWithPartCards = getPartCardsCompatibileEntitiesWithElementFromCollection(
                templateEntityToRemoveType,
                templateEntityToRemoveId);
        removeFromPartCardCompatibileEntity( templateEntityToRemoveType, templateEntityToRemoveId, semiproductTempaltesWithPartCards);
    }

    private void removeFromPartCardCompatibileEntity(
                 EntityType myType, String templateEntityToRemoveId, ProductTemplateEntitiesList tempaltesWithPartCardsList) {
        for (ProductTemplateEntity entityTemplateWithPartCardsToRemove : tempaltesWithPartCardsList) {
            boolean removeAllResult = removeFromPartCardCompatibileEntity(myType, templateEntityToRemoveId, entityTemplateWithPartCardsToRemove);
            if (removeAllResult) {                
                productTemplateDAO.update(entityTemplateWithPartCardsToRemove);
            }
        }
    }

    private boolean removeFromPartCardCompatibileEntity(
            EntityType myType, String templateEntityToRemoveId, ProductTemplateEntity entityTemplateWithPartCardsToRemove) {
        if (null != myType) {
            switch (myType) {
                case MATERIAL_TEMPLATE: {
                    List<MaterialPart> toRemove = new ArrayList();
                    for (MaterialPart materialPart : entityTemplateWithPartCardsToRemove.getPartCard().getMaterialParts()) {
                        if (materialPart.getMaterialTemplate().getId().equals(templateEntityToRemoveId)) {
                            toRemove.add(materialPart);
                        }
                    }
                    return entityTemplateWithPartCardsToRemove.getPartCard().getMaterialParts().removeAll(toRemove);
                }
                case WARE_TEMPLATE: {
                    List<WarePart> toRemove = new ArrayList();
                    for (WarePart warePart : entityTemplateWithPartCardsToRemove.getPartCard().getWareParts()) {
                        if (warePart.getWareTemplate().getId().equals(templateEntityToRemoveId)) {
                            toRemove.add(warePart);
                        }
                    }
                    return entityTemplateWithPartCardsToRemove.getPartCard().getWareParts().removeAll(toRemove);
                }
                case SEMIPRODUCT_TEMPLATE: {
                    List<SemiproductPart> toRemove = new ArrayList();
                    for (SemiproductPart semiproductPart : entityTemplateWithPartCardsToRemove.getPartCard().getSemiproductParts()) {
                        if (semiproductPart.getProductTemplate().getId().equals(templateEntityToRemoveId)) {
                            toRemove.add(semiproductPart);
                        }
                    }
                    return entityTemplateWithPartCardsToRemove.getPartCard().getSemiproductParts().removeAll(toRemove);
                }
                default:
                    return false;
            }
        } else {
            return false;
        }
    }
}
