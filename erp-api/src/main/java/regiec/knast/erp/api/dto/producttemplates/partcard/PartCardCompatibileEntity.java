/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import regiec.knast.erp.api.interfaces.EntityBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface PartCardCompatibileEntity extends EntityBase {

    public PartCard getPartCard();

    public void setPartCard(PartCard partCard);

    public String getId();
}
