/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.MaterialDemandDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.materialDemand.AddMaterialDemandRequest;
import regiec.knast.erp.api.dto.materialDemand.GenerateMaterialDemandRequest;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandDTO;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandsListDTO;
import regiec.knast.erp.api.dto.materialDemand.SatisfyDemandsRequest;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.manager.orders.GenerateMaterialDemandsTransformer;
import regiec.knast.erp.api.manager.satisfier.MaterialDemandSatisfier;
import regiec.knast.erp.api.model.states.MaterialDemandState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialDemandService extends AbstractService<MaterialDemandEntity, MaterialDemandDTO> {

    @Inject
    private MaterialDemandDAOInterface materialDemandDAO;
    @Inject
    private Validator validator;
    @Inject
    private GenerateMaterialDemandsTransformer materialDemandManager;
    @Inject
    private MaterialDemandSatisfier materialDemandSatisfier;

    @NetAPI
    public MaterialDemandDTO addMaterialDemand(AddMaterialDemandRequest request) {
        validator.validateNotNull(request).throww();
        MaterialDemandEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new MaterialDemandEntity());
        entity.setState(MaterialDemandState.NEW);
        MaterialDemandEntity entity1 = materialDemandDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public MaterialDemandDTO getMaterialDemand(BasicGetRequest request) {
        return super.get(request);
    }
    
    @NetAPI
    public MaterialDemandsListDTO listMaterialDemands(PaginableFilterWithCriteriaRequest request) {
        return (MaterialDemandsListDTO) super.list(request);
    }

    @NetAPI
    public Object generateMaterialDemands(GenerateMaterialDemandRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getProductionOrderId()).throww();
        return materialDemandManager.generateMaterialDemands(request.getProductionOrderId());
    }

    @NetAPI
    public MaterialDemandDTO satisfyDemands(SatisfyDemandsRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getId()).throww();
        validator.validateNotNullAndNotEmpty(request.getMaterialId(), ExceptionCodes.SATISFY_DEMANDS__MATERIAL_IS_NOT_SELECTED).throww();
        return materialDemandSatisfier.satisfyDemands(request.getId(), request.getMaterialId());
    }
}
