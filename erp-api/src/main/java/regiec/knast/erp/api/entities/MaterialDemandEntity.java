/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.materialDemand.MaterialDemandDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.MaterialDemandBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("materialDemands")
public class MaterialDemandEntity extends MaterialDemandBase implements EntityBase, ConvertableEntity {

    @Id
    private String id;
    private MaterialTemplateEntity materialTemplate;
    private ProductTemplateEntity detalTemplate;
    private OrderEntity order;
    @ErpIdEntity(entityClass = MaterialEntity.class) 
    private List<String> usedMaterialsIds = new ArrayList();

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public MaterialDemandDTO convertToDTO() {
        MaterialDemandDTO dto = BeanUtilSilent.translateBeanAndReturn(this, new MaterialDemandDTO());
        dto.setMaterialTemplate(this.getMaterialTemplate().toMaterialTemplateDTObasisc());
        dto.setDetalTemplate(
                ImmutableMap.of(
                        "id", this.getDetalTemplate().getId(),
                        "productNo", this.getDetalTemplate().getProductNo(),
                        "name", this.getDetalTemplate().getName()
                ));
        dto.setOrder(
                ImmutableMap.of(
                        "id", this.getOrder().getId(),
                        "nr", this.getOrder().getNr()
                ));
        dto.setRecipient(
                ImmutableMap.of(
                        "id", this.getOrder().getRecipient().getId(),
                        "name", this.getOrder().getRecipient().getName()
                ));
        return dto;
    }

}
