/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto;

import org.jaxygen.dto.collections.PaginableBaseRequestDTO;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CriteriaFilter;
import regiec.knast.erp.api.model.Sort;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.EqualsAndHashCode
public class PaginableFilterWithCriteriaRequest extends PaginableBaseRequestDTO {

    private Sort sort;
    private Projection projection;
    private CriteriaFilter criteriaFilters;

    public static PaginableFilterWithCriteriaRequestBuilder builder() {
        return new PaginableFilterWithCriteriaRequestBuilder();
    }

    public static class PaginableFilterWithCriteriaRequestBuilder {

        private CriteriaFilterBuilder criteriaFilterBuilder;

        private PaginableFilterWithCriteriaRequest request = new PaginableFilterWithCriteriaRequest();

        public PaginableFilterWithCriteriaRequestBuilder() {
        }

        public PaginableFilterWithCriteriaRequestBuilder sort(Sort sort) {
            this.request.setSort(sort);
            return this;
        }

        public PaginableFilterWithCriteriaRequestBuilder projectioinInclude(String... fields) {
            this.request.setProjection(Projection.include(fields));
            return this;
        }

        public PaginableFilterWithCriteriaRequestBuilder projectioinExclude(String... fields) {
            this.request.setProjection(Projection.exclude(fields));
            return this;
        }

        public CriteriaFilterBuilder addCriteriaFilterBuilder() {
            criteriaFilterBuilder = new CriteriaFilterBuilder(this);
            return criteriaFilterBuilder;
        }

        public PaginableFilterWithCriteriaRequest build() {
            return request;
        }

        public CriteriaFilterBuilder addCriteriaFilter(CriteriaFilter criteriaFilter) {
            this.request.setCriteriaFilters(criteriaFilter);
            return criteriaFilterBuilder;
        }
    }

    public static class CriteriaFilterBuilder {

        private CriteriaFilter criteriaFilter1 = new CriteriaFilter();
        private PaginableFilterWithCriteriaRequestBuilder uberBuilder;

        private CriteriaFilterBuilder(PaginableFilterWithCriteriaRequestBuilder uberBuilder) {
            this.uberBuilder = uberBuilder;
        }

        public static CriteriaFilterBuilder builder(PaginableFilterWithCriteriaRequestBuilder uberBuilder) {
            return new CriteriaFilterBuilder(uberBuilder);
        }

        public CriteriaFilterBuilder addConstraint(String field, String value, ConstraintType constraintType) {
            this.criteriaFilter1.getConstraints().add(new Constraint(field, value, constraintType));
            return this;
        }

        public CriteriaFilterBuilder addConstraint(Constraint constraint) {
            this.criteriaFilter1.getConstraints().add(constraint);
            return this;
        }

        public PaginableFilterWithCriteriaRequestBuilder buildCriteriaFilter() {
            uberBuilder.addCriteriaFilter(criteriaFilter1);
            return uberBuilder;
        }
    }
}
