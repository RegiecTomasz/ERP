/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.deliveryorder;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class MoveDeliveryOrderPositionToOtherProviderRequest {

    private String deliveryOrderId;
    private String positionId;
    
    private String otherProviderId;
    private String otherAssotrmentId;
}
