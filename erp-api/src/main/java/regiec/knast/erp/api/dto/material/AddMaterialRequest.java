/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.material;

import regiec.knast.erp.api.model.CuttingSize;
import regiec.knast.erp.api.model.bases.MaterialBase;
import regiec.knast.sheet.nesting.core.Bin;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddMaterialRequest extends MaterialBase {

    private String materialTemplateId;
    private CuttingSize length;
    private CreationContext creationContext;

    public AddMaterialRequest(String materialTemplateId, CuttingSize length, int count, CuttingSize reservedLength, CuttingSize leftLength, Double percentUsage, Bin sheetBin,
            String comments, String localization, String deliveryOrderId, CreationContext creationContext) {
        super(count, reservedLength, leftLength, percentUsage, sheetBin, comments, localization, deliveryOrderId);
        this.materialTemplateId = materialTemplateId;
        this.length = length;
        this.creationContext = creationContext;
    }

    public AddMaterialRequest(String materialTemplateId, CuttingSize length, int count, CreationContext creationContext) {
        this.materialTemplateId = materialTemplateId;
        this.length = length;
        this.creationContext = creationContext;
        this.setCount(count);
    }

            
    public static enum CreationContext{
        MANUAL,
        SHOPPING_CART;
    } 
}
