/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.productionjob.ProductionJobDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.ProductionJobBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("productionJobs")
public class ProductionJobEntity extends ProductionJobBase implements EntityBase, ConvertableEntity<ProductionJobDTO> {

    @Id
    private String id;
    @Reference
    private ProductionJobEntity parent;
    @Reference
    private List<ProductionJobEntity> children = new ArrayList();
    @Reference
    private ProductEntity productEntity;
    private boolean removed = false;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public ProductionJobDTO convertToDTO() {
        ProductionJobDTO dto = BeanUtilSilent.translateBeanAndReturn(this, new ProductionJobDTO());
        dto.setParent(null);
//        if (this.getChildren() != null) {
        for (ProductionJobEntity childJob : this.getChildren()) {
            childJob.getProductEntity().getProductTemplate().setPartCard(null);
            childJob.setParent(null);
            dto.getChildren().add(childJob.convertToDTO());
        }
//        }
//        if (this.getProductEntity()!= null) {
        dto.setDetal(this.getProductEntity().convertToDetalDTO());
//        }
        return dto;

    }

}
