/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ShoppingCartDAOInterface;
import regiec.knast.erp.api.entities.ShoppingCartEntity;
import regiec.knast.erp.api.entities.containers.ShoppingCartEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ShoppingCartDAO extends DAOBase<ShoppingCartEntity, ShoppingCartEntitiesList> implements ShoppingCartDAOInterface {

    private final Datastore datastore;

    @Inject
    public ShoppingCartDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

    @Override
    public ShoppingCartEntity getExistingOne() {
        ShoppingCartEntity singleton = get("shoppingCartSingleton");
        if (singleton == null) {
            singleton = add(new ShoppingCartEntity(), "shoppingCartSingleton");
        }
        return singleton;
    }

    @Override
    public ShoppingCartEntity resetShoppingCart() {
        ShoppingCartEntity singleton = add(new ShoppingCartEntity(), "shoppingCartSingleton");
        return singleton;
    }
}
