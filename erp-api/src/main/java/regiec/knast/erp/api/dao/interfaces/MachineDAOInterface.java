/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.MachineEntity;
import regiec.knast.erp.api.entities.containers.MachineEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface MachineDAOInterface extends DAOBaseInterface<MachineEntity, MachineEntitiesList> {

}
