/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Map;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTypeDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.MaterialTypeEntity;
import regiec.knast.erp.api.entities.containers.MaterialTemplateEntitiesList;
import regiec.knast.erp.api.model.Constraint;
import regiec.knast.erp.api.model.ConstraintType;
import regiec.knast.erp.api.model.CriteriaFilter;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class MaterialTypeUtil {

    @Inject
    private MaterialDAOInterface materialDAO;

    @Inject
    private MaterialTypeDAOInterface materialTypeDAO;

    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;

    @Inject
    private MaterialTemplateUtil materialTemplateUtil;

    @Inject
    private Validator validator;

    public Map<String, Object> buildRemoveConsequencesMap(MaterialTypeEntity entity) {
        Map<String, Object> consequencesMap = ImmutableMap.<String, Object>builder()
                .put("entityType", entity.getClass().getSimpleName())
                .put("id", entity.getId())
                .put("name", entity.getName())
                .put("baseDimension", entity.getBaseDimensionUnit())
                .put("leafs", new ArrayList<>())
                .build();
        final MaterialTemplateEntitiesList materialTemplateEntities = getMaterialTemplatesWithMaterialType(entity.getId());
        for (MaterialTemplateEntity materialTemplateEntity : materialTemplateEntities) {
            Map<String, Object> materialTemplateConsequencesMap = materialTemplateUtil.buildRemoveConsequencesMap(materialTemplateEntity);
            ((ArrayList) consequencesMap.get("leafs")).add(materialTemplateConsequencesMap);
        }
        return consequencesMap;
    }

    public MaterialTemplateEntitiesList getMaterialTemplatesWithMaterialType(String materialTypeEntityId) {
        PaginableFilterWithCriteriaRequest req = new PaginableFilterWithCriteriaRequest();
        CriteriaFilter criteriaFilter = new CriteriaFilter();
        req.setCriteriaFilters(criteriaFilter);
        criteriaFilter.setConstraints(Lists.newArrayList(new Constraint("materialType.id", materialTypeEntityId, ConstraintType.EQUALS)));
        return materialTemplateDAO.list(req);
    }

}
