/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.resourceinfo;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class GetResourceByIdRequest {

    private String id;
    private boolean forceApplicationDataContentType = false;

    public boolean getForceApplicationDataContentType() {
        return forceApplicationDataContentType;
    }

}
