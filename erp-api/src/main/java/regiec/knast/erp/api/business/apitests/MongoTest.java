/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business.apitests;

import com.google.inject.Inject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.dao.interfaces.MaterialDAOInterface;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.entities.MaterialTemplateEntity;
import regiec.knast.erp.api.entities.containers.MaterialTemplateEntitiesList;
import regiec.knast.erp.api.model.CriteriaFilter;
import regiec.knast.erp.api.model.Sort;
import regiec.knast.erp.api.model.SortOrder;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class MongoTest extends GuiceInjector {

    @Inject
    private MaterialDAOInterface materialDAO;

    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;

    private String filterString = "{\n"
            + "      \"value\": \"AND\",\n"
            + "      \"constraints\": [\n"
            + "        {\n"
            + "          \"field\": \"materialType.id\",\n"
            + "          \"value\": \"58b73eb94c1926221c03a215\",\n"
            + "          \"constraintType\": \"EQUALS\"\n"
            + "        }\n"
            + "      ],\n"
            + "      \"criteriaFilters\": [\n"
            + "        {\n"
            + "          \"value\": \"OR\",\n"
            + "          \"constraints\": [\n"
            + "            {\n"
            + "              \"field\": \"particularDimension\",\n"
            + "              \"value\": \"160\",\n"
            + "              \"constraintType\": \"LIKE\"\n"
            + "            },\n"
            + "            {\n"
            + "              \"field\": \"materialCode\",\n"
            + "              \"value\": \"4501020350\",\n"
            + "              \"constraintType\": \"EQUALS\"\n"
            + "            }\n"
            + "          ],\n"
            + "          \"criteriaFilters\": []\n"
            + "        }\n"
            + "      ]\n"
            + "    }";

    private String filterString2 = "{\n"
            + "	\"value\": \"AND\",\n"
            + "	\"constraints\": ["
            + "         {\n"
            + "                  \"field\": \"particularDimension\",\n"
            + "                  \"value\": \"160\",\n"
            + "                  \"constraintType\": \"LIKE\",\n"
            + "                  \"constraints\": []\n"
            + "                },\n"
            + "                {\n"
            + "                  \"field\": \"materialCode\",\n"
            + "                  \"value\": \"4501020350\",\n"
            + "                  \"constraintType\": \"EQUALS\",\n"
            + "                  \"constraints\": []\n"
            + "                }\n"
            + "	]\n"
            + "}";

    public static void main(String[] args) throws ObjectCreateError {
        Object create = ObjectBuilderFactory.instance().create(MongoTest.class);
        MongoTest mongoTest = (MongoTest) create;
        try {
            mongoTest.testGenereteToFile();
        } catch (Exception ex) {
            Logger.getLogger(MongoTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void testGenereteToFile() throws ConversionError {
        CriteriaFilter criteriaFilters = g.fromJson(filterString, CriteriaFilter.class);
        PaginableFilterWithCriteriaRequest request = new PaginableFilterWithCriteriaRequest();
        request.setCriteriaFilters(criteriaFilters);
        request.setSort(new Sort("materialCode", SortOrder.ASCENDING));
        request.setPage(0);
        request.setPageSize(0);
        MaterialTemplateEntitiesList ret = materialTemplateDAO.list(request);
        for (MaterialTemplateEntity materialTemplateEntity : ret) {
            System.out.println("materiałTempalte: " + materialTemplateEntity.getMaterialCode());
        }
    }
}
