/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import regiec.knast.erp.api.dto.producttemplates.partcard.AddMaterialPart;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter 
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class AddProductTemplateMaterialPartRequest {

    private String productTemplateId;    
    private AddMaterialPart materialPart;            
}
