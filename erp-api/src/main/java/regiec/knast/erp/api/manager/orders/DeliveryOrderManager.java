/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.orders;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import regiec.knast.erp.api.dao.interfaces.MaterialTemplateDAOInterface;
import regiec.knast.erp.api.dao.interfaces.ProgramIndexesDAOInterface;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.bases.DeliveryOrderBase;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author fbrzuzka
 */
@Singleton
public class DeliveryOrderManager {

    @Inject
    private ProgramIndexesDAOInterface programIndexesDAO;
    @Inject
    private MaterialTemplateDAOInterface materialTemplateDAO;
    @Inject
    private Validator validator;

    public String getNextDeliveryOrderNo() {
        Long nextIndex = programIndexesDAO.nextIndex(EntityType.DELIVERY_ORDER);

        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String ret = date + "-" + String.valueOf(nextIndex);
        return ret;
    }

    public String generateDeliveryOrderMessage(DeliveryOrderBase deliveryOrder) {

        StringBuilder builder = new StringBuilder();
        // nazwa firmy z jakiej, Jangra BIS, Plus        
        // do jakiej firmy        
        // co na zamowieniu        
        // prosze o potwierrdzenie dostepności i terminu realizacji

        /*
        Dzień dobry
        
        Jangra BIS składa zamówienie na:
        
        Proszę o potwierdzenie dostępności, ceny i terminu realizacji.
         */
        
        MyArray col1 = new MyArray();
        MyArray col2 = new MyArray();
        MyArray col3 = new MyArray();
        MyArray col4 = new MyArray();
        col1.add("Typ");
        col2.add("Wymiar");
        col3.add("Długość");
        col4.add("Ilość");
        builder.append("Dzień dobry.\n")
                .append("\n")
                .append("Jangra BIS składa zamówienie na:\n")
                .append("\n");

        for (DeliveryOrderPosition position : deliveryOrder.getPositions()) {
            String name = position.getMaterialName();
            String particularDimension = position.getMaterialParticularDimention();
            String salesLength = position.getSalesLength().getOrigSize();
            String count = String.valueOf(position.getCount());
            
            col1.add(name);
            col2.add(particularDimension);
            col3.add(salesLength);            
            col4.add(count);            
        }
        col1.normalizeLengths();
        col2.normalizeLengths();
        col3.normalizeLengths();
        col4.normalizeLengths();
        for (int i = 0; i < col1.size(); i++) {
            builder.append("\t").append(col1.get(i)).append(col2.get(i)).append(col3.get(i)).append(col4.get(i)).append("\n");
        }        
        builder
                .append("\n")
                .append("Proszę o potwierdzenie dostępności, ceny i terminu realizacji.\n")
                .append("\n")
                .append("Pozdrawiam,\n")
                .append("Mariola Schoener\n");
        return builder.toString();
    }
    
    class MyArray extends ArrayList<String>{
        public int maxLength = 0;

        @Override
        public boolean add(String e) {
            if(e.length() > maxLength){
                maxLength = e.length();
            }
            return super.add(e); 
        }
        
        public void normalizeLengths(){
            for (int i = 0; i < this.size(); i++) {
                String e = this.get(i);
                String newe = StringUtils.rightPad(e, maxLength + 3, " ");
                this.set(i, newe);
            }
        }
        
        
    }
}
