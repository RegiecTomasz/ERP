/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ProgramIndexesDAOInterface;
import regiec.knast.erp.api.dto.otherstuff.ProgramIdexesEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.utils.IDUtil;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProgramIndexesDAO implements ProgramIndexesDAOInterface {

    private final Datastore datastore;

    @Inject
    public ProgramIndexesDAO(MongoInterface driver) {
        this.datastore = driver.getDatastore();
    }

    @Override
    public Long nextIndex(EntityType indexType) {
        ProgramIdexesEntity entity = getProperIndex(indexType);
        entity = initIfNull(entity, indexType);

        entity.setGenerationDate(new Date());
        entity.setIndexValue(entity.getIndexValue() + 1);
        datastore.save(entity);
        return entity.getIndexValue();
    }

    @Override
    public Long nextIndexOrReserInNewYear(EntityType indexType) {
        ProgramIdexesEntity entity = getProperIndex(indexType);
        entity = initIfNull(entity, indexType);

        int actualYear = Calendar.getInstance().get(Calendar.YEAR);
        Calendar calPrev = Calendar.getInstance();
        calPrev.setTime(entity.getGenerationDate());
        int previousIndexYear = calPrev.get(Calendar.YEAR);
        if (actualYear > previousIndexYear) {
            entity.setIndexValue(0L);
        }
        entity.setGenerationDate(new Date());
        entity.setIndexValue(entity.getIndexValue() + 1);
        datastore.save(entity);
        return entity.getIndexValue();
    }
    
    @Override
    public Long nextIndexOrReserInNewDay(EntityType indexType) {
        ProgramIdexesEntity entity = getProperIndex(indexType);
        entity = initIfNull(entity, indexType);

        int actualDay = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        Calendar calPrev = Calendar.getInstance();
        calPrev.setTime(entity.getGenerationDate());
        int previousIndexDay = calPrev.get(Calendar.DAY_OF_YEAR);
        if (actualDay != previousIndexDay) {
            entity.setIndexValue(0L);
        }
        entity.setGenerationDate(new Date());
        entity.setIndexValue(entity.getIndexValue() + 1);
        datastore.save(entity);
        return entity.getIndexValue();
    }

    private ProgramIdexesEntity getProperIndex(EntityType indexType) {
        return datastore.createQuery(ProgramIdexesEntity.class).disableValidation().field("indexType").equal(indexType).get();
    }

    private ProgramIdexesEntity initIfNull(ProgramIdexesEntity entity, EntityType indexType) {
        if (entity == null) {
            entity = new ProgramIdexesEntity();
            entity.setIndexType(indexType);
            entity.setId(IDUtil.nextId_());
            entity.setIndexValue(0L);
            entity.setGenerationDate(new Date());
        }
        return entity;
    }

}
