/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.ProfessionGroupDAOInterface;
import regiec.knast.erp.api.dao.interfaces.WorkerDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.RestoreRequest;
import regiec.knast.erp.api.dto.workers.AddWorkerRequest;
import regiec.knast.erp.api.dto.workers.FireWorkerRequest;
import regiec.knast.erp.api.dto.workers.HireWorkerRequest;
import regiec.knast.erp.api.dto.workers.RemoveWorkerRequest;
import regiec.knast.erp.api.dto.workers.UpdateWorkerRequest;
import regiec.knast.erp.api.dto.workers.WorkerDTO;
import regiec.knast.erp.api.dto.workers.WorkersListDTO;
import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.WorkerState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.manager.WorkerUtil;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class WorkerService extends AbstractService<WorkerEntity, WorkerDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.WORKER;

    @Inject
    private WorkerDAOInterface workerDAO;
    @Inject
    private Validator validator;
    @Inject
    private WorkerUtil workerUtil;
    @Inject
    private ProfessionGroupDAOInterface professionGroupDAO;

    @NetAPI
    public WorkerDTO addWorker(AddWorkerRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getProfessionGroupId(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "professionGroupId").throww();
        WorkerEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new WorkerEntity());

        ProfessionGroupEntity professionGroup = professionGroupDAO.get(request.getProfessionGroupId());
        validator.validateNotNull(professionGroup, ExceptionCodes.NULL_VALUE, "professionGroup").throww();
        entity.setProfessionGroup(professionGroup);
        entity.setState(WorkerState.HIRED);
        String minorNo = workerUtil.calculateWorkerMinorNo(professionGroup);
        entity.setWorkerMinorNo(minorNo);
        entity.setWorkerCode(workerUtil.calculateWorkerCode(professionGroup, minorNo));

        WorkerEntity entity1 = workerDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public WorkerDTO getWorker(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public WorkerDTO updateWorker(UpdateWorkerRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateNotNullAndNotEmpty(request.getProfessionGroupId(), ExceptionCodes.NULL_OR_EMPTY_STRING_VALUE, "professionGroupId").throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        WorkerEntity entity = workerDAO.get(id);
        validator.validateNotNull(entity).throww();

        WorkerEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new WorkerEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
        if (!entity.getProfessionGroup().getId().equals(request.getProfessionGroupId())) {
            ProfessionGroupEntity professionGroup = professionGroupDAO.get(request.getProfessionGroupId());
            validator.validateNotNull(professionGroup, ExceptionCodes.NULL_VALUE, "professionGroup").throww();
            entity.setProfessionGroup(professionGroup);
        }

        workerDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeWorker(RemoveWorkerRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        WorkerEntity workerEntity = workerDAO.get(id);
        if (request.getConfirmed()) {
            workerEntity.setRemoved(true);
            workerDAO.update(workerEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(workerEntity));
            return new RemoveResponse(false, null);
        }
    }

    @NetAPI
    public WorkerDTO restoreWorker(RestoreRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        WorkerEntity workerEntity = workerDAO.get(id);
        if (workerEntity.getRemoved() == false) {
            throw new ConversionErrorWithCode("Restore is avaliable only if worker is removed", ExceptionCodes.CANNOT_RESTORE_WORKER_IN_THIS_STATE);
        }
        workerEntity.setState(WorkerState.RESTORED);
        WorkerEntity updated = workerDAO.update(workerEntity);
        return updated.convertToDTO();
    }

    @NetAPI
    public WorkerDTO fireWorker(FireWorkerRequest request) {
        validator.validateNotNull(request, "FireWorkerRequest").throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        WorkerEntity workerEntity = workerDAO.get(id);
        validator.validateNotNull(workerEntity, "workerEntity").throww();
        if (workerEntity.getRemoved()) {
            throw new ConversionErrorWithCode("You must first restore worker to fire him", ExceptionCodes.CANNOT_RESTORE_WORKER_IN_THIS_STATE);
        }
        workerEntity.setState(WorkerState.FIRED);
        WorkerEntity updated = workerDAO.update(workerEntity);
        return updated.convertToDTO();
    }

    @NetAPI
    public WorkerDTO hireWorker(HireWorkerRequest request) {
        validator.validateNotNull(request, "HireWorkerRequest").throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        WorkerEntity workerEntity = workerDAO.get(id);
        validator.validateNotNull(workerEntity, "workerEntity").throww();
        if (workerEntity.getRemoved()) {
            throw new ConversionErrorWithCode("You must first restore worker to hire him", ExceptionCodes.CANNOT_RESTORE_WORKER_IN_THIS_STATE);
        }
        workerEntity.setState(WorkerState.HIRED);
        WorkerEntity updated = workerDAO.update(workerEntity);
        return updated.convertToDTO();
    }

    @NetAPI
    public WorkersListDTO listWorkers(PaginableFilterWithCriteriaRequest request) {
        return (WorkersListDTO) super.list(request);
    }
}
