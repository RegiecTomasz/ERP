/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.interfaces;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface EntityBase extends IndexableEntity{

    public String getId();

    public void setId(String id);

}
