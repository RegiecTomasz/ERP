/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.entities;

import java.util.Date;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import regiec.knast.erp.api.dto.waretemplates.WareTemplateDTO;
import regiec.knast.erp.api.dto.waretypes.WareTypeDTO;
import regiec.knast.erp.api.interfaces.ConvertableEntity;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.model.bases.WareTemplateBase;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@Entity("wareTemplates")
public class WareTemplateEntity extends WareTemplateBase implements EntityBase, ConvertableEntity {

    @Id
    private String id;

    private WareTypeEntity wareType;

    @PrePersist
    void saveDate() {
        setLastModificationDate(new Date());
        if (getCreationDate() == null) {
            setCreationDate(getLastModificationDate());
        }
    }

    @Override
    public WareTemplateDTO convertToDTO() {
        WareTemplateDTO dTO = BeanUtilSilent.translateBeanAndReturn(this, new WareTemplateDTO());
        if (this.getWareType() != null) {
            dTO.setWareType((WareTypeDTO)this.getWareType().convertToDTO());
            dTO.setWareTypeId(dTO.getWareType().getId());
        }
        return dTO;
    }
    
}
