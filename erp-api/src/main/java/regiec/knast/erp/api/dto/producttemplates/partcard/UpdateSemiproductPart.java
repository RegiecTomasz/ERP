/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class UpdateSemiproductPart extends AddSemiproductPart {

    private String positionId;

    public UpdateSemiproductPart(String positionId, String productTemplateId, Integer count, String positionOnTechDraw, String comments) {
        super(productTemplateId, count, positionOnTechDraw, comments);
        this.positionId = positionId;
    }

    public UpdateSemiproductPart(String positionId, String productTemplateId) {
        super(productTemplateId);
        this.positionId = positionId;
    }
    
}
