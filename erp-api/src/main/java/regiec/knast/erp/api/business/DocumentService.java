/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.business;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.jaxygen.annotations.NetAPI;
import regiec.knast.erp.api.dao.interfaces.DocumentDAOInterface;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.RemoveResponse;
import regiec.knast.erp.api.dto.RestoreRequest;
import regiec.knast.erp.api.dto.document.AddDocumentRequest;
import regiec.knast.erp.api.dto.document.DocumentDTO;
import regiec.knast.erp.api.dto.document.DocumentsListDTO;
import regiec.knast.erp.api.dto.document.RemoveDocumentRequest;
import regiec.knast.erp.api.dto.document.UpdateDocumentRequest;
import regiec.knast.erp.api.entities.DocumentEntity;
import regiec.knast.erp.api.model.EntityType;
import regiec.knast.erp.api.model.states.DocumentState;
import regiec.knast.erp.api.utils.BeanUtilSilent;
import regiec.knast.erp.api.validation.ConversionErrorWithCode;
import regiec.knast.erp.api.validation.ExceptionCodes;
import regiec.knast.erp.api.validation.Validator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@Singleton
public class DocumentService extends AbstractService<DocumentEntity, DocumentDTO> {

    public final static EntityType MY_ENTITY_TYPE = EntityType.DOCUMENT;
    @Inject
    private DocumentDAOInterface documentDAO;
    @Inject
    private Validator validator;

    @NetAPI
    public DocumentDTO addDocument(AddDocumentRequest request) {
        validator.validateNotNull(request).throww();
        validator.validateUniqueness("name", request.getName(), null, MY_ENTITY_TYPE).throww();

        DocumentEntity entity = BeanUtilSilent.translateBeanAndReturn(request, new DocumentEntity());
        entity.setState(DocumentState.NORMAL);
        DocumentEntity entity1 = documentDAO.add(entity);
        return entity1.convertToDTO();
    }

    @NetAPI
    public DocumentDTO getDocument(BasicGetRequest request) {
        return super.get(request);
    }

    @NetAPI
    public DocumentDTO updateDocument(UpdateDocumentRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        validator.validateUniqueness("name", request.getName(), id, MY_ENTITY_TYPE).throww();
//        validator.validateUniqueness("nip", request.getNip(), id, MY_ENTITY_TYPE).throww();
        DocumentEntity entity = documentDAO.get(id);
        validator.validateNotNull(entity).throww();
        DocumentEntity updatedEntity = BeanUtilSilent.translateBeanAndReturn(entity, new DocumentEntity());
        BeanUtilSilent.translateBean(request, updatedEntity);
        documentDAO.update(updatedEntity);
        return updatedEntity.convertToDTO();
    }

    @NetAPI
    public RemoveResponse removeDocument(RemoveDocumentRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();
        DocumentEntity documentEntity = documentDAO.get(id);
        if (request.getConfirmed()) {
            documentEntity.setRemoved(true);
            documentDAO.update(documentEntity);
            return new RemoveResponse(true, null);
        } else {
            //   return new RemoveResponse(false, buildRemoveConsequencesMap(documentEntity));
            return new RemoveResponse(false, null);
        }

    }

    @NetAPI
    public DocumentDTO restoreDocument(RestoreRequest request) {
        validator.validateNotNull(request).throww();
        String id = request.getId();
        validator.validateNotNull(id).throww();

        DocumentEntity documentEntity = documentDAO.get(id);
        if (documentEntity.getRemoved() == false) {
            throw new ConversionErrorWithCode("Restore is avaliable only when document is removed", ExceptionCodes.CANNOT_RESTORE_DOCUMENT_IN_THIS_STATE);
        }
        documentEntity.setState(DocumentState.NORMAL);
        documentDAO.update(documentEntity);
        return documentEntity.convertToDTO();
    }

    @NetAPI
    public DocumentsListDTO listDocuments(PaginableFilterWithCriteriaRequest request) {
        return (DocumentsListDTO) super.list(request);
    }
}
