/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates.partcard;

import java.util.ArrayList;
import java.util.List;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.dto.waretemplates.WareTemplateDTO;
import regiec.knast.erp.api.entities.WareTemplateEntity;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WarePart extends WarePartBase {

    private String positionId;

    @Reference
    private WareTemplateEntity wareTemplate;

    public WarePartDTO warePartToDTO() {
        final WarePartDTO warePartDTO = new WarePartDTO();
        BeanUtilSilent.translateBean(this, warePartDTO);
        warePartDTO.setWareTemplate((WareTemplateDTO) this.getWareTemplate().convertToDTO());
        warePartDTO.setWareTemplateId(warePartDTO.getWareTemplate().getId());
        return warePartDTO;
    }

    public static List<WarePartDTO> warePartsListToDto(List<WarePart> wareParts) {
        List<WarePartDTO> ret = new ArrayList();
        if (wareParts != null) {
            for (final WarePart warePart : wareParts) {
                ret.add(warePart.warePartToDTO());
            }
        }
        return ret;
    }

    public String getWareSizeTypeOrEmptyString() {
        if (this.getWareTemplate() != null
                && this.getWareTemplate().getWareType() != null
                && this.getWareTemplate().getWareType().getSizeType() != null) {
            return this.getWareTemplate().getWareType().getSizeType().toString();

        } else {
            return "";
        }
    }
}
