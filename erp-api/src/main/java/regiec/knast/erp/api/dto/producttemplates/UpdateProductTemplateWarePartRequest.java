/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.producttemplates;

import regiec.knast.erp.api.dto.producttemplates.partcard.UpdateWarePart;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class UpdateProductTemplateWarePartRequest{
    
    private String productTemplateId;    
    private UpdateWarePart warePart;    
}
