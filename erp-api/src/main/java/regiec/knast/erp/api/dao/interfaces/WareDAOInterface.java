/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao.interfaces;

import regiec.knast.erp.api.entities.WareEntity;
import regiec.knast.erp.api.entities.containers.WareEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface WareDAOInterface extends DAOBaseInterface<WareEntity, WareEntitiesList> {

}
