/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.model.wzgenerator;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
//@lombok.Getter
//@lombok.Setter
@lombok.NoArgsConstructor
//@lombok.AllArgsConstructor
public class SellerInfo extends CompanyInfoBase{

    public SellerInfo(String name, String street, String postal, String city) {
        super(name, street, postal, city);
    }

}
