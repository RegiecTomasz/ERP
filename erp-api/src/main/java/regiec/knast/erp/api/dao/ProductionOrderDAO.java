/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dao;

import com.google.inject.Inject;
import org.mongodb.morphia.Datastore;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dao.interfaces.ProductionOrderDAOInterface;
import regiec.knast.erp.api.entities.ProductionOrderEntity;
import regiec.knast.erp.api.entities.containers.ProductionOrderEntitiesList;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductionOrderDAO extends DAOBase<ProductionOrderEntity, ProductionOrderEntitiesList> implements ProductionOrderDAOInterface {

    private final Datastore datastore;

    private final Class entityClass = ProductionOrderEntity.class;

    @Inject
    public ProductionOrderDAO(MongoInterface driver) {
        super(driver);
        this.datastore = driver.getDatastore();
    }

}
