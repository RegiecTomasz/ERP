/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.dto.product;

import regiec.knast.erp.api.dto.producttemplates.ProductTemplateDTO;
import regiec.knast.erp.api.dto.recipient.RecipientDTO;
import regiec.knast.erp.api.model.bases.ProductBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
@lombok.Getter
@lombok.Setter
public class ProductDTO extends ProductBase {

    private String id;
    private ProductTemplateDTO productTemplate;
    private String productTemplateId;
    private RecipientDTO recipient;
}
