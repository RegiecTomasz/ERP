/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.dto.orderedproducts;

import java.util.Date;
import regiec.knast.erp.api.entities.OrderEntity;
import regiec.knast.erp.api.entities.ProductionOrderEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public interface OrdinaryOrderPosition extends OrderPositionInterface {

    Date getDeliveryDate();

    void setDeliveryDate(Date deliveryDate);

    String getProductionOrderId();

    void setProductionOrderId(String productionOrderId);

    
    public default ProductionOrderEntity createOrdinaryProductionOrder(OrderEntity orderEntity, DispositionOrderPosition.ProductionOrderNoGenerator generator){        
        ProductionOrderEntity productionOrderEntity = createBaseOfProductionOrder(orderEntity, generator);
        
        productionOrderEntity.setDeliveryDate(this.getDeliveryDate());
        productionOrderEntity.setCount(this.getCount());
        return productionOrderEntity;
    }
}
