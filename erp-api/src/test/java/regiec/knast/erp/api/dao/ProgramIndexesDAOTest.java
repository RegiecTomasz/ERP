/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.dao;

import com.mongodb.gridfs.GridFS;
import java.util.Calendar;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.FieldEnd;
import org.mongodb.morphia.query.Query;
import regiec.knast.erp.api.dao.driver.MongoInterface;
import regiec.knast.erp.api.dto.otherstuff.ProgramIdexesEntity;
import regiec.knast.erp.api.model.EntityType;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProgramIndexesDAOTest {

    private Datastore datastoreMock;
    private Query queryMock;
    private FieldEnd fieldEndMock;

    class TestMongoInterface implements MongoInterface {

        @Override
        public Datastore getDatastore() {
            return datastoreMock;
        }

        @Override
        public GridFS getGridFS() {
            return null;
        }

        @Override
        public void closeConnections() {
        }
    }

    @Before
    public void setUp() {
        datastoreMock = Mockito.mock(Datastore.class);
        queryMock = Mockito.mock(Query.class);
        fieldEndMock = Mockito.mock(FieldEnd.class);

        Mockito.when(datastoreMock.save(Matchers.any(ProgramIdexesEntity.class))).thenReturn(null); // do nothing
        Mockito.when(datastoreMock.createQuery(Matchers.any())).thenReturn(queryMock); // do nothing
        Mockito.when(queryMock.disableValidation()).thenReturn(queryMock); // do nothing
        Mockito.when(queryMock.field(Matchers.any())).thenReturn(fieldEndMock); // do nothing
        Mockito.when(fieldEndMock.equal(Matchers.any())).thenReturn(queryMock); // do nothing

    }

    @Test
    public void shall_resetIndexIfNewYear() {
        // given
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2015);
        ProgramIdexesEntity oldYear = new ProgramIdexesEntity();
        oldYear.setId("111122223333444455556666");
        oldYear.setIndexType(EntityType.INVOICE);
        oldYear.setIndexValue(5555l);
        oldYear.setGenerationDate(cal.getTime());
        Long expected = new Long(1);

        // when
//        Mockito.when( datastoreMock.createQuery(ProgramIdexesEntity.class).disableValidation().field("indexType").equal(EntityType.INVOICE).get()).thenreturn
        Mockito.when(queryMock.get()).thenReturn(oldYear); // do nothing
        ProgramIndexesDAO programIndexesDAO = new ProgramIndexesDAO(new TestMongoInterface());
        Long result = programIndexesDAO.nextIndexOrReserInNewYear(EntityType.INVOICE);

        // then 
        Assertions.assertThat(result).isEqualTo(expected);
    }

}
