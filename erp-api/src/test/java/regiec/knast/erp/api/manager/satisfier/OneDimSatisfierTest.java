/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.manager.satisfier;

import com.google.inject.AbstractModule;
import static org.assertj.core.api.Assertions.assertThat;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;

/**
 *
 * @author fbrzuzka
 */
public class OneDimSatisfierTest {

    private OneDimSatisfier oneDimSatisfier;

    @Before
    public void setUp() throws ObjectCreateError {
         AbstractModule module = new TestBinderBuilder(false)
//        .override(MaterialDAOInterface.class, mMock)
//        .override(MaterialDemandDAOInterface.class, mdMock)
//        .override(ProductionOrderManager.class, pomMock)
//        .override(DeliveryOrderPositionsManager.class, deliveryOrderPositionsManagerMock)
        .build();
        TestGuiceBuilder objBuilder = new TestGuiceBuilder(module);
        oneDimSatisfier = (OneDimSatisfier) objBuilder.create(OneDimSatisfier.class);
    }

    @Test
    public void testHowManyDemandsOnMaterial() {
        assertThat(oneDimSatisfier.calculateHowManyDemandsPerMaterial(6.0, 1.5)).isEqualTo(4);
        assertThat(oneDimSatisfier.calculateHowManyDemandsPerMaterial(6.0, 2.0)).isEqualTo(3);
        assertThat(oneDimSatisfier.calculateHowManyDemandsPerMaterial(6.0, 2.1)).isEqualTo(2);
        assertThat(oneDimSatisfier.calculateHowManyDemandsPerMaterial(6.0, 3.1)).isEqualTo(1);
        assertThat(oneDimSatisfier.calculateHowManyDemandsPerMaterial(6.0, 6.1)).isEqualTo(0);
    }

    @Test
    public void testHowManyMaterialsNeedForWholeDemand() {
        assertThat(oneDimSatisfier.calculateHowManyNeedMaterialForWholeDemand(1, 1)).isEqualTo(1);
        assertThat(oneDimSatisfier.calculateHowManyNeedMaterialForWholeDemand(2, 1)).isEqualTo(2);

        assertThat(oneDimSatisfier.calculateHowManyNeedMaterialForWholeDemand(1, 2)).isEqualTo(1);
        assertThat(oneDimSatisfier.calculateHowManyNeedMaterialForWholeDemand(2, 2)).isEqualTo(1);
        assertThat(oneDimSatisfier.calculateHowManyNeedMaterialForWholeDemand(3, 2)).isEqualTo(2);
        assertThat(oneDimSatisfier.calculateHowManyNeedMaterialForWholeDemand(4, 2)).isEqualTo(2);
    }

}
