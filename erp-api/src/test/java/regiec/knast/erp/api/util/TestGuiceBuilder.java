package regiec.knast.erp.api.util;


import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.jaxygen.objectsbuilder.ObjectBuilder;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;

/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
/**
 *
 * @author fbrzuzka
 */
public class TestGuiceBuilder implements ObjectBuilder{
    
    private Injector injector = null;

    public TestGuiceBuilder(AbstractModule module) {
         injector = Guice.createInjector(module);
    }

    public final <E extends Object> E create(final Class<E> clazz) throws ObjectCreateError {
        return injector.getInstance(clazz);
    }

    
}
