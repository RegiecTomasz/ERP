/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.manager.orders;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.dto.deliveryorder.DeliveryOrderPosition;
import regiec.knast.erp.api.entities.DeliveryOrderEntity;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.bases.DeliveryOrderBase;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class DeliveryOrderManagerTest {
    
    public DeliveryOrderManagerTest() {
    }
    
    @Before
    public void setUp() {
    }
    public DeliveryOrderPosition create(String name, Integer count, String particularDimension, Dimension salesLengthDim,  String salesLength){
        DeliveryOrderPosition ret = new DeliveryOrderPosition();
        ret.setMaterialName(name);
        ret.setCount(count);
        ret.setMaterialParticularDimention(particularDimension);
        ret.setSalesLength(CuttingSizeManager.createCuttingSize(salesLengthDim, salesLength));
        return ret;
    }

    @Test
    public void shall_generateEmailMessage() {
        
        DeliveryOrderManager manager = new DeliveryOrderManager();
        DeliveryOrderBase deliveryOrder = new DeliveryOrderEntity();
        List<DeliveryOrderPosition> positions = new ArrayList();
        deliveryOrder.setPositions(positions);
        positions.add(create("taka nazwa tego profila", 3, "12x133x5", Dimension.dł, "12"));
        positions.add(create("Blacha", 8, "152x135" , Dimension.AxB, "1,5x3"));
        positions.add(create("Płaskownik", 13, "12x5" , Dimension.dł, "6"));
        positions.add(create("Płaskownik", 5, "12x5" , Dimension.dł, "12"));
        
        String result = manager.generateDeliveryOrderMessage(deliveryOrder);
        System.out.println(result);
    }
    
}
