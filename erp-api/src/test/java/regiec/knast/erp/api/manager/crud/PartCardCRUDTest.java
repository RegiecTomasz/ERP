/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2018
 */
package regiec.knast.erp.api.manager.crud;

import com.google.common.collect.Lists;
import com.google.inject.AbstractModule;
import java.math.BigDecimal;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.business.MaterialTemplateService;
import regiec.knast.erp.api.business.MaterialTypeService;
import regiec.knast.erp.api.business.ProductTemplateService;
import regiec.knast.erp.api.business.WareTemplateService;
import regiec.knast.erp.api.business.WareTypeService;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.materialtemplates.AddMaterialTemplateRequest;
import regiec.knast.erp.api.dto.materialtemplates.MaterialTemplateDTO;
import regiec.knast.erp.api.dto.materialtypes.AddMaterialTypeRequest;
import regiec.knast.erp.api.dto.materialtypes.MaterialTypeDTO;
import regiec.knast.erp.api.dto.producttemplates.*;
import regiec.knast.erp.api.dto.producttemplates.partcard.*;
import regiec.knast.erp.api.dto.waretemplates.AddWareTemplateRequest;
import regiec.knast.erp.api.dto.waretemplates.WareTemplateDTO;
import regiec.knast.erp.api.dto.waretypes.AddWareTypeRequest;
import regiec.knast.erp.api.dto.waretypes.WareTypeDTO;
import regiec.knast.erp.api.model.Dimension;
import regiec.knast.erp.api.model.MoneyERP;
import regiec.knast.erp.api.model.Norm;
import regiec.knast.erp.api.model.ProductTemplateType;
import regiec.knast.erp.api.model.SalesOffer;
import regiec.knast.erp.api.model.WareDimension;
import regiec.knast.erp.api.model.bases.RecipientBaseBrief;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class PartCardCRUDTest {

    private static final String PRODUCT_NO_1 = "product_no_1";
    private static final String RECIPIENT_1_ID = "000000000000000000000000";
    private static final String RECIPIENT_1_NAME = "recipient_1_name";
    private static MoneyERP price = MoneyERP.create(new BigDecimal(12.0), "PLN");
    private static final List<SalesOffer> SALE_OFFERS = Lists.newArrayList(new SalesOffer(new RecipientBaseBrief(RECIPIENT_1_NAME, RECIPIENT_1_ID), price, 0.0));
    

    private TestGuiceBuilder objBuilder;
    private ProductTemplateService productTemplateService;
    private MaterialTemplateService materialTemplateService;
    private WareTemplateService wareTemplateService;
    private MaterialTypeService materialTypeService;
    private WareTypeService wareTypeService;

    @Before
    public void setUp() throws ObjectCreateError {
        AbstractModule module = new TestBinderBuilder(true)
                .build();
        objBuilder = new TestGuiceBuilder(module);
        ObjectBuilderFactory.configure(objBuilder);

        productTemplateService = objBuilder.create(ProductTemplateService.class);
        materialTemplateService = objBuilder.create(MaterialTemplateService.class);
        wareTemplateService = objBuilder.create(WareTemplateService.class);
        materialTypeService = objBuilder.create(MaterialTypeService.class);
        wareTypeService = objBuilder.create(WareTypeService.class);

    }

    private ProductTemplateDTO addPT(String name, String no, ProductTemplateType saleType, List<SalesOffer> salesOffers) {
        AddProductTemplateRequest request = new AddProductTemplateRequest();
        request.setName(name);
        request.setProductNo(no);
        request.setType(saleType);
        request.setSalesOffers(salesOffers);
        ProductTemplateDTO productTemplate = productTemplateService.addProductTemplate(request);
        return productTemplate;
    }

    private ProductTemplateDTO addPT_NO_SALE(String name, String no) {
        return addPT(name, no, ProductTemplateType.NO_SALE, null);
    }

    private ProductTemplateDTO addPT_SALE(String name, String no) {
        return addPT(name, no, ProductTemplateType.SALE, SALE_OFFERS);
    }

    private MaterialTemplateDTO addMaterialTemplate(String name, String code) {
        MaterialTypeDTO materialType = addMaterialType(name);
        AddMaterialTemplateRequest request = new AddMaterialTemplateRequest();

        request.setMaterialCode(code);
        request.setMaterialTypeId(materialType.getId());
        request.setParticularDimension("0,3x0.45");
        MaterialTemplateDTO materialTemplate = materialTemplateService.addMaterialTemplate(request);
        return materialTemplate;
    }

    private MaterialTypeDTO addMaterialType(String name) {
        AddMaterialTypeRequest request = new AddMaterialTypeRequest();
        request.setName(name);
        request.setBaseDimensionTemplate(Dimension.AxB);
        request.setBaseDimensionUnit("m x m");
        request.setCuttingDimensionTemplate(Dimension.dł);
        request.setCuttingDimensionUnit("m");

        MaterialTypeDTO materialType = materialTypeService.addMaterialType(request);
        return materialType;
    }

    private WareTemplateDTO addWareTemplate(String name, String code) {
        WareTypeDTO wareType = addWareType(name);
        AddWareTemplateRequest request = new AddWareTemplateRequest();

        request.setWareCode(code);
        request.setWareTypeId(wareType.getId());
        request.setNorm(new Norm("12m3"));
        WareTemplateDTO wareTemplate = wareTemplateService.addWareTemplate(request);
        return wareTemplate;
    }

    private WareTypeDTO addWareType(String name) {
        AddWareTypeRequest request = new AddWareTypeRequest();
        request.setName(name);
        request.setDimension(WareDimension.AxB);

        WareTypeDTO wareType = wareTypeService.addWareType(request);
        return wareType;
    }

    @Test
    public void shall_createPT_SALE() throws ObjectCreateError {
        // given
        String givenName = "name1";

        // when
        ProductTemplateDTO productTemplate = addPT_SALE(givenName, PRODUCT_NO_1);
        ProductTemplateDTO result = productTemplateService.getProductTemplate(new BasicGetRequest(productTemplate.getId()));

        // then
        Assertions.assertThat(productTemplate).isNotNull();
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(productTemplate.getName()).isEqualTo(givenName);
        Assertions.assertThat(productTemplate).isEqualToComparingFieldByFieldRecursively(result);
    }

    @Test
    public void shall_createPT_NO_SALE() throws ObjectCreateError {
        // given
        String givenName = "name1";

        // when
        ProductTemplateDTO productTemplate = addPT_NO_SALE(givenName, PRODUCT_NO_1);
        ProductTemplateDTO result = productTemplateService.getProductTemplate(new BasicGetRequest(productTemplate.getId()));

        // then
        Assertions.assertThat(productTemplate).isNotNull();
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(productTemplate.getName()).isEqualTo(givenName);
        Assertions.assertThat(productTemplate).isEqualToComparingFieldByFieldRecursively(result);
    }

    @Test
    public void shall_refresh_materialsWhenAddToPartCard() throws ObjectCreateError {
        // given
        String givenName = "name1";
        MaterialTemplateDTO mt = addMaterialTemplate("Profile pedofile", "1234");
        ProductTemplateDTO productTemplate = addPT_SALE(givenName, PRODUCT_NO_1);

        // when
        AddProductTemplateMaterialPartRequest request = new AddProductTemplateMaterialPartRequest(
                productTemplate.getId(), new AddMaterialPart(mt.getId(), "2", "3", 1, "", "010"));
        productTemplateService.addProductTemplateMaterialPart(request);

        // then
        MaterialTemplateDTO mtResult = materialTemplateService.getMaterialTemplate(new BasicGetRequest(mt.getId()));

        Assertions.assertThat(mtResult).isNotNull();
        Assertions.assertThat(mtResult.getMaterialUsages()).isNotNull();
        Assertions.assertThat(mtResult.getMaterialUsages()).hasSize(1);
        Assertions.assertThat(mtResult.getMaterialUsages()).containsExactly("numer: product_no_1, nazwa: name1");
    }

    @Test
    public void shall_refresh_WaresWhenAddToPartCard() throws ObjectCreateError {
        // given
        String givenName = "name2";
        WareTemplateDTO wt = addWareTemplate("Profile pedofile", "1234");
        ProductTemplateDTO productTemplate = addPT_NO_SALE(givenName, PRODUCT_NO_1);

        // when
        AddProductTemplateWarePartRequest request = new AddProductTemplateWarePartRequest(
                productTemplate.getId(), new AddWarePart(wt.getId(), 1, "", "010"));
        productTemplateService.addProductTemplateWarePart(request);

        // then
        WareTemplateDTO wtResult = wareTemplateService.getWareTemplate(new BasicGetRequest(wt.getId()));

        Assertions.assertThat(wtResult).isNotNull();
        Assertions.assertThat(wtResult.getWareUsages()).isNotNull();
        Assertions.assertThat(wtResult.getWareUsages()).hasSize(1);
        Assertions.assertThat(wtResult.getWareUsages()).containsExactly("numer: product_no_1, nazwa: name2");
    }

    @Test
    public void shall_refresh_ProductsWhenAddToPartCard() throws ObjectCreateError {
        // given
        String givenName = "name3";
        ProductTemplateDTO pt = addPT_NO_SALE("Profile pedofile", "1234");
        ProductTemplateDTO productTemplate = addPT_NO_SALE(givenName, PRODUCT_NO_1);

        // when
        AddProductTemplateProductPartRequest request = new AddProductTemplateProductPartRequest(
                productTemplate.getId(), new AddSemiproductPart(pt.getId(), 1, "", "010"));
        productTemplateService.addProductTemplateProductPart(request);

        // then
        ProductTemplateDTO ptResult = productTemplateService.getProductTemplate(new BasicGetRequest(pt.getId()));

        Assertions.assertThat(ptResult).isNotNull();
        Assertions.assertThat(ptResult.getPtUsages()).isNotNull();
        Assertions.assertThat(ptResult.getPtUsages()).hasSize(1);
        Assertions.assertThat(ptResult.getPtUsages()).containsExactly("numer: product_no_1, nazwa: name3");
    }

    /*
    ---------------------------------- UPDATE--------------------------------
     */
    @Test
    public void shall_notRefresh_materialsWhenUpdateToPartCard() throws ObjectCreateError {
        // given
        String givenName = "name1";
        MaterialTemplateDTO mt = addMaterialTemplate("Profile pedofile", "1234");
        ProductTemplateDTO productTemplate = addPT_NO_SALE(givenName, PRODUCT_NO_1);

        AddProductTemplateMaterialPartRequest addRequest = new AddProductTemplateMaterialPartRequest(
                productTemplate.getId(), new AddMaterialPart(mt.getId(), "2", "3", 1, "", "010"));
        MaterialPartListResponse addResponse = productTemplateService.addProductTemplateMaterialPart(addRequest);

        // when
        UpdateProductTemplateMaterialPartRequest request = new UpdateProductTemplateMaterialPartRequest(
                productTemplate.getId(), new UpdateMaterialPart(addResponse.getMaterialParts().get(0).getPositionId(), mt.getId(), "2", "3", 1, "", "010"));
        productTemplateService.updateProductTemplateMaterialPart(request);

        // then
        MaterialTemplateDTO mtResult = materialTemplateService.getMaterialTemplate(new BasicGetRequest(mt.getId()));

        Assertions.assertThat(mtResult).isNotNull();
        Assertions.assertThat(mtResult.getMaterialUsages()).isNotNull();
        Assertions.assertThat(mtResult.getMaterialUsages()).hasSize(1);
        Assertions.assertThat(mtResult.getMaterialUsages()).containsExactly("numer: product_no_1, nazwa: name1");
    }

    @Test
    public void shall_notRefresh_waresWhenUpdateToPartCard() throws ObjectCreateError {
        // given
        String givenName = "name1";
        WareTemplateDTO wt = addWareTemplate("Profile pedofile", "1234");
        ProductTemplateDTO productTemplate = addPT_NO_SALE(givenName, PRODUCT_NO_1);

        AddProductTemplateWarePartRequest addRequest = new AddProductTemplateWarePartRequest(
                productTemplate.getId(), new AddWarePart(wt.getId(), 1, "", "010"));
        WarePartListResponse addResponse = productTemplateService.addProductTemplateWarePart(addRequest);

        // when
        UpdateProductTemplateWarePartRequest request = new UpdateProductTemplateWarePartRequest(
                productTemplate.getId(), new UpdateWarePart(addResponse.getWareParts().get(0).getPositionId(), wt.getId(), 1, "", "010"));
        productTemplateService.updateProductTemplateWarePart(request);

        // then
        WareTemplateDTO wtResult = wareTemplateService.getWareTemplate(new BasicGetRequest(wt.getId()));

        Assertions.assertThat(wtResult).isNotNull();
        Assertions.assertThat(wtResult.getWareUsages()).isNotNull();
        Assertions.assertThat(wtResult.getWareUsages()).hasSize(1);
        Assertions.assertThat(wtResult.getWareUsages()).containsExactly("numer: product_no_1, nazwa: name1");
    }

    @Test
    public void shall_notRefresh_productsWhenUpdateToPartCard() throws ObjectCreateError {
        // given
        String givenName = "name1";
        ProductTemplateDTO pt = addPT_NO_SALE("Profile pedofile", "1234");
        ProductTemplateDTO productTemplate = addPT_NO_SALE(givenName, PRODUCT_NO_1);

        AddProductTemplateProductPartRequest addRequest = new AddProductTemplateProductPartRequest(
                productTemplate.getId(), new AddSemiproductPart(pt.getId(), 1, "", "010"));
        SemiproductPartListResponse addResponse = productTemplateService.addProductTemplateProductPart(addRequest);

        // when
        UpdateProductTemplateProductPartRequest request = new UpdateProductTemplateProductPartRequest(
                productTemplate.getId(), new UpdateSemiproductPart(addResponse.getSemiproductParts().get(0).getPositionId(), pt.getId(), 1, "", "010"));
        productTemplateService.updateProductTemplateProductPart(request);

        // then
        ProductTemplateDTO ptResult = productTemplateService.getProductTemplate(new BasicGetRequest(pt.getId()));

        Assertions.assertThat(ptResult).isNotNull();
        Assertions.assertThat(ptResult.getPtUsages()).isNotNull();
        Assertions.assertThat(ptResult.getPtUsages()).hasSize(1);
        Assertions.assertThat(ptResult.getPtUsages()).containsExactly("numer: product_no_1, nazwa: name1");
    }

}
