/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.utils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.annotations.Reference;
import regiec.knast.erp.api.exceptions.ERPInternalError;
import regiec.knast.erp.api.interfaces.EntityBase;
import regiec.knast.erp.api.utils.ErpPrinter.TablePrinter;
import regiec.knast.erp.api.utils.anotations.ErpIdEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class EPRSafetyCheckTest {

    @Before
    public void setUp() {
    }

    @Test
    public void testSomeMethod() {
        ERPSafetyCheck safetyCheck = new ERPSafetyCheck();
        Map<Class, List<ReflectionUtil.FieldInfo>> map = safetyCheck.getFieldsAnotatedByErpIdEntity();
        for (Map.Entry<Class, List<ReflectionUtil.FieldInfo>> entry : map.entrySet()) {
            Class<? extends EntityBase> clazz = entry.getKey();
            List<ReflectionUtil.FieldInfo> result = entry.getValue();
            if (!result.isEmpty()) {
                System.out.println("\n---- for: " + clazz.getSimpleName());
                for (ReflectionUtil.FieldInfo fieldInfo : result) {
                    Field field = fieldInfo.field;
                    boolean isAnnotated = field.isAnnotationPresent(ErpIdEntity.class);
                    if (!isAnnotated) {
                        throw new ERPInternalError("Cannot start ERP, Validation exception, class: " + clazz.getSimpleName() + " has (inner) field: " + fieldInfo.fieldPath()
                                + " that has \'Id\' in name, but it is not annotated with: " + ErpIdEntity.class.getSimpleName() + " annotation");
                    } else {
                        System.out.print("\tfield: " + fieldInfo.fieldPath() + " ");
                        System.out.println("is annotated by " + field.getAnnotation(ErpIdEntity.class).entityClass().getSimpleName());
                        System.out.println("");
                    }
                }
            }
        }
    }

    @Test
    public void test_recursiveWithPredicate_Id_inAllEntities() {
        System.out.println("#### There are String Id fields in other entities");
        Set<Class<? extends EntityBase>> allEntitiesClasses = ReflectionUtil.getAllEntitiesClasses();
        for (Class<? extends EntityBase> clazz : allEntitiesClasses) {
            Predicate<? super Field> namePredicate = (f -> f.getName().contains("Id"));
            Predicate<? super Field> stopInnerPredicate = (f -> EntityBase.class.isAssignableFrom(f.getType()));
            List<ReflectionUtil.FieldInfo> result = ReflectionUtil.getRecursiveFieldsByPredicate(clazz, namePredicate, stopInnerPredicate);
            printFieldInfoList(result, clazz);
        }
    }

    @Test
    public void test_recursiveWithPredicate_Entities_inAllEntities() {
        System.out.println("#### There are entities fields in other entities");
        Set<Class<? extends EntityBase>> allEntitiesClasses = ReflectionUtil.getAllEntitiesClasses();
        for (Class<? extends EntityBase> clazz : allEntitiesClasses) {
            Predicate<? super Field> entityFieldPredicate = (f -> EntityBase.class.isAssignableFrom(f.getType()));
            Predicate<? super Field> stopInnerPredicate = (f -> EntityBase.class.isAssignableFrom(f.getType()));

            List<ReflectionUtil.FieldInfo> result = ReflectionUtil.getRecursiveFieldsByPredicate(clazz, entityFieldPredicate, stopInnerPredicate);
            printFieldInfoList(result, clazz);
        }
    }

    private void printFieldInfoList(List<ReflectionUtil.FieldInfo> result, Class clazz) {
        if (!result.isEmpty()) {
            System.out.println("\n---- for: " + clazz.getSimpleName());
            for (ReflectionUtil.FieldInfo fieldInfo : result) {
                System.out.println("field: " + fieldInfo.fieldPath());
            }
        }
    }

    /**
     * print all fields in Entities classes that are annotates with erpIdEntity
     */
    @Test
    public void print_allFieldsAnnotatedWithEntityId() {
        TablePrinter tablePrinter = new TablePrinter("simple references in entities");
        ERPSafetyCheck safetyCheck = new ERPSafetyCheck();
        Set<Class<? extends EntityBase>> allEntitiesClasses = ReflectionUtil.getAllEntitiesClasses();
        for (Class<? extends EntityBase> clazz : allEntitiesClasses) {
            List<ReflectionUtil.FieldInfo> fieldInfos = safetyCheck.getFieldsAnotatedByErpIdEntityInClass(clazz);
            for (ReflectionUtil.FieldInfo fi : fieldInfos) {
                ErpIdEntity annotation = fi.field.getAnnotation(ErpIdEntity.class);
                tablePrinter.addRow(annotation.entityClass().getSimpleName(), " is simple referenced in class: ", fi.clazz.getSimpleName(), " on field: ", fi.fieldPath());
            }
        }
        tablePrinter.print();
    }

    @Test
    public void print_allEntitiesFields() {
        TablePrinter tablePrinter = new TablePrinter("enities refrences in entities");
        ERPSafetyCheck safetyCheck = new ERPSafetyCheck();
        Set<Class<? extends EntityBase>> allEntitiesClasses = ReflectionUtil.getAllEntitiesClasses();
        for (Class<? extends EntityBase> clazz : allEntitiesClasses) {
            List<ReflectionUtil.FieldInfo> fieldInfos = safetyCheck.getFieldsEntityOfType(clazz);
            for (ReflectionUtil.FieldInfo fi : fieldInfos) {
                if (fi.field.isAnnotationPresent(Reference.class)) {
                    tablePrinter.addRow(fi.field.getType().getSimpleName(), " is @Reference entity object in class: ", fi.clazz.getSimpleName(), " on field: ", fi.fieldPath());
                } else {
                    tablePrinter.addRow(fi.field.getType().getSimpleName(), " is entity object referenced in class: ", fi.clazz.getSimpleName(), " on field: ", fi.fieldPath());
                }
            }
        }
        tablePrinter.print();
    }
}
