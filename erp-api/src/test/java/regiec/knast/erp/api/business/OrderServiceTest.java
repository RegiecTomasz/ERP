/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiec.knast.erp.api.business;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.business.apitests.AllServicesInjector;
import regiec.knast.erp.api.dto.BasicGetRequest;
import regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest;
import regiec.knast.erp.api.dto.material.*;
import regiec.knast.erp.api.dto.materialDemand.*;
import regiec.knast.erp.api.dto.materialtemplates.*;
import regiec.knast.erp.api.dto.materialtypes.*;
import regiec.knast.erp.api.dto.operationName.*;
import regiec.knast.erp.api.dto.order.*;
import regiec.knast.erp.api.dto.orderedproducts.UpdateOrderPosition;
import regiec.knast.erp.api.dto.productionorder.ProductionOrdersListDTO;
import regiec.knast.erp.api.dto.producttemplates.*;
import regiec.knast.erp.api.dto.producttemplates.partcard.*;
import regiec.knast.erp.api.dto.producttemplates.productionplan.AddProductionPlan;
import regiec.knast.erp.api.dto.professionGroup.*;
import regiec.knast.erp.api.dto.recipient.*;
import regiec.knast.erp.api.manager.CuttingSizeManager;
import regiec.knast.erp.api.model.*;
import regiec.knast.erp.api.model.bases.RecipientBaseBrief;
import regiec.knast.erp.api.model.states.*;
import regiec.knast.erp.api.util.TestBinderBuilder;
import regiec.knast.erp.api.util.TestGuiceBuilder;
import regiec.knast.erp.api.utils.BeanUtilSilent;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class OrderServiceTest extends AllServicesInjector {

    private static final boolean runWithFakeMongo = true;

    private static final String PRODUCT_NO_1 = "product_no_1";
    private static final String RECIPIENT_1_ID = "000000000000000000000000";
    private static final String RECIPIENT_1_NAME = "recipient_1_name";
    private MoneyERP money = MoneyERP.create(new BigDecimal(1.0), "PLN");
    private static MoneyERP price = MoneyERP.create(new BigDecimal(12.0), "PLN");
    private static final List<SalesOffer> SALE_OFFERS = Lists.newArrayList(new SalesOffer(new RecipientBaseBrief(RECIPIENT_1_NAME, RECIPIENT_1_ID), price, 0.0));
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private TestGuiceBuilder objBuilder;

    @Before
    public void setUp() throws ObjectCreateError {
        AbstractModule module = new TestBinderBuilder(runWithFakeMongo)
                .build();
        objBuilder = new TestGuiceBuilder(module);
        ObjectBuilderFactory.configure(objBuilder);
        super.initServices();
    }

    private ProductTemplateDTO addPT_NO_SALE(String name, String no) {
        return addPT(name, no, ProductTemplateType.NO_SALE, null);
    }

    private ProductTemplateDTO addPT_SALE(String name, String no) {
        return addPT(name, no, ProductTemplateType.SALE, SALE_OFFERS);
    }

    private ProductTemplateDTO addPT(String name, String no, ProductTemplateType saleType, List<SalesOffer> salesOffers) {
        AddProductTemplateRequest request = new AddProductTemplateRequest();
        request.setName(name);
        request.setProductNo(no);
        request.setType(saleType);
        request.setSalesOffers(salesOffers);
        ProductTemplateDTO productTemplate = productTemplateService.addProductTemplate(request);
        return productTemplate;
    }

    private MaterialTemplateDTO addMaterialTemplate(String name, String code) {
        MaterialTypeDTO materialType = addMaterialType(name);
        AddMaterialTemplateRequest request = new AddMaterialTemplateRequest();

        request.setMaterialCode(code);
        request.setMaterialTypeId(materialType.getId());
        request.setParticularDimension("0,3x0.45");
        MaterialTemplateDTO materialTemplate = materialTemplateService.addMaterialTemplate(request);
        return materialTemplate;
    }

    private MaterialTypeDTO addMaterialType(String name) {
        AddMaterialTypeRequest request = new AddMaterialTypeRequest();
        request.setName(name);
        request.setBaseDimensionTemplate(Dimension.AxB);
        request.setBaseDimensionUnit("m x m");
        request.setCuttingDimensionTemplate(Dimension.dł);
        request.setCuttingDimensionUnit("m");

        MaterialTypeDTO materialType = materialTypeService.addMaterialType(request);
        return materialType;
    }

    public OperationNameDTO addOperationName(String operationName) {
        OperationNameDTO addOperationName = operationNameService.addOperationName(new AddOperationNameRequest(operationName, null));
        // validate operation name exists
        OperationNameDTO operation = operationNameService.getOperationName(new BasicGetRequest(addOperationName.getId()));
        Assertions.assertThat(operation).isNotNull();
        Assertions.assertThat(operation.getId()).isEqualTo(addOperationName.getId());
        Assertions.assertThat(operation.getName()).isEqualTo(operationName);
        Assertions.assertThat(operation.getState()).isEqualTo(OperationNameState.NORMAL);
        return operation;
    }

    public ProfessionGroupDTO addProfessionGroup(String professionGroupCode, String professionGroupName, List<String> operationNamesIds) {
        ProfessionGroupDTO addProfessionGroup = professionGroupService.addProfessionGroup(
                new AddProfessionGroupRequest(operationNamesIds, professionGroupCode, professionGroupName, null, null));
        String professionGroupId = addProfessionGroup.getId();
        // validate profession group exists
        ProfessionGroupDTO professionGroup = professionGroupService.getProfessionGroup(new BasicGetRequest(professionGroupId));
        Assertions.assertThat(professionGroup).isNotNull();
        Assertions.assertThat(professionGroup.getName()).isEqualTo(professionGroupName);
        Assertions.assertThat(professionGroup.getCode()).isEqualTo(professionGroupCode);
        Assertions.assertThat(professionGroup.getAllowOperations()).isNotNull();
        Assertions.assertThat(professionGroup.getAllowOperations()).hasSize(operationNamesIds.size());
//        Assertions.assertThat(professionGroup.getAllowOperations().get(0).getName()).isEqualTo(operationNameName);
        Assertions.assertThat(professionGroup.getAllowOperations()).usingRecursiveFieldByFieldElementComparator().containsAll(addProfessionGroup.getAllowOperations());
        return professionGroup;
    }

    @Test
    public void test_addOrderWithPositions() {

        String operationNameName = "ciecie";
        String professionGroupName = "prof1";
        String professionGroupCode = "01";
        String ptName = "profilek produkt";
        String materialTypeName = "Profile";
        String materialTemplateCode = "material1";
        String recipientName = "recipient1";
        String orderNo = "order1";

        String productTemplateId = null;
        String materialTypetId = null;
        String materialTemplateId = null;
        String materialId = null;
        String recipientId = null;
        String orderId = null;
        String productionOrderId = null;
        String materialDemandId = null;
        String operationNameId = null;
        String professionGroupId = null;

        // add operation name
        OperationNameDTO operation = addOperationName(operationNameName);
        operationNameId = operation.getId();

        // add profession group
        ProfessionGroupDTO professionGroup = addProfessionGroup(professionGroupCode, professionGroupName, Lists.newArrayList(operationNameId));
        professionGroupId = professionGroup.getId();

        // add product template
        ProductTemplateDTO ptDTO = addPT_SALE(ptName, PRODUCT_NO_1);
        productTemplateId = ptDTO.getId();
        // validate product template exists
        ProductTemplateDTO profilekProduktEntity = productTemplateService.getProductTemplate(new BasicGetRequest(productTemplateId));
        Assertions.assertThat(profilekProduktEntity).isNotNull();
        Assertions.assertThat(profilekProduktEntity.getName()).isEqualTo(ptName);
//        Assertions.assertThat(profilekProduktEntity).isEqualToComparingFieldByFieldRecursively(result);

        // add material1 to database        
        MaterialTemplateDTO mtDTO1 = addMaterialTemplate(materialTypeName, materialTemplateCode);
        materialTypetId = mtDTO1.getMaterialTypeId();
        materialTemplateId = mtDTO1.getId();
        // validate material template exists
        MaterialTemplateDTO mtDTO = materialTemplateService.getMaterialTemplate(new BasicGetRequest(mtDTO1.getId()));
        Assertions.assertThat(mtDTO).isNotNull();
        Assertions.assertThat(mtDTO.getMaterialType().getName()).isEqualTo(materialTypeName);
        Assertions.assertThat(mtDTO.getMaterialCode()).isEqualTo(materialTemplateCode);
        Assertions.assertThat(mtDTO.getMaterialType().getId()).isEqualTo(mtDTO.getMaterialTypeId());

        // add material1 to produc's part card
        AddMaterialPart addMaterialPart = new AddMaterialPart(mtDTO.getId(), "2.9", "3.0", 2, "comments", "010");
        MaterialPartListResponse materialPartListResponse1 = productTemplateService.addProductTemplateMaterialPart(new AddProductTemplateMaterialPartRequest(productTemplateId, addMaterialPart));
        // validate product has material part in part card        
        ProductTemplateDTO pt_withMaterialPart = productTemplateService.getProductTemplate(new BasicGetRequest(productTemplateId));
        Assertions.assertThat(pt_withMaterialPart).isNotNull();
        Assertions.assertThat(pt_withMaterialPart.getPartCard()).isNotNull();
        Assertions.assertThat(pt_withMaterialPart.getPartCard().getMaterialParts()).isNotNull();
        Assertions.assertThat(pt_withMaterialPart.getPartCard().getMaterialParts()).hasSize(1);
        Assertions.assertThat(pt_withMaterialPart.getPartCard().getMaterialParts()).usingRecursiveFieldByFieldElementComparator().containsAll(materialPartListResponse1.getMaterialParts());

        // Add ciecie to production plan
        AddProductionPlan prodPlanItem = new AddProductionPlan(operationNameId, professionGroupId, 0.2, 0.3, 0.5, 1, "comments", "comments2");
        ProductionPlanListResponse addProductTemplateProdPlanItem = productTemplateService.addProductTemplateProdPlanItem(
                new AddProductTemplateProdPlanItemRequest(productTemplateId, prodPlanItem));
        // validate product has one operation in production plan
        ProductTemplateDTO pt_withProductionPlan = productTemplateService.getProductTemplate(new BasicGetRequest(productTemplateId));
        Assertions.assertThat(pt_withProductionPlan).isNotNull();
        Assertions.assertThat(pt_withProductionPlan.getProductionPlan()).isNotNull();
        Assertions.assertThat(pt_withProductionPlan.getProductionPlan()).hasSize(1);
        Assertions.assertThat(pt_withProductionPlan.getProductionPlan()).usingRecursiveFieldByFieldElementComparator().containsAll(addProductTemplateProdPlanItem.getProductionPlan());

        // add recipient
        RecipientDTO addrecipient = recipientService.addRecipient(new AddRecipientRequest(recipientName, "nip", "street", "postal", "city", "email@pl.pl", "+48 123456789", 10, null, "comments"));
        recipientId = addrecipient.getId();
        //validate recipient exists
        RecipientDTO recipient = recipientService.getRecipient(new BasicGetRequest(recipientId));
        Assertions.assertThat(recipient).isNotNull();
        Assertions.assertThat(recipient.getState()).isEqualTo(RecipientState.NORMAL);
        Assertions.assertThat(recipient.getName()).isEqualTo(recipientName);

        // add empty order
        OrderDTO addOrder = orderService.addOrder(new AddOrderRequest(recipientId, orderNo, "comments", new Date(), false));
        orderId = addOrder.getId();
        //validate order exists
        OrderDTO order = orderService.getOrder(new BasicGetRequest(orderId));
        Assertions.assertThat(order).isNotNull();
        Assertions.assertThat(order.getState()).isEqualTo(OrderState.NEW);
        Assertions.assertThat(order.getNr()).isEqualTo(orderNo);

        // add product1 as position to order1
        UpdateOrderRequest updateOrderRequest = BeanUtilSilent.translateBeanAndReturn(order, new UpdateOrderRequest());
        updateOrderRequest.getOrderPositions().add(
                new UpdateOrderPosition(productTemplateId, null, "010", 1, money, money, 0.0, "comments", new Date(), null, 0, 0, new ArrayList()));
        OrderDTO updateOrder = orderService.updateOrder(updateOrderRequest);
        //validate order has position        
        OrderDTO order2 = orderService.getOrder(new BasicGetRequest(orderId));
        Assertions.assertThat(order2).isNotNull();
        Assertions.assertThat(order2.getState()).isEqualTo(OrderState.NEW);
        Assertions.assertThat(order2.getNr()).isEqualTo(orderNo);
        Assertions.assertThat(order2.getOrderPositions()).hasSize(1);
        Assertions.assertThat(order2.getOrderPositions().get(0).getProductTemplate().getId()).isEqualTo(productTemplateId);

        // generate production orders
        GenerateProductionOrderResponse generateProductionOrder = productionOrderService.generateProductionOrder(new GenerateProductionOrderRequest(orderId));
        //validate
        ProductionOrdersListDTO listProductionOrders = productionOrderService.listProductionOrders(new PaginableFilterWithCriteriaRequest());
        Assertions.assertThat(listProductionOrders).isNotNull();
        Assertions.assertThat(listProductionOrders.getElements()).isNotNull();
        Assertions.assertThat(listProductionOrders.getElements()).hasSize(1);
        Assertions.assertThat(listProductionOrders.getElements().get(0).getOrderId()).isEqualTo(orderId);
        productionOrderId = listProductionOrders.getElements().get(0).getId();

        Object generateMaterialDemands = materialDemandService.generateMaterialDemands(new GenerateMaterialDemandRequest(productionOrderId));
        // validate generated demand
        MaterialDemandsListDTO listMaterialDemands = materialDemandService.listMaterialDemands(PaginableFilterWithCriteriaRequest.builder().addCriteriaFilterBuilder().addConstraint("order.id", orderId, ConstraintType.EQUALS).buildCriteriaFilter().build());
        Assertions.assertThat(listMaterialDemands).isNotNull();
        Assertions.assertThat(listMaterialDemands.getElements()).isNotNull();
        Assertions.assertThat(listMaterialDemands.getElements()).hasSize(1);
        Assertions.assertThat(listMaterialDemands.getElements().get(0).getProductionOrderId()).isEqualTo(productionOrderId);
        Assertions.assertThat(listMaterialDemands.getElements().get(0).getMaterialTemplate().getId()).isEqualTo(materialTemplateId);
        Assertions.assertThat(listMaterialDemands.getElements().get(0).getOrder().get("id")).isEqualTo(orderId);
        Assertions.assertThat(listMaterialDemands.getElements().get(0).getRecipient().get("id")).isEqualTo(recipientId);
        materialDemandId = listMaterialDemands.getElements().get(0).getId();

        // add material to stock
        CuttingSize length = CuttingSizeManager.createOneDimCuttingSize(6.0);
        MaterialDTO addMaterial = materialService.addMaterial(
                new AddMaterialRequest(materialTemplateId, length, 3, null, null, null, null, null, null, null, AddMaterialRequest.CreationContext.MANUAL));
        materialId = addMaterial.getId();
        // validate material template exists
        MaterialDTO material = materialService.getMaterial(new BasicGetRequest(addMaterial.getId()));
        Assertions.assertThat(material).isNotNull();
        Assertions.assertThat(addMaterial.getState()).isEqualTo(MaterialState.ADDED_MANUALY);
        Assertions.assertThat(material.getState()).isEqualTo(MaterialState.ADDED_MANUALY);
        Assertions.assertThat(material.getMaterialTemplate().getId()).isEqualTo(materialTemplateId);
        Assertions.assertThat(material.getMaterialTemplate().getMaterialCode()).isEqualTo(materialTemplateCode);
        Assertions.assertThat(material.getMaterialTemplate().getMaterialType().getId()).isEqualTo(materialTypetId);
        Assertions.assertThat(material.getMaterialTemplate().getMaterialType().getName()).isEqualTo(materialTypeName);

        // satisfy material demand
        MaterialDemandDTO satisfyDemands = materialDemandService.satisfyDemands(new SatisfyDemandsRequest(materialDemandId, materialId));
        MaterialDemandDTO materialDemand2 = materialDemandService.getMaterialDemand(new BasicGetRequest(materialDemandId));
        Assertions.assertThat(satisfyDemands).isNotNull();
        Assertions.assertThat(materialDemand2).isNotNull();
        Assertions.assertThat(satisfyDemands).isEqualToComparingFieldByFieldRecursively(materialDemand2);
        //validate demand are satisfied
        Assertions.assertThat(materialDemand2.getState()).isEqualTo(MaterialDemandState.SATISFIED);
        Assertions.assertThat(materialDemand2.getSatisfied()).isEqualTo(2);
        Assertions.assertThat(materialDemand2.getUnsatisfied()).isEqualTo(0);
        Assertions.assertThat(materialDemand2.getCount()).isEqualTo(2);

        // validate there is two materials
        MaterialListDTO listMaterials = materialService.listMaterials(PaginableFilterWithCriteriaRequest.builder().build());
        Assertions.assertThat(listMaterials).isNotNull();
        Assertions.assertThat(listMaterials.getSize()).isEqualTo(2);
        Assertions.assertThat(listMaterials.getElements().size()).isEqualTo(2);
        Assertions.assertThat(listMaterials.getElements().get(0)).isNotNull();
        Assertions.assertThat(listMaterials.getElements().get(1)).isNotNull();
        MaterialDTO e1 = listMaterials.getElements().get(0);
        MaterialDTO e2 = listMaterials.getElements().get(1);
        Assertions.assertThat(e1.getMaterialTemplateId()).isEqualTo(e2.getMaterialTemplateId());
        Assertions.assertThat(e1.getId()).isNotEqualTo(e2.getId());
        Assertions.assertThat(e1.getState()).isEqualTo(MaterialState.ADDED_MANUALY_RESERVED);
        Assertions.assertThat(e2.getState()).isEqualTo(MaterialState.ADDED_MANUALY);

        printObject("e1", e1);
        printObject("e2", e2);

//        materialService.getMaterial(new GetMaterialRequest(material))
    }
}
