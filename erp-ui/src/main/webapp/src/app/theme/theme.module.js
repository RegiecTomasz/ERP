(function () {
  'use strict';

  angular.module('JangraERP.theme', [
      'JangraERP.theme.components',
      'JangraERP.theme.inputs'
  ]);

})();
