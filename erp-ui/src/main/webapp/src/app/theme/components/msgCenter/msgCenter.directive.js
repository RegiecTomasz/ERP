(function () {
    'use strict';

    angular.module('JangraERP.theme.components')
        .directive('msgCenter', msgCenter);

    /** @ngInject */
    function msgCenter() {
        return {
            restrict: 'E',
            templateUrl: 'app/theme/components/msgCenter/msgCenter.html',
            controller: function MsgCenterCtrl($scope, ShoppingCartModel) {
                $scope.shoppingCart = ShoppingCartModel;
                ShoppingCartModel.getShoppingCart();

                $scope.finishShoppingCart = function(){
                    ShoppingCartModel.finish();
                }
            }
        };
    }
})();
