/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec 2017
 */
(function () {
    angular
        .module('JangraERP.core')
        .factory('RequestServiceClass', RequestServiceClass);

    /** @ngInject */
    function RequestServiceClass(requestHandler) {
        return class RequestServiceClass {
            constructor(name, subName) {
                this.name = name;
                this.subName = subName;
                this.requestHandler = requestHandler
            }

            create(data, isFormData) {
                return this.requestHandler.send({
                    method: 'add' + this.name,
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.Add' + this.name + 'Request',
                    data: data
                }, isFormData);
            };

            update(data, isFormData) {
                return this.requestHandler.send({
                    method: 'update' + this.name,
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.Update' + this.name + 'Request',
                    data: data
                }, isFormData);
            };

            remove(data) {
                return this.requestHandler.send({
                    method: 'remove' + this.name,
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.' + this.subName + '.Remove' + this.name + 'Request',
                    data: data
                });
            };
            
            restore(data) {
                return this.requestHandler.send({
                    method: 'restore' + this.name,
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.RestoreRequest',
                    data: data
                });
            };

            getById(id) {
                return this.requestHandler.send({
                    method: 'get' + this.name,
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.BasicGetRequest',
                    data: {id: id}
                });
            };

            list(data) {
                return this.requestHandler.send({
                    method: 'list' + this.name + 's',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.PaginableFilterWithCriteriaRequest',
                    data: data
                });
            };
        }
    }
})();
