/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec 2017
 */
(function () {
    angular
        .module('JangraERP.core')
        .factory('BaseModelClass', BaseModelClass);

    /** @ngInject */
    function BaseModelClass(AlertService, UtilsService) {
        return class BaseModelClass {
            /**
             *
             * @param requestService
             * @param data
             * @param options
             */
            constructor(requestService, data = {}, options = {}) {
                this.id = data.id || null;
                this.data = data;
                this._data = angular.copy(data);
                this.name = requestService.name;
                this.rService = requestService;
                this.reqParser = options.reqParser;
            }

            getById(id) {
                return this.rService.getById(this.id || id)
                    .then(responseData => {
                        this.id = responseData.id;
                        this.data = responseData;
                        return this;
                    });
            }

            /**
             *
             * @param data - undefined, true, object
             * @param formData - undefined, formData
             */
            save(data, formData) {
                let reqData = data ? typeof data === "boolean" ? this.tempData : data : this.data;
                if (this.reqParser) {
                    this.reqParser(reqData);
                }
                if (formData) {
                    reqData = UtilsService.toFormData(formData, reqData)
                }
                if (!this.id) {
                    return this.rService.create(reqData, !!formData)
                        .then(responseData => {
                            this.id = responseData.id;
                            this.data = responseData;
                            AlertService.success(this.rService.name + '.alert.create', null, responseData);
                            return this;
                        });
                } else {
                    return this.rService.update(reqData, !!formData)
                        .then(responseData => {
                            this.data = responseData;
                            AlertService.success(this.rService.name + '.alert.update', null, responseData);
                            return this;
                        });
                }
            }


            remove(confirmed = true) {
                return this.rService.remove({confirmed: confirmed, id: this.id})
                    .then(responseData => {
                        if (responseData.success) {
                            AlertService.success(this.rService.name + '.alert.remove');
                        }
                        return responseData
                    });
            }

            restore() {
                return this.rService.restore({id: this.id})
                    .then(responseData => {
                        if (responseData) {
                            AlertService.success(this.rService.name + '.alert.restore', null, responseData);
                        }
                        return responseData;
                    });
            }

            refresh() {
                return this.rService.getById(this.id)
                    .then(responseData => {
                        this.data = responseData;
                        return this;
                    });
            }

            clean() {
                this.id = null;
                this.data = {};
                this.tempData = {};
            }

            resetData() {

            }

            updateTemp() {
                this.tempData = angular.copy(this.data);
                return this;
            }

            isNew() {
                return !this.id;
            }

            getName() {
                return this.name || this.rService.name;
            }

            runServiceFunc(functionName, data, options = {}) {
                return this.rService[functionName](data)
                    .then(responseData => {
                        if (options.update) {
                            this.data = responseData;
                        }
                        if (options.message) {
                            AlertService.success(options.message, null, this.data);
                        }
                        return responseData
                    });
            }

            static runServiceFunc(runServiceFunction, rService, data, options = {}) {
                return rService[runServiceFunction](data)
                    .then(responseData => {
                        if (options.message) {
                            AlertService.success(options.message, null, responseData);
                        }
                        return responseData
                    });
            }
        }
    }
})();
