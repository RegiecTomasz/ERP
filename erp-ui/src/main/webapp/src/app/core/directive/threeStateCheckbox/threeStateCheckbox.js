(function () {
    'use strict';

    angular.module('JangraERP.core')
        .directive('threeStateCheckbox', threeStateCheckbox);

    /** @ngInject */
    function threeStateCheckbox($compile) {
        return {
            restrict: "E",
            require: 'ngModel',
            templateUrl: 'app/core/directive/threeStateCheckbox/threeStateCheckbox.html',
            scope: {
                'ngChange': "&ngChange",
                'ngModel': "=ngModel"
            },
            link: function (scope, element, attrs, ngModelCtrl) {
                let states = [
                    {value: null, className: ''},
                    {value: false, className: 'unchecked'},
                    {value: true, className: 'checked'}
                ];

                scope.click = function () {
                    let state = ngModelCtrl.$modelValue === null ? true : null;
                    ngModelCtrl.$setViewValue(state);
                    ngModelCtrl.$render();
                };

                scope.getClassName = function(){
                    let className;
                    states.forEach(function(state, index){
                        if(ngModelCtrl.$modelValue === state.value){
                            className = states[index].className;
                        }
                    });
                    return className;
                };

                if(angular.isUndefined(ngModelCtrl.$modelValue)){
                    ngModelCtrl.$setViewValue(states[0].value);
                    ngModelCtrl.$render();
                }
            }
        };
    }

})();
