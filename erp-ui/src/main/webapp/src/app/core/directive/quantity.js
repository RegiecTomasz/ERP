(function () {
    'use strict';

    angular.module('JangraERP.core')
        .directive('quantity', quantity);

    function quantity() {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                element.on("keypress", function(evt) {
                    if (evt.which < 48 || evt.which > 57) {
                        return false;
                    }
                });
            }
        };
    }
}());
