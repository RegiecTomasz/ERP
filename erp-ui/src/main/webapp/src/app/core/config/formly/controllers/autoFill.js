/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core')
        .controller('AutoFillCtrl', AutoFillCtrl);

    /** @ngInject */
    function AutoFillCtrl($scope, requestHandler, UtilsService) {
        function generate() {
            let requestData = {};
            let haveAdditionalArguments = true;

            if ($scope.to.dependancies && $scope.to.dependancies.length) {
                $scope.to.dependancies.forEach(dep => {
                    requestData.recipientId = $scope.formOptions.data.mainModel.data.recipientId;
                    if (angular.isString(dep)) {
                        requestData[dep] = $scope.model[dep];
                    } else {
                        requestData[dep.r] = UtilsService.objFromPath($scope.model, dep.m);
                    }
                });
            }


            if (angular.isArray($scope.to.additionalArguments)) {
                haveAdditionalArguments = $scope.to.additionalArguments.some(dep => {
                    if (angular.isString(dep)) {
                        return requestData[dep] = $scope.model[dep];
                    } else {
                        return requestData[dep.r] = UtilsService.objFromPath($scope.model, dep.m);
                    }
                });
            }

            if(!haveAdditionalArguments){
                return;
            }

            requestHandler.send({
                method: $scope.to.method,
                service: $scope.to.service,
                class: $scope.to.clazz,
                data: requestData
            }).then(responseSuccessData => {
                // todo: use setModel
                $scope.model[$scope.options.key] = responseSuccessData;
            }, responseErrorData => {
                // todo: use setModel
                console.warn('Error: ' + responseErrorData);
                $scope.model[$scope.options.key] = null;
            });
        }

        let wachedDependencies = $scope.to.dependancies.map(dep => {
            if (angular.isString(dep)) {
                return 'model.' + dep;
            } else {
                return 'model.' + dep.m;
            }
        });

        if ($scope.to.dependancies && $scope.to.dependancies.length) {
            $scope.$watchGroup(wachedDependencies, function (newVal, oldVal) {
                if (newVal.indexOf(undefined) === -1) {
                    generate();
                }
            });
        } else {
            generate();
        }
    }
})();
