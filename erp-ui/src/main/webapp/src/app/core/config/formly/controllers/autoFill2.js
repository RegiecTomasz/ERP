/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
            .module('JangraERP.core')
            .controller('AutoFillCtrl2', AutoFillCtrl2);

    /** @ngInject */
    function AutoFillCtrl2($scope, requestHandler, UtilsService) {

        function generate() {
            let requestData = {};

            var allDependencies = [];
            allDependencies = allDependencies.concat($scope.to.dependancies ? $scope.to.dependancies : []);
            allDependencies = allDependencies.concat($scope.to.additionalArguments ? $scope.to.additionalArguments : []);

            if (allDependencies && allDependencies.length) {
                requestData.recipientId = $scope.formOptions.data.mainModel.data.recipientId;
                allDependencies.forEach(dep => {
                    console.log(dep);
                    if (angular.isString(dep)) {
                        requestData[dep] = $scope.model[dep];
                    } else {
                        requestData[dep.r] = UtilsService.objFromPath($scope.model, dep.m);
                    }
                });
            }

            requestHandler.send({
                method: $scope.to.method,
                service: $scope.to.service,
                class: $scope.to.clazz,
                data: requestData
            }).then(responseSuccessData => {
                // todo: use setModel
                $scope.model[$scope.options.key] = responseSuccessData;
            }, responseErrorData => {
                // todo: use setModel
                console.warn('Error: ' + responseErrorData);
                $scope.model[$scope.options.key] = null;
            });
        }

        function validateAllOk() {
            var isValid = true;
            var mandatoryValues = $scope.to.mandatoryValues ? $scope.to.mandatoryValues : [];
            mandatoryValues.forEach(man => {
                console.log("mandatory: " + man);
            });
            var v = mandatoryValues.every(field => {
                console.log(field);
                var isSimpleField = field.indexOf(".") === -1;
                if (isSimpleField) {
                    var exist = $scope.model[field] !== undefined;
                    console.log("simple dependency: " + field + " exist status: " + exist);
                    if (!exist) {
                        isValid = false;
                    }
                    return exist;
                } else {
                    var exist = UtilsService.objFromPath($scope.model, field) !== undefined;
                    console.log("nested dependency: " + field + " exist status: " + exist);
                    if (!exist) {
                        isValid = false;
                    }
                    return exist;
                }
            });
            console.log("---V: " + v);
            return isValid;
        }

        let wachedDependencies = $scope.to.dependancies.map(dep => {
            if (angular.isString(dep)) {
                return 'model.' + dep;
            } else {
                return 'model.' + dep.m;
            }
        });
        var deepWatch = true;
        if ($scope.to.dependancies && $scope.to.dependancies.length) {
            wachedDependencies.forEach(dep => {
                $scope.$watch(dep, function (newVal, oldVal) {
                    if (newVal !== undefined) {
                        if (validateAllOk()) {
                            generate();
                        }
                    }
                }, deepWatch);
            });
        } else {
            if (validateAllOk()) {
                generate();
            }
        }

    }
})();
