(function () {
    'use strict';

    angular.module('JangraERP.core')
        .config(translateConfiguration);

    /** @ngInject */
    function translateConfiguration($translateProvider) {
        $translateProvider
            .useSanitizeValueStrategy(null)
            .useStaticFilesLoader({
                prefix: 'assets/i18n/',
                suffix: '.json'
            })
            .preferredLanguage('pl');
    }
})();
