(function () {
    'use strict';

    angular
        .module('JangraERP.core')
        .config(['$compileProvider', function ($compileProvider) {
            //TODO: uncomment on production
            //$compileProvider.debugInfoEnabled(false);
        }]);
})();
