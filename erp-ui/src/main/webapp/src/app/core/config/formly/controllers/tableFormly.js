/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core')
        .controller('TableFormlyCtrl', TableFormlyCtrl);

    /** @ngInject */
    function TableFormlyCtrl ($scope) {
        $scope.formOptions = {formState: $scope.formState};
        $scope.addNew = addNew;
        $scope.copyFields = copyFields;

        function copyFields(fields) {
            fields = angular.copy(fields);
            addRandomIds(fields);
            return fields;
        }

        function addNew() {
            let model = $scope.model[$scope.options.key] = $scope.model[$scope.options.key] || [];
            if (!$scope.to.addFrom) {
                model.push({});
            } else if ($scope.to.addFrom && !$.isEmptyObject($scope.addModel)) {
                model.push(angular.copy($scope.addModel));
            }
        }

        function addRandomIds(fields) {
            angular.forEach(fields, function (field, index) {
                if (field.fieldGroup) {
                    addRandomIds(field.fieldGroup);
                    return; // fieldGroups don't need an ID
                }

                if (field.templateOptions && field.templateOptions.fields) {
                    addRandomIds(field.templateOptions.fields);
                }

                field.id = field.id || (field.key + '_' + index + '_' + getRandomInt(0, 9999));
            });
        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
    }
})();
