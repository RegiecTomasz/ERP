(function () {
    'use strict';

    angular.module('JangraERP.core.components')
        .directive('tableList', tableList);

    /** @ngInject */
    function tableList($compile) {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                model: "=",
                configs: "="
            },
            template: '',
            link: function (scope, element, attr, controller) {

                if (!scope.model) {
                    scope.model = [];
                }

                scope.add = function (form) {
                    if (form.$valid) {
                        if (scope.model === undefined) {
                            scope.model = [];
                        }
                        let key = scope.configs.addForm[0].key;
                        if (scope.toAdd[key].id) {
                            scope.toAdd[key + 'Id'] = scope.toAdd[key].id;
                        }
                        scope.model.push(scope.toAdd);
                        scope.toAdd = {};
                    }
                };

                scope.remove = function (index) {
                    scope.model.splice(index, 1);
                };

                function genarateTemplate(configs) {
                    let title = '';
                    let tbody = '';
                    configs.columns.forEach(function (column, index) {
                        title += '<th>' + column.templateOptions.label + '</th>';
                        if (column.xeditable) {
                            if (column.xeditable.type === 'select') {
                                tbody += '<td><span editable-select="item.' + column.key + '" e-ng-options="' + column.templateOptions.options + ' configs.columns[' + index + '].templateOptions.items" buttons="no" blur="submit">{{item.' + column.key + '.label || "brak"}}</span></td>';
                            }
                            else {
                                tbody += '<td><span editable-' + column.xeditable.type + '="item.' + column.key + '" buttons="no" blur="submit">{{item.' + column.key + ' || "brak"}}</span></td>';
                            }
                        }
                        else if (column.type === "index") {
                            tbody += '<td>{{$index + 1}}</td>';
                        }
                        else {
                            tbody += '<td><span>{{item.' + column.key + '}}</span></td>';
                        }
                    });

                    if (configs.controls) {
                        title += '<th>' + configs.controls.title + '</th>';
                        tbody += '<td>' + configs.controls.tbody + '</td>';
                    }
                    let adding_buttonTitle = configs.adding_buttonTitle ? configs.adding_buttonTitle : "Dodaj";

                    title = '<tr>' + title + '</tr>';

                    let thead = '<thead>' + title + '</thead>';
                    tbody = '<tbody' + (configs.sortable ? ' ui-sortable ng-model="model"' : '') + '><tr ng-repeat="item in model" class="editable-tr-wrap">' + tbody + '</tr></tbody>';

                    let tfoot = '<tfoot><tr><td colspan="' + (configs.columns.length + (configs.controls ? 1 : 0)) + '"><form novalidate name="' + configs.id + 'Form"><formly-form model="toAdd" fields="configs.addForm" form="' + configs.id + 'Form"><button type="submit" class="btn btn-info btn-with-icon" ng-click="add(' + configs.id + 'Form)"><i class="ion-plus"></i>' + adding_buttonTitle + '</button></formly-form></form></td></tr></tfoot>';
                    let table = '<table id="' + (configs.id || 'formly') + 'ListTable" class="table erp-table" st-table="editableTableData">' + thead + tbody + tfoot + '</table>';

                    element.append($compile(table)(scope));
                }

                genarateTemplate(scope.configs)
            }
        };
    }

})();
