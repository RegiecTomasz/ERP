/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.list')
        .factory('ListModelClass', ListModelClass);

    function isObjectEmpty(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    function extendFilter(...filters) {
        let retFilter = {};

        filters.forEach(filter => {
            if (angular.isObject(filter)) {
                for (let key in filter) {
                    if (filter.hasOwnProperty(key)) {
                        if (angular.isArray(filter[key])) {
                            if (filter[key].length) {
                                retFilter[key] = filter[key];
                            }
                        } else {
                            retFilter[key] = filter[key];
                        }
                    }
                }
            }
        });

        return retFilter;
    }

    function isDefined(value) {
        return value !== '' && value !== undefined && value !== null;
    }

    /** @ngInject */
    function ListModelClass(AlertService, UtilsService) {
        return class ListClass {
            /**
             *
             * @param requestService
             * @param elementClass
             */
            constructor(requestService, elementClass) {
                this.page = 1;
                this.pageSize = 25;
                this.pageSizeList = [15, 25, 50];
                this.size = 0;
                this.sort = {};
                this.projection = [];
                this.filter = {};
                this.preFilter = {
                    removed: {
                        value: "true",
                        constraintType: "NOT_EQUALS"
                    }
                };
                this.postFilter = {};
                this.elements = [];
                this.rService = requestService;
                this.elementClass = elementClass;
            }

            getData() {
                return {
                    page: this.page - 1,
                    pageSize: this.pageSize,
                    sort: this.sort,
                    projection: this.projection,
                    criteriaFilters: this.parseFilter(angular.extend({}, this.preFilter, this.filter, this.postFilter))
                }
            }

            sortBy(columnName) {
                this.sort.field = columnName;
                this.sort.sortOrder = this.sort.sortOrder === 'DESCENDING' ? 'ASCENDING' : "DESCENDING";
            }

            parseFilter(filter, AND_OR) {
                let base = {
                    value: AND_OR || 'AND',
                    constraints: [],
                    criteriaFilters: []
                };

                for (let i in filter) {
                    if (filter.hasOwnProperty(i) && isDefined(filter[i])) {
                        if (angular.isArray(filter[i]) && filter[i].length) {
                            if (filter[i].value) {
                                //MULTI_INPUT
                                base.criteriaFilters.push({
                                    value: 'OR',
                                    constraints: filter[i].map(key => {
                                        return {
                                            field: key,
                                            value: filter[i].value,
                                            constraintType: 'LIKE_IGNORE_CASE',
                                        }
                                    })
                                })
                            } else {
                                // SELECT
                                base.criteriaFilters.push({
                                    value: 'OR',
                                    constraints: filter[i].map(item => {
                                        return {
                                            field: i,
                                            value: item,
                                            constraintType: 'EQUALS',
                                        }
                                    })
                                })
                            }
                        } else if (angular.isObject(filter[i])) {
                            if ('constraintType' in filter[i]) {
                                if (filter[i].constraintType) {
                                    // FILTER_DEFINITION
                                    base.constraints.push({
                                        field: filter[i].field || i,
                                        value: filter[i].value,
                                        constraintType: filter[i].constraintType,
                                    })
                                }
                            } else if ('from' in filter[i] || 'to' in filter[i]) {
                                if (filter[i].from || filter[i].to) {
                                    // NUMBER
                                    let constraints = [];
                                    if (filter[i].from) {
                                        constraints.push({
                                            field: i,
                                            value: filter[i].from,
                                            constraintType: 'GRATHER_THAN_OR_EQUAL',
                                        });
                                    }
                                    if (filter[i].to) {
                                        constraints.push({
                                            field: i,
                                            value: filter[i].to,
                                            constraintType: 'LESS_THAN_OR_EQUAL',
                                        });
                                    }
                                    base.criteriaFilters.push({
                                        value: 'AND',
                                        constraints: constraints
                                    })
                                }
                            } else if ('startDate' in filter[i] && 'endDate' in filter[i]) {
                                if (filter[i].startDate && filter[i].endDate) {
                                    // DATE
                                    let constraints = [];
                                    constraints.push({
                                        field: i,
                                        value: filter[i].startDate,
                                        constraintType: 'GRATHER_THAN_OR_EQUAL',
                                    });
                                    constraints.push({
                                        field: i,
                                        value: filter[i].endDate,
                                        constraintType: 'LESS_THAN_OR_EQUAL',
                                    });
                                    base.criteriaFilters.push({
                                        value: 'AND',
                                        constraints: constraints
                                    })
                                }
                            } else {
                                base.criteriaFilters.push({
                                    value: 'AND',
                                    constraints: UtilsService.objectToPathMap(filter[i], [], i, 'o').map(item => ({
                                        field: item.key,
                                        value: item.value,
                                        constraintType: 'LIKE_IGNORE_CASE',
                                    }))
                                })
                            }
                        } else {
                            if (angular.isString(filter[i])) {
                                // STRING
                                base.constraints.push({
                                    field: i,
                                    value: filter[i],
                                    constraintType: 'LIKE_IGNORE_CASE',
                                })
                            } else if (angular.isNumber(filter[i])) {
                                // NUMBER
                                base.constraints.push({
                                    field: i,
                                    value: filter[i],
                                    constraintType: 'EQUALS',
                                })
                            } else if (typeof filter[i] === 'boolean') {
                                // BOOLEAN
                                base.constraints.push({
                                    field: i,
                                    value: filter[i],
                                    constraintType: 'EQUALS',
                                })
                            }
                        }
                    }
                }

                return base;
            }

            resetFilter() {
                this.filter = {};
            }

            refresh() {
                return this.rService.list(this.getData())
                    .then(responseData => {
                        this.size = responseData.size;
                        this.elements = responseData.elements.map(element => new this.elementClass(element));
                        return this;
                    });
            }

            setPreFilter(preFilter) {
                if (preFilter) {
                    this.preFilter = preFilter;
                }
                return this;
            }

            setPostFilter(postFilter) {
                if (postFilter) {
                    this.postFilter = postFilter;
                }
                return this;
            }

            runServiceFunc(functionName, data, options = {}) {
                return this.rService[functionName](data)
                    .then(responseData => {
                        if (options.update) {
                            this.data = responseData;
                        }
                        if (options.message) {
                            AlertService.success(options.message, null, responseData);
                        }
                        return responseData;
                    });
            }

            static runServiceFunc(runServiceFunction, data, message) {
                return runServiceFunction(data)
                    .then(responseData => {
                        AlertService.success(message || responseData.message.message);
                        return responseData;
                    });
            }
        }
    }
})();
