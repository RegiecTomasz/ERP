/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular.module('JangraERP.core.components', [
        'JangraERP.core.components.list',
        'JangraERP.core.components.formly',
        'JangraERP.core.components.preview',
        'JangraERP.core.components.postalCode',
        'JangraERP.core.components.searchInput',
        'JangraERP.core.components.nestDataList'
    ]);
})();
