/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.nestDataList')
        .provider('NestDataListConfigService', NestDataListConfigService);

    /** @ngInject */
    function NestDataListConfigService() {
        const defaultControls = {
            nestDataListControls:
                `<ul>
                    <li><a ng-click="modalEdit(item)">Edytuj</a>
                    <li><a ng-click="modalRemove(item)">Usuń</a>
                </ul>`
        };
        const defaultConfigs = {};

        this.addDefaultControls = function (controlsName, config) {
            defaultControls[controlsName] = config;
        };

        this.addDefaultConfig  = function (configName, config) {
            defaultConfigs[configName] = config;
        };

        this.$get = function () {
            let nestDataListConfigs = {};

            class NestDataListConfigClass {
                constructor(defaultConfig) {
                    this.id = defaultConfig.id;
                    this.name = this.id + 'NestDataListConfig';
                    this.options = defaultConfig.options;
                    this.controls = defaultConfig.controls || defaultControls.nestDataListControls;
                }
            }

            function getNestDataListConfig(listName) {
                if (!nestDataListConfigs[listName]) {
                    return nestDataListConfigs[listName] = new NestDataListConfigClass(defaultConfigs[listName])
                }
                return nestDataListConfigs[listName];
            }

            return {
                getNestDataListConfig: getNestDataListConfig
            }
        };
    }
})();
