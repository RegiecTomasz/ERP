/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.formly')
        .provider('FormlyConfigService', FormlyConfigService);

    /** @ngInject */
    function FormlyConfigService() {
        const defaultConfigs = {};

        this.addDefaultConfig = function (configNames, config) {
            if(angular.isString(configNames)){
                defaultConfigs[configNames] = config;
            } else if (angular.isArray(configNames)){
                configNames.forEach(configName => {defaultConfigs[configName] = config})
            }
        };

        this.$get = function (FieldsService) {
            let formlyConfigs = {};
            let fields = FieldsService.getFields();

            function generateField(fieldConfig) {
                let field = angular.isString(fieldConfig) ? fields[fieldConfig] : fields[fieldConfig.id];
                if(fieldConfig.config){
                    return angular.merge({}, field, fieldConfig.config);
                }
                return angular.copy(field);
            }

            class FormlyConfigClass {
                constructor(defaultConfig) {
                    this.id = defaultConfig.id;
                    this.name = this.id + 'FormlyConfig';
                    this.fields = defaultConfig.fields.map(generateField);
                    this.options = {};
                }

                static getFields() {
                    return fields;
                }
            }

            function getFormlyConfig(formlyName) {
                return new FormlyConfigClass(defaultConfigs[formlyName])
            }

            return {
                getFormlyConfig: getFormlyConfig
            }
        };
    }
})();
