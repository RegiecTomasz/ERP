/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.preview')
        .provider('PreviewConfigService', PreviewConfigService);

    /** @ngInject */
    function PreviewConfigService() {
        const defaultConfigs = {};
        const fieldsTemplates = {
            basic: {
                prevBody: field => '{{::model.' + field.key + '}}',
                prevPreview: () => '',
                prevButton: field => '<button ng-if="config.availableFields[\'' + field.id + '\'].preview" class="btn btn-primary btn-sm" type="button" ng-click="::showPreview(\'' + field.id + '\')"><i class=ion-eye></i></button>'
            },
            horizontalUpload: {
                prevBody: field => `<a ng-href="${window.location.origin}/api/invoker/ResourceInfoService/getResourceById?id={{model.${field.key}dto.resourceId}}"  target="_blank">{{model.${field.key}dto.resourceName}}</a>`,
                prevPreview: field => field.templateOptions.previewType === 'pdf' ? `<div pdf-preview src='{{"${window.location.origin}/api/invoker/ResourceInfoService/getResourceById?id=" + model.${field.key}dto.resourceId}}'></div>` : '',
            },
            horizontalTextarea: {
                prevBody: field => '<div class="horizontal-textarea" ng-if="model.' + field.key + '"><pre>{{model.' + field.key + '}}</pre></div>'
            },
            horizontalText: {
                prevBody: field => '<div ng-bind-html="model.' + field.key + '">'
            },
            horizontalDatepicker: {
                prevBody: field => {
                    if (field.templateOptions.type === 'time') {
                        return `<div><span><i class="fa fa-clock-o"></i> {{::model.${field.key} | date:"mediumTime"}}</span></div>`;
                    } else if (field.templateOptions.type === 'date') {
                        return `<div><span><i class="fa fa-calendar"></i> {{::model.${field.key} | date:"mediumDate"}}</span></div>`;
                    } else {
                        return `<div><span><i class="fa fa-clock-o"></i> {{::model.${field.key} | date:"mediumTime"}}</span></div><div><span><i class="fa fa-calendar"></i> {{::model.${field.key} | date:"mediumDate"}}</span></div>`;
                    }
                },
                prevButton: () => ''
            },
            horizontalTemplateBase: {
                prevBody: field => `<a ui-sref="${field.templateOptions.uiSref}.element.detail({ id: model.${field.key}.id })">{{::model.${field.key}['${field.templateOptions.valueProp}']}}</a>`
            },
            horizontalReferenceBase: {
                prevBody: field => `<a ui-sref="${field.templateOptions.uiSref}.element.detail({ id: model.${field.key}.id })">{{::model.${field.key}['${field.templateOptions.valueProp}']}}</a>`
            },
            horizontalSelect: {
                prevBody: field => (field.templateOptions.type === 'const') ? `{{::model.${field.key}}}</td>` : `<span>{{::config.availableFields.${field.id}.templateOptions.items[model.${field.key}].translation}}</span>`,
                prevButton: () => ''
            },
            limitCardTable: {
                prevBody: field => `<div>Liczba towarów: {{::model.${field.key}.wareParts.length || 0}}</div>
                                    <div>Liczba materiałów: {{::model.${field.key}.materialParts.length || 0}}</div>
                                    <div>Liczba półproduktów: {{::model.${field.key}.semiproductParts.length || 0}}</div>`,
                prevPreview: field => `<nest-table-preview id="limitCardPreview" model="model.${field.key}" template-id="app/core/components/preview/limitCardPreview.html"></nest-table-preview>`
            },
            validationMessageTable: {
                prevBody: field => `<div>${field.templateOptions.countLabel} {{::model.${field.key}.length || 0}}</div>`,
                prevPreview: field => `<ul style="padding: 12px; list-style-type: none;">
                                            <li ng-repeat="obj in ::model.${field.key} track by $index">
                                               {{::$index + 1}} - {{::obj}}
                                            </li>
                                        </ul>`,
                prevButton: field => '<button ng-if="model.' + field.id + '.length" class="btn btn-primary btn-sm" type="button" ng-click="::showPreview(\'' + field.id + '\')"><i class=ion-eye></i></button>'
            },
            horizontalEmail: {
                prevBody: field => `<a href="mailto:{{::model.${field.key}}}">{{::model.${field.key}}}</a>`,
                prevButton: () => ''
            },
            horizontalLink: {
                prevBody: field => `<a ng-if="::model.${field.key}" ng-href="${field.templateOptions.url}?${field.templateOptions.fieldName || 'id'}={{::model.${column.key}}}" target="_blank">Pobierz</a>`,
                prevButton: () => ''
            },
            horizontalCheckbox: {
                prevBody: field => `{{("_." + model.${field.key}) | translate}}`,
                prevButton: () => ''
            }
        };

        function generateBody(field) {
            return field.templateOptions.prevBody
                || fieldsTemplates[field.type]
                && fieldsTemplates[field.type].prevBody
                && fieldsTemplates[field.type].prevBody(field)
                || fieldsTemplates.basic.prevBody(field);
        }

        function generatePreview(field) {
            return field.templateOptions.prevPreview
                || fieldsTemplates[field.type]
                && fieldsTemplates[field.type].prevPreview
                && fieldsTemplates[field.type].prevPreview(field)
                || fieldsTemplates.basic.prevPreview(field);
        }

        function generateButton(field) {
            return field.templateOptions.prevButton
                || fieldsTemplates[field.type]
                && fieldsTemplates[field.type].prevButton
                && fieldsTemplates[field.type].prevButton(field)
                || fieldsTemplates.basic.prevButton(field);
        }

        this.addDefaultConfig = function (configName, config) {
            defaultConfigs[configName] = config;
        };

        this.$get = function (FieldsService) {
            let previewConfigs = {};
            let fields = FieldsService.getFields();

            function generateField(fieldName, mergeOptions) {
                let field = mergeOptions ? angular.merge({}, fields[fieldName] || {}, mergeOptions) : angular.copy(fields[fieldName]);
                field.tbody = `<tr>
                                <th>${field.templateOptions.label}</th>
                                <td>${generateBody(field)}</td>
                                <td>${generateButton(field)}</td>
                            </tr>`;
                field.preview = generatePreview(field);
                return field;
            }

            class PreviewConfigClass {
                constructor(defaultConfig) {
                    this.id = defaultConfig.id;
                    this.name = this.id + 'PreviewConfig';
                    this.fields = [];
                    this.availableFields = {};

                    let defaultAvailableFields = defaultConfig.availableFields;
                    for (let fieldName in defaultAvailableFields) {
                        if (defaultAvailableFields.hasOwnProperty(fieldName)) {
                            this.fields.push(this.availableFields[fieldName] = generateField(fieldName, defaultAvailableFields[fieldName]));
                        }
                    }
                }

                static getFields() {
                    return fields;
                }
            }

            function getPreviewConfig(previewName) {
                if (!previewConfigs[previewName]) {
                    return previewConfigs[previewName] = new PreviewConfigClass(defaultConfigs[previewName])
                }
                return previewConfigs[previewName];
            }

            return {
                getPreviewConfig: getPreviewConfig
            }
        };
    }
})();
