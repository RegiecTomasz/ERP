
(function () {
    angular
        .module('JangraERP.core.components.searchInput', [])
        .directive('searchInput', SearchInputComponent)
        .directive('rangeSearch', RangeSearchDirective);

    function SearchInputComponent() {
        return {
            templateUrl: 'app/core/components/searchInput/searchInput.html',
            require: 'ngModel',
            restrict: 'E',
            scope: {
                ngModel: '=',
                change: '&'
            },
            link: function ($scope, element, attrs, ngModelCtrl) {
                $scope.popover = {
                    title: '',
                    isOpen: false,
                    template: 'searchInputPopoverTemplate.html'
                };

                $scope.fields = [{
                    id: 'from',
                    key: 'from',
                    type: 'horizontalInput',
                    templateOptions: {
                        type: 'Number',
                        label: 'Od',
                        placeholder: 'Od'
                    }
                }, {
                    id: 'to',
                    key: 'to',
                    type: 'horizontalInput',
                    templateOptions: {
                        type: 'Number',
                        label: 'Do',
                        placeholder: 'Do'
                    }
                }];

                $scope.copyModel = function (){
                    return angular.copy($scope.ngModel);
                };

                $scope.resetModel = function(model){
                    model.from = null;
                    model.to = null;
                };

                $scope.onSubmit = function (model) {
                    $scope.popover.isOpen = false;
                    $scope.ngModel = model;
                    $scope.change();
                };
            }
        };
    }

    function RangeSearchDirective() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function ($scope, element, attrs, ngModelCtrl) {
                ngModelCtrl.$formatters.push(function(value){
                    if (!value){
                        return '';
                    }
                    let stringValue = '';

                    stringValue += value.from ? ('od ' + value.from + ' ') : '';
                    stringValue += value.to ? ('do ' + value.to) : '';

                    console.warn(stringValue);

                    return stringValue;
                });
            }
        };
    }
})();
