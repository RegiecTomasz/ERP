/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.list')
        .controller('ListCustomizeCtrl', ListCustomizeCtrl);

    /** @ngInject */
    function ListCustomizeCtrl($scope, UtilsService, listConfig, listGenerate) {
        $scope.title = UtilsService.capitalizeFirstLetter(listConfig.id);
        $scope.customize = listConfig.getCustomizeObject();

        $scope.sortableOptions = {
            axis:'y',
            placeholder: "sortable-placeholder"
        };

        $scope.toggleColumn = function (column) {
            if (column.enabled) {
                $scope.customize.columns.push(column);
            } else {
                $scope.customize.columns.splice($scope.customize.columns.indexOf(column), 1);
            }
        };

        $scope.offColumn = function (column) {
            column.enabled = false;
            $scope.customize.columns.splice($scope.customize.columns.indexOf(column), 1);
        };

        $scope.save = function(){
            listConfig.setColumnsFromMap($scope.customize.columns);
            listConfig.saveInLocalStorage();
            listGenerate(listConfig);
            $scope.$close();
        };
    }
})();
