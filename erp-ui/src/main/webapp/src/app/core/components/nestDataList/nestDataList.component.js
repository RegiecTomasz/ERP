/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.core.components.nestDataList')
        .directive('nestDataList', NestDataListComponent);

    /** @ngInject */
    function NestDataListComponent(NestDataListConfigService, FormlyConfigService, $uibModal, $parse, AlertService) {
        return {
            restrict: 'E',
            scope: {
                model: "=",
                config: "="
            },
            template: '<ng-include src="getTemplateUrl()"/>',
            link: function (scope, element, attr, controller) {
                const postId = "NestDataListComponent";
                let config = (typeof scope.config === 'string') ? NestDataListConfigService.getNestDataListConfig(scope.config) : scope.config;
                let options = config.options;
                let methods = options.methods || null;
                scope.path = options.path;
                scope.viewTemplateUrl = options.viewTemplateUrl;
                scope.previewConfig = options.previewConfig || config.id;

                scope.getTemplateUrl = function () {
                    return options.customTemplateUrl || 'app/core/components/nestDataList/nestDataList.component.html';
                };

                let getter = $parse(scope.path);
                let setter = getter.assign;

                scope.getModel = function () {
                    return getter(scope.model.data);
                };

                scope.update = function () {
                    setTimeout(function () {
                        scope.model.save();
                    }, 0);
                };

                scope.edit = function (index) {
                    let isNew = !angular.isNumber(index);
                    let model = scope.model.updateTemp();
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'app/core/components/nestDataList/nestDataList.edit.html',
                        size: 'md',
                        backdrop: 'static',
                        controller: function (elementModel, mainModel) {
                            console.log('nestDataListController');
                            this.elementModel = angular.copy(elementModel);
                            this.config = FormlyConfigService.getFormlyConfig((elementModel ? 'Create' : 'Update') + (options.formlyConfig || config.id));
                            this.modalTitle = elementModel ? 'Edycja' : 'Nowy';                           
                            this.config.options.data = {
                                mainModel: mainModel
                            };

                            if (!angular.isArray(getter(model.tempData))) {
                                setter(model.tempData, []);
                            }

                            this.save = function () {
                                if (this.editForm.$valid) {
                                    if (isNew) {
                                        if (methods && methods.create) {
                                            scope.model[methods.create](this.elementModel).then(responseDate => {
                                                setter(scope.model.data, responseDate[options.main]);
                                                this.$close();
                                            })
                                        } else {
                                            getter(model.tempData).push(angular.copy(this.elementModel));
                                            this.cgBusy = model.save(true).then(() => {
                                                this.$close();
                                            }, () => {
                                                model.updateTemp()
                                            })
                                        }
                                    } else {
                                        if (methods && methods.update) {
                                            scope.model[methods.update](this.elementModel).then(responseDate => {
                                                setter(scope.model.data, responseDate[options.main]);
                                                this.$close();
                                            })
                                        } else {
                                            getter(model.tempData)[index] = this.elementModel;
                                            this.cgBusy = model.save(true).then(() => {
                                                this.$close();
                                            }, () => {
                                                model.updateTemp()
                                            })
                                        }
                                    }
                                }
                            };
                        },
                        controllerAs: '$ctrl',
                        bindToController: true,
                        resolve: {
                            mainModel: model,
                            elementModel: isNew ? null : getter(model.tempData)[index],
                        }
                    }).result.then(angular.noop, angular.noop);

                };

                scope.remove = function (index, elementId) {
                    if (methods && methods.remove) {
                        scope.model[methods.remove](elementId).then(responseDate => {
                            setter(scope.model.data, responseDate[options.main]);
                        })
                    } else {
                        let model = scope.model.updateTemp();
                        getter(model.tempData).splice(index, 1);
                        model.save(true)
                    }
                };
            }
        };
    }
})();
