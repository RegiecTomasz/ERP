/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BaseReferListCtrl {
        constructor(elementModel, configName) {
            this.model = elementModel.updateTemp();
            this.config = configName;
        }
    }

    angular
        .module('JangraERP.core')
        .controller('BaseReferListCtrl', BaseReferListCtrl);
})();
