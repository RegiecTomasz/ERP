/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BaseRemoveCtrl {
        constructor(listConfig, elementModel, removeConsequences) {
            this.listConfig = listConfig;
            this.model = elementModel;
            this.removeConsequences = removeConsequences;
        }
    }

    angular
        .module('JangraERP.core')
        .controller('BaseRemoveCtrl', BaseRemoveCtrl);
})();
