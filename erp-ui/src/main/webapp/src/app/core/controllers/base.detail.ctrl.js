/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class BaseDetailCtrl {
        constructor(FormlyConfigService, elementModel, $state) {
            this.model = elementModel.updateTemp();
            this.config = FormlyConfigService.getFormlyConfig('Update' + elementModel.getName());
            if($state.params && $state.params.typeId){
                this.config.options.data = {
                    typeId: $state.params.typeId
                };
            }
        }

        save () {
            this.detailForm.$setSubmitted();
            if (this.detailForm.$valid) {
                this.cgBusy = this.model.save(true, this.config.options.formState.formData).then(() => {
                    this.model.updateTemp();
                }, angular.noop)
            }
        };
    }

    angular
        .module('JangraERP.core')
        .controller('BaseDetailCtrl', BaseDetailCtrl);
})();
