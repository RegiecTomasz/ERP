/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class FieldReferenceCtrl {
        constructor($scope, $injector) {
            let requestService = $injector.get($scope.to.requestService);

            $scope.to.refresh = function (search) {
                let filter = {
                    constraints: [],
                    value: 'AND'
                };
                filter.constraints.push({
                    value: $scope.formState.materialTypeId,
                    field: 'materialType.id',
                    constraintType: 'EQUALS'
                });
                if (search) {
                    filter.criteriaFilters = [{
                        constraints: [],
                        value: 'OR'
                    }];
                    filter.criteriaFilters[0].constraints.push({
                        value: search,
                        field: 'materialCode',
                        constraintType: 'LIKE_IGNORE_CASE'
                    });
                    filter.criteriaFilters[0].constraints.push({
                        value: search,
                        field: 'particularDimension',
                        constraintType: 'LIKE_IGNORE_CASE'
                    });
                } else {
                    delete filter.criteriaFilters;
                }
                return requestService.list({
                    page: 0,
                    pageSize: 0,
                    criteriaFilters: filter
                }).then(function (responseData) {
                    $scope.to.items = responseData.elements;
                });
            };
        }
    }

    angular
        .module('JangraERP.core')
        .controller('FieldReferenceCtrl', FieldReferenceCtrl);
})();
