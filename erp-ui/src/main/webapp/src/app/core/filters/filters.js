(function () {
    'use strict';

    angular.module('JangraERP.core')
        .filter('unsafe', function ($sce) {
            return $sce.trustAsHtml;
        });
})();
