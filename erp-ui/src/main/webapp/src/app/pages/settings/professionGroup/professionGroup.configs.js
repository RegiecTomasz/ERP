/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.professionGroup')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('ProfessionGroup',
            {
                vm: '$ctrl',
                id: 'ProfessionGroup',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['name', 'code', 'label', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'settings.professionGroup'},
                    name: null,
                    code: null,
                    label: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateProfessionGroup', 'UpdateProfessionGroup'],
            {
                id: 'ProfessionGroup',
                options: {},
                fields: [
                    {id: 'name', config: {templateOptions: {required: true}}},
                    {id: 'code', config: {templateOptions: {required: true}}},
                    {id: 'allowOperationsIds', config: {templateOptions: {required: true}}}
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('ProfessionGroup',
            {
                id: 'ProfessionGroup',
                options: {},
                availableFields: {
                    name: null,
                    code: null,
                    label: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'settings.professionGroup',
                abstract: true,
                url: '/grupy_zawodowe',
                title: 'Grupy zawodowe',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 500,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista grup zawodowych',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (ProfessionGroupListModel) {
                                return ProfessionGroupListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'ProfessionGroup',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Grupa zawodowa',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Grupa zawodowa';
                        },
                        resolve: {
                            elementModel: function ($stateParams, ProfessionGroupModel) {
                                return ProfessionGroupModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/ustawienia/grupy_zawodowe', '/ustawienia/grupy_zawodowe/lista');
    }
})();
