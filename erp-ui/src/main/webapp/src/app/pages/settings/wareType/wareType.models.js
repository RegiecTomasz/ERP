/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.wareType')
        .factory('WareTypeModel', WareTypeModel)
        .factory('WareTypeListModel', WareTypeListModel);

    /** @ngInject */
    function WareTypeModel(BaseModelClass, WareTypeRequestService) {
        return class WareTypeClass extends BaseModelClass {
            constructor(wareTypeData) {
                super(WareTypeRequestService, wareTypeData);
            }

            static getById(wareTypeId) {
                return new WareTypeClass().getById(wareTypeId);
            };
        }
    }

    /** @ngInject */
    function WareTypeListModel(ListModelClass, WareTypeRequestService, WareTypeModel) {
        return class WareTypesListClass extends ListModelClass {
            constructor() {
                super(WareTypeRequestService, WareTypeModel);
            }

            static list() {
                return new WareTypesListClass().refresh();
            }
        }
    }
})();
