/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.materialType')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('MaterialType',
            {
                vm: '$ctrl',
                id: 'MaterialType',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['name', 'baseDimensionTemplate', 'cuttingDimensionTemplate', 'baseDimensionUnit', 'cuttingDimensionUnit'],
                availableColumns: {
                    _id: {router: 'settings.materialType'},
                    name: null,
                    baseDimensionTemplate: null,
                    cuttingDimensionTemplate: null,
                    baseDimensionUnit: null,
                    removed:null,
                    cuttingDimensionUnit: null,
                    MATERIAL_TYPE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateMaterialType', 'UpdateMaterialType'],
            {
                id: 'MaterialType',
                options: {},
                fields: [
                    {id: 'name', config: {templateOptions: {required: true}}},
                    {id: 'baseDimensionTemplate', config: {templateOptions: {required: true}}},
                    {id: 'cuttingDimensionTemplate', config: {templateOptions: {required: true}}},
                    {id: 'baseDimensionUnit', config: {templateOptions: {required: true}}},
                    {id: 'cuttingDimensionUnit', config: {templateOptions: {required: true}}}
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('MaterialType',
            {
                id: 'MaterialType',
                options: {},
                availableFields: {
                    name: null,
                    baseDimensionTemplate: null,
                    cuttingDimensionTemplate: null,
                    baseDimensionUnit: null,
                    cuttingDimensionUnit: null,
                    MATERIAL_TYPE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'settings.materialType',
                abstract: true,
                url: '/typy_materialu',
                title: 'Typy materiału',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 200,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista typów materiałów',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (MaterialTypeListModel) {
                                return MaterialTypeListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'MaterialType',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Typ materiału',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Typ materiału';
                        },
                        resolve: {
                            elementModel: function ($stateParams, MaterialTypeModel) {
                                return MaterialTypeModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/ustawienia/typy_materialu', '/ustawienia/typy_materialu/lista');
    }
})();
