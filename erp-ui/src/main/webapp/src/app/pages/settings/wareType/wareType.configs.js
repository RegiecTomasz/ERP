/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.wareType')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('WareType',
            {
                vm: '$ctrl',
                id: 'WareType',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['name', 'wareDimension', 'sizeType', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'settings.wareType'},
                    name: null,
                    wareDimension: null,
                    sizeType: null,
                    WARE_TYPE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateWareType', 'UpdateWareType'],
            {
                id: 'WareType',
                options: {},
                fields: [
                    {id: 'name', config: {templateOptions: {required: true}}},
                    {id: 'wareDimension', config: {templateOptions: {required: true}}},
                    {id: 'sizeType', config: {templateOptions: {required: true}}}
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('WareType',
            {
                id: 'WareType',
                options: {},
                availableFields: {
                    name: null,
                    wareDimension: null,
                    sizeType: null,
                    WARE_TYPE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'settings.wareType',
                abstract: true,
                url: '/typy_towaru',
                title: 'Typy towaru',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 400,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista typów towarów',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (WareTypeListModel) {
                                return WareTypeListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'WareType',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Typ towaru',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Typ towaru';
                        },
                        resolve: {
                            elementModel: function ($stateParams, WareTypeModel) {
                                return WareTypeModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/ustawienia/typy_towaru', '/ustawienia/typy_towaru/lista');
    }
})();
