/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.professionGroup')
        .factory('ProfessionGroupModel', ProfessionGroupModel)
        .factory('ProfessionGroupListModel', ProfessionGroupListModel);

    /** @ngInject */
    function ProfessionGroupModel(BaseModelClass, ProfessionGroupRequestService) {
        return class ProfessionGroupClass extends BaseModelClass {
            constructor(professionGroupData) {
                super(ProfessionGroupRequestService, professionGroupData);
            }

            static getById(professionGroupId) {
                return new ProfessionGroupClass().getById(professionGroupId);
            };
        }
    }

    /** @ngInject */
    function ProfessionGroupListModel(ListModelClass, ProfessionGroupRequestService, ProfessionGroupModel) {
        return class ProfessionGroupsListClass extends ListModelClass {
            constructor() {
                super(ProfessionGroupRequestService, ProfessionGroupModel);
            }

            static list() {
                return new ProfessionGroupsListClass().refresh();
            }
        }
    }
})();
