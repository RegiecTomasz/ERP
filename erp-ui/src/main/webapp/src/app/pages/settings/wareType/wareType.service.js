/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.wareType')
        .service('WareTypeRequestService', WareTypeRequestService);

    /** @ngInject */
    function WareTypeRequestService(RequestServiceClass) {
        class WareTypeRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('WareType', 'waretypes');
            }
        }

        return new WareTypeRequestServiceClass();
    }
})();
