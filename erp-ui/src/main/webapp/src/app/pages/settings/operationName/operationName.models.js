/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.operationName')
        .factory('OperationNameModel', OperationNameModel)
        .factory('OperationNameListModel', OperationNameListModel);

    /** @ngInject */
    function OperationNameModel(BaseModelClass, OperationNameRequestService) {
        return class OperationNameClass extends BaseModelClass {
            constructor(operationNameData) {
                super(OperationNameRequestService, operationNameData);
            }

            static getById(operationNameId) {
                return new OperationNameClass().getById(operationNameId);
            };
        }
    }

    /** @ngInject */
    function OperationNameListModel(ListModelClass, OperationNameRequestService, OperationNameModel) {
        return class OperationNamesListClass extends ListModelClass {
            constructor() {
                super(OperationNameRequestService, OperationNameModel);
            }

            static list() {
                return new OperationNamesListClass().refresh();
            }
        }
    }
})();
