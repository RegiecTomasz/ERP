(function () {
    'use strict';

    angular.module('JangraERP.pages.settings', [
        'JangraERP.pages.settings.materialType',
        'JangraERP.pages.settings.wareType',
        'JangraERP.pages.settings.operationName',
        'JangraERP.pages.settings.professionGroup'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('settings', {
                url: '/ustawienia',
                abstract: true,
                template: '<ui-view />',
                title: 'Ustawienia',
                sidebarMeta: {
                    icon: 'ion-wrench',
                    order: 10000,
                }
            });
    }

})();
