/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.settings.operationName')
        .service('OperationNameRequestService', OperationNameRequestService);

    /** @ngInject */
    function OperationNameRequestService(RequestServiceClass) {
        class OperationNameRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('OperationName', 'operationName');
            }
        }

        return new OperationNameRequestServiceClass();
    }
})();
