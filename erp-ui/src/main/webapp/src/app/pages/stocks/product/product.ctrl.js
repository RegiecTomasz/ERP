/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class ProductListCtrl {
        constructor($uibModal, UtilsService, listModel, listConfig) {
            this.listModel = listModel;
            this.listConfig = listConfig;

            this.modalPreview = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/core/controllers/base.preview.modal.html',
                    size: 'auto',
                    backdrop: 'static',
                    controller: 'BasePreviewCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        elementModel: element
                    }
                }).result.then(angular.noop, angular.noop);
            };

            this.modalCreate = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/core/controllers/base.create.modal.html',
                    size: 'md',
                    backdrop: 'static',
                    controller: 'BaseCreateCtrl',
                    controllerAs: '$ctrl',
                    bindToController: true,
                    resolve: {
                        elementModel: element || new this.listModel.elementClass()
                    }
                }).result.then(() => {
                    this.refresh();
                }, () => {
                    this.refresh();
                });
            };

            this.modalRemove = function (element) {
                element.remove(false).then(responseData => {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'app/core/controllers/base.remove.modal.html',
                        size: 'auto',
                        backdrop: 'static',
                        controller: 'BaseRemoveCtrl',
                        controllerAs: '$ctrl',
                        resolve: {
                            elementModel: element
                        }
                    }).result.then(() => {
                        element.remove().then(() => {
                            this.refresh();
                        });
                    }, () => {
                        this.refresh();
                    });
                });
            };

            this.modalGenerateWZ = function () {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/stocks/product/generateDocument.modal.html',
                    size: 'md',
                    backdrop: 'static',
                    controller: 'GenerateDocumentCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        config: function(){
                            return {
                                type: "WZ",
                                title: 'Wygeneruj WZ',
                                formlyConfig: 'GenerateWZModalConfig',
                                url: 'ProductService/generateWZPdf'
                            }
                        }
                    },
                }).result.then((data) => {
                    this.refresh();
                }, () => {
                    this.refresh();
                });
            };

            this.modalGenerateInvoice = function () {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/stocks/product/generateDocument.modal.html',
                    size: 'md',
                    backdrop: 'static',
                    controller: 'GenerateDocumentCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        config: function(){
                            return {
                                type: "INVOICE",
                                title: 'Wygeneruj fakturę',
                                formlyConfig: 'GenerateInvoiceModalConfig',
                                url: 'ProductService/generateInvoicePdf'
                            }
                        }
                    },
                }).result.then((data) => {
                    this.refresh();
                }, () => {
                    this.refresh();
                });
            };

            this.generateWZ = function (data) {
                this.listModel.generateWZ(data).then(() => {
                    this.refresh();
                });
            };

            this.modalFinishProduct = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/stocks/product/finishProduct.modal.html',
                    size: 'md',
                    backdrop: 'static',
                    controller: 'FinishProductCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        elementModel: element
                    }
                }).result.then((data) => {
                    this.refresh();
                }, () => {
                    this.refresh();
                });
            };

            this.finishProduct = function (item) {
                item.finishProduct().then(() => {
                    this.listModel.refresh();
                });
            };

        }

        refresh() {
            this.listModel.refresh();
        }
    }

    angular
            .module('JangraERP.pages.stocks.product')
            .controller('ProductListCtrl', ProductListCtrl)
})();
