(function () {
    'use strict';

    angular.module('JangraERP.pages.stocks', [
        'JangraERP.pages.stocks.material',
        'JangraERP.pages.stocks.product',
        'JangraERP.pages.stocks.ware'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('stocks', {
                url: '/magazyn',
                abstract: true,
                template: '<ui-view />',
                title: 'Magazyn',
                sidebarMeta: {
                    icon: 'ion-cube',
                    order: 300
                }
            });
    }

})();
