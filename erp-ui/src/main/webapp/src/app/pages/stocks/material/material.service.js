/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.stocks.material')
        .service('MaterialRequestService', MaterialRequestService);

    /** @ngInject */
    function MaterialRequestService(RequestServiceClass) {
        class MaterialRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Material', 'material');
            }
            
            create(data) {
                data.creationContext = 'MANUAL';
                return this.requestHandler.send({
                    method: 'addMaterial',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.material.AddMaterialRequest',
                    data: data
                }, false);
            };            
        }

        return new MaterialRequestServiceClass();
    }
})();
