/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.stocks.ware')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Ware',
            {
                vm: '$ctrl',
                id: 'Ware',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['wareCode', 'name', 'count', 'reserved', 'free', 'WARE_STATE', 'countInKg', 'select_norm', 'deliveryDate', 'orderNo', 'comments', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'stock.ware'},
                    wareCode: {key: 'wareTemplate.wareCode'},
                    name: {key: 'wareTemplate.name'},
                    count: null,
                    reserved: null,
                    free: null,
                    countInKg: null,
                    select_norm: {key: 'wareTemplate.norm.norm'},
                    deliveryDate: null,
                    orderNo: null,
                    comments: null,
                    wareTemplateId: {key: 'wareTemplate'},
                    WARE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateWare', 'UpdateWare'],
            {
                id: 'Ware',
                options: {},
                fields: [
                    {id: 'wareTemplateId', config: {templateOptions: {required: true}}},
                    'count',
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Ware',
            {
                id: 'Ware',
                options: {},
                availableFields: {
                    wareCode: null,
                    count: null,
                    reserved: null,
                    free: null,
                    countInKg: null,
                    norm: null,
                    deliveryDate: null,
                    orderNo: null,
                    comments: null,
                    WARE_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'stocks.ware',
                url: '/towar/typ',
                abstract: true,
                template: '<ui-view />',
                title: 'Towary',
                sidebarMeta: {
                    order: 200,
                },
                resolve: {
                    typeListModel: function (WareTypeListModel) {
                        return WareTypeListModel.list();
                    }
                },
                children: [
                    {
                        name: 'list',
                        url: '/:typeId/lista',
                        title: 'Lista towarów',
                        templateUrl: 'app/core/controllers/base.listWithType.html',
                        controller: "BaseListWithTypeCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (WareListModel, typeListModel, $stateParams, $state) {
                                if ($stateParams.typeId) {
                                    return WareListModel.list(null, {
                                        'wareTemplate.wareType.id': {
                                            value: $stateParams.typeId,
                                            constraintType: 'EQUALS'
                                        }
                                    });
                                } else if (!$stateParams.typeId && typeListModel.elements.length) {
                                    $state.transition.then(function () {
                                        $state.transitionTo('stocks.ware.list', {typeId: typeListModel.elements[0].id})
                                    });
                                    return null;
                                } else {
                                    return WareListModel;
                                }
                            },
                            listConfig: function () {
                                return {
                                    id: 'Ware',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/:typeId/{id:[0-9a-f]{24}}',
                        title: 'Towar',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Towar';
                        },
                        resolve: {
                            elementModel: function ($stateParams, WareModel) {
                                return WareModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/magazyn/towar/typ', '/magazyn/towar/typ//lista');
    }
})();
