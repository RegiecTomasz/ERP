/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.stocks.material')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Material',
            {
                vm: '$ctrl',
                id: 'Material',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['particularDimension', 'materialCode', 'count', 'lengthObj', 'reservedLengthObj', 'leftLengthObj', 'MATERIAL_STATE', 'select_tolerance', 'select_norm', 'comments', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'stock.material'},
                    particularDimension: {key: 'materialTemplate.particularDimension'},
                    materialCode: {key: 'materialTemplate.materialCode'},
                    count: null,
                    removed: null,
                    lengthObj: {key: "length", templateOptions: {field: 'length'}},
                    reservedLengthObj: {key: "reservedLength", templateOptions: {field: 'length'}},
                    leftLengthObj: {key: "leftLength", templateOptions: {field: 'length'}},
                    select_tolerance: {key: 'materialTemplate.tolerance.tolerance'},
                    select_norm: {key: 'materialTemplate.norm.norm'},
                    comments: null,
                    materialTemplateId: {key: 'materialTemplate'},
                    MATERIAL_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
                /*
                 * 
                 * @param {type} FormlyConfigServiceProvider
                 * @returns {undefined}
                 * templateOptions: {
                            label: 'Nazwa',
                            placeholder: 'Nazwa'
                        }
                 */
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateMaterial', 'UpdateMaterial'],
            {
                id: 'Material',
                options: {},
                fields: [
                    {id: 'materialTemplateId', config: {templateOptions: {required: true}}},
                    'length',
                    'count',
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Material',
            {
                id: 'Material',
                options: {},
                availableFields: {
                    particularDimension: {key: 'materialTemplate.particularDimension'},
                    materialCode: {key: 'materialTemplate.materialCode'},
                    length: null,
                    count: null,
                    reservedLength: {key: 'reservedLength.origSize'},
                    leftLength: {key: 'leftLength.origSize'},
                    select_tolerance: {key: 'materialTemplate.tolerance'},
                    select_norm: {key: 'materialTemplate.norm'},
                    comments: null,
                    MATERIAL_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'stocks.material',
                url: '/material/typ',
                abstract: true,
                template: '<ui-view />',
                title: 'Materiały',
                sidebarMeta: {
                    order: 100,
                },
                resolve: {
                    typeListModel: function (MaterialTypeListModel) {
                        return MaterialTypeListModel.list();
                    }
                },
                children: [
                    {
                        name: 'list',
                        url: '/:typeId/lista',
                        title: 'Lista materiałów',
                        templateUrl: 'app/core/controllers/base.listWithType.html',
                        controller: "BaseListWithTypeCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (MaterialListModel, typeListModel, $stateParams, $state) {
                                if ($stateParams.typeId) {
                                    return MaterialListModel.list(null, {
                                        'materialTemplate.materialType.id': {
                                            value: $stateParams.typeId,
                                            constraintType: 'EQUALS'
                                        }
                                    });
                                } else if (!$stateParams.typeId && typeListModel.elements.length) {
                                    $state.transition.then(function(){
                                        $state.transitionTo('stocks.material.list', {typeId: typeListModel.elements[0].id})
                                    });
                                    return null;
                                } else {
                                    return MaterialListModel;
                                }
                            },
                            listConfig: function () {
                                return {
                                    id: 'Material',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/:typeId/{id:[0-9a-f]{24}}',
                        title: 'Materiał',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Materiał';
                        },
                        resolve: {
                            elementModel: function ($stateParams, MaterialModel) {
                                return MaterialModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/magazyn/material/typ', '/magazyn/material/typ//lista');
    }
})();
