/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
            .module('JangraERP.pages.stocks.product')
            .service('ProductRequestService', ProductRequestService);

    /** @ngInject */
    function ProductRequestService(RequestServiceClass) {
        class ProductRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Product', 'product');
            }

            generateWZ(data) {
                return this.requestHandler.send({
                    method: 'generateWZPdf',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.product.GenerateWzPdfRequest',
                    data: data
                });
            }

            generateInvoice(data) {
                return this.requestHandler.send({
                    method: 'generateInvoicePdf',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.product.GenerateInvoicePdfRequest',
                    data: data
                });
            }

            finishProduct(data) {
                return this.requestHandler.send({
                    method: 'finishProduct',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.product.FinishProductRequest',
                    data: data
                });
            }

            listProductsForWz(data) {
                return this.requestHandler.send({
                    method: 'listProductsForWz',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.product.ListProductsForWzPdf',
                    data: data
                });
            }
            
            listProductsForInvoice(data) {
                return this.requestHandler.send({
                    method: 'listProductsForInvoice',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.product.ListProductsForInvoicePdf',
                    data: data
                });
            }
        }

        return new ProductRequestServiceClass();
    }
})();
