/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.seller')
        .service('SellerRequestService', SellerRequestService);

    /** @ngInject */
    function SellerRequestService(RequestServiceClass) {
        class SellerRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Seller', 'seller');
            }
        }

        return new SellerRequestServiceClass();
    }
})();
