/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
            .module('JangraERP.pages.contractors.seller')
            .config(listConfig)
            .config(formlyConfig)
            .config(previewConfig)
            .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Seller',
                {
                    vm: '$ctrl',
                    id: 'Seller',
                    options: {selectable: true},
                    controls: {name: 'listControls'},
                    defaultColumns: ['name', 'nip', 'smallAddress', 'email', 'phone', 'SELLER_STATE', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                    availableColumns: {
                        _id: {router: 'contractors.seller'},
                        name: null,
                        nip: null,
                        smallAddress: null,
                        street: null,
                        postalCode: null,
                        city: null,
                        email: null,
                        phone: null,
                        regon: null,
                        swift: null,
                        bankNamePLN: null,
                        bankAccountPLN: null,
                        bankNameEUR: null,
                        bankAccountEUR: null,
                        SELLER_STATE: null,
                        datePicker_creationDate: null,
                        datePicker_lastModificationDate: null
                    }
                }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateSeller', 'UpdateSeller'],
                {
                    id: 'Seller',
                    options: {},
                    fields: [
                        {id: 'name', config: {templateOptions: {required: true}}},
                        {id: 'nip', config: {templateOptions: {required: true}}},
                        {id: 'street', config: {templateOptions: {required: true}}},
                        {id: 'postalCode', config: {templateOptions: {required: true}}},
                        {id: 'city', config: {templateOptions: {required: true}}},
                        'regon',
                        'swift',
                        'bankNamePLN',
                        'bankAccountPLN',
                        'bankNameEUR',
                        'bankAccountEUR',
                        'email',
                        'phone',
                        'comments'
                    ]
                }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Seller',
                {
                    id: 'Seller',
                    options: {},
                    availableFields: {
                        name: null,
                        nip: null,
                        street: null,
                        postalCode: null,
                        city: null,
                        regon: null,
                        swift: null,
                        bankNamePLN: null,
                        bankAccountPLN: null,
                        bankNameEUR: null,
                        bankAccountEUR: null,
                        email: null,
                        phone: null,
                        comments: null,
                        SELLER_STATE: null,
                        datePicker_creationDate: null,
                        datePicker_lastModificationDate: null
                    }
                }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
                .state({
                    name: 'contractors.seller',
                    abstract: true,
                    url: '/sprzedawcy',
                    title: 'Sprzedawcy',
                    template: '<ui-view />',
                    sidebarMeta: {
                        order: 300,
                    },
                    children: [
                        {
                            name: 'list',
                            url: '/lista',
                            title: 'Lista sprzedawców',
                            templateUrl: 'app/core/controllers/base.list.html',
                            controller: "BaseListCtrl",
                            controllerAs: "$ctrl",
                            resolve: {
                                listModel: function (SellerListModel) {
                                    return SellerListModel.list();
                                },
                                listConfig: function () {
                                    return {
                                        id: 'Seller',
                                        removeConsequencesTpl: null
                                    };
                                }
                            }
                        },
                        {
                            name: 'element',
                            abstract: true,
                            url: '/{id:[0-9a-f]{24}}',
                            title: 'Sprzedawca',
                            templateUrl: 'app/core/controllers/base.element.html',
                            controller: 'BaseElementCtrl',
                            controllerAs: "$ctrl",
                            breadcrumb: function () {
                                return 'Sprzedawca';
                            },
                            resolve: {
                                elementModel: function ($stateParams, SellerModel) {
                                    return SellerModel.getById($stateParams.id);
                                }
                            },
                            children: [
                                {
                                    name: 'detail',
                                    url: '/szczegoly',
                                    title: 'Szczegóły',
                                    templateUrl: 'app/core/controllers/base.detail.html',
                                    controller: 'BaseDetailCtrl',
                                    controllerAs: "$ctrl"
                                }
                            ]
                        }
                    ]
                });

        $urlRouterProvider.when('/Kontrahenci/sprzedawcy', '/Kontrahenci/sprzedawcy/lista');
    }
})();
