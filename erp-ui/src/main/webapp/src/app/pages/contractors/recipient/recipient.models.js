/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.recipient')
        .factory('RecipientModel', RecipientModel)
        .factory('RecipientListModel', RecipientListModel);

    /** @ngInject */
    function RecipientModel(BaseModelClass, RecipientRequestService) {
        return class RecipientClass extends BaseModelClass {
            constructor(recipientData) {
                super(RecipientRequestService, recipientData);
            }

            static getById(recipientId) {
                return new RecipientClass().getById(recipientId);
            };
        }
    }

    /** @ngInject */
    function RecipientListModel(ListModelClass, RecipientRequestService, RecipientModel) {
        return class RecipientsListClass extends ListModelClass {
            constructor() {
                super(RecipientRequestService, RecipientModel);
            }

            static list() {
                return new RecipientsListClass().refresh();
            }
        }
    }
})();
