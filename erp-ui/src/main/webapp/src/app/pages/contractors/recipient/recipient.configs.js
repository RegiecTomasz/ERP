/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.recipient')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Recipient',
            {
                vm: '$ctrl',
                id: 'Recipient',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['name', 'nip', 'smallAddress', 'email', 'phone', 'daysForPayment', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'contractors.recipient'},
                    name: {templateOptions: {required: true}},
                    nip: {templateOptions: {required: true}},
                    smallAddress: null,
                    street: null,
                    postalCode: null,
                    city: null,
                    email: null,
                    phone: null,
                    daysForPayment: {templateOptions: {required: true}},
                    comments: null,
                    RECIPIENT_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateRecipient', 'UpdateRecipient'],
            {
                id: 'Recipient',
                options: {},
                fields: [
                    {id: 'name', config: {templateOptions: {required: true}}},
                    {id: 'nip', config: {templateOptions: {required: true}}},
                    {id: 'street', config: {templateOptions: {required: true}}},
                    {id: 'postalCode', config: {templateOptions: {required: true}}},
                    {id: 'city', config: {templateOptions: {required: true}}},
                    'email',
                    'phone',
                    {id: 'daysForPayment', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Recipient',
            {
                id: 'Recipient',
                options: {},
                availableFields: {
                    name: null,
                    nip: null,
                    street: null,
                    postalCode: null,
                    city: null,
                    email: null,
                    phone: null,
                    daysForPayment: null,
                    comments: null,
                    // RECIPIENT_STATE: null, // #68
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'contractors.recipient',
                abstract: true,
                url: '/odbiorcy',
                title: 'Odbiorcy',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 100,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista odbiorców',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (RecipientListModel) {
                                return RecipientListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'Recipient',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Odbiorca',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Odbiorca';
                        },
                        resolve: {
                            elementModel: function ($stateParams, RecipientModel) {
                                return RecipientModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/Kontrahenci/odbiorcy', '/Kontrahenci/odbiorcy/lista');
    }
})();
