/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.contractors.provider')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(nestDataListConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Provider',
            {
                vm: '$ctrl',
                id: 'Provider',
                options: {selectable: true},
                controls: {name: 'listControls'},
                defaultColumns: ['name', 'nip', 'smallAddress', 'email', 'phone', 'daysForPayment', 'PROVIDER_STATE', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'contractors.provider'},
                    name: null,
                    nip: null,
                    smallAddress: null,
                    street: null,
                    postalCode: null,
                    city: null,
                    email: null,
                    phone: null,
                    daysForPayment: null,
                    comments: null,
                    PROVIDER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateProvider', 'UpdateProvider'],
            {
                id: 'Provider',
                options: {},
                fields: [
                    {id: 'name', config: {templateOptions: {required: true}}},
                    {id: 'nip', config: {templateOptions: {required: true}}},
                    {id: 'street', config: {templateOptions: {required: true}}},
                    {id: 'postalCode', config: {templateOptions: {required: true}}},
                    {id: 'city', config: {templateOptions: {required: true}}},
                    {id: 'email', config: {templateOptions: {required: true}}},
                    {id: 'phone', config: {templateOptions: {required: true}}},
                    {id: 'daysForPayment', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['CreateMaterialAssortment', 'UpdateMaterialAssortment'],
            {
                id: 'MaterialAssortment',
                options: {},
                fields: [
                    {id: 'materialTemplateId', config: {templateOptions: {required: true}}},
                    'materialAssortmentPositions'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Provider',
            {
                id: 'Provider',
                options: {},
                availableFields: {
                    name: null,
                    nip: null,
                    street: null,
                    postalCode: null,
                    city: null,
                    email: null,
                    phone: null,
                    daysForPayment: null,
                    comments: null,
                    PROVIDER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );

        PreviewConfigServiceProvider.addDefaultConfig('MaterialAssortment',
            {
                id: 'MaterialAssortment',
                options: {},
                availableFields: {
                    ref_materialTemplate: {
                        templateOptions: {
                            prevBody: '<a ui-sref="templates.materialTemplate.element.detail({ id: model.materialTemplateId })">{{::model.materialTypeName}}</a>'
                        }
                    },
                    materialAssortmentPositions: {
                        templateOptions: {
                            prevBody: '<ul><li ng-repeat="material in model.materialAssortmentPositions">{{material.salesLength.cuttingDimension}} : {{material.salesLength.origSize}}\n\
     -> {{material.salesPrice.amount}}{{material.salesPrice.currency}} / {{material.sellUnit}}</li></ul>'
                        }
                    }
                }
            }
        );
    }

    /** @ngInject */
    function nestDataListConfig(NestDataListConfigServiceProvider) {
        NestDataListConfigServiceProvider.addDefaultConfig('MaterialAssortment',
            {
                id: 'MaterialAssortment',
                options: {
                    path: 'assortment.materialAssortment',
                    formlyConfig: 'MaterialAssortment',
                    previewConfig: 'MaterialAssortment',
                    viewTemplateUrl: 'app/core/templates/nestDataList/materialAssortment.html'
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'contractors.provider',
                abstract: true,
                url: '/dostawcy',
                title: 'Dostawcy',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 200,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista dostawców',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (ProviderListModel) {
                                return ProviderListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'Provider',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Dostawca',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Dostawca'
                        },
                        resolve: {
                            elementModel: function ($stateParams, ProviderModel) {
                                return ProviderModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            },
                            {
                                name: 'materialAssortment',
                                url: '/materialy',
                                title: 'Materiały',
                                templateUrl: 'app/core/components/nestDataList/nestDataList.base.html',
                                controller: 'BaseNestDataListCtrl',
                                controllerAs: "$ctrl",
                                breadcrumb: function () {
                                    return 'Asortyment - Materiały'
                                },
                                resolve: {
                                    configName: function () {
                                        return 'MaterialAssortment'
                                    }
                                }
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/Kontrahenci/dostawcy', '/Kontrahenci/dostawcy/lista');
    }
})();
