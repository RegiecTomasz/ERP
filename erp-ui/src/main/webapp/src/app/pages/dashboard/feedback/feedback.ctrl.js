(function () {
    'use strict';

    angular.module('JangraERP.pages.dashboard')
        .controller('FeedbackCtrl', FeedbackCtrl);

    /** @ngInject */
    function FeedbackCtrl($scope, ProductTemplateRequestService) {
        function list(){
            ProductTemplateRequestService.list({
                page: 0,
                pageSize: 20,
                criteriaFilters: {
                    value: "AND",
                    constraints: [
                        {
                            field: "validationStatus",
                            value: "nie",
                            constraintType: "EQUALS"
                        }
                    ]
                }
            }).then(listData => {
                let ret = listData;
                if(ret.size === 0){
                     ret.elements.push({name: "Nie ma błędów"});
                }
                $scope.feedback = ret;
            });
        }

        $scope.expandMessage = function (message) {
            message.expanded = !message.expanded;
        };

        list();
    }
})();
