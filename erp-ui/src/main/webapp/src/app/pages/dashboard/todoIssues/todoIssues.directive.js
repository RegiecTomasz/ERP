(function () {
    'use strict';

    angular.module('JangraERP.pages.dashboard')
        .directive('todoIssues', TodoIssues);

    /** @ngInject */
    function TodoIssues() {
        return {
            restrict: 'EA',
            controller: 'TodoIssuesCtrl',
            templateUrl: 'app/pages/dashboard/todoIssues/todoIssues.html'
        };
    }
})();
