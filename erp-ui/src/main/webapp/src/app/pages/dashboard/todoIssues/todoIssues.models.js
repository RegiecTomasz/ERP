/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.dashboard')
        .factory('TodoIssueModel', TodoIssueModel)
        .factory('TodoIssueListModel', TodoIssueListModel);

    /** @ngInject */
    function TodoIssueModel(BaseModelClass, TodoIssuesRequestService) {
        return class TodoIssueClass extends BaseModelClass {
            constructor(TodoIssueData) {
                super(TodoIssuesRequestService, TodoIssueData);
            }

            static getById(TodoIssueId) {
                return new TodoIssueClass().getById(TodoIssueId);
            };
        }
    }

    /** @ngInject */
    function TodoIssueListModel(ListModelClass, TodoIssuesRequestService, TodoIssueModel) {
        return class TodoIssuesListClass extends ListModelClass {
            constructor() {
                super(TodoIssuesRequestService, TodoIssueModel);
            }

            static list() {
                return new TodoIssuesListClass().refresh();
            }
        }
    }
})();
