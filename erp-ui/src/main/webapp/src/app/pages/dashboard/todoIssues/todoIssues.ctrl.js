(function () {
    'use strict';

    angular.module('JangraERP.pages.dashboard')
        .controller('TodoIssuesCtrl', TodoIssuesCtrl);

    /** @ngInject */
    function TodoIssuesCtrl($scope, TodoIssuesRequestService) {
        $scope.todoList = [];

        function list(){
            TodoIssuesRequestService.list().then(listData => {
                $scope.todoList = listData.elements;
            });
        }

        $scope.createToDoItem = function(text){
            TodoIssuesRequestService.create({text: text, resolved: false}).then(() => {
                list();
                $scope.todoText = '';
            }, angular.noop)
        };

        $scope.toogleToDoItem = function(item){
            TodoIssuesRequestService.update(item).then(list, angular.noop)
        };

        $scope.removeToDoItem = function(item){
            TodoIssuesRequestService.remove({id: item.id, confirmed: true, removeRequestFromUp: true}).then(list, angular.noop)
        };

        list()
    }
})();
