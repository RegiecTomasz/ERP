(function () {
    'use strict';

    angular.module('JangraERP.pages.dashboard')
        .directive('screenshotDev', screenshotDev);

    /** @ngInject */
    function screenshotDev() {
        return {
            restrict: 'E',
            templateUrl: 'app/pages/dashboard/screenshotDev/screenshotDev.html',
            link: function (scope, element, attr) {
                scope.takeScreenshot = function(){
                    html2canvas(document.body).then(canvas => {
                        let image = new Image();
                        image.src = canvas.toDataURL();

                        let w = window.open("");
                        w.document.write(image.outerHTML);
                    });
                }
            }
        };
    }
})();
