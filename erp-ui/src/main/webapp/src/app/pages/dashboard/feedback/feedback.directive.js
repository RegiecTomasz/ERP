(function () {
    'use strict';

    angular.module('JangraERP.pages.dashboard')
        .directive('feedback', feedback);

    /** @ngInject */
    function feedback() {
        return {
            restrict: 'E',
            controller: 'FeedbackCtrl',
            templateUrl: 'app/pages/dashboard/feedback/feedback.html'
        };
    }
})();
