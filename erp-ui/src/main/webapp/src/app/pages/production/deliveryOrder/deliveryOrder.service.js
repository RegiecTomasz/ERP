/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.production.deliveryOrder')
        .service('DeliveryOrderRequestService', DeliveryOrderRequestService);

    /** @ngInject */
    function DeliveryOrderRequestService(RequestServiceClass) {
        class DeliveryOrderRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('DeliveryOrder', 'deliveryorder');
            }
            
            confirmPosition(data) {
                return this.requestHandler.send({
                    method: 'confirmDeliveryOrderPosition',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.deliveryorder.ConfirmDeliveryOrderPositionRequest',
                    data: data
                });
            };
            
            findOtherProviderForMaterial(data) {
                return this.requestHandler.send({
                    method: 'findOtherProviderForMaterial',
                    service: 'ProviderService',
                    class: 'regiec.knast.erp.api.dto.provider.FindOtherProviderForMaterialRequest',
                    data: data
                });
            };

            moveDeliveryOrderPositionToOtherProvider(data) {
                return this.requestHandler.send({
                    method: 'moveDeliveryOrderPositionToOtherProvider',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.deliveryorder.MoveDeliveryOrderPositionToOtherProviderRequest',
                    data: data
                });
            };
        }

        return new DeliveryOrderRequestServiceClass();
    }
})();
