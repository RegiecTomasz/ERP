(function () {
    'use strict';

    angular.module('JangraERP.pages.production', [
        'JangraERP.pages.production.deliveryOrder',
        'JangraERP.pages.production.materialDemand'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('production', {
                url: '/zakupy',
                abstract: true,
                template: '<ui-view />',
                title: 'Zakupy',
                sidebarMeta: {
                    icon: 'ion-briefcase',
                    order: 200
                }
            });
    }

})();
