/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class DeliveryOrderListCtrl {
        constructor($uibModal, listModel, listConfig) {
            this.listModel = listModel;
            this.listConfig = listConfig;

            this.showTextModal = function (elementModel) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/production/deliveryOrder/DeliveryOrder.list.html',
                    size: 'lg',
                    backdrop: 'static',
                    controllerAs: '$ctrl',
                    controller: function () {
                        this.orderMail = elementModel.data.orderMail;
                    }
                }).result.then(angular.noop, angular.noop);
            };

            this.modalPreview = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/core/controllers/base.preview.modal.html',
                    size: 'md',
                    backdrop: 'static',
                    controller: 'BasePreviewCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        elementModel: element
                    }
                }).result.then(angular.noop, angular.noop);
            };

            this.confirmPosition = function (elementModel) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/production/deliveryOrder/DeliveryOrderConfirmPosition.html',
                    size: 'lg',
                    backdrop: 'static',
                    controllerAs: '$ctrl',
                    controller: function () {
                        this.elementModel = elementModel;
                        this.openIndex = false;
                        this.assortmentForMaterial = [];

                        this.confirmPositionOnClick = function (positionId) {
                            elementModel.confirmPosition(positionId);
                        };
                        this.changeProviderOnClick = function (item, provider) {
                            elementModel
                                .moveDeliveryOrderPositionToOtherProvider(item.positionId, provider.materialAssortment.positionId, provider.providerId)
                                .then(() => {
                                    this.openIndex = false;
                                })
                        };

                        this.findProviderOnClick = function (item, index) {
                            elementModel.findOtherProviderForMaterial(item.materialTemplateId, item.assortmentId, item.salesLength.origSize).then((responseData) => {
                                this.openIndex = index;
                                this.assortmentForMaterial = responseData.assortmentForMaterialResponse;
                            })
                        };

                    }
                }).result.then(() => {
                    this.listModel.refresh();
                }, () => {
                    this.listModel.refresh();
                });
            };

        }

        refresh() {
            this.listModel.refresh();
        }
    }

    angular
        .module('JangraERP.pages.production.deliveryOrder')
        .controller('DeliveryOrderListCtrl', DeliveryOrderListCtrl);
})();
