/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.production.materialDemand')
        .service('MaterialDemandRequestService', MaterialDemandRequestService);

    /** @ngInject */
    function MaterialDemandRequestService(RequestServiceClass) {
        class MaterialDemandRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('MaterialDemand', 'materialDemand');
            }

            generateMaterialDemands(productionOrderId) {
                return this.requestHandler.send({
                    method: 'generateMaterialDemands',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.materialDemand.GenerateMaterialDemandRequest',
                    data: {productionOrderId: productionOrderId}
                });
            };

            satisfyDemands(data) {
                return this.requestHandler.send({
                    method: 'satisfyDemands',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.materialDemand.SatisfyDemandsRequest',
                    data: {id: data.id, materialId: data.materialId}
                });
            };
        }

        return new MaterialDemandRequestServiceClass();
    }
})();
