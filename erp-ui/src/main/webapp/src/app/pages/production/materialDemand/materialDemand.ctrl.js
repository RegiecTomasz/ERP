/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class MaterialDemandCtrl {
        constructor($uibModal, listModel, listConfig) {
            this.listModel = listModel;
            this.listConfig = listConfig;

            this.modalDemandSatisfying = function (elementModel) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/production/materialDemand/materialDemandSatisfying.modal.html',
                    size: 'lg',
                    backdrop: 'static',
                    controllerAs: '$ctrl',
                    controller: function ($scope, UtilsService, ShoppingCartModel, materialListModel, providerForMaterial) {
                        this.item = elementModel;

                        this.materialListModel = materialListModel;
                        this.providerForMaterial = {
                            elements: providerForMaterial.assortmentForMaterialResponse,
                            size: providerForMaterial.assortmentForMaterialResponse.length
                        };

                        this.assign = function (listElement) {
                            elementModel.satisfyDemands(listElement.id).then((responseData) => {
                                if (responseData.state === 'SATISFIED') {
                                    $scope.$close();
                                }
                            });
                        };

                        this.addPositionsToShoppingCart = function () {
                            let positionsList = [];

                            this.providerForMaterial.elements.forEach(provider => {
                                provider.materialAssortment.forEach(material => {
                                    if (material.count) {
                                        positionsList.push({
                                            count: material.count,
                                            materialTemplateId: providerForMaterial.materialTemplateId,
                                            providerId: provider.providerId,
                                            assortmentId: material.positionId
                                        });
                                        material.count = null;
                                    }
                                });
                            });

                            UtilsService.requestQueue(data => ShoppingCartModel.addPosition(data), positionsList)
                                .then(function () {
                                    materialListModel.refresh();
                                });
                        };
                    },
                    resolve: {
                        providerForMaterial: function (ProviderRequestService) {
                            return ProviderRequestService.findProviderForMaterial(
                                {
                                    materialTemplateId: elementModel.data.materialTemplate.id,
                                    materialCode: elementModel.data.materialTemplate.materialCode
                                }
                            );
                        },
                        materialListModel: function (MaterialListModel) {
                            return MaterialListModel.list(null, {
                                'materialTemplate.materialCode': {
                                    value: elementModel.data.materialTemplate.materialCode,
                                    constraintType: 'EQUALS'
                                }
                            });
                        }
                    }
                }).result.then(() => {
                    this.refresh()
                }, () => {
                    this.refresh()
                });
            };
        }

        refresh() {
            this.listModel.refresh()
        }
    }

    angular
        .module('JangraERP.pages.production.materialDemand')
        .controller('MaterialDemandCtrl', MaterialDemandCtrl);
})();
