/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.productTemplate')
        .factory('ProductTemplateModel', ProductTemplateModel)
        .factory('ProductTemplateListModel', ProductTemplateListModel);

    /** @ngInject */
    function ProductTemplateModel(BaseModelClass, ProductTemplateRequestService) {
        return class ProductTemplateClass extends BaseModelClass {
            constructor(productTemplateData) {
                super(ProductTemplateRequestService, productTemplateData);
            }

            static getById(productTemplateId) {
                return new ProductTemplateClass().getById(productTemplateId);
            };

            /* MaterialPart */

            addMaterialPart(materialPart) {
                return this.runServiceFunc('addProductTemplateMaterialPart', {
                    productTemplateId: this.data.id,
                    materialPart: materialPart
                }, {message: 'ProductTemplate.alert.addProductTemplateMaterialPart', update: false});
            };

            updateMaterialPart(materialPart) {
                return this.runServiceFunc('updateProductTemplateMaterialPart', {
                    productTemplateId: this.data.id,
                    materialPart: materialPart
                }, {message: 'ProductTemplate.alert.updateProductTemplateMaterialPart', update: false});
            };

            removeMaterialPart(materialPartId) {
                return this.runServiceFunc('removeProductTemplateMaterialPart', {
                    productTemplateId: this.data.id,
                    materialPartId: materialPartId
                }, {message: 'ProductTemplate.alert.removeProductTemplateMaterialPart', update: false});
            };

            /* SemiproductPart */

            addSemiproductPart(semiproductPart) {
                return this.runServiceFunc('addProductTemplateProductPart', {
                    productTemplateId: this.data.id,
                    semiproductPart: semiproductPart
                }, {message: 'ProductTemplate.alert.addProductTemplateProductPart', update: false});
            };

            updateSemiproductPart(semiproductPart) {
                return this.runServiceFunc('updateProductTemplateSemiproductPart', {
                    productTemplateId: this.data.id,
                    semiproductPart: semiproductPart
                }, {message: 'ProductTemplate.alert.updateProductTemplateSemiproductPart', update: false});
            };

            removeSemiproductPart(semiproductPartId) {
                return this.runServiceFunc('removeProductTemplateSemiproductPart', {
                    productTemplateId: this.data.id,
                    semiproductPartId: semiproductPartId
                }, {message: 'ProductTemplate.alert.removeProductTemplateSemiproductPart', update: false});
            };

            /* WarePart */

            addWarePart(warePart) {
                return this.runServiceFunc('addProductTemplateWarePart', {
                    productTemplateId: this.data.id,
                    warePart: warePart
                }, {message: 'ProductTemplate.alert.addProductTemplateWarePart', update: false});
            };

            updateWarePart(warePart) {
                return this.runServiceFunc('updateProductTemplateWarePart', {
                    productTemplateId: this.data.id,
                    warePart: warePart
                }, {message: 'ProductTemplate.alert.updateProductTemplateWarePart', update: false});
            };

            removeWarePart(warePartId) {
                return this.runServiceFunc('removeProductTemplateWarePart', {
                    productTemplateId: this.data.id,
                    warePartId: warePartId
                }, {message: 'ProductTemplate.alert.removeProductTemplateWarePart', update: false});
            };

            /* ProdPlan */

            addProdPlanItem(prodPlanItem) {
                return this.runServiceFunc('addProductTemplateProdPlanItem', {
                    productTemplateId: this.data.id,
                    prodPlanItem: prodPlanItem
                }, {message: 'ProductTemplate.alert.addProductTemplateProdPlanItem', update: false});
            };

            updateProdPlanItem(prodPlanItem) {
                return this.runServiceFunc('updateProductTemplateProdPlanItem', {
                    productTemplateId: this.data.id,
                    prodPlanItem: prodPlanItem
                }, {message: 'ProductTemplate.alert.updateProductTemplateProdPlanItem', update: false});
            };

            removeProdPlanItem(prodPlanItemId) {
                return this.runServiceFunc('removeProductTemplateProdPlanItem', {
                    productTemplateId: this.data.id,
                    prodPlanItemId: prodPlanItemId
                }, {message: 'ProductTemplate.alert.removeProductTemplateProdPlanItem', update: false});
            };

            /* validators */

            validateProductTemplate() {
                return this.runServiceFunc('validateProductTemplate', {productTemplateId: this.data.id}, {
                    message: 'ProductTemplate.alert.validateProductTemplate',
                    update: false
                });
            };
        }
    }

    /** @ngInject */
    function ProductTemplateListModel(ListModelClass, ProductTemplateRequestService, ProductTemplateModel) {
        return class ProductTemplatesListClass extends ListModelClass {
            constructor() {
                super(ProductTemplateRequestService, ProductTemplateModel);
            }

            validateAllProductsTemplate() {
                return this.runServiceFunc('validateAllProductsTemplate', {updateValidationErrorsField : true}, {
                    message: 'ProductTemplate.alert.validateAllProductsTemplate',
                    update: false
                });
            };

            static list() {
                return new ProductTemplatesListClass().refresh();
            }
        }
    }
})();
