(function () {
    'use strict';

    angular.module('JangraERP.pages.templates', [
        'JangraERP.pages.templates.materialTemplate',
        'JangraERP.pages.templates.productTemplate',
        'JangraERP.pages.templates.wareTemplate'
    ])
        .config(routeConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(nestDataListConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('templates', {
                url: '/szablony',
                abstract: true,
                template: '<ui-view />',
                title: 'Szablony',
                sidebarMeta: {
                    icon: 'ion-folder',
                    order: 350
                }
            });
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateProductionPlan', 'UpdateProductionPlan'],
            {
                id: 'ProductionPlan',
                options: {},
                fields: [
                    {id: 'operationNumber', config: {templateOptions: {required: true}}},
                    {id: 'operationNameId', config: {templateOptions: {required: true}}},
                    {id: 'professionGroupId', config: {templateOptions: {required: true}}},
                    {id: 'transportTime', config: {templateOptions: {required: true}}},
                    {id: 'preparationTime', config: {templateOptions: {required: true}}},
                    {id: 'operationTime', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['CreateMaterialParts', 'UpdateMaterialParts'],
            {
                id: 'MaterialParts',
                options: {},
                fields: [
                    {id: 'ref_materialTemplate', config: {templateOptions: {required: true}}},
                    {id: 'cuttedSectionSize', config: {
                        templateOptions: {required: true, tempLabel: 'Ciętka'},
                        expressionProperties: {
                            'templateOptions.label': 'to.tempLabel + " [ " + model.materialTemplate.materialType.cuttingDimensionUnit + " ]"',
                            'templateOptions.disabled': '!model.materialTemplate'
                        }
                    }},
                    {id: 'cuttedSectionSizeWithBorders', config: {
                        templateOptions: {required: true, tempLabel: 'Limit ciętki'},
                        expressionProperties: {
                            'templateOptions.label': 'to.tempLabel + " [ " + model.materialTemplate.materialType.cuttingDimensionUnit + " ]"',
                            'templateOptions.disabled': '!model.materialTemplate'
                        }
                    }},
                    {id: 'count', config: {templateOptions: {required: true}}},
                    'positionOnTechDraw',
                    'comments'
                ]
            }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['CreateSemiproductParts', 'UpdateSemiproductParts'],
            {
                id: 'SemiproductParts',
                options: {},
                fields: [
                    {id: 'ref_productTemplate', config: {templateOptions: {required: true}}},
                    {id: 'count', config: {templateOptions: {required: true}}},
                    'positionOnTechDraw',
                    'comments'
                ]
            }
        );

        FormlyConfigServiceProvider.addDefaultConfig(['CreateWareParts', 'UpdateWareParts'],
            {
                id: 'WareParts',
                options: {},
                fields: [
                    {id: 'ref_wareTemplate', config: {templateOptions: {required: true}}},
                    {id: 'count', config: {templateOptions: {required: true}}},
                    'positionOnTechDraw',
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('ProductionPlan',
            {
                id: 'ProductionPlan',
                options: {},
                availableFields: {
                    operationNumber: null,
                    operationNameId: null,
                    transportTime: null,
                    preparationTime: null,
                    operationTime: null,
                    comments: null
                }
            }
        );

        PreviewConfigServiceProvider.addDefaultConfig('MaterialParts',
            {
                id: 'MaterialParts',
                options: {},
                availableFields: {
                    ref_materialTemplate: null,
                    cuttedSectionSize: null,
                    cuttedSectionSizeWithBorders: null,
                    count: null,
                    positionOnTechDraw: null,
                    comments: null
                }
            }
        );

        PreviewConfigServiceProvider.addDefaultConfig('SemiproductParts',
            {
                id: 'SemiproductParts',
                options: {},
                availableFields: {
                    ref_productTemplate: null,
                    count: null,
                    positionOnTechDraw: null,
                    comments: null
                }
            }
        );

        PreviewConfigServiceProvider.addDefaultConfig('WareParts',
            {
                id: 'WareParts',
                options: {},
                availableFields: {
                    ref_wareTemplate: null,
                    count: null,
                    positionOnTechDraw: null,
                    comments: null
                }
            }
        );
    }

    /** @ngInject */
    function nestDataListConfig(NestDataListConfigServiceProvider) {
        NestDataListConfigServiceProvider.addDefaultConfig('ProductionPlan',
            {
                id: 'ProductionPlan',
                options: {
                    main: 'productionPlan',
                    path: 'productionPlan',
                    formlyConfig: 'ProductionPlan',
                    previewConfig: 'ProductionPlan',
                    viewTemplateUrl: 'app/core/templates/nestDataList/productionPlan.html',
                    methods: {
                        create: 'addProdPlanItem',
                        update: 'updateProdPlanItem',
                        remove: 'removeProdPlanItem'
                    }
                }
            }
        );

        NestDataListConfigServiceProvider.addDefaultConfig('MaterialParts',
            {
                id: 'MaterialParts',
                options: {
                    main: 'materialParts',
                    path: 'partCard.materialParts',
                    formlyConfig: 'MaterialParts',
                    previewConfig: 'MaterialParts',
                    viewTemplateUrl: 'app/core/templates/nestDataList/partCardMaterial.html',
                    methods: {
                        create: 'addMaterialPart',
                        update: 'updateMaterialPart',
                        remove: 'removeMaterialPart'
                    }
                }
            }
        );

        NestDataListConfigServiceProvider.addDefaultConfig('SemiproductParts',
            {
                id: 'SemiproductParts',
                options: {
                    main: 'semiproductParts',
                    path: 'partCard.semiproductParts',
                    formlyConfig: 'SemiproductParts',
                    previewConfig: 'SemiproductParts',
                    viewTemplateUrl: 'app/core/templates/nestDataList/partCardSemiproduct.html',
                    methods: {
                        create: 'addSemiproductPart',
                        update: 'updateSemiproductPart',
                        remove: 'removeSemiproductPart'
                    }
                }
            }
        );

        NestDataListConfigServiceProvider.addDefaultConfig('WareParts',
            {
                id: 'WareParts',
                options: {
                    main: 'wareParts',
                    path: 'partCard.wareParts',
                    formlyConfig: 'WareParts',
                    previewConfig: 'WareParts',
                    viewTemplateUrl: 'app/core/templates/nestDataList/partCardWare.html',
                    methods: {
                        create: 'addWarePart',
                        update: 'updateWarePart',
                        remove: 'removeWarePart'
                    }
                }
            }
        );
    }

})();
