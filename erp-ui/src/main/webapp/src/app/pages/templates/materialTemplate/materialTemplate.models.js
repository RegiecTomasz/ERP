/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.materialTemplate')
        .factory('MaterialTemplateModel', MaterialTemplateModel)
        .factory('MaterialTemplateListModel', MaterialTemplateListModel);

    /** @ngInject */
    function MaterialTemplateModel(BaseModelClass, MaterialTemplateRequestService) {
        return class MaterialTemplateClass extends BaseModelClass {
            constructor(materialTemplateData) {
                super(MaterialTemplateRequestService, materialTemplateData);
            }

            static getById(materialTemplateId) {
                return new MaterialTemplateClass().getById(materialTemplateId);
            };
        }
    }

    /** @ngInject */
    function MaterialTemplateListModel(ListModelClass, MaterialTemplateRequestService, MaterialTemplateModel) {
        return class MaterialTemplatesListClass extends ListModelClass {
            constructor() {
                super(MaterialTemplateRequestService, MaterialTemplateModel);
            }

            static list() {
                return new MaterialTemplatesListClass().refresh();
            }
        }
    }
})();
