/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.templates.wareTemplate')
        .factory('WareTemplateModel', WareTemplateModel)
        .factory('WareTemplateListModel', WareTemplateListModel);

    /** @ngInject */
    function WareTemplateModel(BaseModelClass, WareTemplateRequestService) {
        return class WareTemplateClass extends BaseModelClass {
            constructor(wareTemplateData) {
                super(WareTemplateRequestService, wareTemplateData);
            }

            static getById(wareTemplateId) {
                return new WareTemplateClass().getById(wareTemplateId);
            };
        }
    }

    /** @ngInject */
    function WareTemplateListModel(ListModelClass, WareTemplateRequestService, WareTemplateModel) {
        return class WareTemplatesListClass extends ListModelClass {
            constructor() {
                super(WareTemplateRequestService, WareTemplateModel);
            }

            static list() {
                return new WareTemplatesListClass().refresh();
            }
        }
    }
})();
