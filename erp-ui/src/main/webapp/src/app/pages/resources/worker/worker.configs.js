/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.resources.worker')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('Worker',
            {
                vm: '$ctrl',
                id: 'Worker',
                options: {selectable: true},
                controls: {
                    name: 'listControls',
                    menu: [{
                        index: 3,
                        item: {
                            html: '<a class=btn-with-icon href><i class=ion-help></i>Zwolnij</a>',
                            click: (item, scope) => scope.$ctrl.fireWorker(item),
                            displayed: item => item.data.state === 'HIRED'
                        }
                    }, {
                        index: 4,
                        item: {
                            html: '<a class=btn-with-icon href ng-show="item.data.state === \'FIRED\' || item.data.state === \'RESTORED\'"><i class=ion-flash></i>Zatrudnij</a>',
                            click: (item, scope) => scope.$ctrl.hireWorker(item),
                            displayed: item => (item.data.state === 'FIRED' || item.data.state === 'RESTORED')
                        }
                    }, {
                        index: 5,
                        item: null
                    }]
                },
                defaultColumns: ['firstName', 'secondName', 'workerCode', 'comment', 'WORKER_STATE', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'resources.worker'},
                    firstName: null,
                    secondName: null,
                    workerCode: null,
                    comment: null,
                    WORKER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateWorker', 'UpdateWorker'],
            {
                id: 'Worker',
                options: {},
                fields: [
                    {id: 'firstName', config: {templateOptions: {required: true}}},
                    {id: 'secondName', config: {templateOptions: {required: true}}},
                    {id: 'professionGroupId2', config: {templateOptions: {required: true}}},
                    'comment'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('Worker',
            {
                id: 'Worker',
                options: {},
                availableFields: {
                    firstName: null,
                    secondName: null,
                    workerCode: null,
                    comment: null,
                    WORKER_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }


    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'resources.worker',
                abstract: true,
                url: '/pracownicy',
                title: 'Pracownicy',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 200,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista pracowników',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "WorkerListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (WorkerListModel) {
                                return WorkerListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'Worker',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Maszyna',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Maszyna';
                        },
                        resolve: {
                            elementModel: function ($stateParams, WorkerModel) {
                                return WorkerModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/zasoby/pracownicy', '/zasoby/pracownicy/lista');
    }
})();
