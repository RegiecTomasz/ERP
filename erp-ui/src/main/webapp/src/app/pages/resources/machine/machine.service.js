/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.resources.machine')
        .service('MachineRequestService', MachineRequestService);

    /** @ngInject */
    function MachineRequestService(RequestServiceClass) {
        class MachineRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Machine', 'machines');
            }
        }

        return new MachineRequestServiceClass();
    }
})();
