/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.resources.worker')
        .service('WorkerRequestService', WorkerRequestService);

    /** @ngInject */
    function WorkerRequestService(RequestServiceClass) {
        class WorkerRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Worker', 'workers');
            }
            
            fireWorker(workerId) {
                return this.requestHandler.send({
                    method: 'fireWorker',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.workers.FireWorkerRequest',
                    data: {id: workerId}
                });
            };
            
            hireWorker(workerId) {
                return this.requestHandler.send({
                    method: 'hireWorker',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.workers.HireWorkerRequest',
                    data: {id: workerId}
                });
            };            
        }

        return new WorkerRequestServiceClass();
    }
})();
