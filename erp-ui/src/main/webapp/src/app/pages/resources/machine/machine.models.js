/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.resources.machine')
        .factory('MachineModel', MachineModel)
        .factory('MachineListModel', MachineListModel);

    /** @ngInject */
    function MachineModel(BaseModelClass, MachineRequestService) {
        return class MachineClass extends BaseModelClass {
            constructor(machineData) {
                super(MachineRequestService, machineData, {
                    reqParser: function(reqData){
                        if(reqData.machineWorkers && reqData.machineWorkers.length){
                            reqData.machineWorkers.forEach(el => {
                                el.workerId = el.worker.id
                            })
                        }
                    }
                });
            }

            static getById(machineId) {
                return new MachineClass().getById(machineId);
            };
        }
    }

    /** @ngInject */
    function MachineListModel(ListModelClass, MachineRequestService, MachineModel) {
        return class MachinesListClass extends ListModelClass {
            constructor() {
                super(MachineRequestService, MachineModel);
            }

            static list() {
                return new MachinesListClass().refresh();
            }
        }
    }
})();
