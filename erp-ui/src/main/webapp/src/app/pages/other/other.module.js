(function () {
    'use strict';

    angular.module('JangraERP.pages.other', [
        'JangraERP.pages.other.shoppingCart'
    ])
})();
