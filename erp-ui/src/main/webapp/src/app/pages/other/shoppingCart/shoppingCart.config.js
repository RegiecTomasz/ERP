/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.other.shoppingCart')
        .config(routeConfig);


    /** @ngInject */
    function routeConfig(stateHelperProvider) {
        stateHelperProvider
            .state({
                name: 'shoppingCart',
                url: '/koszyk',
                title: 'Koszyk',
                templateUrl: 'app/pages/other/shoppingCart/shoppingCart.template.html',
                controller: "ShoppingCartCtrl",
                controllerAs: "$ctrl",
                resolve: {
                    shoppingCartModel : function(ShoppingCartRequestService){
                        return ShoppingCartRequestService.getShoppingCart();
                    }
                }
            });
    }
})();
