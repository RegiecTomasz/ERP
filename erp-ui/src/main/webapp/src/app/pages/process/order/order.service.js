/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.order')
        .service('OrderRequestService', OrderRequestService);

    /** @ngInject */
    function OrderRequestService(RequestServiceClass) {
        class OrderRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('Order', 'order');
            }

            cancelOrder(orderId) {
                return this.requestHandler.send({
                    method: 'cancelOrder',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.order.CancelOrderRequest',
                    data: {orderId: orderId}
                });
            };

            cancelAllOrders() {
                return this.requestHandler.send({
                    method: 'cancelAllOrders',
                    service: this.name + 'Service'
                });
            };

            generateAllNewOrdersProdOrdAndProdJobs() {
                return this.requestHandler.send({
                    method: 'generateAllNewOrdersProdOrdAndProdJobs',
                    service: this.name + 'Service'
                });
            };

            regenerateAll() {
                return this.requestHandler.send({
                    method: 'regenerateAll',
                    service: this.name + 'Service'
                });
            };
        }

        return new OrderRequestServiceClass();
    }
})();
