/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    function ProductionOrderListCtrl($controller, listModel, listConfig) {
        angular.extend(this, $controller('BaseListCtrl', {listModel: listModel, listConfig: listConfig}));

        this.generateProductionJobs = function (item) {
            item.generateProductionJobs().then(() => {
                this.listModel.refresh();
            })
        };

        this.generateMaterialDemands = function (item) {
            item.generateMaterialDemands().then(() => {
                this.listModel.refresh();
            })
        };

        this.finishProductionOrder = function (item) {
            item.finishProductionOrder().then(() => {
                this.listModel.refresh();
            })
        };
    }

    angular
        .module('JangraERP.pages.process.productionOrder')
        .controller('ProductionOrderListCtrl', ProductionOrderListCtrl)
})();
