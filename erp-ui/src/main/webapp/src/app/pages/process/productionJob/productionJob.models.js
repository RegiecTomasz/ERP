/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.productionJob')
        .factory('ProductionJobModel', ProductionJobModel)
        .factory('ProductionJobListModel', ProductionJobListModel);

    /** @ngInject */
    function ProductionJobModel(BaseModelClass, ProductionJobRequestService) {
        return class ProductionJobClass extends BaseModelClass {
            constructor(productionJobData) {
                super(ProductionJobRequestService, productionJobData);
            }

            static getById(productionJobId) {
                return new ProductionJobClass().getById(productionJobId);
            };
        }
    }

    /** @ngInject */
    function ProductionJobListModel(ListModelClass, ProductionJobRequestService, ProductionJobModel) {
        return class ProductionJobsListClass extends ListModelClass {
            constructor() {
                super(ProductionJobRequestService, ProductionJobModel);
            }

            static list() {
                return new ProductionJobsListClass().refresh();
            }
        }
    }
})();
