/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
            .module('JangraERP.pages.process.productionOrder')
            .service('ProductionOrderRequestService', ProductionOrderRequestService);

    /** @ngInject */
    function ProductionOrderRequestService(RequestServiceClass) {
        class ProductionOrderRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('ProductionOrder', 'productionorder');
            }

            generateProductionOrder(orderId) {
                return this.requestHandler.send({
                    method: 'generateProductionOrder',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.order.GenerateProductionOrderRequest',
                    data: {orderId: orderId}
                });
            }

            generateProductionOrderDisposition(data) {
                return this.requestHandler.send({
                    method: 'generateProductionOrderDisposition',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.order.GenerateProductionOrderDispositionRequest',
                    data: data
                });
            }

            finishProductionOrder(data) {
                return this.requestHandler.send({
                    method: 'finishProductionOrder',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.productionorder.FinishProductionOrderRequest',
                    data: {"productionOrderId": data.id, "closeProductionJobs": true}
                });
            }

        }
        return new ProductionOrderRequestServiceClass();
    }
})();
