/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class GenerateDispositionCtrl {
        constructor(FormlyConfigService, elementModel) {
//            this.modalTitle = elementModel.getName() + '.remove.title';
            this.modalInclude = 'app/pages/process/order/generateDisposition.modal.html';
            this.config = FormlyConfigService.getFormlyConfig('GenerateDispositionModalConfig');
            this.element = elementModel;
            var toDispose = elementModel.data.orderPositions && elementModel.data.orderPositions[0] && elementModel.data.orderPositions[0].toDisposeCount ? elementModel.data.orderPositions[0].toDisposeCount : 0;
            var disposed = elementModel.data.orderPositions && elementModel.data.orderPositions[0] && elementModel.data.orderPositions[0].disposedCount ? elementModel.data.orderPositions[0].disposedCount : 0;
            this.model = {
                toDisposeCount: toDispose,
                disposedCount: disposed
            };
        }

        doIt($close) {
            this.dispositionForm.$setSubmitted();
            if (this.dispositionForm.$valid) {
                var requestData = {
                    orderId: this.element.id,
                    deliveryDate: this.model.deliveryDate.toDate(),
                    count: this.model.count
                };
                this.cgBusy = this.element.generateProductionOrderDisposition(requestData).then(() => {
//                    this.model.updateTemp();
                    $close();
                }, angular.noop);
            }
        }

    }

    angular
            .module('JangraERP.core')
            .controller('GenerateDispositionCtrl', GenerateDispositionCtrl);
})();
