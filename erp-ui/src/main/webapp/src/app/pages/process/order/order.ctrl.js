/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    /** @ngInject */
    class OrderListCtrl {
        constructor($uibModal, UtilsService, listModel, listConfig) {
            this.listModel = listModel;
            this.listConfig = listConfig;

            this.modalPreview = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/core/controllers/base.preview.modal.html',
                    size: 'auto',
                    backdrop: 'static',
                    controller: 'BasePreviewCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        elementModel: element
                    }
                }).result.then(angular.noop, angular.noop);
            };

            this.modalCreate = function (element) {
                $uibModal.open({
                    animation: true,
                    templateUrl: 'app/core/controllers/base.create.modal.html',
                    size: 'md',
                    backdrop: 'static',
                    controller: 'BaseCreateCtrl',
                    controllerAs: '$ctrl',
                    bindToController: true,
                    resolve: {
                        elementModel: element || new this.listModel.elementClass(),
                    }
                }).result.then(() => {
                    this.refresh()
                }, () => {
                    this.refresh()
                });
            };

            this.modalRemove = function (element) {
                element.remove(false).then(responseData => {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'app/core/controllers/base.remove.modal.html',
                        size: 'auto',
                        backdrop: 'static',
                        controller: 'BaseRemoveCtrl',
                        controllerAs: '$ctrl',
                        resolve: {
                            elementModel: element
                        }
                    }).result.then(() => {
                        element.remove().then(() => {
                            this.refresh();
                        })
                    }, () => {
                        this.refresh()
                    })
                })
            };

            this.generateProductionOrderDisposition = function (element) {
                if(element.data.frameworkContractOrder){
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'app/pages/process/order/generateDisposition.modal.html',
                        size: 'md',
                        backdrop: 'static',
                        controller: 'GenerateDispositionCtrl',
                        controllerAs: '$ctrl',
                        resolve: {
                            elementModel: element
                        }
                    }).result.then((data) => {
                        this.refresh();
                    }, () => {
                        this.refresh()
                    });
                }
            };

            this.print = function () {
                UtilsService.print('#' + (this.listConfig.id || 'Base') + 'ListTable');
            };

            this.generateJobs = function (item) {
                item.generateJobs().then(() => {
                    this.refresh();
                })
            };

            this.generateProductionOrder = function (item) {
                if(!item.data.frameworkContractOrder){
                    item.generateProductionOrder().then(() => {
                        this.refresh();
                    })
                }
            };

            this.cancelOrder = function (item) {
                item.cancelOrder().then(() => {
                    this.refresh();
                })
            };

            this.cancelAllOrders = function () {
                this.listModel.cancelAllOrders().then(() => {
                    this.refresh();
                })
            };

            this.generateAllNewOrdersProdOrdAndProdJobs = function () {
                this.listModel.generateAllNewOrdersProdOrdAndProdJobs().then(() => {
                    this.refresh();
                })
            };

            this.regenerateAll = function () {
                this.listModel.regenerateAll().then(() => {
                    this.refresh();
                })
            };

        }

        refresh() {
            this.listModel.refresh()
        }
    }

    angular
            .module('JangraERP.pages.process.productionOrder')
            .controller('OrderListCtrl', OrderListCtrl)
})();
