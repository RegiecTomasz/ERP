/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.productionOrder')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('ProductionOrder',
            {
                vm: '$ctrl',
                id: 'ProductionOrder',
                options: {selectable: true},
                controls: {
                    name: 'listControlsWithoutAdd',
                    menu: [{
//                        index: 3,
//                        item: {
//                            html: '<a class=btn-with-icon href><i class=ion-flash></i>Generuj zapotrzebowanie materiałow</a>',
//                            click: (item, scope) => scope.$ctrl.generateMaterialDemands(item)
//                        }
//                    }, {
//                        index: 4,
//                        item: {
//                            html: '<a class=btn-with-icon href><i class=ion-flash></i>Generuj zadania</a>',
//                            click: (item, scope) => scope.$ctrl.generateProductionJobs(item)
//                        }
//                    }, {
                        index: 5,
                        item: {
                            html: '<a class=btn-with-icon href><i class=ion-ios-cloud-download-outline></i>Pobierz przewodnik</a>',
                            click: function(item){window.open("/api/invoker/ProductionOrderService/getProductionGuide?productionOrderId=" + item.data.id, "_blank");},
                            compile: true
                        }
                    }, {
                        index: 6,
                        item: null
                    }]
                },
                defaultColumns: ['orderNo', 'nr', 'productNo', 'name', 'count', 'PRODUCTION_ORDER_STATE', 'comments', 'datePicker_deliveryDate', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'process.productionOrder'},
                    orderNo: null,
                    nr: {key: 'no'},
                    productNo: {key: 'product.productTemplate.productNo'},
                    name: {key: 'product.productTemplate.name'},
                    count: {key: 'count'},
                    comments: null,
                    PRODUCTION_ORDER_STATE: null,
                    datePicker_deliveryDate: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateProductionOrder', 'UpdateProductionOrder'],
            {
                id: 'ProductionOrder',
                options: {},
                fields: [
                    {id: 'nr', config: {key: 'no', templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('ProductionOrder',
            {
                id: 'ProductionOrder',
                options: {},
                availableFields: {
                    nr: {key: 'no'},
                    // datePicker_orderCreationDate: null,
                    // provider: null,
                    comments: null,
                    //  {
                    //      key: 'materialDemandsStr',
                    //      type: 'validationMessageTable',
                    //      templateOptions: {
                    //          label: 'Zapotrzebowania',
                    //          countLabel: 'Ilość:'
                    //      }
                    //  },
                    guideForProductionOrder: null,
                    PRODUCTION_ORDER_STATE: null,
                    datePicker_deliveryDate: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'process.productionOrder',
                abstract: true,
                url: '/zamowieniaProdukcyjne',
                title: 'Zamówienia produkcyjne',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 200,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista zamówień produkcyjnych',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "ProductionOrderListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (ProductionOrderListModel) {
                                return ProductionOrderListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'ProductionOrder',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Maszyna',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Zamówienie produkcyjne';
                        },
                        resolve: {
                            elementModel: function ($stateParams, ProductionOrderModel) {
                                return ProductionOrderModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/procesy/zamowieniaProdukcyjne', '/procesy/zamowieniaProdukcyjne/lista');
    }
})();
