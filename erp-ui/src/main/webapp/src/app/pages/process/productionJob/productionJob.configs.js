/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.productionJob')
        .config(listConfig)
        .config(formlyConfig)
        .config(previewConfig)
        .config(routeConfig);

    /** @ngInject */
    function listConfig(ListConfigServiceProvider) {
        ListConfigServiceProvider.addDefaultConfig('ProductionJob',
            {
                vm: '$ctrl',
                id: 'ProductionJob',
                options: {selectable: true},
                controls: {name: 'listControlsWithoutAdd'},
                defaultColumns: ['productionOrderNo', 'nr', 'productNo', 'name', 'count', 'comments', 'PRODUCTION_JOB_STATE', 'datePicker_creationDate', 'datePicker_lastModificationDate'],
                availableColumns: {
                    _id: {router: 'process.productionJob'},
                    productionOrderNo: null,
                    nr: {key: 'no'},
                    productNo: {key: 'detal.no'},
                    name: {key: 'detal.name'},
                    count: {key: 'detal.count'},
                    comments : null,
                    PRODUCTION_JOB_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function formlyConfig(FormlyConfigServiceProvider) {
        FormlyConfigServiceProvider.addDefaultConfig(['CreateProductionJob', 'UpdateProductionJob'],
            {
                id: 'ProductionJob',
                options: {},
                fields: [
//                    {id: 'productionOrderNo', config: {templateOptions: {required: true}}},
                    'comments'
                ]
            }
        );
    }

    /** @ngInject */
    function previewConfig(PreviewConfigServiceProvider) {
        PreviewConfigServiceProvider.addDefaultConfig('ProductionJob',
            {
                id: 'ProductionJob',
                options: {},
                availableFields: {
                    productionOrderNo: null,
                    PRODUCTION_JOB_STATE: null,
                    datePicker_creationDate: null,
                    datePicker_lastModificationDate: null
                }
            }
        );
    }

    /** @ngInject */
    function routeConfig(stateHelperProvider, $urlRouterProvider) {
        stateHelperProvider
            .state({
                name: 'process.productionJob',
                abstract: true,
                url: '/zadaniaProdukcyjne',
                title: 'Zadania produkcyjne',
                template: '<ui-view />',
                sidebarMeta: {
                    order: 300,
                },
                children: [
                    {
                        name: 'list',
                        url: '/lista',
                        title: 'Lista zadań produkcyjnych',
                        templateUrl: 'app/core/controllers/base.list.html',
                        controller: "BaseListCtrl",
                        controllerAs: "$ctrl",
                        resolve: {
                            listModel: function (ProductionJobListModel) {
                                return ProductionJobListModel.list();
                            },
                            listConfig: function () {
                                return {
                                    id: 'ProductionJob',
                                    removeConsequencesTpl: null
                                };
                            }
                        }
                    },
                    {
                        name: 'element',
                        abstract: true,
                        url: '/{id:[0-9a-f]{24}}',
                        title: 'Zadanie produkcyjne',
                        templateUrl: 'app/core/controllers/base.element.html',
                        controller: 'BaseElementCtrl',
                        controllerAs: "$ctrl",
                        breadcrumb: function () {
                            return 'Zadanie produkcyjne';
                        },
                        resolve: {
                            elementModel: function ($stateParams, ProductionJobModel) {
                                return ProductionJobModel.getById($stateParams.id);
                            }
                        },
                        children: [
                            {
                                name: 'detail',
                                url: '/szczegoly',
                                title: 'Szczegóły',
                                templateUrl: 'app/core/controllers/base.detail.html',
                                controller: 'BaseDetailCtrl',
                                controllerAs: "$ctrl"
                            }
                        ]
                    }
                ]
            });

        $urlRouterProvider.when('/procesy/zadaniaProdukcyjne', '/procesy/zadaniaProdukcyjne/lista');
    }
})();
