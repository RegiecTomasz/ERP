/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.productionJob')
        .service('ProductionJobRequestService', ProductionJobRequestService);

    /** @ngInject */
    function ProductionJobRequestService(RequestServiceClass) {
        class ProductionJobRequestServiceClass extends RequestServiceClass {
            constructor() {
                super('ProductionJob', 'productionjob');
            }

            generateProductionJobs(productionOrderId) {
                return this.requestHandler.send({
                    method: 'generateProductionJobs',
                    service: this.name + 'Service',
                    class: 'regiec.knast.erp.api.dto.productionjob.GenerateProductionJobsRequest',
                    data: {productionOrderId: productionOrderId}
                });
            };
        }

        return new ProductionJobRequestServiceClass();
    }
})();
