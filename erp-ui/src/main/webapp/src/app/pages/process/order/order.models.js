/**
 * @author Tomasz Regiec <regiectomasz@gmail.com>
 * @copyright Tomasz Regiec
 */
(function () {
    angular
        .module('JangraERP.pages.process.order')
        .factory('OrderModel', OrderModel)
        .factory('OrderListModel', OrderListModel);

    /** @ngInject */
    function OrderModel(BaseModelClass, OrderRequestService, ProductionOrderRequestService) {
        return class OrderClass extends BaseModelClass {
            constructor(orderData) {
                super(OrderRequestService, orderData);
            }

            generateProductionOrder () {
                return BaseModelClass.runServiceFunc('generateProductionOrder', ProductionOrderRequestService,  this.data.id, {message: "ProductionOrder.alert.generate"});
            };

            generateProductionOrderDisposition (data) {
                return BaseModelClass.runServiceFunc('generateProductionOrderDisposition', ProductionOrderRequestService,  data, {message: "ProductionOrder.alert.generate"});
            };

            cancelOrder () {
                return this.runServiceFunc('cancelOrder', this.data.id, {message: "Order.alert.cancelOrder"});
            };

            static getById(orderId) {
                return new OrderClass().getById(orderId);
            };
        }
    }

    /** @ngInject */
    function OrderListModel(ListModelClass, OrderRequestService, OrderModel) {
        return class OrdersListClass extends ListModelClass {
            constructor() {
                super(OrderRequestService, OrderModel);
            }

            cancelAllOrders(){
                return this.runServiceFunc('cancelAllOrders');
            }

            regenerateAll(){
                return this.runServiceFunc('regenerateAll');
            };

            generateAllNewOrdersProdOrdAndProdJobs(){
                return this.runServiceFunc('generateAllNewOrdersProdOrdAndProdJobs');
            };

            static list() {
                return new OrdersListClass().refresh();
            }
        }
    }
})();
