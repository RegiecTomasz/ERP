const express = require('express');
const proxy = require('express-http-proxy');
const opn = require('opn');
const app = express();
const port = 8081;

app.use('/src', express.static('src'));
app.use('/ui', express.static('development'));
app.use('/bower_components', express.static('bower_components'));

app.use('/', proxy('http://localhost:8080'));

app.listen(port, () => {
    opn(`http://localhost:${port}/ui`);
    console.log(`ERP development app listening on port ${port}!`);
});
