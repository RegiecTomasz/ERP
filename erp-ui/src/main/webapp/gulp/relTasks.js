let del = require('del');
let gulp = require('gulp');
let config = require('./config');

let plugins = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
    scope: ['devDependencies']
});

/**
 * Release - main task
 */
gulp.task('rel', ['rel_clean'], () =>
    gulp.start('rel_main')
);

/**
 * Release - main task
 */
gulp.task('rel_main', ['rel_fonts', 'rel_js_lib', 'rel_css_lib', 'rel_copy_assets', 'rel_js', 'rel_scss'], () =>
    gulp.start('rel_html')
);

/*
 * Release - clean task
 */
gulp.task('rel_clean', () => del([config.paths.dist, config.paths.tmp]));

/**
 * Release - fonts task
 */
gulp.task('rel_fonts', () =>
    gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe(gulp.dest(`${config.paths.dist}/fonts`))
);

/**
 * Release - js library task
 */
gulp.task('rel_js_lib', () =>
    gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter(['**/*.js', '!**/require.js']))
        // .pipe(plugins.uglify({
        //     mangle: false
        // }))
        .pipe(plugins.concat('vendors.js'))
        .pipe(plugins.rev())
        .pipe(gulp.dest(`${config.paths.dist}/js`))
);

/**
 * Release - css library task
 */
gulp.task('rel_css_lib', () =>
    gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter(['**/*.css']))
        .pipe(plugins.concat('vendors.css'))
        .pipe(plugins.csso())
        .pipe(plugins.rev())
        .pipe(gulp.dest(`${config.paths.dist}/css`))
);

/**
* Release - copy assets task
*/
gulp.task('rel_copy_assets', () =>
    gulp.src(`${config.paths.src}/assets/**/*`)
        .pipe(gulp.dest(`${config.paths.dist}/assets`))
);

/**
 * Release - html partials task
 */
gulp.task('rel_partials', () =>
    gulp.src(`${config.paths.src}/app/**/*.html`)
        .pipe(plugins.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(plugins.angularTemplatecache('templateCacheHtml.js', {
            module: 'JangraERP',
            root: 'app'
        }))
        .pipe(gulp.dest(`${config.paths.tmp}/templates`))
);

/**
 * Release - javaScript task
 */
gulp.task('rel_js', ['rel_partials'], () =>
    gulp.src([
        `${config.paths.src}/assets/js/*.js`,
        `!${config.paths.src}/assets/js/html2canvas.min.js`,
        `${config.paths.src}/app/**/*.module.js`,
        `${config.paths.src}/app/app.js`,
        `${config.paths.src}/app/**/*.js`,
        `${config.paths.tmp}/templates/templateCacheHtml.js`
    ])
        .pipe(plugins.stripCode({
            start_comment: "start-dev-code",
            end_comment: "end-dev-code"
        }))
        .pipe(plugins.babel({
            presets: ["es2015"]
        }))
        .pipe(plugins.ngAnnotate())
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.concat('main.js'))
        .pipe(plugins.rev())
        .pipe(gulp.dest(`${config.paths.dist}/js`))
);

/*
 * Release - scss task
 */
gulp.task('rel_scss', () =>
    gulp.src(`${config.paths.src}/main.scss`)
        .pipe(plugins.inject(
            gulp.src([
                `${config.paths.src}/scss/**/*.scss`,
                `!${config.paths.src}/scss/theme/conf/**/*.scss`,
                `!${config.paths.src}/scss/404.scss`,
                `!${config.paths.src}/scss/auth.scss`
            ], {read: false}), {
                transform: function (filePath) {
                    return `@import '${filePath}';`;
                },
                starttag: '// injector',
                endtag: '// endinjector',
                ignorePath: [config.paths.src],
                addRootSlash: false
            }
        ))
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.csscomb())
        .pipe(plugins.autoprefixer())
        .pipe(plugins.csso())
        .pipe(plugins.rev())
        .pipe(gulp.dest(`${config.paths.dist}/css`))
);

/*
 * Release - html task
 */
gulp.task('rel_html', () =>
    gulp.src(`${config.paths.src}/index.html`)
        .pipe(plugins.data(config.data))
        .pipe(plugins.nunjucksRender())
        .pipe(plugins.inject(
            gulp.src([
                `${config.paths.dist}/css/vendors-*.css`,
                `${config.paths.dist}/css/main-*.css`
            ], {read: false}), {
                ignorePath: config.paths.dist,
                addRootSlash: false
            }
        ))
        .pipe(plugins.inject(
            gulp.src([
                `${config.paths.dist}/js/vendors-*.js`,
                `${config.paths.dist}/js/main-*.js`
            ], {read: false}), {
                relative: false,
                ignorePath: [config.paths.dev],
                addRootSlash: false,
                addPrefix: '..'
            })
        )
        .pipe(plugins.minifyHtml({
            empty: true,
            spare: true,
            quotes: true,
            conditionals: true
        }))
        .pipe(gulp.dest(config.paths.dist))
);
