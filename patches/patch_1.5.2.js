
db.semiproductTemplates.find({ "partCard.materialParts": { $exists: true } }).forEach(function(spt){
	spt.partCard.materialParts.forEach(function(mp){
		mp.positionId = ObjectId().str
	})
	db.semiproductTemplates.save(spt)
})
db.semiproductTemplates.find({ "partCard.wareParts": { $exists: true } }).forEach(function(spt){
	spt.partCard.wareParts.forEach(function(mp){
		mp.positionId = ObjectId().str
	})
	db.semiproductTemplates.save(spt)
})
db.semiproductTemplates.find({ "partCard.semiproductParts": { $exists: true } }).forEach(function(spt){
	spt.partCard.semiproductParts.forEach(function(mp){
		mp.positionId = ObjectId().str
	})
	db.semiproductTemplates.save(spt)
})


db.productTemplates.find({ "partCard.materialParts": { $exists: true } }).forEach(function(spt){
	spt.partCard.materialParts.forEach(function(mp){
		mp.positionId = ObjectId().str
	})
	db.productTemplates.save(spt)
})
db.productTemplates.find({ "partCard.wareParts": { $exists: true } }).forEach(function(spt){
	spt.partCard.wareParts.forEach(function(mp){
		mp.positionId = ObjectId().str
	})
	db.productTemplates.save(spt)
})
db.productTemplates.find({ "partCard.semiproductParts": { $exists: true } }).forEach(function(spt){
	spt.partCard.semiproductParts.forEach(function(mp){
		mp.positionId = ObjectId().str
	})
	db.productTemplates.save(spt)
})


db.semiproductTemplates.find({ "productionPlan": { $exists: true } }).forEach(function(spt){
	spt.productionPlan.forEach(function(pp){
		pp.positionId = ObjectId().str
	})
	db.semiproductTemplates.save(spt)
})
db.productTemplates.find({ "productionPlan": { $exists: true } }).forEach(function(pt){
	pt.productionPlan.forEach(function(pp){
		pp.positionId = ObjectId().str
	})
	db.productTemplates.save(pt)
})