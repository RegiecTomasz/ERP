
var mSet = []
var wSet = []
var pSet = []

function addToSet(set, obj){
	if(set.indexOf(obj) < 0){
		set.push(obj)
	}
}

var printOnlyErrors = true
function pr(text, status){
	if(printOnlyErrors){
		if(!status){
			print(text)
		}
	}else{		
		print(text)
	}
}

function checkIfRemoved(o, type){
	if(o.state && o.state === "REMOVED"){
		var t = "Object " + type + " " + o._id + " has state REMOVED "
		addToSet(mSet, t)
	}	
	if(o.removed){
		var t = "Object " + type + " " + o._id + " is removed \t | "+type +" \t  "  + (o.materialCode || o.name ) 
		addToSet(mSet, t)
	}
}


function checkMaterialParts(mps, parent){
  mps.forEach(function(mp){
    if(!mp.materialTemplate){
    	print("error in materialPart: no materialTemplate")
    }
    else{
      var entity = db.materialTemplates.findOne({"_id" : mp.materialTemplate.$id})
      var b = entity ? "ok" : ("nie istnieje!!!! w " + parent)
	  checkIfRemoved(entity, "materialTemplate");
      pr("materialTemplate._id: " +  mp.materialTemplate.$id + " " + b, b)
    }
  })
}
function checkWareParts(wps, parent){
  wps.forEach(function(wp){
    if(!wp.wareTemplate){
    	print("error in warePart: no wareTemplate")
    }
    else{
      var entity = db.wareTemplates.findOne({"_id" : wp.wareTemplate.$id})
      var b = entity ? "ok" : ("nie istnieje!!!! w " + parent)
	  checkIfRemoved(entity, "wareTemplate");
      pr("wareTemplate._id: " +  wp.wareTemplate.$id + " " + b, b)
    }
  })
}

function checkProductParts(spps, parent){
  spps.forEach(function(spp){    
    if(!spp.productTemplate){
    	print("error in productTemplate: no productTemplate")
    }
    else{
      var entity = db.productTemplates.findOne({"_id" : spp.productTemplate.$id})
      var b = entity ? "ok" : ("nie istnieje!!!! w " + parent)    
	  checkIfRemoved(entity, "productTemplate");
      pr("productTemplate._id: " +  spp.productTemplate.$id + " " + b, b)  
      if(entity && entity.partCard){    
		checkPartCard(entity, ("productTemplate: " + entity._id))
      }
    }    
  })
}
function checkPartCard(pt, parent){
	var partCard = pt.partCard
	if(partCard.materialParts){
		checkMaterialParts(partCard.materialParts, parent)  
	}
	if(partCard.wareParts){
		checkWareParts(partCard.wareParts, parent)  
	}
	if(partCard.semiproductParts){
		checkProductParts(partCard.semiproductParts, parent)  
	}
}

var productTemplates = db.productTemplates.find({ "partCard": { $exists: true } })
productTemplates.forEach(function(pt){
  checkPartCard(pt, ("productTemplate: " + pt._id))
})

mSet.forEach(function(o){
	print(o)
})