
var cs = 0
var cso = 0
var c1 = 0
var c2 = 0

// prepare recipients cache
var recipientsCache = {}
db.recipients.find({}).forEach(function(r){
	var properName = r.name
	var refName = r.name
	if(refName === "SCANCLIMBER Sp. z o.o"){
		refName = "Scanclimber"
		properName = "SCANCLIMBER Sp. z o.o";
	}else if(refName === "HOMAG POLSKA  Sp. z o.o"){
		refName = "Homag"
		properName = "HOMAG POLSKA  Sp. z o.o";
	}
	recipientsCache[refName] = {"id" : r._id, "name" : properName}
})

// Add 'sale' marker to every producttemplate
db.productTemplates.find({}).forEach(function(pt){
	pt.type = "SALE"
	cs++
	db.productTemplates.save(pt)
})
print("add sale to  " + cs + " productsTemplate ")

// Add 'saleOffers' with price, removes productType
db.productTemplates.find({"price" : {$exists : true}}).forEach(function(pt){
	var reci = recipientsCache[pt.productType.name];
	if(!reci){
		print("there is no recipient for name: " + pt.productType.name)
	}
	pt.salesOffers = [{"price" : pt.price, "recipient" : reci}]
	cso++
	delete pt.productType
	db.productTemplates.save(pt)
})
print("add saleOffer to  " + cso + " productsTemplate ")

// move semiproductTemplates to productTemplates collection
db.semiproductTemplates.find({}).forEach(function(spt){
  	spt.className = "regiec.knast.erp.api.entities.ProductTemplateEntity"
  	spt.creationProcess = "from SPT"
	spt.productNo = spt.semiproductNo
	spt.type = "NO_SALE"
	delete spt.semiproductNo
	db.productTemplates.save(spt)
	c1++
})
print("move " + c1 + " semiproductsTemplate to productTempaltes collection")


// change references from semiproductTemplate to productTemplate on partCard
db.productTemplates.find({ "partCard.semiproductParts": { $exists: true } }).forEach(function(pt){
	pt.partCard.semiproductParts.forEach(function(spp){
	 	var id = spp.semiproductTemplate.$id
		spp.productTemplate = DBRef("productTemplates", id)
		delete spp.semiproductTemplate
	})
	c2++
	db.productTemplates.save(pt)
})

var do1 = db.deliveryOrders.drop()
var md1 = db.materialDemands.drop()
var or1 = db.orders.drop()
var po1 = db.productionOrders.drop()
var po1 = db.productTypes.drop()
var po1 = db.products.drop()
var sp1 = db.semiproductTemplates.drop()

print("fix partcard references in " + c2 + " products")


