/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author fbrzuzka
 */
@lombok.Getter
@lombok.Setter
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class TestResult {

    private String caseName;
    private List<AlgorithmResult> algResult;

    public TestResult(String caseName) {
        this.caseName = caseName;
    }
    
    public void addValue(int makespan, String algorithm) {
        this.addValue(new AlgorithmResult(makespan, algorithm));
    }
    
    public void addValue(AlgorithmResult algorithmResult) {
        if(this.algResult == null){
            this.algResult = new ArrayList();
        }
        this.algResult.add(algorithmResult);
    }

    public AlgorithmResult findWinner() {
        return Collections.min(algResult, (AlgorithmResult a1, AlgorithmResult a2) -> a1.getMakespan().compareTo(a2.getMakespan()));         
    }

    public List<AlgorithmResult> findWinners() {        
        Collections.sort(algResult, (AlgorithmResult a1, AlgorithmResult a2) -> a1.getMakespan().compareTo(a2.getMakespan()));
        int mint = algResult.get(0).getMakespan();
        return algResult.stream().filter(a -> a.getMakespan() == mint).collect(Collectors.toList());
    }
}
