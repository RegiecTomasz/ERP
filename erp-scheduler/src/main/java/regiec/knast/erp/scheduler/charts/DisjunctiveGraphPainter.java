/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.charts;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.DefaultVisualizationModel;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationModel;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.ConstantDirectionalEdgeValueTransformer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.renderers.VertexLabelAsShapeRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.apache.commons.collections15.Transformer;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.scheduler.charts.utils.DisjunctiveGraphLayout;
import regiec.knast.erp.api.scheduler.charts.utils.MyVertexRenderer;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Edge;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.sa.SimulatedAnnealing;

/**
 *
 * @author fbrzuzka
 */
public class DisjunctiveGraphPainter extends JFrame {

    public DisjunctiveGraphPainter(DisjunctiveGraph graph, String title, int makespan) {
        this(graph, title + " makespan: " + String.valueOf(makespan));
    }

    public DisjunctiveGraphPainter(DisjunctiveGraph graph, String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Operation root = graph.getVertices().stream().filter(v -> v.vertexNo == 0).findFirst().get();
        Layout layout = new DisjunctiveGraphLayout(graph, root, 160, 160);
        Dimension preferredSize = new Dimension(1300, 700);
        final VisualizationModel<Operation, Edge> visualizationModel = new DefaultVisualizationModel(layout, preferredSize);

        VisualizationViewer<Operation, Edge> vv = new VisualizationViewer(visualizationModel, preferredSize);
//            VisualizationViewer<SimpleGraph.Vertex, Integer> vv = new VisualizationViewer(new ISOMLayout(graph), new Dimension(900, 50));
        // this class will provide both label drawing and vertex shapes
        VertexLabelAsShapeRenderer<Operation, Edge> vlasr = new VertexLabelAsShapeRenderer(vv.getRenderContext());

        // customize the render context
        vv.getRenderContext().setVertexLabelTransformer((Operation op) -> op.operationsToGraphLabel());

        vv.getRenderContext().setVertexShapeTransformer(vlasr);

        Transformer<Edge, String> stringer;
        stringer = new Transformer<Edge, String>() {
            @Override
            public String transform(Edge e) {
                if (graph.getEdgeType(e) == EdgeType.DIRECTED) {
                    return e.no + " : "
                            + e.weight.toString();
                }
//                Pair<OperationBase> ep = graph.getEndpoints(e);
//                return ep.getFirst().operationDuration.toString() + " -- " + ep.getSecond().operationDuration.toString();
                return "";
            }
        };
        vv.getRenderContext().setEdgeLabelTransformer(stringer);
        vv.getRenderContext().setEdgeDrawPaintTransformer(new Transformer<Edge, Paint>() {
            @Override
            public Paint transform(Edge input) {
                if(input.isDisjunctive){
                    return Color.ORANGE;
                }else{
                    return Color.BLACK;
                }
            }
        });
        MutableDirectionalEdgeValue mv = new MutableDirectionalEdgeValue(.5, .5);
        vv.getRenderContext().setEdgeLabelClosenessTransformer(mv);

        // customize the renderer
//        vv.getRenderer().setVertexRenderer(new GradientVertexRenderer(Color.gray, Color.white, true));
        vv.getRenderer().setVertexRenderer(new MyVertexRenderer(GanttChartPainter.sortKeys(graph.getVertices())));
        vv.getRenderer().setVertexLabelRenderer(vlasr);
        vv.setBackground(Color.white);
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());

        final DefaultModalGraphMouse<Operation, Integer> graphMouse = new DefaultModalGraphMouse();
        vv.setGraphMouse(graphMouse);

        Container content = getContentPane();
        GraphZoomScrollPane gzsp = new GraphZoomScrollPane(vv);
        content.add(gzsp);

        JComboBox modeBox = graphMouse.getModeComboBox();
        modeBox.addItemListener(graphMouse.getModeListener());
        graphMouse.setMode(ModalGraphMouse.Mode.PICKING);

        final ScalingControl scaler = new CrossoverScalingControl();

        JButton plus = new JButton("+");
        plus.addActionListener((ActionEvent e) -> {
            scaler.scale(vv, 1.1f, vv.getCenter());
        });
        JButton minus = new JButton("-");
        minus.addActionListener((ActionEvent e) -> {
            scaler.scale(vv, 1 / 1.1f, vv.getCenter());
        });

        JPanel controls = new JPanel();
        JPanel zoomControls = new JPanel(new GridLayout(2, 1));
        zoomControls.setBorder(BorderFactory.createTitledBorder("Zoom"));
        zoomControls.add(plus);
        zoomControls.add(minus);
        controls.add(zoomControls);
        controls.add(modeBox);

        Label makespanLabel = new Label(title);
        makespanLabel.setSize(300, 80);
        makespanLabel.setFont(new Font("Serif", Font.PLAIN, 20));
        controls.add(makespanLabel);
        content.add(controls, BorderLayout.SOUTH);
        vv.addMouseListener(new MyMouseAdapter(graphMouse));

        getContentPane().add(vv);
        pack();
        setVisible(true);
    }

    class MyMouseAdapter extends MouseAdapter {

        private final DefaultModalGraphMouse<Operation, Integer> graphMouse;
        private ModalGraphMouse.Mode lastModalGraphMouse = ModalGraphMouse.Mode.PICKING;

        public MyMouseAdapter(DefaultModalGraphMouse<Operation, Integer> graphMouse) {
            this.graphMouse = graphMouse;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getButton() == 3) {
                lastModalGraphMouse = switchMode(lastModalGraphMouse);
                graphMouse.setMode(lastModalGraphMouse);
            }
        }
    }

    public ModalGraphMouse.Mode switchMode(ModalGraphMouse.Mode mode) {
        return mode == ModalGraphMouse.Mode.PICKING ? ModalGraphMouse.Mode.TRANSFORMING : ModalGraphMouse.Mode.PICKING;
    }

    class MutableDirectionalEdgeValue extends ConstantDirectionalEdgeValueTransformer<Operation, Edge> {

        BoundedRangeModel undirectedModel = new DefaultBoundedRangeModel(5, 0, 0, 10);
        BoundedRangeModel directedModel = new DefaultBoundedRangeModel(5, 0, 0, 10);

        public MutableDirectionalEdgeValue(double undirected, double directed) {
            super(undirected, directed);
            undirectedModel.setValue(5);
            directedModel.setValue(5);
        }

        public BoundedRangeModel getDirectedModel() {
            return directedModel;
        }

        public BoundedRangeModel getUndirectedModel() {
            return undirectedModel;
        }
    }

    public static void main(String[] args) throws ObjectCreateError {
        SimulatedAnnealing.main(null);
    }
}
