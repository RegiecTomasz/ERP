/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;


/**
 *
 * @author fbrzuzka
 */
public enum JobState {
    jobReady,
    jobWaitingForDependentJobs,
    jobCompleted
}
