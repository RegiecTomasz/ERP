/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.utils;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author fbrzuzka
 */
public class KahnTopoligicSorter {
    
    private final DisjunctiveGraph graph;
    private final Operation operationZero;
    
    public KahnTopoligicSorter(DisjunctiveGraph g) {
        this.graph = g;
        operationZero = graph.getVertices().stream().filter(v -> v.vertexNo == 0).findFirst().get();        
    }
    public List<Operation> sort() {
        List<Operation> sorted = new ArrayList();
        List<Operation> queue = Lists.newArrayList(operationZero);
        
        Map<Operation, Integer> inDegreeMap = new HashMap();        
        for (Operation op : graph.getVertices()) {
            inDegreeMap.put(op, graph.inDegree(op));
        }
        Operation node;
        while (!queue.isEmpty()) {
            node = queue.get(queue.size() - 1);
            queue.remove(node);
            sorted.add(node);
            for (Operation child : graph.getChildren(node)) {
                Integer inDegreeChild = inDegreeMap.get(child);
                inDegreeChild--;
                inDegreeMap.put(child, inDegreeChild);
                if(inDegreeChild == 0){
                    queue.add(child);
                }
            }
        }
        return sorted;
    }
}
