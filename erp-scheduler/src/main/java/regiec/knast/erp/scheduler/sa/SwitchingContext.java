/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.sa;

import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author fbrzuzka
 */
public class SwitchingContext {
    public Operation pre;
    public Operation v;
    public Operation post;

    public SwitchingContext(Operation pre, Operation v, Operation post) {
        this.pre = pre;
        this.v = v;
        this.post = post;
    }

    public SwitchingContext() {
    }

    @Override
    public String toString() {
        return "SwitchingContext{" + "pre=" + pre + ", v=" + v + ", post=" + post + '}';
    }
    
    
}
