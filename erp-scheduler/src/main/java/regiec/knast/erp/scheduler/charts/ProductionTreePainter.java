/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.charts;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.renderers.GradientVertexRenderer;
import edu.uci.ics.jung.visualization.renderers.VertexLabelAsShapeRenderer;
import java.awt.Color;
import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.jaxygen.objectsbuilder.ObjectBuilderFactory;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.business.apitests.GuiceInjector;
import regiec.knast.erp.api.dao.interfaces.ProductionJobDAOInterface;
import regiec.knast.erp.api.manager.orders.GenerateProductionJobsTransformer;
import regiec.knast.erp.api.dto.producttemplates.partcard.SemiproductPart;
import regiec.knast.erp.api.entities.ProductionJobEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ProductionTreePainter extends GuiceInjector {

    @Inject
    private GenerateProductionJobsTransformer productionJobManager;

    @Inject
    private ProductionJobDAOInterface productionJobDAO;

    private Gson g = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) throws ObjectCreateError {
        GuiceInjector.init();
        Object create = ObjectBuilderFactory.instance().create(ProductionTreePainter.class);
        ProductionTreePainter mongoTest = (ProductionTreePainter) create;
        try {
            mongoTest.drawTreeChart();
            //   VertexLabelAsShapeDemo.main(null);
        } catch (Exception ex) {
            Logger.getLogger(ProductionTreePainter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            mongoTest.cleanUp();
        }
    }

    public void drawTreeChart() throws ConversionError {
        ProductionJobEntity productionJobEntityRoot = productionJobDAO.get("59c0de6896bc733f64781924");

        SimpleGraph simpleGraph = new SimpleGraph(productionJobEntityRoot);
        //   simpleGraph.fromProductionTasks
    }

    class SimpleGraph extends JFrame {

        private Integer edgeNo = 0;

        public SimpleGraph(ProductionJobEntity root) {
            super("Mój drugi graf");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            DelegateTree<SimpleGraph.A, Integer> delegateTree = new DelegateTree();
//        delegateTree.addVertex(root.getDetalEntity().getDetalTemplateEntity().getNo());
            delegateTree.addVertex(new SimpleGraph.A(
                    root.getNo(),
                    root.getDetalEntity().getDetalTemplateEntity().getNo(),
                    root.getDetalEntity().getCount(),
                    root.getDetalEntity().getCount(),
                    root.getDetalEntity().getDetalTemplateEntity().getName()));
            System.out.println("r: " + root.getDetalEntity().getDetalTemplateEntity().getId());
            addChildren(root, delegateTree);

            VisualizationViewer<SimpleGraph.A, Integer> vv = new VisualizationViewer(new TreeLayout(delegateTree, 110, 100), new Dimension(1300, 700));
            // this class will provide both label drawing and vertex shapes
            VertexLabelAsShapeRenderer<SimpleGraph.A, Integer> vlasr = new VertexLabelAsShapeRenderer(vv.getRenderContext());

            // customize the render context
            vv.getRenderContext().setVertexLabelTransformer(
                    // this chains together Transformers so that the html tags
                    // are prepended to the toString method output
                    new ChainedTransformer<SimpleGraph.A, String>(new Transformer[]{
                //new ToStringLabeller<A>(),
                new Transformer<SimpleGraph.A, String>() {
                    public String transform(SimpleGraph.A input) {
                        return "<html><center>" + input.no
                                + "<p>" + input.templateNo
                                + "<p>" + input.name
                                + "<p>total c: " + input.totalCount
                                + "<p>partC c: " + input.countOnPartCard;
                    }
                }}));
            vv.getRenderContext().setVertexShapeTransformer(vlasr);
            // customize the renderer
            vv.getRenderer().setVertexRenderer(new GradientVertexRenderer(Color.gray, Color.white, true));
            vv.getRenderer().setVertexLabelRenderer(vlasr);
            // vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
            vv.setBackground(Color.white);
            vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
            getContentPane().add(vv);

            pack();
            setVisible(true);
        }

        class A {

            public String no;
            public String templateNo;
            public Integer countOnPartCard;
            public Integer totalCount;
            public String name;

            public A(String no, String templateNo, Integer countOnPartCard, Integer totalCount, String name) {
                this.no = no;
                this.templateNo = templateNo;
                this.countOnPartCard = countOnPartCard;
                this.totalCount = totalCount;
                this.name = name.length() <= 17 ? name : name.substring(0, 17);
            }

            @Override
            public boolean equals(Object obj) {
                if (!(obj instanceof A)) {
                    return false;
                }
                return ((A) obj).no.equals(this.no);
            }

            @Override
            public int hashCode() {
                return no.hashCode();
            }
        }

        private Integer calculateCountFromParent(ProductionJobEntity root, ProductionJobEntity me) {
            if (root.getDetalEntity().getDetalTemplateEntity().getPartCard() != null
                    && root.getDetalEntity().getDetalTemplateEntity().getPartCard().getSemiproductParts() != null) {

                for (SemiproductPart semiproductPart : root.getDetalEntity().getDetalTemplateEntity().getPartCard().getSemiproductParts()) {
                    if (semiproductPart.getSemiproductTemplate().getId().equals(me.getDetalEntity().getDetalTemplateEntity().getId())) {
                        return semiproductPart.getCount();
                    }
                }
            }
            return 0;
        }

        private void addChildren(ProductionJobEntity root, DelegateTree delegateTree) {
            for (ProductionJobEntity pte : root.getChildren()) {
                System.out.println("\nr: " + root.getDetalEntity().getDetalTemplateEntity().getNo());
                System.out.println("l: " + pte.getDetalEntity().getDetalTemplateEntity().getNo());
                A r = new A(
                        root.getNo(),
                        root.getDetalEntity().getDetalTemplateEntity().getNo(),
                        0,
                        root.getDetalEntity().getCount(),
                        root.getDetalEntity().getDetalTemplateEntity().getName());
                A l = new A(
                        pte.getNo(),
                        pte.getDetalEntity().getDetalTemplateEntity().getNo(),
                        calculateCountFromParent(root, pte),
                        pte.getDetalEntity().getCount(),
                        pte.getDetalEntity().getDetalTemplateEntity().getName());
                //  delegateTree.addChild(edgeNo++, root.getDetalEntity().getDetalTemplateEntity().getId(), pte.getDetalEntity().getDetalTemplateEntity().getId(), EdgeType.DIRECTED);
                delegateTree.addChild(edgeNo++, r, l, EdgeType.DIRECTED);
                addChildren(pte, delegateTree);
            }
        }
    }
}
