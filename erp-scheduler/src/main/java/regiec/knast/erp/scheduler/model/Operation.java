/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import regiec.knast.erp.api.dto.producttemplates.productionplan.ProductionPlan;
import regiec.knast.erp.api.entities.OperationNameEntity;
import regiec.knast.erp.api.entities.ProfessionGroupEntity;
import regiec.knast.erp.api.scheduler.charts.model.OperationGantKey;
import regiec.knast.erp.api.utils.IDUtil;

/**
 *
 * @author fbrzuzka
 */
public class Operation {

    protected String hash = IDUtil.nextId();

    public Job job;
    public Integer operationIndex;
    public Integer operationsInJob;
    public String operationName;
    public Worker worker;
    public Integer cost;
    public Integer operationStartTime;
    public Integer operationEndTime;
    public boolean isPrimitive;
    public boolean isOnCriticalPath = false;
    public OperationGantKey key;
    public Integer vertexNo;
    public ProfessionGroupEntity professionGroup;
    
    public List<String> operationNames = new ArrayList();
    public Integer preparationTime;
    public Integer operationTime;
    public boolean isMultipleOperation = false;
    public Integer operationNumberInPlan;
    public OperationState operationState;
    //used only in construction algorithm. It's not guarantee that this field
    //will be consistient in other situations
    public List<Operation> tempPreviousDisjunctiveOperation = new ArrayList();

    protected Operation() {
    }

    public Operation(Job job, ProductionPlan productionPlan, int operationIndex, String materialNo) {
        this.job = job;
        this.operationIndex = operationIndex;

        this.operationsInJob = job.getOperationsCount();
        this.operationName = productionPlan.getOperationName().toString();
        this.operationNames.add(this.operationName);
        this.professionGroup = productionPlan.getProfessionGroup();
        this.professionGroup.setLabel(parseProffessionGroupLabel(this.professionGroup.getLabel()));

        this.preparationTime = (int)(productionPlan.getPreparationTime() * 60);
        this.operationTime = (int)(productionPlan.getOperationTime() * 60);
        this.operationNumberInPlan = productionPlan.getOperationNumber();
        this.operationState = defineOperationState(job);
        this.isPrimitive = isPrimitive(productionPlan);
        calculateOperationDuration();
    }

    public List<Operation> getPre() {
        return this.job.getPreOp(operationIndex);
    }

    public Integer calculateOperationDuration() {
        if (this.operationName.equals(OperationNameEntity.getTransportOperationName())) {
            this.cost = this.preparationTime + this.operationTime;
        } else {
            this.cost = this.preparationTime + this.job.count * this.operationTime;
        }
        return this.cost;
    }

    public OperationState defineOperationState(Job t) {
        if (t.getNotDoneChildrenJobs().isEmpty() && this.isFirst()) {
            return OperationState.operationReady;
        } else {
            return OperationState.operationWaiting;
        }
    }

    public Operation getNextOperation() {
        return this.job.operationsToDo.get(this.operationIndex + 1);
    }

    public Operation getPreviousOperation() {
        return this.job.operationsToDo.get(this.operationIndex - 1);
    }

    public Integer calculateOperationEndTime(int time) {
        return time + cost;
    }

    public OperationGantKey createKey() {
        this.key = new OperationGantKey(job.jobNo, worker);
        return key;
    }

    protected boolean isPrimitive(ProductionPlan productionPlan) {
        return productionPlan.getProfessionGroup().getCode().equals("01")
                || productionPlan.getProfessionGroup().getCode().equals("02")
                || productionPlan.getProfessionGroup().getCode().equals("15");
    }

    public boolean isFirst() {
        return this.operationIndex == 0;
    }

    public boolean isLast() {
        return this.operationIndex == this.operationsInJob - 1;
    }

    public boolean hasNextOperation() {
        return !isLast();
    }

    public boolean hasPreviousOperation() {
        return !isFirst();
    }

    protected String parseProffessionGroupLabel(String profLabel) {
        if (profLabel.length() > 15) {
            return profLabel.substring(0, 15);
        }
        return profLabel;
    }

    public static Operation newTransportOperation(Job job, int transportTime, int operationIndex) {
        ProductionPlan pp = new ProductionPlan();
        pp.setOperationName(OperationNameEntity.getTransportOperationName());
        pp.setOperationNumber(null);
        pp.setOperationTime(new Double(transportTime)/60);
        pp.setPreparationTime(0.0);
        pp.setProfessionGroup(ProfessionGroup.getTransportGroup());
        Operation transportOperation = new Operation(job, pp, operationIndex, null);
        return transportOperation;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.hash);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Operation other = (Operation) obj;
        if (!Objects.equals(this.hash, other.hash)) {
            return false;
        }
        return true;
    }

    public String operationsToGraphLabel() {
        StringBuilder label = new StringBuilder();

        label.append("<html><center>");
        label.append(isOnCriticalPath ? "|||||||||- " : "");
        if (this.vertexNo != null) {
            label.append(this.vertexNo.toString());
        }
        label.append(isOnCriticalPath ? " -|||||||||" : "");
        if (this.worker != null) {
            label.append("<p>");
            label.append(this.worker.workerLabel);
        }
        if (this.operationNames != null) {
            for (String n : operationNames) {
                label.append("<p>");
                label.append(n);
            }
        }
//        if (this.operationName != null) {
//            label.append("<p>");
//            label.append(this.operationName);
//        }
        label.append("<p>job:");
        if (this.job != null) {
            label.append(job.jobNo);
        }
        label.append("<p>start:");
        if (this.operationStartTime != null) {
            label.append(String.valueOf(this.operationStartTime));
        }
        label.append("<p>end:");
        if (this.operationEndTime != null) {
            label.append(String.valueOf(this.operationEndTime));
        }
        return label.toString();
    }

    @Override
    public String toString() {
        return vertexNo != null ? vertexNo.toString() : job.jobNo + " " + professionGroup.getLabel();
    }
}
