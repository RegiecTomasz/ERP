/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.utils;

import com.google.common.collect.Lists;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Operation;

public class CriticalPathDetector {

    public static final int MINUS_INFINITE_DISTANCE = Integer.MIN_VALUE;
    
    private final DisjunctiveGraph graph;
    private final Map<Operation, Integer> longestDistances = new HashMap();
    private final Map<Operation, Operation> predecessors = new HashMap();

    private ArrayDeque<Operation> sortedTopologically = new ArrayDeque();    
    private Operation startVertex;
    private Operation endVertex;

    public CriticalPathDetector(DisjunctiveGraph graph) {
        this.graph = graph;
    }

    private void init() {
        this.startVertex = this.graph.getVertices().stream().filter(v -> v.operationName.equals("zero")).findFirst().get();
        this.endVertex = this.graph.getVertices().stream().filter(v -> v.operationName.equals("last")).findFirst().get();
        longestDistances.clear();
        predecessors.clear();
        // add source
        setLongestDistance(startVertex, 0);
        sortedTopologically.addAll(new KahnTopoligicSorter(graph).sort());
    }
    public static long duration = 0;

    public static void markOperationsOnCriticalPath(DisjunctiveGraph graph, List<Integer> criticalPath) {
        for (Operation vertice : graph.getVertices()) {
            vertice.isOnCriticalPath = criticalPath.contains(vertice.vertexNo);
        }
    }

    public CriticalPathDetector execute() {
        long startTime = System.currentTimeMillis();
        init();
        Operation next = null;
        
        while (!sortedTopologically.isEmpty()) {
            next = sortedTopologically.pollFirst();
            relaxNeighbors(next);
        }
        duration += (System.currentTimeMillis() - startTime);
        return this;
    }

    private void relaxNeighbors(Operation u) {
        for (Operation v : graph.getChildren(u)) {
            if (getLongestDistance(v) <= getLongestDistance(u) + u.cost) {
                setLongestDistance(v, getLongestDistance(u) + u.cost);
                setPredecessor(v, u);
            }
        }
    }

    public int getLongestDistance(Operation op) {
        Integer d = longestDistances.get(op);
        return (d == null) ? MINUS_INFINITE_DISTANCE : d;
    }

    public int getLongestDistanceToEnd() {
        Integer d = longestDistances.get(endVertex);
        return (d == null) ? MINUS_INFINITE_DISTANCE : d;
    }

    private void setLongestDistance(Operation op, int distance) {
        longestDistances.put(op, distance);
    }

    public Operation getPredecessor(Operation op) {
        return predecessors.get(op);
    }

    public List<Integer> getPathToEndVertex() {
        return getPathTo(endVertex);
    }

    public List<Integer> getPathTo(Operation endVertex) {
        List<Integer> criticalPath = new ArrayList();
        Operation end = endVertex;
        while (true) {
            criticalPath.add(end.vertexNo);
            Operation next = getPredecessor(end);
            if (next.vertexNo == startVertex.vertexNo) {
                criticalPath.add(next.vertexNo);
                break;
            }
            end = next;
        }
        return Lists.reverse(criticalPath);
    }

    private void setPredecessor(Operation a, Operation b) {
        predecessors.put(a, b);
    }

    public Result getResult() {
        return new Result(getLongestDistanceToEnd(), getPathToEndVertex(), longestDistances);
    }

    public class Result {

        public Integer makespan;
        public List<Integer> criticalPath;
        public Map<Operation, Integer> longestDistances;

        public Result(Integer makespan, List<Integer> criticalPath, Map<Operation, Integer> longestDistances) {
            this.makespan = makespan;
            this.criticalPath = criticalPath;
            this.longestDistances = longestDistances;
        }

        public void markOperationsOnCriticalPath(DisjunctiveGraph graph) {
            for (Operation vertice : graph.getVertices()) {
                vertice.isOnCriticalPath = criticalPath.contains(vertice.vertexNo);
            }
        }

        public void markDistances(DisjunctiveGraph g) {
            for (Operation op : g.getVertices()) {
                if (!longestDistances.containsKey(op)) {
                    throw new ConversionError("łoooo, powinien być wierzchołek w liście ale nie ma");
                }
                Integer distanceFromZero = longestDistances.get(op);
                op.operationStartTime = distanceFromZero;
                op.operationEndTime = distanceFromZero + op.cost;
            }
        }
    }

}
