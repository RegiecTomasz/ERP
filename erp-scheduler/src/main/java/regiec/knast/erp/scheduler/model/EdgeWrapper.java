/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author fbrzuzka
 */
public class EdgeWrapper {

    public Edge e;
    public Operation o1;
    public Operation o2;

    public EdgeWrapper(Edge e, Operation o1, Operation o2) {
        this.e = e;
        this.o1 = o1;
        this.o2 = o2;
    }

    public EdgeWrapper() {
    }

}
