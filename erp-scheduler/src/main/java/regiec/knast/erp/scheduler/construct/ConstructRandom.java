/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.construct;

import java.util.ArrayList;
import java.util.Collections;
import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ConstructRandom extends ConstructFirstComeFirstServed {

    @Override
    public ArrayList<Operation> getReadyOperations() {
        ArrayList<Operation> ready = super.getReadyOperations();
        Collections.shuffle(ready);
        return ready;
    }
}
