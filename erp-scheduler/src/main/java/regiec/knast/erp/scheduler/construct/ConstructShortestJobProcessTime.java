/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.construct;

import java.util.ArrayList;
import java.util.Collections;
import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class ConstructShortestJobProcessTime extends ConstructFirstComeFirstServed {

    @Override
    public ArrayList<Operation> getReadyOperations() {
        ArrayList<Operation> ready = super.getReadyOperations();
        Collections.sort(ready, (Operation o1, Operation o2) -> o1.job.estimatedJobDuration.compareTo(o2.job.estimatedJobDuration));
        return ready;
    }
}
