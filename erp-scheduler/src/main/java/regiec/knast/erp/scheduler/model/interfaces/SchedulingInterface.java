/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model.interfaces;

import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;
import regiec.knast.erp.api.scheduler.model.TestCase;
import regiec.knast.erp.api.scheduler.model.Schedule;

/**
 *
 * @author fbrzuzka
 */
public interface SchedulingInterface {
    
    Schedule run(String title, TestCase casee, WorkerEntitiesList workers, boolean withTransport, int transportTime, int transportWorkerCount);
    String getName();
}
 