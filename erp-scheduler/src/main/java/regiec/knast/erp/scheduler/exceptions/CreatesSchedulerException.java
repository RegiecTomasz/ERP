/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.exceptions;

/**
 *
 * @author fbrzuzka
 */
public class CreatesSchedulerException extends RuntimeException{

    public CreatesSchedulerException() {
    }

    public CreatesSchedulerException(String message) {
        super(message);
    }

    public CreatesSchedulerException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreatesSchedulerException(Throwable cause) {
        super(cause);
    }

    public CreatesSchedulerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
