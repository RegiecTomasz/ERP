/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.model.ProfessionGroup;

/**
 *
 * @author fbrzuzka
 */
public class Worker {
    
    public String code;
    public String groupName;
    public String groupCode;
    public String workerLabel;
    public boolean doingOperation;
    public Operation doneOperation;

    public Worker(WorkerEntity workerEntity) {
        this.code = workerEntity.getWorkerCode();
        this.groupCode = workerEntity.getProfessionGroup().getCode();
        this.doingOperation = false;
        this.groupName = workerEntity.getProfessionGroup().getName();
        this.workerLabel = createWorkerLabel(workerEntity);
    }

    private Worker() {
    }
    
    public static String createWorkerLabel(WorkerEntity workerEntity){
        return workerEntity.getProfessionGroup().getName() + "_" +  workerEntity.getWorkerCode();
    }
    
    public static Worker newTransportWorker(int number){
        ProfessionGroup transportGroup = ProfessionGroup.getTransportGroup();
        Worker w = new Worker();
        w.code = transportGroup.getCode() + "-0" + String.valueOf(number);
        w.groupCode = transportGroup.getCode();
        w.doingOperation = false;
        w.groupName = transportGroup.getName();
        w.workerLabel = w.groupName + "_" +  w.code;
        return w;
    }    

    @Override
    public String toString() {
        return code;
    }
    
}
