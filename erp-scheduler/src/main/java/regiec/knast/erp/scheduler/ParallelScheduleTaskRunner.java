/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler;

import java.util.concurrent.Callable;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;
import regiec.knast.erp.api.scheduler.model.AlgorithmResult;
import regiec.knast.erp.api.scheduler.model.LauncherTask;
import regiec.knast.erp.api.scheduler.model.TestCase;

/**
 *
 * @author fbrzuzka
 */
public class ParallelScheduleTaskRunner extends ScheduleTaskRunner implements Callable {

    public ParallelScheduleTaskRunner(LauncherTask launcherTask, TestCase testCase, WorkerEntitiesList workers) {
        super(launcherTask, testCase, workers);
    }
    
    public AlgorithmResult call() {
       return run();
    }    

}
