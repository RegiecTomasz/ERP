/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.sa;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.business.apitests.GuiceInjector;
import regiec.knast.erp.api.entities.WorkerEntity;
import regiec.knast.erp.api.entities.containers.WorkerEntitiesList;
import regiec.knast.erp.api.model.ProfessionGroup;
import regiec.knast.erp.api.model.TreeNodeType;
import regiec.knast.erp.api.scheduler.ScheduleLauncher;
import static regiec.knast.erp.api.scheduler.ScheduleLauncher.ENABLE_PRINTING;
import regiec.knast.erp.api.scheduler.charts.DisjunctiveGraphPainter;
import regiec.knast.erp.api.scheduler.charts.GanttChartPainter;
import regiec.knast.erp.api.scheduler.exceptions.SchedulerException;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Edge;
import regiec.knast.erp.api.scheduler.model.EdgeWrapper;
import regiec.knast.erp.api.scheduler.model.Job;
import regiec.knast.erp.api.scheduler.model.Operation;
import regiec.knast.erp.api.scheduler.model.OperationLast;
import regiec.knast.erp.api.scheduler.model.OperationZero;
import regiec.knast.erp.api.scheduler.model.Permutation;
import regiec.knast.erp.api.scheduler.model.Schedule;
import regiec.knast.erp.api.scheduler.model.TestCase;
import regiec.knast.erp.api.scheduler.model.Worker;
import regiec.knast.erp.api.scheduler.model.interfaces.SaSchedulingInterface;
import regiec.knast.erp.api.scheduler.utils.CriticalPathDetector;
import regiec.knast.erp.api.scheduler.utils.CycleDetector;
import regiec.knast.erp.api.scheduler.zzzother.Logg;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class SimulatedAnnealing extends GuiceInjector implements SaSchedulingInterface {

    private SaUtil saUtil = new SaUtil();

    private Integer TRANSPORT_WORKERS_COUNT = 1;
    private Integer TRANSPORT_TIME = 60;
    private boolean WITH_TRANSPORT = false;
    private Random rand = new Random();
    private Gson g = new GsonBuilder().setPrettyPrinting().create();
    private Map<Integer, TestCase> cases;
    private Map<String, List<Worker>> grupedWorkers = new HashMap();
    private Map<String, Integer> grupedWorkersSizes = new HashMap();
    private static final boolean DO_PRINT = ENABLE_PRINTING;

    public SimulatedAnnealing() {
    }

    public static void main(String[] args) throws ObjectCreateError {
        ScheduleLauncher.main(null);
    }

    @Override
    public Schedule run(String algorithmName, TestCase casee, WorkerEntitiesList workers, boolean withTransport, int transportTime, int transportWorkerCount) {
        return run(algorithmName, casee, workers, null, withTransport, transportTime, transportWorkerCount, false, false);
    }

    @Override
    public Schedule run(String algorithmName, TestCase casee, WorkerEntitiesList workers, Schedule initSchedule, boolean withTransport, int transportTime, int transportWorkerCount) {
        return run(algorithmName, casee, workers, initSchedule, withTransport, transportTime, transportWorkerCount, false, false);
    }

    public Schedule run(String algorithmName, TestCase casee, WorkerEntitiesList workers, Schedule initSchedule, boolean withTransport, int transportTime, int transportWorkerCount, boolean drawDisjunctiveGraph, boolean drawGant) {

        long start = System.currentTimeMillis();
        TRANSPORT_TIME = transportTime;
        TRANSPORT_WORKERS_COUNT = transportWorkerCount;
        WITH_TRANSPORT = withTransport;
        initLists(workers);

        DisjunctiveGraph initGraph = initSchedule.getGraph();
        Schedule improvedSchedule = runSAFromInitializedGraph(initGraph, drawDisjunctiveGraph, drawGant);
//        System.out.println(getName() + " criticalpath2 trwał " + DijkstraEngine.duration);
//        System.out.println(getName() + " DijkstraLongestPath          trwał " + CriticalPathDetector.duration);
//        System.out.println(getName() + " skończył w " + (System.currentTimeMillis() - start));
        improvedSchedule.setAlgName(algorithmName);
        return improvedSchedule;
    }

    public Schedule runSAFromInitializedGraph(DisjunctiveGraph initGraph, boolean drawDisjunctiveGraph, boolean drawGant) {
        signPermutationsAreDoingInManyJobs(initGraph);
        DisjunctiveGraph saImproved = doSA(initGraph);
        int makespan = 0;
        CriticalPathDetector.Result result = new CriticalPathDetector(saImproved).execute().getResult();
        makespan = result.makespan;
        Schedule schedule = null;
        schedule = new Schedule(saImproved, getName());
        if (drawDisjunctiveGraph) {
            DisjunctiveGraphPainter disjunctiveGraph = new DisjunctiveGraphPainter(saImproved, getName(), makespan);
        }
        if (drawGant) {
            GanttChartPainter.ganttFromSchedule("G " + String.valueOf(makespan), schedule, true);
        }
        return schedule;
    }

    private void signPermutationsAreDoingInManyJobs(DisjunctiveGraph graph) {
        Map<String, Boolean> areOk = new HashMap();
        Map<String, List<Touple>> missingEdges = new HashMap();

        for (Map.Entry<String, List<Operation>> entry : graph.disjunctiveOperations.entrySet()) {
            // init areOK map downthere
            areOk.put(entry.getKey(), true);
            for (int i = 1; i < entry.getValue().size(); i++) {
                Operation pre = entry.getValue().get(i - 1);
                Operation op = entry.getValue().get(i);
                Edge edge = saUtil.findEdge(pre, op, graph);
                if (edge == null) {
                    areOk.put(entry.getKey(), false);
                    if (!missingEdges.containsKey(entry.getKey())) {
                        missingEdges.put(entry.getKey(), new ArrayList());
                    }
                    missingEdges.get(entry.getKey()).add(new Touple(pre, op));
                }
            }
        }
        graph.permutationOnOnlyOneGraphLevel = areOk;
        List<String> ok = new ArrayList();
        List<String> notOk = new ArrayList();
        for (Map.Entry<String, Boolean> e : areOk.entrySet()) {
            if (e.getValue()) {
                ok.add(e.getKey());
            } else {
                notOk.add(e.getKey());
            }
        }
//        System.out.println("areOk?  " + areOk + "\n");
//        System.out.println("Ok?     " + ok);
//        System.out.println("Not Ok? " + notOk);
//        for (Map.Entry<String, List<Touple>> e : missingEdges.entrySet()) {
//            System.out.println(e.getKey());
//            for (Touple t : e.getValue()) {
//                System.out.println(t);
//            }
//        }
    }

    public DisjunctiveGraph prepareRandomInitializationGraph(TestCase c, boolean drawDisjunctiveGraph, boolean drawGant) {
        saUtil.resetVertexNo();
        saUtil.resetEdgeNo();

        List<Job> rootJobs = c.stream()
                .filter(pt -> pt.getTreeNodeType() == TreeNodeType.ROOT)
                .collect(Collectors.mapping(pt -> new Job(pt, null, WITH_TRANSPORT, TRANSPORT_TIME), Collectors.toList()));
        DisjunctiveGraph initGraph = new DisjunctiveGraph(null);
        Operation zeroV = new OperationZero();
        Operation lastV = new OperationLast();
        initGraph.addVertex(zeroV);
        saUtil.addRootsOfProductionJobs(rootJobs, initGraph, zeroV, lastV);
        addRandomWorkerToOperations(initGraph);
        Map<String, List<Operation>> grouppedDisjunctiveEdges = saUtil.grouppDisjunctiveArcsByWorkerLabel(initGraph);
//        List<Touple> disjunctiveVertexes = prepareDisjunctiveArcs(grouppedDisjunctiveEdges);
//        addDisjunctiveArcs(graph, disjunctiveVertexes);
        saUtil.randInitialSolution(initGraph, grouppedDisjunctiveEdges);
        return initGraph;
    }

    private boolean stopCondition(Double temp, Integer iter) {
        return temp < 0.3;//|| iter > 5000;
    }
    private Logg logger = new Logg(DO_PRINT);

    private DisjunctiveGraph doSA(DisjunctiveGraph initialSolution) {
        Integer iter = 0;
//      1. Generuj rozwiązanie początkowe x.
        initialSolution.name = "0";
        CriticalPathDetector.Result initialResult = initialSolution.calculateResult();
//      2. Połóż x∗ := x.
        Logg.print(DO_PRINT, "init makespan : " + initialResult.makespan.toString() + "  | " + saUtil.disjOpToString(initialSolution.disjunctiveOperations.values()));
        DisjunctiveGraph bestSolution = saUtil.cloneGraph(initialSolution);
        CriticalPathDetector.Result bestResult = saUtil.cloneResult(initialResult);

//        DisjunctiveGraphDrawer disjunctiveGraphss = new DisjunctiveGraphDrawer(initialSolution, "init " + initialResult.makespan);
        Double temp = 30.0;

//      3. Dla k = 0, 1, 2, ... wykonuj kroki 4-7.
        while (true) {
//            System.out.print(String.format("iter %5d temp: %.2f", iter, temp));
            logger.append(String.format("iter %5d temp: %.2f", iter, temp));
//          4. Wylosuj rozwiązanie y ∈ N(x).

//            DisjunctiveGraph nextSolutionY = trySoHardToFildNextSolution(initialSolution);
            DisjunctiveGraph neighboringSolution = generateNeighboringSolutionFromCriticalPath(initialSolution, new ArrayList(initialResult.criticalPath));
            neighboringSolution.name = iter.toString();

//          5. Jeżeli F(best) < F(next), to połóż best:= next.
            CriticalPathDetector.Result neighboringResult = neighboringSolution.calculateResult();

//            nextResult.markOperationsOnCriticalPath(nextSolutionY);
//            nextResult.markDistances(nextSolutionY);
//            DisjunctiveGraphDrawer disjunctiveGraph2 = new DisjunctiveGraphDrawer(nextSolutionY, "next " + iter, nextResult.makespan);
            boolean isBest = neighboringResult.makespan < bestResult.makespan;
            if (isBest) {
                bestSolution = neighboringSolution;
                bestResult = neighboringResult;
            }

//          6. Połóż init := next z prawdopodobieństwem P(ck).
//          Oblicz wartość ∆ = F(next)−F(init) i jeżeli R < e−∆/T to połóż init := next
//          ΔC = Cmax(R’)-Cmax(R);   If ((ΔC≤0) or (prob < exp(-ΔC/T))) Then
            int delta = neighboringResult.makespan - initialResult.makespan;
            boolean isBetterOrEqualThanInit = delta <= 0;
            Boolean prob = null;
            if (isBetterOrEqualThanInit) {
                initialSolution = neighboringSolution;
                initialResult = neighboringResult;
            } else {
                prob = rand.nextDouble() < acceptanceFunctionValue(delta, temp);
                if (prob) {
                    initialSolution = neighboringSolution;
                    initialResult = neighboringResult;
                }
            }
            logger.append(" | init C: ");
            logger.append(initialResult.makespan.toString());
            logger.append(" | new C: ");
            logger.append(neighboringResult.makespan.toString());
            logger.append(" | best C: ");
            logger.append(bestResult.makespan.toString());
            logger.append(String.format(" | isBetter: %5s | prob: %5s", isBetterOrEqualThanInit, prob != null && prob ? prob : ""));
            logger.append(" | i: ");
            logger.append(initialSolution.name);
            logger.append(" | b:");
            logger.append(bestSolution.name);
            logger.append(" | n:");
            logger.append(neighboringSolution.name);
            logger.printAndClear();

//          7. Jeżeli spełniony jest warunek końca, to zwróć rozwiązanie x∗ i STOP.
            temp = coolDown(temp);
            iter++;
            if (stopCondition(temp, iter)) {
                bestResult.markOperationsOnCriticalPath(bestSolution);
                bestResult.markDistances(bestSolution);
//                DijkstraLongest.markOperationsOnCriticalPath(bestSolution, bestResult.criticalPath);
                return bestSolution;
            }
        }
    }

    private Worker randOtherWorker(List<Worker> profWorkers, Worker actulWorker) {
        List<Worker> temp = new ArrayList(profWorkers);
        temp.remove(actulWorker);
        if (temp.size() == 1) {
            return temp.get(0);
        }
        return temp.get(rand.nextInt(temp.size()));
    }

    private SwitchingContext chooseOperationToSwitch(Worker newWorker, Map<String, List<Operation>> shuffledPermutations) {
        List<Operation> permutation = shuffledPermutations.get(newWorker.workerLabel);
        int vIndex = rand.nextInt(permutation.size());
        Operation v = permutation.get(vIndex);
        Operation pre = vIndex == 0 ? null : permutation.get(vIndex - 1);
        Operation post = vIndex == permutation.size() - 1 ? null : permutation.get(vIndex + 1);
        return new SwitchingContext(pre, v, post);
    }

    private SwitchingContext createSwitchingContextForOperation(Operation v, Map<String, List<Operation>> shuffledPermutations) {
        try {
            List<Operation> permutation = shuffledPermutations.get(v.worker.workerLabel);
            int vIndex = permutation.indexOf(v);
            Operation pre = vIndex == 0 ? null : permutation.get(vIndex - 1);
            Operation post = vIndex == permutation.size() - 1 ? null : permutation.get(vIndex + 1);
            return new SwitchingContext(pre, v, post);
        } catch (IndexOutOfBoundsException e) {
            throw new SchedulerException("", e);
        }
    }

    private Pair<SwitchingContext> switchOperations(SwitchingContext v1Context, SwitchingContext v2Context, DisjunctiveGraph nextSolution) {
        // weź dysjunkcyjne dochodzace do op2 i je usuń. potem wstaw je przez v1.
        // weź dysjunkcyjne dochodzace do v1 i je usuń. potem wstaw je przez op2.

        if (v1Context.pre != null) {
            Edge edge11 = saUtil.findEdge(v1Context.pre, v1Context.v, nextSolution);
            nextSolution.removeEdge(edge11);
        }
        if (v1Context.post != null) {
            Edge edge12 = saUtil.findEdge(v1Context.v, v1Context.post, nextSolution);
            nextSolution.removeEdge(edge12);
        }
        if (v2Context.pre != null) {
            Edge edge21 = saUtil.findEdge(v2Context.pre, v2Context.v, nextSolution);
            nextSolution.removeEdge(edge21);
        }
        if (v2Context.post != null) {
            Edge edge22 = saUtil.findEdge(v2Context.v, v2Context.post, nextSolution);
            nextSolution.removeEdge(edge22);
        }
        if (v1Context.pre != null) {
            nextSolution.addEdge(new Edge(saUtil.nextEdgeNo(), v1Context.pre.cost, true), v1Context.pre, v2Context.v, EdgeType.DIRECTED);
        }
        if (v1Context.post != null) {
            nextSolution.addEdge(new Edge(saUtil.nextEdgeNo(), v2Context.v.cost, true), v2Context.v, v1Context.post, EdgeType.DIRECTED);
        }
        if (v2Context.pre != null) {
            nextSolution.addEdge(new Edge(saUtil.nextEdgeNo(), v2Context.pre.cost, true), v2Context.pre, v1Context.v, EdgeType.DIRECTED);
        }
        if (v2Context.post != null) {
            nextSolution.addEdge(new Edge(saUtil.nextEdgeNo(), v1Context.v.cost, true), v1Context.v, v2Context.post, EdgeType.DIRECTED);
        }
        SwitchingContext newV1 = new SwitchingContext(v1Context.pre, v2Context.v, v1Context.post);
        SwitchingContext newV2 = new SwitchingContext(v2Context.pre, v1Context.v, v2Context.post);
        Pair<SwitchingContext> newContexts = new Pair(newV1, newV2);
        return newContexts;
    }

    private void updatePermutationsInfoInGraph(SwitchingContext v1Context, SwitchingContext v2Context, DisjunctiveGraph nextSolution) {
        List<Operation> v1perm = nextSolution.disjunctiveOperations.get(v1Context.v.worker.workerLabel);
        int v1Index = v1perm.indexOf(v1Context.v);
        v1perm.set(v1Index, v2Context.v);
        List<Operation> v2perm = nextSolution.disjunctiveOperations.get(v2Context.v.worker.workerLabel);
        int v2Index = v2perm.indexOf(v2Context.v);
        v2perm.set(v2Index, v1Context.v);
    }

    private void swapWorkersInOperations(SwitchingContext c1, SwitchingContext c2) {
        Worker tmp = c1.v.worker;
        c1.v.worker = c2.v.worker;
        c2.v.worker = tmp;
    }

    private DisjunctiveGraph generateNeighboringSolutionFromCriticalPath(DisjunctiveGraph initSolution, List<Integer> criticalPath) {
        if (criticalPath.size() < 3) {
            throw new SchedulerException("Critical path is to short");
        }
        DisjunctiveGraph nextSolution = saUtil.cloneGraph(initSolution);
        int iter = 1;
        while (true) {
            Operation verticleToSwap = randVerticleFromCriticalPath(nextSolution, criticalPath);
            DisjunctiveGraph newSolution = null;

            if (rand.nextBoolean()) {
                // change worker of operation to another worker from his profession group.
                newSolution = swapWorkersFromProfessionGroup(nextSolution, iter, verticleToSwap);
            } else {
                //change order of disjunctive edges
                newSolution = swapDisjunctiveOperationsFromWorker(nextSolution, iter, verticleToSwap, criticalPath);
            }
            if (newSolution != null) {
                return newSolution;
            }
            if (iter++ > 100) {
                throw new SchedulerException("iteration limit exceed, cannot find solution with no cycle");
            }
        }
    }

    private DisjunctiveGraph swapWorkersFromProfessionGroup(DisjunctiveGraph nextSolution, int iter, Operation vToChange) {

        Map<String, List<Operation>> disjunctionsPermutations = nextSolution.disjunctiveOperations;
        if (!disjunctionsPermutations.containsKey(vToChange.worker.workerLabel)) {
            return null;
        }
        String profCode = vToChange.professionGroup.getCode();

        Worker actulWorker = vToChange.worker;
        List<Worker> profWorkers = nextSolution.grupedWorkers.get(profCode);
        if (profWorkers.size() == 1) {
            // there is only one worker from that profession group so there is no sense to swap anything
            return null;
        }

        Worker newWorker = randOtherWorker(profWorkers, actulWorker);
        if (!disjunctionsPermutations.containsKey(newWorker.workerLabel)) {
            //there is no disjunctive operations for that particular worker
            return null;
        }
        SwitchingContext vContextToSwitch = createSwitchingContextForOperation(vToChange, disjunctionsPermutations);
        SwitchingContext operationToSwitch = chooseOperationToSwitch(newWorker, disjunctionsPermutations);
        Pair<SwitchingContext> newContexts = switchOperations(vContextToSwitch, operationToSwitch, nextSolution);

        boolean hasCycle = new CycleDetector().detectCycleInGraph(nextSolution);
        if (!hasCycle) {
            updatePermutationsInfoInGraph(vContextToSwitch, operationToSwitch, nextSolution);
            swapWorkersInOperations(vContextToSwitch, operationToSwitch);
            logger.append(" | vert ");
            logger.append(String.format(" | Znalazł w iteracji: %3d ", iter));
            return nextSolution;
        } else {
            // withdraw changes
            switchOperations(newContexts.getFirst(), newContexts.getSecond(), nextSolution);
            return null;
        }
    }

    private DisjunctiveGraph swapDisjunctiveOperationsFromWorker(DisjunctiveGraph nextSolution, int iter, Operation verticleToSwap, List<Integer> criticalPath) {
        Map<String, List<Operation>> shuffledPermutations = nextSolution.disjunctiveOperations;

        String workerLabel = verticleToSwap.worker.workerLabel;
        List<Operation> actualWorkerDisjunctivePermutation = shuffledPermutations.get(workerLabel);
        if (actualWorkerDisjunctivePermutation == null || actualWorkerDisjunctivePermutation.size() < 2) {
            // there is only one operation on that worker in whole schedule.
            return null;
        }
        Permutation newPerm;
        if (nextSolution.permutationOnOnlyOneGraphLevel.get(workerLabel)) {
            // there is easy way, just swap two neightbous verticles
            newPerm = saUtil.createNewPermutation(new Permutation(actualWorkerDisjunctivePermutation), verticleToSwap, criticalPath, nextSolution);
        } else {
            return null;
        }
        saUtil.updateEdgesToNewPermutation(newPerm, new Permutation(actualWorkerDisjunctivePermutation), nextSolution);
        boolean hasCycle = new CycleDetector().detectCycleInGraph(nextSolution);
        if (!hasCycle) {
            // todo update shuffled perm and (probably edges...)
            nextSolution.disjunctiveOperations.put(workerLabel, new ArrayList(newPerm));
            logger.append(" | edge ");
            logger.append(String.format(" | Znalazł w iteracji: %3d ", iter));
            return nextSolution;
        } else {
            // there is a cycle in new solution. Revetr changes
            saUtil.updateEdgesToNewPermutation(new Permutation(actualWorkerDisjunctivePermutation), new Permutation(newPerm), nextSolution);
            return null;
        }
    }

    private Operation randVerticleFromCriticalPath(DisjunctiveGraph nextSolution, List<Integer> criticalPath) {            
            Integer verticleToSwapNo = criticalPath.get(rand.nextInt(criticalPath.size() - 2) + 1);
            return nextSolution.getVertices().stream().filter(v -> v.vertexNo == verticleToSwapNo).findFirst().get();
    }

    /*    
        // znajdź ścieżkę krytyczną
    
        // wylosuj 100 rozwiązań sąsiednich
                
        //   50 zmienionych kolejności krawędzi dysjunkcyjnych
        
        
        //   50 zmienonych pracowników
        // odfiltruj poprawne
        // zwróć najlepsze
    
     */
    private DisjunctiveGraph generateNextSolution(int uberIter, DisjunctiveGraph initSolution) {
        DisjunctiveGraph nextSolution = saUtil.cloneGraph(initSolution);
        Map<String, List<EdgeWrapper>> disjunctiveEdgesMap = nextSolution.disjunctiveEdgesMap;
        Map<String, List<Operation>> shuffledPermutations = nextSolution.disjunctiveOperations;

        int howManyToRand = rand.nextInt(disjunctiveEdgesMap.size()) + 1;
        Map<String, List<EdgeWrapper>> edgesToChange = new HashMap(howManyToRand);
        Map<String, List<Operation>> operationsToChange = new HashMap(howManyToRand);
        String zmieniam = "";
        for (int i = 1; i <= howManyToRand; i++) {
            int r = rand.nextInt(disjunctiveEdgesMap.size());
            String workersKey = (String) disjunctiveEdgesMap.keySet().toArray()[r];
            edgesToChange.put(workersKey, disjunctiveEdgesMap.get(workersKey));
            operationsToChange.put(workersKey, shuffledPermutations.get(workersKey));
            zmieniam += String.valueOf(i) + ": " + workersKey.substring(0, 3) + " ";
        }
        String text = " | zmieniam " + saUtil.addspace(disjunctiveEdgesMap.size() * (1 + 3 + 3) - zmieniam.length(), zmieniam);
        for (List<EdgeWrapper> edges : edgesToChange.values()) {
            for (EdgeWrapper e : edges) {
                nextSolution.removeEdge(e.e);
            }
        }

        Map<String, List<EdgeWrapper>> newEdges = new HashMap(howManyToRand);
        Map<String, List<Operation>> newPermutations = new HashMap(howManyToRand);
        int iter = 1;
        while (true) {
            newEdges.clear();
            newPermutations.clear();
            for (String workerKey : operationsToChange.keySet()) {
                Permutation newPerm = Permutation.prepareBetterShuffledPermutation(new Permutation(operationsToChange.get(workerKey)));
                boolean isTheSame = Permutation.areEqual(newPerm, (operationsToChange.get(workerKey)));
                if (isTheSame) {
                    throw new SchedulerException("ZNOWU TAKA SAMA!!!!!!!!!!!!");
                }
                List<EdgeWrapper> edges2 = new ArrayList();
                newEdges.put(workerKey, edges2);
                newPermutations.put(workerKey, newPerm);
                for (int i = 1; i < newPerm.size(); i++) {
                    EdgeWrapper newEdge = new EdgeWrapper(new Edge(saUtil.nextEdgeNo(), newPerm.get(i - 1).cost, true), newPerm.get(i - 1), newPerm.get(i));
                    edges2.add(newEdge);
                    nextSolution.addEdge(newEdge.e, newEdge.o1, newEdge.o2, EdgeType.DIRECTED);
                }
            }

            boolean hasCycle = new CycleDetector().detectCycleInGraph(nextSolution);
            if (!hasCycle) {
                nextSolution.disjunctiveEdgesMap.putAll(newEdges);
                nextSolution.disjunctiveOperations.putAll(newPermutations);
                System.out.print(String.format("%s | Znalazł w iteracji: %3d ", text, iter * uberIter));
//                System.out.print(String.format(" | Znalazł w iteracji: %3d  %s ", iter, saUtil.disjOpToString(nextSolution.disjunctiveOperations.values())));
                return nextSolution;
            }
            for (List<EdgeWrapper> edges2 : newEdges.values()) {
                for (EdgeWrapper e : edges2) {
                    nextSolution.removeEdge(e.e);
                }
            }
            if (iter++ > 100) {
//                System.out.print(" |            cannot find solution with no cycle          ");
                return null;
//                throw new SchedulerException("iteration limit exceed");
            }
        }
    }

    private Double coolDown(Double temp) {
        return temp * (1 - 0.005);
    }

    private Double acceptanceFunctionValue(Integer delta, Double temp) {
        Double p = Math.exp(-1 * delta / temp);
        return Double.min(1, p);
        /*
                delta = F(x')−F(x) 
        P(x, x') = min(1, e−∆/T )
         */
    }

    private void addRandomWorkerToOperations(DisjunctiveGraph graph) {
        addRandomWorkerToOperations(graph.getVertices());
    }

    private void addRandomWorkerToOperations(Collection<Operation> operations) {
        for (Operation op : operations) {
            if (!op.operationName.equals("zero") && !op.operationName.equals("last")) {
                final String code = op.professionGroup.getCode();
                op.worker = grupedWorkers.get(code).get(rand.nextInt(grupedWorkersSizes.get(code)));
                op.createKey();
            }
        }
    }

    public void initLists(WorkerEntitiesList workers) {
        for (WorkerEntity workerEntty : workers) {
            String code = workerEntty.getProfessionGroup().getCode();
            if (!grupedWorkers.containsKey(code)) {
                grupedWorkers.put(code, new ArrayList());
            }
            grupedWorkers.get(code).add(new Worker(workerEntty));
        }

        List<Worker> transportWorkers = new ArrayList();
        ProfessionGroup transportGroup = ProfessionGroup.getTransportGroup();
        for (int i = 0; i < TRANSPORT_WORKERS_COUNT; i++) {
            transportWorkers.add(Worker.newTransportWorker(i));
        }
        grupedWorkers.put(transportGroup.getCode(), transportWorkers);

        // add countes
        for (Map.Entry<String, List<Worker>> e : grupedWorkers.entrySet()) {
            grupedWorkersSizes.put(e.getKey(), e.getValue().size());
        }
    }

    @Override
    public String getName() {
        return "SA";
    }

    class Touple {

        public Operation o1;
        public Operation o2;

        public Touple(Operation o1, Operation o2) {
            this.o1 = o1;
            this.o2 = o2;
        }

        @Override
        public String toString() {
            return " (" + o1 + ", " + o2 + ") ";
        }

    }
}
