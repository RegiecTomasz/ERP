/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.zzzother;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.jaxygen.typeconverter.exceptions.ConversionError;
import org.jfree.chart.axis.CategoryLabelPositions;
import regiec.knast.erp.api.scheduler.charts.ResultBarChartPainter;
import regiec.knast.erp.api.scheduler.model.TestResult;
import regiec.knast.erp.api.scheduler.model.TestResultsList;

/**
 *
 * @author fbrzuzka
 */
public class GraphTests {

    private Gson g = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) {
//        new GraphTests().runPoints("wyniki1transport", "Jeden pracownik transportujący, przypadki testowe 1-30");
//        new GraphTests().runPoints("wyniki2transport", "Dwóch pracowników transportujących, przypadki testowe 1-30");
//        new GraphTests().runPoints("wyniki3transport", "Trzech pracowników transportujących, przypadki testowe 1-30");
//        new GraphTests().runTimes("wyniki1transport-0-9", "Jeden pracownik transportujący, przypadki testowe 1-10");
//        new GraphTests().runTimes("wyniki1transport-10-19", "Jeden pracownik transportujący, przypadki testowe 11-20");
//        new GraphTests().runTimes("wyniki1transport-20-29", "Jeden pracownik transportujący, przypadki testowe 21-30");
        
        new GraphTests().runTimes("wyniki2transport-0-9", "Dwóch pracowników transportujących, przypadki testowe 1-10");
        new GraphTests().runTimes("wyniki2transport-10-19", "Dwóch pracowników transportujących, przypadki testowe 11-20");
        new GraphTests().runTimes("wyniki2transport-20-29", "Dwóch pracowników transportujących, przypadki testowe 21-30");
        
        new GraphTests().runTimes("wyniki3transport-0-9", "Trzech pracowników transportujących, przypadki testowe 1-10");
        new GraphTests().runTimes("wyniki3transport-10-19", "Trzech pracowników transportujących, przypadki testowe 11-20");
        new GraphTests().runTimes("wyniki3transport-20-29", "Trzech pracowników transportujących, przypadki testowe 21-30");

    }

    public void runTimes(String filename, String title) {
        run(filename, title, false);
    }

    public void runPoints(String filename, String title) {
        run(filename, title, true);

    }

    public void run(String filename, String title, boolean points) {
        String testResultString = "";
        try (InputStream is = new FileInputStream(new File("/opt/" + filename + ".json"))) {
            testResultString = IOUtils.toString(is);
        } catch (Exception e) {
            throw new ConversionError("", e);
        }
        TestResultsList testResults = g.fromJson(testResultString, TestResultsList.class);

        for (TestResult t : testResults) {
            int caseNumner = Integer.parseInt(t.getCaseName());
            t.setCaseName(++caseNumner + "");
        }

        if (points) {
            ResultBarChartPainter.fromDataset(testResults.toPointsDataset(), title, CategoryLabelPositions.UP_90);
        } else {    
            ResultBarChartPainter.fromDataset(testResults.toTimeDataset(), title, CategoryLabelPositions.STANDARD);
        }

    }

}
