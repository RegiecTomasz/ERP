/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;
import org.jfree.data.category.DefaultCategoryDataset;
import regiec.knast.erp.api.scheduler.zzzother.GraphTests;

/**
 *
 * @author fbrzuzka
 */
public class TestResultsList extends ArrayList<TestResult> {

    public static void main(String[] args) {
        GraphTests.main(null);
    }

    public DefaultCategoryDataset toPointsDataset() {
        return new PointsTable(this).toDataset();
    }

    public DefaultCategoryDataset toTimeDataset() {

        DefaultCategoryDataset d = new DefaultCategoryDataset();
        for (TestResult casee : this) {
            List<AlgorithmResult> algResults = casee.getAlgResult();
            Collections.sort(algResults, (AlgorithmResult o1, AlgorithmResult o2) -> o1.getAlgorithm().compareTo(o2.getAlgorithm()));
            for (AlgorithmResult ar : casee.getAlgResult()) {
                d.addValue(ar.getMakespan(), ar.getAlgorithm(), casee.getCaseName());
            }
        }
        return d;
    }

    private class PointsTable {

        private final Map<String, Integer> pointsMap = new HashMap();

        public PointsTable(TestResultsList results1) {
            removeConstructPhraseFromNames(results1);
            initKeys(results1);
            for (TestResult casee : results1) {
                List<AlgorithmResult> winners = casee.findWinners();
                this.addPointTo(winners);
            }
        }

        private void addPointTo(AlgorithmResult winner) {
            if (!pointsMap.containsKey(winner.getAlgorithm())) {
                pointsMap.put(winner.getAlgorithm(), 0);
            } else {
                int points = pointsMap.get(winner.getAlgorithm());
                points++;
                pointsMap.put(winner.getAlgorithm(), points);
            }
        }

        private void addPointTo(List<AlgorithmResult> winners) {
            for (AlgorithmResult w : winners) {
                addPointTo(w);
            }
        }

        private DefaultCategoryDataset toDataset() {
            Map<String, Foo> map = new TreeMap();
            for (Map.Entry<String, Integer> e : pointsMap.entrySet()) {
                if (e.getKey().contains("SA")) {
                    String k = StringUtils.replace(e.getKey(), " SA", "");
                    if (!map.containsKey(k)) {
                        map.put(k, new Foo());
                    }
                    map.get(k).saValue = e.getValue();
                } else {
                    String k = e.getKey();
                    if (!map.containsKey(k)) {
                        map.put(k, new Foo());
                    }
                    map.get(k).conValue = e.getValue();
                }

            }
            DefaultCategoryDataset d = new DefaultCategoryDataset();
            for (Map.Entry<String, Foo> e : map.entrySet()) {
                String key = e.getKey();
                d.addValue(e.getValue().conValue, "Construction", key);
                d.addValue(e.getValue().saValue, "SA", key);
            }
//            for (String key : map.keySet()) {
//                Foo foo = map.get(key)
//
//            }
//            for (String key : sa.keySet()) {
//                int savalue = sa.get(key);
//
//            }
//            System.out.println("con: " + con);
//            System.out.println("sa: " + sa);
//
//            DefaultCategoryDataset d = new DefaultCategoryDataset();
//            for (Map.Entry<String, Integer> e : pointsMap.entrySet()) {
//                d.addValue(e.getValue(), "zdobyte punkty za najkrótsze uszeregowanie (również za ex aequo)", e.getKey());
//            }
            return d;
        }

        private void initKeys(TestResultsList results1) {
            if (results1.size() > 0) {
                for (AlgorithmResult ar : results1.get(0).getAlgResult()) {
                    pointsMap.put(ar.getAlgorithm(), 0);
                }
            }
        }

        private void removeConstructPhraseFromNames(TestResultsList results1) {
            for (TestResult r : results1) {
                for (AlgorithmResult ar : r.getAlgResult()) {
                    ar.setAlgorithm(StringUtils.replace(ar.getAlgorithm(), "Construct", "") + "  ");

                }
            }
        }

        class Foo {

            Integer saValue = 0;
            Integer conValue = 0;
        }
    }
}
