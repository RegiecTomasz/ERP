/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.zzzother;

/**
 *
 * @author fbrzuzka
 */
public class Logg {

    private final boolean enabled;
    private StringBuilder sout = new StringBuilder();

    public Logg(boolean enabled) {
        this.enabled = enabled;
    }

    public Logg append(CharSequence s) {
        if (enabled) {
            this.sout.append(s);
        }
        return this;
    }

    public void printAndClear() {
        if (enabled) {
            System.out.println(sout);
            sout = new StringBuilder();
        }
    }

    public static void print(boolean enabledd, String s) {
        if (enabledd) {
            System.out.println(s);
        }
    }
}
