package regiec.knast.erp.api.scheduler.zzzother;

///*
//* ERP
//* copyright Jakub Knast and Tomasz Regiec
//* 2017
// */
//package regiec.knast.erp.api.utils.scheduler.sa;
//
//import com.google.common.collect.Lists;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import regiec.knast.erp.api.utils.scheduler.model.OperationBase;
//
///**
// *
// * @author fbrzuzka
// */
//public class CriticalPathCalculator {
//
//    public static long duration = 0;
//    private final Map<Integer, Integer> passesSequence = new HashMap();
//    private List<Integer> criticalPath = new ArrayList();
//    DisjunctiveGraph graph ;
//    public Result run(DisjunctiveGraph graph) {
//        OperationBase end = graph.getVertices().stream().filter(v -> v.operationName.equals("last")).findFirst().get();
//        return run(graph, end);
//    }
//
//    public Result run(DisjunctiveGraph graph, OperationBase endVertex) {
//        long start = System.currentTimeMillis();
//        this.graph = graph;
//        ArrayList verticles = new ArrayList(graph.getVertices());
//        int verticlesCount = verticles.size();
////        long starttime = System.nanoTime();
//        boolean visited[] = new boolean[endVertex.vertexNo + 1];
//        initialize(verticlesCount, visited);
//        int max = longestPath(endVertex, visited);
//        endVertex.operationStartTime = max;
//        endVertex.operationEndTime = max;
//
////        long runtime = System.nanoTime() - starttime;
////        System.out.println("Runtime =" + runtime + " nano seconds");
////        System.out.println("Longest Path Length = " + max);
////        System.out.println("distances " + distances);
//        int end = endVertex.vertexNo;
//        while (true) {
//            criticalPath.add(end);
//            Integer next = passesSequence.get(end);
//            if (next == null) {
//                break;
//            }
//            end = next;
//        }
//        criticalPath.add(0);
////        System.out.println("\n");
//        System.out.println("\nCriticalPathCalculator " + Lists.reverse(criticalPath));
//        duration += (System.currentTimeMillis() - start);
//        return new Result(max, Lists.reverse(criticalPath));
//    }
//
//    public Result runWithParalelWorkers(DisjunctiveGraph graph) {
//        return run(graph);
//    }
//
//    private int longestPath(OperationBase v, boolean visited[]) {
//        int dist, max = 0;
//        visited[v.vertexNo] = true;
//        for (OperationBase op : graph.getParents(v)) {
//            if (!visited[op.vertexNo]) {
//                dist = op.operationDuration + longestPath(op, visited);
//                op.operationStartTime = dist - op.operationDuration;
//                op.operationEndTime = dist;
//                if (dist > max) {
//                    max = dist;
//                    passesSequence.put(v.vertexNo, op.vertexNo);
//                }
//            }
//        }
//        visited[v.vertexNo] = false;
//        return max;
//    }
//
//    private void initialize(int verticlesCount, boolean visited[]) {
//        for (int i = 0; i < verticlesCount; i++) {
//            visited[i] = false;
//        }
//    }
//
//    public class Result {
//
//        public Integer makespan;
//        public List<Integer> criticalPath;
//
//        public Result(Integer makespan, List<Integer> criticalPath) {
//            this.makespan = makespan;
//            this.criticalPath = criticalPath;
//        }
//
//        public void markOperationsOnCriticalPath(DisjunctiveGraph graph) {
//            for (OperationBase vertice : graph.getVertices()) {
//                vertice.isOnCriticalPath = criticalPath.contains(vertice.vertexNo);
//            }
//        }
//
//    }
//}
