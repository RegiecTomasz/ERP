/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.zzzother;

import com.google.common.collect.Lists;
import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.jaxygen.objectsbuilder.exceptions.ObjectCreateError;
import regiec.knast.erp.api.scheduler.ScheduleLauncher;
import regiec.knast.erp.api.scheduler.model.DisjunctiveGraph;
import regiec.knast.erp.api.scheduler.model.Edge;
import regiec.knast.erp.api.scheduler.model.Operation;

/**
 *
 * @author fbrzuzka
 */
public class TopoligalSortOrigWithEdges {
    
    public static void main(String[] args) throws ObjectCreateError {
        ScheduleLauncher.main(null);
    }
    
    private final DisjunctiveGraph graph;
    private final Operation operationZero;
    
    public TopoligalSortOrigWithEdges(DisjunctiveGraph g) {
        this.graph = new Cloner().deepClone(g);
        operationZero = graph.getVertices().stream().filter(v -> v.vertexNo == 0).findFirst().get();        
    }
    public List<Operation> topoSort() {
        List<Operation> ret = new ArrayList();
        List<Operation> S = Lists.newArrayList(operationZero);
        

        Operation node;
        Operation dest;

        while (!S.isEmpty()) {
            node = S.get(S.size() - 1);
            S.remove(node);
//            System.out.println("node: " + node);
            ret.add(node);

            Collection<Edge> outEdges = new ArrayList();
            outEdges.addAll(graph.getOutEdges(node));

            for (Edge outedge : outEdges) {
                dest = graph.getDest(outedge);
                graph.removeEdge(outedge);
                if (graph.inDegree(dest) == 0) {
                    S.add(dest);
                }
            }
        }
        if (graph.getEdgeCount() != 0) {
            System.out.println("Error: Cycles present : " + graph.getEdges().toString());
        } else {
//            System.out.println(ret.toString());
        }
        return ret;
    }
}
