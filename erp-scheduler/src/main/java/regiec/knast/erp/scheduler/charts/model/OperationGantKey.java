/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.scheduler.charts.model;

import java.util.Objects;
import regiec.knast.erp.api.scheduler.model.Worker;

/**
 *
 * @author fbrzuzka
 */
public class OperationGantKey implements Comparable<Object> {

        public String jobNo;
        public String workerLabel;
        public String thiskey;

        public OperationGantKey(String jobNo, Worker worker) {
            this.jobNo = jobNo;
            this.workerLabel = worker.workerLabel;
            this.thiskey = this.workerLabel + "-" + this.jobNo;
        }

        public OperationGantKey(String thiskey) {
            this.thiskey = thiskey;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final OperationGantKey other = (OperationGantKey) obj;
            if (!Objects.equals(this.jobNo, other.jobNo)) {
                return false;
            }
            if (!Objects.equals(this.workerLabel, other.workerLabel)) {
                return false;
            }
            return true;
        }

        @Override
        public int compareTo(Object o) {
            if (o instanceof OperationGantKey) {
                OperationGantKey oo = (OperationGantKey) o;
                return this.thiskey.compareTo(oo.thiskey);
            } else {
                return -1;
            }
        }

        @Override
        public String toString() {
            return thiskey;
        }

    }