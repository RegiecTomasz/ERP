/*
* ERP
* copyright Jakub Knast and Tomasz Regiec
* 2017
 */
package regiec.knast.erp.api.utils;

import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import regiec.knast.erp.api.entities.MaterialDemandEntity;
import regiec.knast.erp.api.entities.MaterialEntity;
import regiec.knast.erp.api.entities.SemiproductTemplateEntity;

/**
 *
 * @author jknast jakub.knast@gmail.com
 */
public class EntityFieldsCacheTest {

    @Before
    public void setUp() {
    }

//    @Test
//    public void shall_pasasasWithMaterialEntityClass() {
//        Random rand = new Random();
//        for (int i = 0; i < 30; i++) {
//            System.out.println(rand.nextInt(3)
//            );
//        }
//
//        try {
//            createNewPermutation(perm5, null, null, null);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//
////        List<String> pre = permutation.subList(0, vToChangeIndexinPermutation);
////        List<String> post = permutation.subList(vToChangeIndexinPermutation + 1, permutation.size());
//        System.out.println("pre");
//    }


    @Test
    public void shall_pasWithMaterialEntityClass() {
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(MaterialEntity.class);
        System.out.println("Fields:  --------------------");
        for (Map.Entry<String, Map<String, Class>> entry : EntityFieldsCache.entityMap.entrySet()) {
            System.out.println("---class: " + entry.getKey());
            for (Map.Entry<String, Class> entry1 : entry.getValue().entrySet()) {
                System.out.println(entry1.getKey() + "  " + entry1.getValue().getSimpleName());
            }
        }
    }

    @Test
    public void shall_pasWithComplexClass() {
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(ComplexClass.class);
        System.out.println("Fields:  --------------------");
        for (Map.Entry<String, Map<String, Class>> entry : EntityFieldsCache.entityMap.entrySet()) {
            System.out.println("---class: " + entry.getKey());
            for (Map.Entry<String, Class> entry1 : entry.getValue().entrySet()) {
                System.out.println(entry1.getKey() + "  " + entry1.getValue().getSimpleName());
            }
        }
    }

    @Test
    public void shall_pasWithListClass() {
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(ListClass.class);

        System.out.println("Fields:  --------------------");
        for (Map.Entry<String, Map<String, Class>> entry : EntityFieldsCache.entityMap.entrySet()) {
            System.out.println("---class: " + entry.getKey());
            for (Map.Entry<String, Class> entry1 : entry.getValue().entrySet()) {
                System.out.println(entry1.getKey() + "  " + entry1.getValue().getSimpleName());
            }
        }
    }

    @Test
    public void shall_pasRecursiveClass() {
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(A.class);
        System.out.println("Fields:  --------------------");
        for (Map.Entry<String, Map<String, Class>> entry : EntityFieldsCache.entityMap.entrySet()) {
            System.out.println("---class: " + entry.getKey());
            for (Map.Entry<String, Class> entry1 : entry.getValue().entrySet()) {
                System.out.println(entry1.getKey() + "  " + entry1.getValue().getSimpleName());
            }
        }
    }

    @Test
    public void shall_pasWithSemiproductTemplateEntityClass() {
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(SemiproductTemplateEntity.class);
        System.out.println("Fields:  --------------------");
        for (Map.Entry<String, Map<String, Class>> entry : EntityFieldsCache.entityMap.entrySet()) {
            System.out.println("---class: " + entry.getKey());
            for (Map.Entry<String, Class> entry1 : entry.getValue().entrySet()) {
                System.out.println(entry1.getKey() + "  " + entry1.getValue().getSimpleName());
            }
        }
    }

    @Test
    public void shall_pasWithMaterialDemandEntityClass() {
        Map<String, Class> fieldsTypesFromClass = EntityFieldsCache.getFieldsTypesFromClass(MaterialDemandEntity.class);
        System.out.println("Fields:  --------------------");
        for (Map.Entry<String, Map<String, Class>> entry : EntityFieldsCache.entityMap.entrySet()) {
            System.out.println("---class: " + entry.getKey());
            for (Map.Entry<String, Class> entry1 : entry.getValue().entrySet()) {
                System.out.println(entry1.getKey() + "  " + entry1.getValue().getSimpleName());
            }
        }
    }

    class SimpleClass {

        private String id;
        private int dupa;
    }

    class ComplexClass {

        private SimpleClass simpleClass;
        private int dupa;
    }

    class ListClass {

        private List<ComplexClass> complexClasses;
        private SimpleClass simpleClass;
        private int dupa;
    }

}
